//
//  AccountSettingsViewController.swift
//  Cinch
//
//  Created by Amit Garg on 27/02/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class AccountSettingsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, UIAlertViewDelegate {
    

    @IBOutlet weak var accountTableView: UITableView!
    
    var imagePicker = UIImagePickerController()
    var accountData : AccountData?
    var playerList : [Player]?
    var isEditable = false
    
    var profilePicUrl: String?
    var phoneNo: String?
    var emailId: String?
    var password: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Account Settings"
        imagePicker.delegate = self
        
        if revealViewController() != nil {
            Utility.setMenuButton(forViewController: self, revealController: revealViewController())
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        self.accountTableView.registerNib(UINib(nibName: "ProfilePicTableViewCell", bundle: nil), forCellReuseIdentifier: "ProfilePicTableViewCell")
        self.accountTableView.registerNib(UINib(nibName: "SectionHeaderWithLabelAndButton", bundle: nil), forHeaderFooterViewReuseIdentifier: "SectionHeaderWithLabelAndButton")
        self.accountTableView.registerNib(UINib(nibName: "OneButtonTableViewCell", bundle: nil), forCellReuseIdentifier: "OneButtonTableViewCell")
        
        Utility.setRightNavigationButton(forViewController: self, withImage: "", withTitle: "EDIT")
        
        let backbutton = UIBarButtonItem(title: "", style: .Plain, target: nil
            , action: nil)
        self.navigationItem.backBarButtonItem = backbutton
        
        // Do any additional setup after loading the view.
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.getAccountSettingsDataService()

        //self.accountTableView.reloadData()
    }
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        
        if !DataValidationUtility.isNull(self.accountData) {
            
            if isEditable {
                return 3
            }
            else {
                return 5
            }
        }
        
        return 0
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        
        if isEditable {
            if section == 0 {
                return 1
            } else {
                return 2
            }
        }
        else {
            if section == 0 {
                return 1
            } else if section == 1 {
                return 1 + (self.playerList?.count)!
            } else if section == 2 || section == 3 || section == 4 {
                return 2
            } else {
                return 0
            }
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        if isEditable {
            if indexPath.section == 0 {
                return 150
            }
            else if indexPath.section == 2 {
                return 67.0
            }
            else {
                return 50
            }
        }
        else {
            if indexPath.section == 0 {
                return 150
            } else if indexPath.section == 4 {
                return 67.0
            } else {
                return 50
            }
        }
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if isEditable {
            if section == 1 {
                return 50.0
            }
            else {
                return 0.0
            }
        }
        else {
            if section == 2 || section == 3 {
                return 50.0
            }
            return 0.0
        }
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        if isEditable {
            if section == 1 {
                return 20
            }
            else {
                return 0.0
            }
        }
        else {
            if section == 1 || section == 2 || section == 3 {
                return 20
            }
            return 0.0
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if isEditable {
            switch indexPath.section {
            case 0:
                if indexPath.row == 0 {
                    
                    let profilePicTableViewCell = tableView.dequeueReusableCellWithIdentifier("ProfilePicTableViewCell", forIndexPath: indexPath) as! ProfilePicTableViewCell
                    profilePicTableViewCell.selectionStyle = .None
                    Utility.setImageWithURl(self.profilePicUrl, inImageView: profilePicTableViewCell.profileImageView)
                    
                    return profilePicTableViewCell
                }
                
                break
                
            case 1:
                
                let parentCell = tableView.dequeueReusableCellWithIdentifier("tableCell", forIndexPath: indexPath) as! SettingsCell
                parentCell.textLabel?.font = UIFont(name: "Lato-Regular", size: 16.0)
                parentCell.accessoryType = .None
                
                if indexPath.row == 0 {
                    parentCell.textlabel.hidden = true
                    
                    parentCell.textField.userInteractionEnabled = true
                    parentCell.textField.hidden = false
                    parentCell.textField.textColor = UIColor(red: 160/255, green: 160/255, blue: 160/255, alpha: 1.0)

                    
                    parentCell.textField?.text = self.emailId
                } else if indexPath.row == 1 {
                    
                    parentCell.textField.userInteractionEnabled = false
                    parentCell.textField.hidden = true
                    
                    parentCell.textlabel.hidden = false
                    parentCell.textlabel?.text = self.phoneNo
                }
                return parentCell
                
                
            case 2:
                let oneButtonTableViewCell = tableView.dequeueReusableCellWithIdentifier("OneButtonTableViewCell", forIndexPath: indexPath) as! OneButtonTableViewCell
                oneButtonTableViewCell.cellButton.titleLabel?.font = UIFont(name: "Lato-Bold", size: 16.0)
                oneButtonTableViewCell.selectionStyle = .None
                
                
                if indexPath.row == 0 {
                    oneButtonTableViewCell.cellButton.setTitle("Save Changes", forState: .Normal)
                    oneButtonTableViewCell.cellButton.CSBorderColor = UIColor(red: 186/255, green: 186/255, blue: 186/255, alpha: 1.0)
                    oneButtonTableViewCell.cellButton.setTitleColor(UIColor(red: 23/255, green: 22/255, blue: 22/255, alpha: 1.0), forState: .Normal)
                    oneButtonTableViewCell.cellButton.addTarget(self, action: Selector("saveChangeBtnAction:"), forControlEvents: UIControlEvents.TouchUpInside)

                    
                } else if indexPath.row == 1 {
                    oneButtonTableViewCell.cellButton.setTitle("Delete account", forState: .Normal)
                    oneButtonTableViewCell.cellButton.CSBorderColor = UIColor.clearColor()
                    oneButtonTableViewCell.cellButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                    oneButtonTableViewCell.cellButton.backgroundColor = UIColor(red: 255/255, green: 62/255, blue: 62/255, alpha: 1.0)
                    oneButtonTableViewCell.cellButton.addTarget(self, action: Selector("deleteBtnAction:"), forControlEvents: UIControlEvents.TouchUpInside)
                    
                }
                return oneButtonTableViewCell
                

                
            default:
                print("Something broke")
                break
                
            }
            
            return UITableViewCell()
        }
        else {
            switch indexPath.section {
            case 0:
                if indexPath.row == 0 {
                    
                    let profilePicTableViewCell = tableView.dequeueReusableCellWithIdentifier("ProfilePicTableViewCell", forIndexPath: indexPath) as! ProfilePicTableViewCell
                    profilePicTableViewCell.selectionStyle = .None
                    
                    Utility.setImageWithURl(self.accountData?.profilePic, inImageView: profilePicTableViewCell.profileImageView)
                    
                    return profilePicTableViewCell
                }
                
                break
                
            case 1:
                
                let parentCell = tableView.dequeueReusableCellWithIdentifier("tableCell", forIndexPath: indexPath) as! SettingsCell
                parentCell.textLabel?.font = UIFont(name: "Lato-Regular", size: 16.0)
                
                parentCell.textField.userInteractionEnabled = false
                parentCell.textField.hidden = true
                parentCell.textlabel.hidden = false

                if indexPath.row == 0 {
                    parentCell.textlabel?.text = self.accountData?.name
                    parentCell.textlabel?.textColor = UIColor(red: 23/255, green: 22/255, blue: 23/255, alpha: 1.0)
                    parentCell.accessoryType = .None
                    
                }
//                else if indexPath.row == 1 {
//                    parentCell.textlabel?.text = "Spouse: Brian Miller"
//                    parentCell.textlabel?.textColor = UIColor(red: 160/255, green: 160/255, blue: 160/255, alpha: 1.0)
//                    parentCell.accessoryType = .None
//                }
                else {
                    let player = self.playerList![indexPath.row - 1]
                    parentCell.textlabel?.text = player.fname + " " + player.lname
                    parentCell.textlabel?.textColor = UIColor(red: 160/255, green: 160/255, blue: 160/255, alpha: 1.0)
                    parentCell.accessoryType = .DisclosureIndicator
                }
                return parentCell
                
            case 2:
                
                let parentCell = tableView.dequeueReusableCellWithIdentifier("tableCell", forIndexPath: indexPath) as! SettingsCell
                parentCell.textLabel?.font = UIFont(name: "Lato-Regular", size: 16.0)
                parentCell.accessoryType = .None
                parentCell.textField.userInteractionEnabled = false
                parentCell.textField.hidden = true
                parentCell.textlabel.hidden = false

                parentCell.textlabel.textColor = UIColor(red: 160/255, green: 160/255, blue: 160/255, alpha: 1.0)
                if indexPath.row == 0 {
                    
                    parentCell.textlabel.text = self.accountData?.email
                } else if indexPath.row == 1 {
                    
                    parentCell.textlabel.text = self.accountData?.mobile
                }
                return parentCell
                
            case 3:
                
                if indexPath.row == 0 {
                    
                    let parentCell = tableView.dequeueReusableCellWithIdentifier("tableImageCell", forIndexPath: indexPath) as! CellWithImageView
                    parentCell.textLabel?.font = UIFont(name: "Lato-Regular", size: 16.0)
                    parentCell.accessoryType = .None
                    parentCell.textlabel?.textColor = UIColor(red: 160/255, green: 160/255, blue: 160/255, alpha: 1.0)
                    parentCell.textlabel?.text = "Payment Method"
                    
                    let imageUrl = "http://\((self.accountData?.paymentMethod == nil ? "" : self.accountData?.paymentMethod.cardImage!))"
                    parentCell.cardImageView!.image = nil
                    
                    if NSURL(string: imageUrl) != nil {
                        parentCell.cardImageView.setImageWithURL(NSURL(string: imageUrl)!)
                    }
                    
                    return parentCell
                    
                } else if indexPath.row == 1 {
                    
                    let parentCell = tableView.dequeueReusableCellWithIdentifier("tableCell", forIndexPath: indexPath) as! SettingsCell
                    parentCell.textLabel?.font = UIFont(name: "Lato-Regular", size: 16.0)
                    parentCell.accessoryType = .None
                    parentCell.textField.userInteractionEnabled = false
                    parentCell.textField.hidden = true
                    parentCell.textlabel.hidden = false

                    parentCell.textlabel?.textColor = UIColor(red: 160/255, green: 160/255, blue: 160/255, alpha: 1.0)
                    parentCell.textlabel?.text = "Transaction"
                    return parentCell
                }
                
                
            case 4:
                let oneButtonTableViewCell = tableView.dequeueReusableCellWithIdentifier("OneButtonTableViewCell", forIndexPath: indexPath) as! OneButtonTableViewCell
                oneButtonTableViewCell.cellButton.titleLabel?.font = UIFont(name: "Lato-Bold", size: 16.0)
                oneButtonTableViewCell.selectionStyle = .None
                
                
                if indexPath.row == 0 {
                    oneButtonTableViewCell.cellButton.setTitle("Save Changes", forState: .Normal)
                    oneButtonTableViewCell.cellButton.CSBorderColor = UIColor(red: 186/255, green: 186/255, blue: 186/255, alpha: 1.0)
                    oneButtonTableViewCell.cellButton.setTitleColor(UIColor(red: 23/255, green: 22/255, blue: 22/255, alpha: 1.0), forState: .Normal)
                    oneButtonTableViewCell.cellButton.addTarget(self, action: Selector("saveChangeBtnAction:"), forControlEvents: UIControlEvents.TouchUpInside)

                } else if indexPath.row == 1 {
                    oneButtonTableViewCell.cellButton.setTitle("Delete account", forState: .Normal)
                    oneButtonTableViewCell.cellButton.CSBorderColor = UIColor.clearColor()
                    oneButtonTableViewCell.cellButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                    oneButtonTableViewCell.cellButton.backgroundColor = UIColor(red: 255/255, green: 62/255, blue: 62/255, alpha: 1.0)
                    oneButtonTableViewCell.cellButton.addTarget(self, action: Selector("deleteBtnAction:"), forControlEvents: UIControlEvents.TouchUpInside)
                    
                }
                return oneButtonTableViewCell
                
            default:
                print("Something broke")
                break
                
            }
            
            return UITableViewCell()
        }
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if isEditable {
            switch section {
            case 1:
                let headerView = tableView.dequeueReusableHeaderFooterViewWithIdentifier("SectionHeaderWithLabelAndButton") as! SectionHeaderWithLabelAndButton
                headerView.button.hidden = true
                headerView.label.hidden = false
                
                headerView.label.text = "PERSONAL INFORMATION"
                return headerView
                
            default:
                return nil
                
            }
        }
        else {
            switch section {
            case 2,3:
                let headerView = tableView.dequeueReusableHeaderFooterViewWithIdentifier("SectionHeaderWithLabelAndButton") as! SectionHeaderWithLabelAndButton
                headerView.button.hidden = true
                headerView.label.hidden = false
                
                if section == 2 {
                    headerView.label.text = "PERSONAL INFORMATION"
                } else if section == 3 {
                    headerView.label.text = "FINANCIALS"
                }
                return headerView
                
            default:
                return nil
                
            }
        }
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        if isEditable {
            switch section {
            case 1:
                let headerView = tableView.dequeueReusableHeaderFooterViewWithIdentifier("SectionHeaderWithLabelAndButton") as! SectionHeaderWithLabelAndButton
                headerView.button.hidden = true
                headerView.label.hidden = true
                headerView.upperSeperator.hidden = true
                headerView.lowerSeperator.hidden = true
                return headerView
                
            default:
                return nil
                
            }
        }
        else {
            switch section {
            case 1,2,3:
                let headerView = tableView.dequeueReusableHeaderFooterViewWithIdentifier("SectionHeaderWithLabelAndButton") as! SectionHeaderWithLabelAndButton
                headerView.button.hidden = true
                headerView.label.hidden = true
                headerView.upperSeperator.hidden = true
                headerView.lowerSeperator.hidden = true
                return headerView
                
            default:
                return nil
                
            }
        }
    }

    // MARK: - Textfield delegate
    func textFieldDidEndEditing(textField: UITextField) {
        emailId = textField.text
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        emailId = textField.text
        
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: - Table view delegate
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if isEditable {
            switch indexPath.section {
                
            case 0:
                
                    self.view.endEditing(true)
                    self.showActionSheet()
              
            case 1:
                if indexPath.row == 1 {
                    let phoneNoVerificationViewControllerObj = mainStoryBoard.instantiateViewControllerWithIdentifier("PhoneNoVerificationViewController") as! PhoneNoVerificationViewController
                    phoneNoVerificationViewControllerObj.isComingFromCreateAccount = false
                    
                    ROOT_NAVVIGATION_CONROLLER.topViewController?.navigationController!.pushViewController(phoneNoVerificationViewControllerObj, animated: true)
                }
                
            default:
                print("something broke")
                
            }
        }
        else {
            switch indexPath.section {
                
            case 1:
                
                if indexPath.row > 0 {
                    
                    let playerProfileViewController = amitStoryBoard.instantiateViewControllerWithIdentifier("PlayerProfileViewController") as! PlayerProfileViewController
                    playerProfileViewController.player = self.playerList![indexPath.row - 1]
                    self.navigationController?.pushViewController(playerProfileViewController, animated: true)
                    
                }
                
            case 3:
                if indexPath.row == 1 {
                    
                    let balanceRootViewObj = mainStoryBoard.instantiateViewControllerWithIdentifier("BalanceRootTabBarController")
                    
                    self.navigationController?.pushViewController(balanceRootViewObj, animated: true)
                }
                
            default:
                print("something broke")
                
            }
        }
    }
    
    //MARK: - IBAction
    func deleteBtnAction(sender: UIButton) {
        self.view.endEditing(true)
        
        self.showAlertWithTextFiels()
    }
    
    func saveChangeBtnAction(sender: UIButton) {
        self.view.endEditing(true)

        self.getUpdateAccountSettingsService()
    }
    
    func rightBtnAction() {
        self.view.endEditing(true)
        
        if isEditable {
            isEditable = false
            self.profilePicUrl = self.accountData?.profilePic
            self.emailId = self.accountData?.email
            self.phoneNo = self.accountData?.mobile
            
            Utility.setRightNavigationButton(forViewController: self, withImage: "", withTitle: "EDIT")

        }
        else {
            isEditable = true
            Utility.setRightNavigationButton(forViewController: self, withImage: "", withTitle: "CANCEL")

        }
        
        self.accountTableView.reloadData()
    }
    
    //MARK: - AlertView methods
    func showAlertWithTextFiels() {
        let alert = UIAlertView()
        alert.delegate = self
        alert.title = "Enter Password"
        alert.addButtonWithTitle("Done")
        alert.alertViewStyle = UIAlertViewStyle.SecureTextInput
        alert.addButtonWithTitle("Cancel")
        alert.show()
    }
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        
        if buttonIndex == 0 {
            
            if alertView.textFieldAtIndex(0)?.text?.characters.count != 0 {
                
                password = alertView.textFieldAtIndex(0)?.text
                self.hitDeleteAccountService()
            }
        }
    }
    
    //MARK:- Image Picker method
    func showActionSheet() {
        
        let actionSheet = UIAlertController(title: "Player Headshot", message: "", preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        let imageGalleryAction = UIAlertAction(title: "Image Gallery", style: UIAlertActionStyle.Default) { (action) -> Void in
            self.openImageGallary()
        }
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.Default) { (action) -> Void in
            self.openCamera()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil)
        
        actionSheet.addAction(imageGalleryAction)
        actionSheet.addAction(cameraAction)
        actionSheet.addAction(cancelAction)
        
        self.presentViewController(actionSheet, animated: true, completion: nil)
    }
    
    func openImageGallary() {
        imagePicker.sourceType = .PhotoLibrary
        self.presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    func openCamera() {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
            imagePicker.sourceType = .Camera
            self.presentViewController(imagePicker, animated: true, completion: nil)
        }
        else {
            let alertController = UIAlertController(title: "Oops!!", message: "Camera is not available", preferredStyle: UIAlertControllerStyle.Alert)
            let cancelAction = UIAlertAction(title: ALERT_OK_BUTTON_TITLE, style: UIAlertActionStyle.Cancel, handler: nil)
            
            alertController.addAction(cancelAction)
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    //MARK:- Image Picker Delegate
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        if let pickerImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            print(pickerImage)
            let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
            activityIndicator.frame.size = (self.accountTableView.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0))?.frame.size)!
            
            activityIndicator.startAnimating()
            self.accountTableView.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0))!.addSubview(activityIndicator)
            
            Utility.uploadImageToserver(pickerImage, resetImageViewBlock: {(selectedImage: UIImage?, headShotImageUrl: String?) in
                
                activityIndicator.removeFromSuperview()
                if selectedImage != nil {
                    
                    self.profilePicUrl = headShotImageUrl == nil ? "" : headShotImageUrl!
                    self.accountTableView.reloadData()
                }
                else {
                }
            })
        }
        
        self.imagePicker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.imagePicker.dismissViewControllerAnimated(true, completion: nil)
    }

    //MARK:- Hit Webservice    
    func getAccountSettingsDataService () {
        
        CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Fetching account data...")
        
        let accountSettingServiceObj = CommonServiceClass(httpRequestType: HTTP_REQUEST_TYPE.POST_REQUEST, requestTag: "ACCOUNT_SETTINGS_SERVICE", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
        
        accountSettingServiceObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
            
            if errorMessage != nil
            {
                //print(response)
            }
            
            print(response as! NSDictionary)
            let responseDict = AccountSettingData(fromDictionary: response as! NSDictionary)
            
            if responseDict.errorCode == 1 {
                
                Utility.showAlert(ALERT_TITLE, message: responseDict.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            }
            else {
                
                print(responseDict.data)
                
                if responseDict.data.count != 0 {
                    self.accountData = responseDict.data[0]
                    self.playerList = self.accountData?.players
                    
                    self.profilePicUrl = self.accountData?.profilePic
                    self.emailId = self.accountData?.email
                    self.phoneNo = self.accountData?.mobile
                    
                    self.accountTableView.reloadData()
                }
                else {
                    Utility.showAlert(ALERT_TITLE, message: "Data not found", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
                }
                
            }
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
            
        }
        accountSettingServiceObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
            print("error ------------------------------\(error)")
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
            
        }
        
        
        var parameterDict = Dictionary <String, String>()
        parameterDict["cmd"] = "getUserAccountSettingDetails"
        parameterDict["user_seq"] = UserProfileManager.userSequence!
        
        accountSettingServiceObj.commonServiceHit(parameterDict)
    }

    func getUpdateAccountSettingsService () {
        
        CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Updating account...")
        
        let accountSettingServiceObj = CommonServiceClass(httpRequestType: HTTP_REQUEST_TYPE.POST_REQUEST, requestTag: "ACCOUNT_SETTINGS_SERVICE", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
        
        accountSettingServiceObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
            
            if errorMessage != nil
            {
                //print(response)
            }
            
            print(response as! NSDictionary)
            let responseDict = CSAccountUpdateData(fromDictionary: response as! NSDictionary)
            
            if responseDict.errorCode == 1 {
                
                Utility.showAlert(ALERT_TITLE, message: responseDict.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            }
            else {
                
                self.accountData?.profilePic = self.profilePicUrl
                self.accountData?.email = self.emailId
                self.accountData?.mobile = self.phoneNo

                self.isEditable = false

                Utility.showAlert(ALERT_TITLE, message: "Profile updated successfuly", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)

                self.accountTableView.reloadData()
            }
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
            
        }
        accountSettingServiceObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
            print("error ------------------------------\(error)")
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
            
        }
        
        var parameterDict = Dictionary <String, String>()
        parameterDict["cmd"] = "updateUserAccountSetting"
        parameterDict["email"] = emailId
        parameterDict["profile_pic"] = profilePicUrl
        parameterDict["mobile_no"] = phoneNo
        parameterDict["user_seq"] = UserProfileManager.userSequence

        accountSettingServiceObj.commonServiceHit(parameterDict)
    }

    func hitDeleteAccountService () {
        
        CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Deleting account...")
        
        let accountDeleteServiceObj = CommonServiceClass(httpRequestType: HTTP_REQUEST_TYPE.POST_REQUEST, requestTag: "DELETE_ACCOUNT_SERVICE", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
        
        accountDeleteServiceObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
            
            if errorMessage != nil
            {
                //print(response)
            }
            
            print(response as! NSDictionary)
            let responseDict = OtpVerificationData(fromDictionary: response as! NSDictionary)
            
            if responseDict.errorCode == 1 {
                
                Utility.showAlert(ALERT_TITLE, message: responseDict.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)


            }
            else {
                
                print(responseDict.data)
                
                if responseDict.data == true {
                    
                    UserProfileManager.logOut()

                    let viewController = mainStoryBoard.instantiateViewControllerWithIdentifier("WelcomeScreenBaseViewController")
                    ROOT_NAVVIGATION_CONROLLER.setViewControllers([viewController], animated: true)
                    
                    Utility.showAlert(ALERT_TITLE, message: "Account Deleted", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)

                }
                else {
                    Utility.showAlert(ALERT_TITLE, message: "Please enter the valid password", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)

                }
            }
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
            
        }
        accountDeleteServiceObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
            print("error ------------------------------\(error)")
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
            
        }
        
        var parameterDict = Dictionary <String, String>()
        parameterDict["cmd"] = "deleteUserAccount"
        parameterDict["password"] = password
        
        accountDeleteServiceObj.commonServiceHit(parameterDict)
    }

}

class SettingsCell : UITableViewCell {
    
    @IBOutlet weak var textlabel: UILabel!
    @IBOutlet weak var textField: UITextField!

    
}

class CellWithImageView : UITableViewCell {
    
    @IBOutlet weak var textlabel: UILabel!
    @IBOutlet weak var cardImageView: UIImageView!
    
}


