//
//	CSRecentChat.swift
//
//	Create by Money Mahesh on 2/4/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class CSRecentChat : NSObject, NSCoding{

    var recentChatDetailArray: [ChatDetail]!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
        recentChatDetailArray = [ChatDetail]()

        for i in 0..<dictionary.allKeys.count {
            recentChatDetailArray.append(ChatDetail(fromDictionary: (dictionary.objectForKey(dictionary.allKeys[i]) as? NSDictionary)!))
        }
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
        
        if recentChatDetailArray != nil{
            var dictionaryElements = [NSDictionary]()
            for dataElement in recentChatDetailArray {
                dictionaryElements.append(dataElement.toDictionary())
            }
            dictionary["chatDetail"] = dictionaryElements
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         recentChatDetailArray = aDecoder.decodeObjectForKey("chatDetail") as? [ChatDetail]
    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
        if recentChatDetailArray != nil{
            aCoder.encodeObject(recentChatDetailArray, forKey: "chatDetail")
		}
	}

}