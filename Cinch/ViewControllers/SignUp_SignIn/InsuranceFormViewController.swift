//
//  InsuranceFormViewController.swift
//  Cinch
//
//  Created by MONEY on 01/03/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class InsuranceFormViewController: UIViewController {

    @IBOutlet weak var constarintForImageViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var labelForInsuranceDescription: UILabel!
    @IBOutlet weak var labelForInsuranceContent: UILabel!

    @IBOutlet weak var textfieldForName: UITextField!
    @IBOutlet weak var waiverImageView: UIImageView!
    
    var playerName = String()
    
    var programSeq: String?
    var waiverSeq = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        textfieldForName.text = playerName
        textfieldForName.enabled = false
        self.hitGetInsuranceDetailWebService()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.hidden = true
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.hidden = true
    }

    //MARK: - IBActions
    
    @IBAction func backBtnAction(sender: AnyObject) {
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func agree_SignBtnAction(sender: AnyObject) {
        
        if textfieldForName.text?.characters.count == 0 && waiverSeq == "" {
            Utility.showAlert(nil, message: "Please enter the name", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
        }
        else {
            let signatureViewControllerObj = amitStoryBoard.instantiateViewControllerWithIdentifier("SignatureViewController") as! SignatureViewController
            signatureViewControllerObj.waiverSeq = waiverSeq
            self.navigationController?.pushViewController(signatureViewControllerObj, animated: true)
        }
        
    }

    //MARK:- Hit Webservice
    func hitGetInsuranceDetailWebService() {
        
        CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Loading Insurance Form detail...")
        
        let getInsuranceFormObj = ClubListServices(httpRequestType: HTTP_REQUEST_TYPE.POST_REQUEST, requestTag: "GET_INSURANCE_FORM", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
        
        getInsuranceFormObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
            
            if errorMessage != nil
            {
                print(response)
            }
            print(response as! NSDictionary)
            
            let insuranceWaiverObj =  CSInsuranceWaiver(fromDictionary: response as! NSDictionary)
            
            
            if insuranceWaiverObj.errorCode == 0 {
                
                if insuranceWaiverObj.data.count != 0 {
                    
                    self.constarintForImageViewHeight.constant = 0
                    self.labelForInsuranceDescription.text = insuranceWaiverObj.data[0].descriptionField
                    self.labelForInsuranceContent.text = insuranceWaiverObj.data[0].content
                    self.waiverSeq = insuranceWaiverObj.data[0].waiverSeq
                    
                    if insuranceWaiverObj.data[0].waiverImg == nil {
                        self.waiverImageView.image = nil
                        self.constarintForImageViewHeight.constant = 0
                    }
                    else {
                        Utility.setImageWithURl(insuranceWaiverObj.data[0].waiverImg, inImageView: self.waiverImageView)
                        self.constarintForImageViewHeight.constant = 88
                    }
                }
                else {
                    Utility.showAlert(ALERT_TITLE, message: "Data not found", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
                }
            } else {
                
                Utility.showAlert(ALERT_TITLE, message: insuranceWaiverObj.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            }
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
        }
        getInsuranceFormObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
            
            print("error ------------------------------\(error)")
        }
        
        
        var parameterDict = Dictionary <String, String>()
        
        parameterDict["cmd"] = "getProgramWaiverList"
        parameterDict["program_seq"] = programSeq == nil ? "1" : programSeq!
        
        getInsuranceFormObj.getInsuranceFormHit(parameterDict)
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
