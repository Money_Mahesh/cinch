//
//	CardDetail.swift
//
//	Create by Money Mahesh on 11/3/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class CardDetail : NSObject, NSCoding{
    
	var cardType : String!
	var custName : String!
	var customerId : String!
	var fingerprint : String!
	var isDefault : String!
	var last4 : String!
	var paymentSeq : String!
    var expire_month : String!
    var expire_year : String!
    var card_image : String!

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		cardType = dictionary["card_type"] as? String
		custName = dictionary["cust_name"] as? String
		customerId = dictionary["customer_Id"] as? String
		fingerprint = dictionary["fingerprint"] as? String
		isDefault = dictionary["is_default"] as? String
		last4 = dictionary["last4"] as? String
		paymentSeq = dictionary["payment_seq"] as? String
        expire_month = dictionary["expire_month"] as? String
        expire_year = dictionary["expire_year"] as? String
        card_image = dictionary["card_image"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if cardType != nil{
			dictionary["card_type"] = cardType
		}
		if custName != nil{
			dictionary["cust_name"] = custName
		}
		if customerId != nil{
			dictionary["customer_Id"] = customerId
		}
		if fingerprint != nil{
			dictionary["fingerprint"] = fingerprint
		}
		if isDefault != nil{
			dictionary["is_default"] = isDefault
		}
		if last4 != nil{
			dictionary["last4"] = last4
		}
		if paymentSeq != nil{
			dictionary["payment_seq"] = paymentSeq
		}
        if expire_month != nil{
            dictionary["expire_month"] = expire_month
        }
        if expire_year != nil{
            dictionary["expire_year"] = expire_year
        }
        if card_image != nil{
            dictionary["card_image"] = card_image
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
        cardType = aDecoder.decodeObjectForKey("card_type") as? String
        custName = aDecoder.decodeObjectForKey("cust_name") as? String
        customerId = aDecoder.decodeObjectForKey("customer_Id") as? String
        fingerprint = aDecoder.decodeObjectForKey("fingerprint") as? String
        isDefault = aDecoder.decodeObjectForKey("is_default") as? String
        last4 = aDecoder.decodeObjectForKey("last4") as? String
        paymentSeq = aDecoder.decodeObjectForKey("payment_seq") as? String
        expire_month = aDecoder.decodeObjectForKey("expire_month") as? String
        expire_year = aDecoder.decodeObjectForKey("expire_year") as? String
        card_image = aDecoder.decodeObjectForKey("card_image") as? String
        
    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if cardType != nil{
			aCoder.encodeObject(cardType, forKey: "card_type")
		}
		if custName != nil{
			aCoder.encodeObject(custName, forKey: "cust_name")
		}
		if customerId != nil{
			aCoder.encodeObject(customerId, forKey: "customer_Id")
		}
		if fingerprint != nil{
			aCoder.encodeObject(fingerprint, forKey: "fingerprint")
		}
		if isDefault != nil{
			aCoder.encodeObject(isDefault, forKey: "is_default")
		}
		if last4 != nil{
			aCoder.encodeObject(last4, forKey: "last4")
		}
		if paymentSeq != nil{
			aCoder.encodeObject(paymentSeq, forKey: "payment_seq")
		}
        if expire_month != nil{
            aCoder.encodeObject(expire_month, forKey: "expire_month")
        }
        if expire_year != nil{
            aCoder.encodeObject(expire_year, forKey: "expire_year")
        }
        if card_image != nil{
            aCoder.encodeObject(card_image, forKey: "card_image")
        }

	}

}