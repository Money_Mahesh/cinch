//
//  UploadImage.swift
//  Cinch
//
//  Created by MONEY on 03/03/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class UploadImage: ServerConnect {

    func uploadingImage(parameters: Dictionary<String, AnyObject>?, image : UIImage) -> Void {
        
        let imageData = UIImageJPEGRepresentation(image, 0.5)
        
//        let imageName = String(NSDate().timeIntervalSince1970 * 1000)
        
        ServerConnect.requestOperationManager.POST("https://www.cinchsports.com/user/services_v2/processRequest", parameters: parameters, constructingBodyWithBlock: { (formData) -> Void in
            formData.appendPartWithFileData(imageData!, name: "imageFile", fileName: "image.jpg", mimeType: "image/jpeg")
            },
            success: {(operation : AFHTTPRequestOperation! , responseObject: AnyObject!) in
                
                let serverResponse = self.processAfterMakingRequestWithParameters(responseObject)
                
                if serverResponse != nil {
                    let responseDic =  serverResponse as! NSDictionary
                    
                    
                    self.completionBlockWithSuccess?(statusCode: 0, requestType: self.requestTag, response: responseDic, errorMessage: nil);
                }
                
            },
            failure: {(operation: AFHTTPRequestOperation?, error:NSError) in
               
                if error.code == -1009 || error.code == -1005 {
                    InternetConnectivityManager.internetErrorAlert.show()
                }
                
                self.completionBlockWithFailure?(requestType: self.requestTag, error: error)
                
            }
        )
    }

}
