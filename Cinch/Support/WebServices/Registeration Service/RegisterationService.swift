//
//  RegisterationService.swift
//  Cinch
//
//  Created by MONEY on 20/02/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class RegisterationService: ServerConnect {

    func signUpHit(parameters: Dictionary<String, String>?)->Void{

        ServerConnect.requestOperationManager.POST("https://www.cinchsports.com/user/services_v2/authenticate", parameters: parameters, success: {(operation : AFHTTPRequestOperation! , responseObject: AnyObject!) in
            
            let serverResponse = self.processAfterMakingRequestWithParameters(responseObject)
            
            self.completionBlockWithSuccess?(statusCode: 0, requestType: self.requestTag, response: serverResponse, errorMessage: nil);
            
            self.saveCookies()
            RegisterationService.loadCookies()
            
            },
            failure: {(operation: AFHTTPRequestOperation?, error:NSError) in
                
                if error.code == -1009 || error.code == -1005 {
                    InternetConnectivityManager.internetErrorAlert.show()
                }
                
                self.completionBlockWithFailure?(requestType: self.requestTag, error: error)
                
            }
        )
    }
        
    func signInHit(parameters: Dictionary<String, String>?)->Void{
        
        ServerConnect.requestOperationManager.POST("https://www.cinchsports.com/user/services_v2/authenticate", parameters: parameters, success: {(operation : AFHTTPRequestOperation! , responseObject: AnyObject!) in
            
            let serverResponse = self.processAfterMakingRequestWithParameters(responseObject)
            
            self.completionBlockWithSuccess?(statusCode: 0, requestType: self.requestTag, response: serverResponse, errorMessage: nil);
            
            self.saveCookies()
            RegisterationService.loadCookies()
            
            },
            failure: {(operation: AFHTTPRequestOperation?, error:NSError) in
                
                if error.code == -1009 || error.code == -1005 {
                    InternetConnectivityManager.internetErrorAlert.show()
                }
                
                self.completionBlockWithFailure?(requestType: self.requestTag, error: error)
                
            }
        )
    }
    
    func createParentAccountHit(parameters: Dictionary<String, String>?)->Void{
        
        ServerConnect.requestOperationManager.POST("https://www.cinchsports.com/user/services_v2/processRequest", parameters: parameters, success: {(operation : AFHTTPRequestOperation! , responseObject: AnyObject!) in
            
            let serverResponse = self.processAfterMakingRequestWithParameters(responseObject)
            
            self.completionBlockWithSuccess?(statusCode: 0, requestType: self.requestTag, response: serverResponse, errorMessage: nil);
            
            
            },
            failure: {(operation: AFHTTPRequestOperation?, error:NSError) in
                
                if error.code == -1009 || error.code == -1005 {
                    InternetConnectivityManager.internetErrorAlert.show()
                }
                
                self.completionBlockWithFailure?(requestType: self.requestTag, error: error)
                
            }
        )
    }
    
    func inviteHit(parameters: Dictionary<String, String>?)->Void{
        
        ServerConnect.requestOperationManager.POST("https://www.cinchsports.com/user/services_v2/processRequest", parameters: parameters, success: {(operation : AFHTTPRequestOperation! , responseObject: AnyObject!) in
            
            let serverResponse = self.processAfterMakingRequestWithParameters(responseObject)
            
            self.completionBlockWithSuccess?(statusCode: 0, requestType: self.requestTag, response: serverResponse, errorMessage: nil);
            
            
            },
            failure: {(operation: AFHTTPRequestOperation?, error:NSError) in
                
                if error.code == -1009 || error.code == -1005 {
                    InternetConnectivityManager.internetErrorAlert.show()
                }
                
                self.completionBlockWithFailure?(requestType: self.requestTag, error: error)
                
            }
        )
    }
    
    func forgotPasswordHit(parameters: Dictionary<String, String>?)->Void{
        
        ServerConnect.requestOperationManager.POST("https://www.cinchsports.com/user/services_v2/authenticate", parameters: parameters, success: {(operation : AFHTTPRequestOperation! , responseObject: AnyObject!) in
            
            let serverResponse = self.processAfterMakingRequestWithParameters(responseObject)
            
            self.completionBlockWithSuccess?(statusCode: 0, requestType: self.requestTag, response: serverResponse, errorMessage: nil);
            
            
            },
            failure: {(operation: AFHTTPRequestOperation?, error:NSError) in
                
                if error.code == -1009 || error.code == -1005 {
                    InternetConnectivityManager.internetErrorAlert.show()
                }
                
                self.completionBlockWithFailure?(requestType: self.requestTag, error: error)
                
            }
        )
    }
    
    
    //MARK:- Cookie methods
    private func saveCookies()
    {
        let cookiesData = NSKeyedArchiver.archivedDataWithRootObject(NSHTTPCookieStorage.sharedHTTPCookieStorage().cookies!)
//        NSHTTPCookieStorage.sharedHTTPCookieStorage().cookieAcceptPolicy = .Always
        Utility.saveObjectInDefault("sessionCookies", value: cookiesData)
        
        print(NSHTTPCookieStorage.sharedHTTPCookieStorage().cookies)

    }
    
    class func loadCookies() {
        
        if Utility.fetchObjectFromDefault("sessionCookies") != nil {
            
        let cookies = NSKeyedUnarchiver.unarchiveObjectWithData(Utility.fetchObjectFromDefault("sessionCookies") as! NSData) as! [NSHTTPCookie]
        
        print("ORGcookie: \(cookies)")

//        NSHTTPCookieStorage.sharedHTTPCookieStorage().cookieAcceptPolicy = .Always
        
        for cookie in cookies {
            
//            var cookieProperties = [String: AnyObject]()
//            
//            cookieProperties[NSHTTPCookieName] = cookie.name
//            cookieProperties[NSHTTPCookieValue] = cookie.value
//            cookieProperties[NSHTTPCookieDomain] = cookie.domain
//            cookieProperties[NSHTTPCookiePath] = cookie.path
//            cookieProperties[NSHTTPCookieVersion] = NSNumber(integer: cookie.version)
//            cookieProperties[NSHTTPCookieExpires] = cookie.expiresDate
//            cookieProperties[NSHTTPCookieSecure] = cookie.secure
//            
//            // Setting a Cookie
//            if let newCookie = NSHTTPCookie(properties: cookieProperties) {
//                // Made a copy of cookie (cookie can't be set)
//                print("Newcookie: \(newCookie)")
//                NSHTTPCookieStorage.sharedHTTPCookieStorage().setCookie(newCookie)
//            }
            print("ORGcookie: \(cookie)")
            
            NSHTTPCookieStorage.sharedHTTPCookieStorage().setCookie(cookie)
        }
        print(NSHTTPCookieStorage.sharedHTTPCookieStorage().cookies)
            
        }
    }
    
    class func resetCookie() {
        
        if Utility.fetchObjectFromDefault("sessionCookies") != nil {
            
            let cookies = NSKeyedUnarchiver.unarchiveObjectWithData(Utility.fetchObjectFromDefault("sessionCookies") as! NSData) as! [NSHTTPCookie]
            
            let cookieStorage = NSHTTPCookieStorage.sharedHTTPCookieStorage()
            
            for cookie in cookies {
                cookieStorage.deleteCookie(cookie)
            }
            
            Utility.removeParameterFromUserDefault("sessionCookies")
            
        }
        
    }

}
