//
//  ExpandableTableViewCell.swift
//  Cinch
//
//  Created by MONEY on 21/02/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class ExpandableTableViewCell: UITableViewCell {

    @IBOutlet weak var topSeperator: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bottomSeperator: UIView!
    @IBOutlet weak var accessoryImageView: UIImageView!
    
    @IBOutlet weak var constraintForTextViewHeight: NSLayoutConstraint!
    @IBOutlet weak var textView: UITextView!
    
    var isExpanded = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = UITableViewCellSelectionStyle.None

        constraintForTextViewHeight.constant = 0
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setHeightForTextViewTo(height: CGFloat) {
        constraintForTextViewHeight.constant = height
        
        UIView.animateWithDuration(0.2) { () -> Void in
            self.layoutIfNeeded()
        }
    }
    
}
