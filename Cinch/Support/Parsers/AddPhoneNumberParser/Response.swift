//
//	Response.swift
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class Response : NSObject, NSCoding{

	var apiId : String!
	var message : String!
	var messageUuid : [String]!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		apiId = dictionary["api_id"] as? String
		message = dictionary["message"] as? String
		messageUuid = dictionary["message_uuid"] as? [String]
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if apiId != nil{
			dictionary["api_id"] = apiId
		}
		if message != nil{
			dictionary["message"] = message
		}
		if messageUuid != nil{
			dictionary["message_uuid"] = messageUuid
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         apiId = aDecoder.decodeObjectForKey("api_id") as? String
         message = aDecoder.decodeObjectForKey("message") as? String
         messageUuid = aDecoder.decodeObjectForKey("message_uuid") as? [String]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if apiId != nil{
			aCoder.encodeObject(apiId, forKey: "api_id")
		}
		if message != nil{
			aCoder.encodeObject(message, forKey: "message")
		}
		if messageUuid != nil{
			aCoder.encodeObject(messageUuid, forKey: "message_uuid")
		}

	}

}