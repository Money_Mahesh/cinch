//
//  PaymentPlanViewController.swift
//  Cinch
//
//  Created by MONEY on 08/03/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class PaymentPlanViewController: UIViewController {

    let unSelectedPlanColor = UIColor(red: 119/255, green: 119/255, blue: 119/255, alpha: 1.0)
    let selectedPlanColor = UIColor(red: 0.0/255, green: 159.0/255, blue: 232.0/255, alpha: 1.0)
    
    var selectedPlanName : String?
    
    var paymentPlanDetail : PaymentPlanDetail?
    var planNameArray = [String]()
    var planNameNo = [String]()

    @IBOutlet weak var tableView: UITableView!

    weak var delegate : UpdateCellDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Payment Plans"
        Utility.setBackButton(forViewController: self, withTitle: "")

        self.getPaymentPlanListService()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.hidden = false
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.hidden = true
    }
    
    //MARK:- IBActions
    func backAction() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func doneBtnAction(sender: AnyObject) {
        
        if delegate != nil {
            delegate?.updateCell(CellToBeUpdate.PaymentPlan, withObject: paymentPlanDetail)
        }
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    //MARK: - TableViewDataSource Methods
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return planNameArray.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let planCell = tableView.dequeueReusableCellWithIdentifier("PlanCell")
        
        let planName = planCell?.viewWithTag(101) as! UILabel
        planName.text = planNameArray[indexPath.row]
        
        if selectedPlanName != nil && selectedPlanName == planName.text {
            planName.font = UIFont(name: "Lato-Black", size: 11)
            planName.textColor = selectedPlanColor
        }
        else {
            planName.font = UIFont(name: "Lato-Regular", size: 11)
            planName.textColor = unSelectedPlanColor
        }
        return planCell!
    }
    
    //MARK: - TableViewDelegate Methods
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selectedPlanName = planNameArray[indexPath.row]
        
        paymentPlanDetail?.paymentPlanName = selectedPlanName
        paymentPlanDetail?.paymentPlan = planNameNo[indexPath.row]

        tableView.reloadData()
    }
    
    //MARK: - Web service hit methods
    func getPaymentPlanListService () {
        
        CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Getting payment plan list. Please wait..")
        
        let paymentMethodListServiceObj = CommonServiceClass(httpRequestType: HTTP_REQUEST_TYPE.POST_REQUEST, requestTag: "GET_PAYMENT_PLAN_LIST_SERVICE", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
        
        paymentMethodListServiceObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
            
            
            if errorMessage != nil {
                //print(response)
            }
            
            print(response as! NSDictionary)
            let responseDict = CSPaymentPlan(fromDictionary: response as! NSDictionary)
            
            if responseDict.errorCode == 1 {
                Utility.showAlert(ALERT_TITLE, message: responseDict.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            }
            else {
                
                print(responseDict.data)
                
                if responseDict.data.count > 0 {
                    self.paymentPlanDetail = responseDict.data[0]
                    
                    self.planNameArray = self.paymentPlanDetail!.paymentPlanName.componentsSeparatedByString(", ")
                    self.planNameNo = self.paymentPlanDetail!.paymentPlan.componentsSeparatedByString(", ")
                    self.tableView.reloadData()
                    
                }
                else {
                    Utility.showAlert(ALERT_TITLE, message: "Data not found", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)

                }

            }
            
            CSActivityIndicator.hideAllFromMainWindow()

        }
        paymentMethodListServiceObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
            print("error ------------------------------\(error)")
            CSActivityIndicator.hideAllFromMainWindow()
        }
        
        var parameterDict = Dictionary <String, String>()
        parameterDict["cmd"] = "checkDivisionList"
        parameterDict["division_seq"] = PlayerDetailManager.programSelected!.divisionSeq

        paymentMethodListServiceObj.commonServiceHit(parameterDict)
    }

}
