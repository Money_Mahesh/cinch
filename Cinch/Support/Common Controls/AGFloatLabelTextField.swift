import UIKit

@IBDesignable class AGFloatLabelTextField: UITextField {
    var title = UILabel()
    var borderDrawn : Bool = false
    // MARK:- Properties
    override var accessibilityLabel:String? {
        get {
            if text!.isEmpty {
                return title.text
            } else {
                return text
            }
        }
        set {
            self.accessibilityLabel = newValue
        }
    }
    
    @IBInspectable var hintYPadding:CGFloat = 0.0
    
    @IBInspectable var titleYPadding:CGFloat = 0.0 {
        didSet {
            var r = title.frame
            r.origin.y = titleYPadding
            title.frame = r
        }
    }
    
    //MARK:- Placeholder

    @IBInspectable var PlaceholderColor:UIColor = UIColor.grayColor() {
        didSet {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder!,attributes:[NSForegroundColorAttributeName: PlaceholderColor])
        }
    }
    
    @IBInspectable var LeftPadding: UInt = 0 {
        didSet{
            let paddingView = UIView(frame: CGRectMake(0, 0, CGFloat(LeftPadding), self.frame.height))
            self.leftView  = paddingView
            self.leftViewMode = UITextFieldViewMode.Always
            
        }
    }
    
    @IBInspectable var RightPadding: UInt = 0 {
        didSet{
            let paddingView = UIView(frame: CGRectMake(0, 0, CGFloat(RightPadding), self.frame.height))
            self.rightView  = paddingView
            self.rightViewMode = UITextFieldViewMode.Always
            
        }
    }
    
    @IBInspectable var LineColor:UIColor = UIColor.grayColor()
    @IBInspectable var LineHeight:CGFloat = 1.0
    
    override func borderRectForBounds(bounds: CGRect) -> CGRect {
        
        if borderDrawn {
            return CGRect(x: 0, y: 0, width:  0, height: 0)
        }
        
        let border = CALayer()
        let borderHeight = LineHeight
        border.borderColor = LineColor.CGColor
        border.frame = CGRect(x: 0, y: bounds.size.height - borderHeight, width:  bounds.size.width, height: borderHeight)
        
        border.borderWidth = borderHeight
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
        
        return CGRect(x: 0, y: 0, width:  0, height: 0)
    }
    
    // MARK:- Init
    required init?(coder aDecoder:NSCoder) {
        super.init(coder:aDecoder)
    }
    
    override init(frame:CGRect) {
        super.init(frame:frame)
    }
    
    // MARK:- Overrides
    override func layoutSubviews() {
        super.layoutSubviews()
    }
}
