//
//  UIButtonExtension.swift
//  Cinch
//
//  Created by MONEY on 20/02/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import Foundation

extension UIButton {
    // new functionality to add to SomeType goes here
    
    func setTitleForAllState(title: String) {
        self.setTitle(title, forState: UIControlState.Normal)
        self.setTitle(title, forState: UIControlState.Highlighted)
        self.setTitle(title, forState: UIControlState.Selected)
    }
    
    func setTitleColorForAllState(titleColor: UIColor) {
        self.setTitleColor(titleColor, forState: UIControlState.Normal)
        self.setTitleColor(titleColor, forState: UIControlState.Highlighted)
        self.setTitleColor(titleColor, forState: UIControlState.Selected)
    }
}