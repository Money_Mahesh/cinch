//
//  TableViewCellWithTitle.swift
//  Cinch
//
//  Created by MONEY on 28/02/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class TableViewCellWithTitle: UITableViewCell, UITextFieldDelegate {

    @IBOutlet weak var labelForTitle: UILabel!
    @IBOutlet weak var textfieldForDescription: UITextField!
    
    let datePicker = UIDatePicker()
    var mainView : UIView!
    
    var selectedDate = String()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setUpView()
    }
    

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setUpView() {
        
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "TableViewCellWithTitle", bundle: bundle)
        mainView = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        
        mainView.frame = self.bounds
        mainView.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        self.addSubview(mainView)
    }
    
    func bindCellWithData(title: String, description: String, behaveAsPicker: Bool, withAccessoryImage : Bool, isPickerTypeDate : Bool) {
        
        if withAccessoryImage {
            self.accessoryType = .DisclosureIndicator
        }
        else {
            self.accessoryType = .None
        }
        
        if behaveAsPicker && isPickerTypeDate {
            self.datePicker.datePickerMode = .DateAndTime
            textfieldForDescription.inputView = datePicker
            datePicker.datePickerMode = .DateAndTime
            textfieldForDescription.delegate = self
        }
        else if behaveAsPicker && !isPickerTypeDate {
            textfieldForDescription.delegate = nil
            textfieldForDescription.userInteractionEnabled = false
            self.mainView.userInteractionEnabled = false
        }
        else {
            textfieldForDescription.delegate = nil
        }
        
        labelForTitle.text = title
        textfieldForDescription.text = description
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        
        if textField == self.textfieldForDescription {
            let dateformatter = NSDateFormatter()
//            dateformatter.dateStyle = .LongStyle
            dateformatter.timeStyle = .ShortStyle

            selectedDate = Utility.changeDateFormate(dateformatter.stringFromDate(self.datePicker.date), inputFormat: "h:m a", outputFormat: "h:m:s")
            
            textField.text = dateformatter.stringFromDate(self.datePicker.date)
//            textField.text?.removeRange((textField.text?.rangeOfString("at"))!)
        }
        
    }
    
}
