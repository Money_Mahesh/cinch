//
//	WeatherData.swift
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class WeatherData : NSObject, NSCoding{

	var base : String!
	var clouds : Cloud!
	var cod : Int!
	var coord : Coord!
	var dt : Int!
	var id : Int!
	var main : Main!
	var name : String!
//	var rain : Rain!
	var sys : Sy!
	var weather : [Weather]!
	var wind : Wind!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		base = dictionary["base"] as? String
		if let cloudsData = dictionary["clouds"] as? NSDictionary{
			clouds = Cloud(fromDictionary: cloudsData)
		}
		cod = dictionary["cod"] as? Int
		if let coordData = dictionary["coord"] as? NSDictionary{
			coord = Coord(fromDictionary: coordData)
		}
		dt = dictionary["dt"] as? Int
		id = dictionary["id"] as? Int
		if let mainData = dictionary["main"] as? NSDictionary{
			main = Main(fromDictionary: mainData)
		}
		name = dictionary["name"] as? String
//		if let rainData = dictionary["rain"] as? NSDictionary{
//			rain = Rain(fromDictionary: rainData)
//		}
		if let sysData = dictionary["sys"] as? NSDictionary{
			sys = Sy(fromDictionary: sysData)
		}
		weather = [Weather]()
		if let weatherArray = dictionary["weather"] as? [NSDictionary]{
			for dic in weatherArray{
				let value = Weather(fromDictionary: dic)
				weather.append(value)
			}
		}
		if let windData = dictionary["wind"] as? NSDictionary{
			wind = Wind(fromDictionary: windData)
		}
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if base != nil{
			dictionary["base"] = base
		}
		if clouds != nil{
			dictionary["clouds"] = clouds.toDictionary()
		}
		if cod != nil{
			dictionary["cod"] = cod
		}
		if coord != nil{
			dictionary["coord"] = coord.toDictionary()
		}
		if dt != nil{
			dictionary["dt"] = dt
		}
		if id != nil{
			dictionary["id"] = id
		}
		if main != nil{
			dictionary["main"] = main.toDictionary()
		}
		if name != nil{
			dictionary["name"] = name
		}
//		if rain != nil{
//			dictionary["rain"] = rain.toDictionary()
//		}
		if sys != nil{
			dictionary["sys"] = sys.toDictionary()
		}
		if weather != nil{
			var dictionaryElements = [NSDictionary]()
			for weatherElement in weather {
				dictionaryElements.append(weatherElement.toDictionary())
			}
			dictionary["weather"] = dictionaryElements
		}
		if wind != nil{
			dictionary["wind"] = wind.toDictionary()
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         base = aDecoder.decodeObjectForKey("base") as? String
         clouds = aDecoder.decodeObjectForKey("clouds") as? Cloud
         cod = aDecoder.decodeObjectForKey("cod") as? Int
         coord = aDecoder.decodeObjectForKey("coord") as? Coord
         dt = aDecoder.decodeObjectForKey("dt") as? Int
         id = aDecoder.decodeObjectForKey("id") as? Int
         main = aDecoder.decodeObjectForKey("main") as? Main
         name = aDecoder.decodeObjectForKey("name") as? String
//         rain = aDecoder.decodeObjectForKey("rain") as? Rain
         sys = aDecoder.decodeObjectForKey("sys") as? Sy
         weather = aDecoder.decodeObjectForKey("weather") as? [Weather]
         wind = aDecoder.decodeObjectForKey("wind") as? Wind

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if base != nil{
			aCoder.encodeObject(base, forKey: "base")
		}
		if clouds != nil{
			aCoder.encodeObject(clouds, forKey: "clouds")
		}
		if cod != nil{
			aCoder.encodeObject(cod, forKey: "cod")
		}
		if coord != nil{
			aCoder.encodeObject(coord, forKey: "coord")
		}
		if dt != nil{
			aCoder.encodeObject(dt, forKey: "dt")
		}
		if id != nil{
			aCoder.encodeObject(id, forKey: "id")
		}
		if main != nil{
			aCoder.encodeObject(main, forKey: "main")
		}
		if name != nil{
			aCoder.encodeObject(name, forKey: "name")
		}
//		if rain != nil{
//			aCoder.encodeObject(rain, forKey: "rain")
//		}
		if sys != nil{
			aCoder.encodeObject(sys, forKey: "sys")
		}
		if weather != nil{
			aCoder.encodeObject(weather, forKey: "weather")
		}
		if wind != nil{
			aCoder.encodeObject(wind, forKey: "wind")
		}

	}

}