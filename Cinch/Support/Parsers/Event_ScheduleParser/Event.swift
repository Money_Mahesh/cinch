//
//	Event.swift
//
//	Create by Money Mahesh on 16/3/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class Event : NSObject, NSCoding{

	var address : String!
	var endTime : String!
	var eventDate : String!
	var eventSeq : String!
	var location : String!
	var programSeq : String!
	var startTime : String!
	var teamName : String!
	var teamSeq : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		address = dictionary["address"] as? String
		endTime = dictionary["end_time"] as? String
		eventDate = dictionary["event_date"] as? String
		eventSeq = dictionary["event_seq"] as? String
		location = dictionary["location"] as? String
		programSeq = dictionary["program_seq"] as? String
		startTime = dictionary["start_time"] as? String
		teamName = dictionary["team_name"] as? String
		teamSeq = dictionary["team_seq"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if address != nil{
			dictionary["address"] = address
		}
		if endTime != nil{
			dictionary["end_time"] = endTime
		}
		if eventDate != nil{
			dictionary["event_date"] = eventDate
		}
		if eventSeq != nil{
			dictionary["event_seq"] = eventSeq
		}
		if location != nil{
			dictionary["location"] = location
		}
		if programSeq != nil{
			dictionary["program_seq"] = programSeq
		}
		if startTime != nil{
			dictionary["start_time"] = startTime
		}
		if teamName != nil{
			dictionary["team_name"] = teamName
		}
		if teamSeq != nil{
			dictionary["team_seq"] = teamSeq
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         address = aDecoder.decodeObjectForKey("address") as? String
         endTime = aDecoder.decodeObjectForKey("end_time") as? String
         eventDate = aDecoder.decodeObjectForKey("event_date") as? String
         eventSeq = aDecoder.decodeObjectForKey("event_seq") as? String
         location = aDecoder.decodeObjectForKey("location") as? String
         programSeq = aDecoder.decodeObjectForKey("program_seq") as? String
         startTime = aDecoder.decodeObjectForKey("start_time") as? String
         teamName = aDecoder.decodeObjectForKey("team_name") as? String
         teamSeq = aDecoder.decodeObjectForKey("team_seq") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if address != nil{
			aCoder.encodeObject(address, forKey: "address")
		}
		if endTime != nil{
			aCoder.encodeObject(endTime, forKey: "end_time")
		}
		if eventDate != nil{
			aCoder.encodeObject(eventDate, forKey: "event_date")
		}
		if eventSeq != nil{
			aCoder.encodeObject(eventSeq, forKey: "event_seq")
		}
		if location != nil{
			aCoder.encodeObject(location, forKey: "location")
		}
		if programSeq != nil{
			aCoder.encodeObject(programSeq, forKey: "program_seq")
		}
		if startTime != nil{
			aCoder.encodeObject(startTime, forKey: "start_time")
		}
		if teamName != nil{
			aCoder.encodeObject(teamName, forKey: "team_name")
		}
		if teamSeq != nil{
			aCoder.encodeObject(teamSeq, forKey: "team_seq")
		}

	}

}