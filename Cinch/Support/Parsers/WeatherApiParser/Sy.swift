//
//	Sy.swift
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class Sy : NSObject, NSCoding{

	var country : String!
	var message : Float!
	var sunrise : Int!
	var sunset : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		country = dictionary["country"] as? String
		message = dictionary["message"] as? Float
		sunrise = dictionary["sunrise"] as? Int
		sunset = dictionary["sunset"] as? Int
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if country != nil{
			dictionary["country"] = country
		}
		if message != nil{
			dictionary["message"] = message
		}
		if sunrise != nil{
			dictionary["sunrise"] = sunrise
		}
		if sunset != nil{
			dictionary["sunset"] = sunset
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         country = aDecoder.decodeObjectForKey("country") as? String
         message = aDecoder.decodeObjectForKey("message") as? Float
         sunrise = aDecoder.decodeObjectForKey("sunrise") as? Int
         sunset = aDecoder.decodeObjectForKey("sunset") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if country != nil{
			aCoder.encodeObject(country, forKey: "country")
		}
		if message != nil{
			aCoder.encodeObject(message, forKey: "message")
		}
		if sunrise != nil{
			aCoder.encodeObject(sunrise, forKey: "sunrise")
		}
		if sunset != nil{
			aCoder.encodeObject(sunset, forKey: "sunset")
		}

	}

}