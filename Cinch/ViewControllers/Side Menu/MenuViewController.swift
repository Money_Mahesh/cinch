//
//  MenuViewController.swift
//  Cinch
//
//  Created by Amit Garg on 25/02/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var menuTableView: UITableView!
    var menuItems : [String]!
    var selectedIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Cinch"
        
        menuItems = ["Dashboard", "Add Player", "Add Program", "My Coaches", "Settings"]
        
        self.menuTableView.delegate = self
        self.menuTableView.dataSource = self
        
        self.menuTableView.registerNib(UINib(nibName: "SectionHeaderWithLabelAndButton", bundle: nil), forHeaderFooterViewReuseIdentifier: "SectionHeaderWithLabelAndButton")

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.revealViewController().frontViewController.view.userInteractionEnabled = false
        self.revealViewController().frontViewController.revealViewController().tapGestureRecognizer()
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.revealViewController().frontViewController.view.userInteractionEnabled = true

    }
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        return menuItems.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return 50.0
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 5.0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let menuCell = tableView.dequeueReusableCellWithIdentifier("menuCell", forIndexPath: indexPath)
        menuCell.textLabel?.text = menuItems[indexPath.row]
        menuCell.contentView.backgroundColor = UIColor(red: 23.0/255.0, green: 22/255, blue: 22/255, alpha: 1.0)
        menuCell.selectionStyle = .None
        menuCell.textLabel?.font = UIFont(name: "Lato-Regular", size: 16.0)
        
     if indexPath.row ==  selectedIndex {
            menuCell.textLabel?.textColor = UIColor(red: 0.0, green: 159/255, blue: 232/255, alpha: 1.0)
        } else {
            menuCell.textLabel?.textColor = UIColor.whiteColor()
        }
        
        menuCell.textLabel?.backgroundColor = UIColor.clearColor()
        
        return menuCell
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = tableView.dequeueReusableHeaderFooterViewWithIdentifier("SectionHeaderWithLabelAndButton") as! SectionHeaderWithLabelAndButton
        headerView.label.hidden = true
        headerView.button.hidden = true
        headerView.contentView.backgroundColor = UIColor(red: 23.0/255.0, green: 22/255, blue: 22/255, alpha: 1.0)
        headerView.upperSeperator.hidden = true
        headerView.lowerSeperator.backgroundColor = UIColor(red: 46.0/255.0, green: 51/255, blue: 47/255, alpha: 1.0)
        return headerView
        
    }
    
    // MARK: - Table view delegate
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        switch(indexPath.row) {
        
        case 0:
            let playerDashboardViewController = amitStoryBoard.instantiateViewControllerWithIdentifier("PlayerDashboardViewController")
            let navCont = UINavigationController(rootViewController: playerDashboardViewController)
            self.revealViewController().pushFrontViewController(navCont, animated: true)
            
        case 1:
            let addPlayerVC = amitStoryBoard.instantiateViewControllerWithIdentifier("AddPlayerViewController") as! AddPlayerViewController
            addPlayerVC.isComingFrom = ComingFrom.AddPlayerSideMenu
            let navCont = UINavigationController(rootViewController: addPlayerVC)
            self.revealViewController().pushFrontViewController(navCont, animated: true)
            
        case 2:
            let addProgramVC = mainStoryBoard.instantiateViewControllerWithIdentifier("AddProgramViewController") as! AddProgramViewController

            var selectedPlayer : PlayerData?
            selectedPlayer = PlayerDetailManager.playerDetail
            addProgramVC.isComingFrom = ComingFrom.AddProgramSideMenu
            addProgramVC.playerList = [selectedPlayer!]
            addProgramVC.addressDetail = FatherAdd(fromPlayerDetail: selectedPlayer!)
            addProgramVC.mSeq = (selectedPlayer?.motherSeq)!
            addProgramVC.fSeq = (selectedPlayer?.fatherSeq)!
            addProgramVC.gSeq = (selectedPlayer?.guardianSeq)!
            addProgramVC.selectedPlayer = selectedPlayer
            
            let navCont = UINavigationController(rootViewController: addProgramVC)
            self.revealViewController().pushFrontViewController(navCont, animated: true)
            
        case 3:
            let myCoachesVC = amitStoryBoard.instantiateViewControllerWithIdentifier("MyCoachesViewController")
            let navCont = UINavigationController(rootViewController: myCoachesVC)
            self.revealViewController().pushFrontViewController(navCont, animated: true)
        
            
        case 4:
            let accountSettingsViewController = amitStoryBoard.instantiateViewControllerWithIdentifier("AccountSettingsViewController")
            let navCont = UINavigationController(rootViewController: accountSettingsViewController)
            self.revealViewController().pushFrontViewController(navCont, animated: true)
            
        default:
            print("Something is broken")
            
        }
        selectedIndex = indexPath.row
        self.menuTableView.reloadData()
        
    }
    
    @IBAction func logoutButtonTapped(sender: UIButton) {
        
        UserProfileManager.logOut()
        let viewController = mainStoryBoard.instantiateViewControllerWithIdentifier("WelcomeScreenBaseViewController")
        ROOT_NAVVIGATION_CONROLLER.setViewControllers([viewController], animated: true)
    }
}
