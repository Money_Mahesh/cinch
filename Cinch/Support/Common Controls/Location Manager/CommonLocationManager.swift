//
//  CommonLocationManager.swift
//  Cinch
//
//  Created by MONEY on 05/03/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import Foundation
import CoreLocation

class CommonLocationManger: CLLocationManager, CLLocationManagerDelegate {
    
    static var sharedInstance: CommonLocationManger!
    var locationManager: CLLocationManager!
    var currentLocation : CLLocation?
    
    override init() {
        
        super.init()
        
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        
    }
    
    func locationManager(manager: CLLocationManager, didUpdateToLocation newLocation: CLLocation, fromLocation oldLocation: CLLocation) {
        
        currentLocation = newLocation
    }
}