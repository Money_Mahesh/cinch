//
//  Constants.swift
//  Cinch
//
//  Created by MONEY on 14/02/16.
//  Copyright © 2016 MONEY. All rights reserved.
//


// MARK: - URLS
var BASE_URL: NSURL! { get { return NSURL(string: "")} }

let ROOT_NAVVIGATION_CONROLLER = (UIApplication.sharedApplication().keyWindow?.rootViewController as! ParentViewController)
let TOP_VIEWCONTROLLER = Utility.topViewController()

let KEY_WINDOW = UIApplication.sharedApplication().keyWindow

let amitStoryBoard = UIStoryboard(name: "Amit", bundle: nil)
let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)


//Stripe Constant
// This can be found at https://dashboard.stripe.com/account/apikeys
let StripePublishableKey = "pk_test_r35puzPtCAZLLpApaXVMPzum"
let backendChargeURLString = "https://www.cinchsports.com/user/services_v2/processRequest"

// To learn how to obtain an Apple Merchant ID, head to https://stripe.com/docs/mobile/apple-pay
var AppleMerchantId : String?
