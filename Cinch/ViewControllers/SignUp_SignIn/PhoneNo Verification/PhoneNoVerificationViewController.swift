//
//  PhoneNoVerificationViewController.swift
//  Cinch
//
//  Created by MONEY on 17/02/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class PhoneNoVerificationViewController: UIViewController, PickerViewDelegate, UITextFieldDelegate {

    @IBOutlet weak var backBtnRef: UIButton!

    @IBOutlet weak var textFieldForCounrty: UITextField!
    @IBOutlet weak var textFieldForPhoneNo: UITextField!
    @IBOutlet weak var textFieldForCountryCode: UITextField!

    let customPickerViewObj = CustomPickerView()
    let pickerContentArray = [
        ["Z1", "Z2", "Z3", "Z4"],
    ]
    var selectedRowIndexArray = [0]
    var isComingFromCreateAccount = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        customPickerViewObj.isParentNavigationBarPresent = false
    
        textFieldForCounrty.attributedPlaceholder = NSAttributedString(string:"Country",
            attributes:[NSForegroundColorAttributeName: UIColor(red: 216.0/255.0, green: 216.0/255, blue: 216.0/255.0, alpha: 1)])
        textFieldForPhoneNo.attributedPlaceholder = NSAttributedString(string:"Phone number",
            attributes:[NSForegroundColorAttributeName: UIColor(red: 216.0/255.0, green: 216.0/255, blue: 216.0/255.0, alpha: 1)])
        textFieldForCountryCode.attributedPlaceholder = NSAttributedString(string:"Code",
            attributes:[NSForegroundColorAttributeName: UIColor(red: 216.0/255.0, green: 216.0/255, blue: 216.0/255.0, alpha: 1)])
        
        customPickerViewObj.setUpCustomPickerView()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.hidden = true
    }
    
    //MARK:- Set Navigation Bar
    func setNavigationBar() {
        
        self.navigationController?.navigationBar.hidden = true

    }
    
    //MARK:- PickerView Methods
    func setPickerViewValue() {
        customPickerViewObj.delegate = self
        customPickerViewObj.loadPickerViewForSelected(pickerContentArray, row: self.selectedRowIndexArray, withTitle: nil)
    }
    
    func pickerViewSelected(rowArray: [Int]) {
        
        selectedRowIndexArray = rowArray
        for index in 0..<rowArray.count {
            textFieldForCounrty.text = (pickerContentArray[index][rowArray[index]])
            print("Selected Element \(pickerContentArray[index][rowArray[index]])")
        }
    }

    // MARK: - IBActions
    @IBAction func backBtnAction(sender: AnyObject) {
        self.navigationController?.navigationBar.hidden = true
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func doneBtnAction(sender: AnyObject) {
        
        if textFieldForCounrty.text?.characters.count == 0 {
            Utility.showAlert(nil, message: "Please select Country", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
        }
        else if textFieldForPhoneNo.text?.characters.count == 0 {
            Utility.showAlert(nil, message: "Please enter Mobile no.", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
        }
        else if (textFieldForPhoneNo.text)?.stringByReplacingOccurrencesOfString(" ", withString: "").characters.count < 10 || (textFieldForPhoneNo.text)?.stringByReplacingOccurrencesOfString(" ", withString: "").characters.count > 11 {
            Utility.showAlert(nil, message: "Mobile number should be of 10 digits.", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
        }
        else {
            
            UserProfileManager.country = textFieldForCounrty.text
            UserProfileManager.phoneNo = textFieldForPhoneNo.text
            
            self.hitAddPhoneNumberService()
            
        }
    }
    
    
    @IBAction func terms_CondAction(sender: AnyObject) {
        
        let vc = amitStoryBoard.instantiateViewControllerWithIdentifier("WebViewController") as! WebViewController
        
        vc.urlToLoad = "https://www.cinchsports.com/user/user_terms_condition/"
        vc.navigationHeaderTitle = "Terms & Conditions"
        
        
        let navCont = UINavigationController(rootViewController: vc)
        
        self.presentViewController(navCont, animated: true, completion:nil)
    }
    
    
    @IBAction func selectCountryAction(sender: AnyObject) {
//        self.setPickerViewValue()
    }
    
    //MARK:- TextFeild Delegate
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        if textField.text?.characters.count == 11 && string != "" {
            return false
        }
        return true
    }
    
    //MARK:- Web Service Methods
    func hitAddPhoneNumberService() {
        
        CSActivityIndicator.showToMainWindowWithMessage("Adding Phone Number. Please wait..")
        
        let sendVerificationObj = CommonServiceClass(httpRequestType: HTTP_REQUEST_TYPE.POST_REQUEST, requestTag: "SEND_VERIFICATION_CODE", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
        
        sendVerificationObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
            
            
            if errorMessage != nil
            {
                //print(response)
            }
            
            print(response as! NSDictionary)
            let responseDict = PhoneNumberAddedData(fromDictionary: response as! NSDictionary)
            
            if responseDict.errorCode == 0 {
                print(responseDict.data)

            
                let oTPViewControllerObj = self.storyboard?.instantiateViewControllerWithIdentifier("OTPViewController") as! OTPViewController
                oTPViewControllerObj.userPhoneNumber = self.textFieldForPhoneNo.text!
                oTPViewControllerObj.isComingFromCreateAccount = self.isComingFromCreateAccount
                
                ROOT_NAVVIGATION_CONROLLER.topViewController?.navigationController!.pushViewController(oTPViewControllerObj
                    , animated: true)
                
                Utility.showAlert(ALERT_TITLE, message: "A verification code has been sent to your phone", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            }
            else {
                
                Utility.showAlert(ALERT_TITLE, message: responseDict.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            }
            
            CSActivityIndicator.hideAllFromMainWindow()
            
        }
        sendVerificationObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
            CSActivityIndicator.hideAllFromMainWindow()
            print("error ------------------------------\(error)")
        }
        
        var parameterDict = Dictionary <String, String>()
        parameterDict["cmd"] = "sendVerificationCode"
        parameterDict["mobile_no"] = textFieldForPhoneNo.text!
        sendVerificationObj.commonServiceHit(parameterDict)
    }
    
}
