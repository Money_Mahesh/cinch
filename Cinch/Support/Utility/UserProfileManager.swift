//
//  UserProfileManager.swift
//  Cinch
//
//  Created by MONEY on 24/02/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import Foundation

class UserProfileManager: NSObject {
    
    static func logOut() {
        name = nil
        emailId = nil
        gender = nil
        role = nil
        country = nil
        phoneNo = nil
        accountType = nil
        role = nil
        status = nil
        imageUrl = nil
        fbID = nil
        fbToken = nil
        userSequence = nil
        invitationSeq = nil
        FBSDKLoginManager().logOut()
        
        Utility.removeParameterFromUserDefault(USER_SEQUENCE)
        Utility.removeParameterFromUserDefault(USER_NAME)
        Utility.removeParameterFromUserDefault(USER_EMAIL)
        Utility.removeParameterFromUserDefault(USER_GENDER)
        Utility.removeParameterFromUserDefault(USER_PHONE_NO)
        Utility.removeParameterFromUserDefault(USER_COUNTRY)
        Utility.removeParameterFromUserDefault(USER_ACCOUNT_TYPE)
        Utility.removeParameterFromUserDefault(USER_ROLE)
        Utility.removeParameterFromUserDefault(USER_STATUS)
        Utility.removeParameterFromUserDefault(USER_INVITATION_SEQ)
        Utility.removeParameterFromUserDefault(USER_IMAGE_URL)
        Utility.removeParameterFromUserDefault(FACEBOOK_LOGIN)
        Utility.removeParameterFromUserDefault(USER_FB_ID)
        Utility.removeParameterFromUserDefault(FACEBOOK_TOKEN_STRING)
        Utility.removeParameterFromUserDefault(SIGN_UP_STATUS)
        RegisterationService.resetCookie()
        
        PlayerDetailManager.logOut()
    }
    
    static private var _userSequence: String?
    static var userSequence: String? {
        set{
            _userSequence = newValue
        
            DataValidationUtility.isNull(_userSequence) ? Utility.removeParameterFromUserDefault(USER_SEQUENCE) : Utility.saveStringInDefault(USER_SEQUENCE, value:_userSequence!)

        }
        get{
            
            if DataValidationUtility.isNull(_userSequence) {
                
                if DataValidationUtility.isNull(Utility.fetchStringFromDefault(USER_SEQUENCE)) {
                    return ""
                }
                
                _userSequence = Utility.fetchStringFromDefault(USER_SEQUENCE) as? String
            }
            return _userSequence
        }
    }
    
    static private var _name : String?
    static var name : String? {
        set {
            _name = newValue
        
            DataValidationUtility.isNull(_name) ? Utility.removeParameterFromUserDefault(USER_NAME) : Utility.saveStringInDefault(USER_NAME, value:_name!)

        }
        get {
            
            if DataValidationUtility.isNull(_name) {
                
                if DataValidationUtility.isNull(Utility.fetchStringFromDefault(USER_NAME)) {
                    return ""
                }
                
                _name = Utility.fetchStringFromDefault(USER_NAME) as? String
            }
            return _name
        }
    }
    
    static private var _emailId : String?
    static var emailId : String! {
        set {
        _emailId = newValue
        
        DataValidationUtility.isNull(_emailId) ? Utility.removeParameterFromUserDefault(USER_EMAIL) : Utility.saveStringInDefault(USER_EMAIL, value:_emailId!)
        
        }
        get {
            
            if DataValidationUtility.isNull(_emailId) {
                
                if DataValidationUtility.isNull(Utility.fetchStringFromDefault(USER_EMAIL)) {
                    return ""
                }
                
                _emailId = Utility.fetchStringFromDefault(USER_EMAIL) as? String
            }
            return _emailId
        }
    }
    
    static private var _gender : String?
    static var gender : String! {
        set {
        _gender = newValue
        
        DataValidationUtility.isNull(_gender) ? Utility.removeParameterFromUserDefault(USER_GENDER) : Utility.saveStringInDefault(USER_GENDER, value:_gender!)
        
        }
        get {
            
            if DataValidationUtility.isNull(_gender) {
                
                if DataValidationUtility.isNull(Utility.fetchStringFromDefault(USER_GENDER)) {
                    return ""
                }
                
                _gender = Utility.fetchStringFromDefault(USER_GENDER) as? String
            }
            return _gender
        }
    }
    
    static private var _phoneNo : String?
    static var phoneNo : String! {
        set {
        _phoneNo = newValue
        
        DataValidationUtility.isNull(_phoneNo) ? Utility.removeParameterFromUserDefault(USER_PHONE_NO) : Utility.saveStringInDefault(USER_PHONE_NO, value:_phoneNo!)
        
        }
        get {
            
            if DataValidationUtility.isNull(_phoneNo) {
                
                if DataValidationUtility.isNull(Utility.fetchStringFromDefault(USER_PHONE_NO)) {
                    return ""
                }
                
                _phoneNo = Utility.fetchStringFromDefault(USER_PHONE_NO) as? String
            }
            return _phoneNo
        }
    }

    static private var _country : String?
    static var country : String! {
        set {
        _country = newValue
        
        DataValidationUtility.isNull(_country) ? Utility.removeParameterFromUserDefault(USER_COUNTRY) : Utility.saveStringInDefault(USER_COUNTRY , value:_country!)
        
        }
        get {
            
            if DataValidationUtility.isNull(_country) {
                
                if DataValidationUtility.isNull(Utility.fetchStringFromDefault(USER_COUNTRY)) {
                    return ""
                }
                
                _country = Utility.fetchStringFromDefault(USER_COUNTRY) as? String
            }
            return _country
        }
    }

    static private var _accountType : String?
    static var accountType : String! {
        set {
        _accountType = newValue
        
        DataValidationUtility.isNull(_accountType) ? Utility.removeParameterFromUserDefault(USER_ACCOUNT_TYPE) : Utility.saveStringInDefault(USER_ACCOUNT_TYPE, value:_accountType!)
        
        }
        get {
            
            if DataValidationUtility.isNull(_accountType) {
                
                if DataValidationUtility.isNull(Utility.fetchStringFromDefault(USER_ACCOUNT_TYPE)) {
                    return ""
                }
                
                _accountType = Utility.fetchStringFromDefault(USER_ACCOUNT_TYPE) as? String
            }
            return _accountType
        }
    }
    
    static private var _role : String?
    static var role : String! {
        set {
        _role = newValue
        
        DataValidationUtility.isNull(_role) ? Utility.removeParameterFromUserDefault(USER_ROLE) : Utility.saveStringInDefault(USER_ROLE, value:_role!)
        
        }
        get {
            
            if DataValidationUtility.isNull(_role) {
                
                if DataValidationUtility.isNull(Utility.fetchStringFromDefault(USER_ROLE)) {
                    return ""
                }
                
                _role = Utility.fetchStringFromDefault(USER_ROLE) as? String
            }
            return _role
        }
    }
    
    static private var _status : String?
    static var status : String! {
        set {
        _status = newValue
        
        DataValidationUtility.isNull(_status) ? Utility.removeParameterFromUserDefault(USER_STATUS) : Utility.saveStringInDefault(USER_STATUS, value:_status!)
        
        }
        get {
            
            if DataValidationUtility.isNull(_status) {
                
                if DataValidationUtility.isNull(Utility.fetchStringFromDefault(USER_STATUS)) {
                    return ""
                }
                
                _status = Utility.fetchStringFromDefault(USER_STATUS) as? String
            }
            return _status
        }
    }
    
    static private var _invitationSeq : String?
    static var invitationSeq : String! {
        set {
        _invitationSeq = newValue
        
        DataValidationUtility.isNull(_invitationSeq) ? Utility.removeParameterFromUserDefault(USER_INVITATION_SEQ) : Utility.saveStringInDefault(USER_INVITATION_SEQ, value:_invitationSeq!)
        
        }
        get {
            
            if DataValidationUtility.isNull(_invitationSeq) {
                
                if DataValidationUtility.isNull(Utility.fetchStringFromDefault(USER_INVITATION_SEQ)) {
                    return ""
                }
                
                _invitationSeq = Utility.fetchStringFromDefault(USER_INVITATION_SEQ) as? String
            }
            return _invitationSeq
        }
    }
    
    static private var _imageUrl : String?
    static var imageUrl : String! {
        set {
        _imageUrl = newValue
        
        DataValidationUtility.isNull(_imageUrl) ? Utility.removeParameterFromUserDefault(USER_IMAGE_URL) : Utility.saveStringInDefault(USER_IMAGE_URL, value:_imageUrl!)
        
        }
        get {
            
            if DataValidationUtility.isNull(_imageUrl) {
                
                if DataValidationUtility.isNull(Utility.fetchStringFromDefault(USER_IMAGE_URL)) {
                    return ""
                }
                
                _imageUrl = Utility.fetchStringFromDefault(USER_IMAGE_URL) as? String
            }
            return _imageUrl
        }
    }
    
    static private var _isFacebook : String?
    static var isFacebook : String! {
        set {
        _isFacebook = newValue
        
        DataValidationUtility.isNull(_isFacebook) ? Utility.removeParameterFromUserDefault(FACEBOOK_LOGIN) : Utility.saveStringInDefault(FACEBOOK_LOGIN, value:_isFacebook!)
        
        }
        get {
            
            if DataValidationUtility.isNull(_isFacebook) {
                
                if DataValidationUtility.isNull(Utility.fetchStringFromDefault(FACEBOOK_LOGIN)) {
                    return "N"
                }
                
                _isFacebook = Utility.fetchStringFromDefault(FACEBOOK_LOGIN) as? String
            }
            return _isFacebook
        }
    }
    
    static private var _fbID : String?
    static var fbID : String! {
        set {
        _fbID = newValue
        
            if DataValidationUtility.isNull(_fbID) {
                Utility.removeParameterFromUserDefault(USER_FB_ID)
                UserProfileManager.isFacebook = "N"
            }
            else {
                 Utility.saveStringInDefault(USER_FB_ID, value:_fbID!)
                UserProfileManager.isFacebook = "Y"
            }
        }
        get {
            
            if DataValidationUtility.isNull(_fbID) {
                
                if DataValidationUtility.isNull(Utility.fetchStringFromDefault(USER_FB_ID)) {
                    return ""
                }
                
                _fbID = Utility.fetchStringFromDefault(USER_FB_ID) as? String
            }
            return _fbID
        }
    }
    
    static private var _fbToken : String?
    static var fbToken : String! {
        set {
        _fbToken = newValue
        
        DataValidationUtility.isNull(_fbToken) ? Utility.removeParameterFromUserDefault(FACEBOOK_TOKEN_STRING) : Utility.saveStringInDefault(FACEBOOK_TOKEN_STRING, value:_fbToken!)
        
        }
        get {
            
            if DataValidationUtility.isNull(_fbToken) {
                
                if DataValidationUtility.isNull(Utility.fetchStringFromDefault(FACEBOOK_TOKEN_STRING)) {
                    return ""
                }
                
                _fbToken = Utility.fetchStringFromDefault(FACEBOOK_TOKEN_STRING) as? String
            }
            return _fbToken
        }
    }
}