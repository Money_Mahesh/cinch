//
//	SignInDetail.swift
//
//	Create by Money Mahesh on 19/3/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class AccounDetail : NSObject, NSCoding{
    
	var exist_user_seq : String!
	var invitation_seq : String!
	var invite_user_role : String!
	var update_status : String!
	

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		exist_user_seq = dictionary["exist_user_seq"] as? String
		invitation_seq = dictionary["invitation_seq"] as? String
		invite_user_role = dictionary["invite_user_role"] as? String
		update_status = dictionary["update_status"] as? String
    }

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if exist_user_seq != nil{
			dictionary["exist_user_seq"] = exist_user_seq
		}
		if invitation_seq != nil{
			dictionary["invitation_seq"] = invitation_seq
		}
		if invite_user_role != nil{
			dictionary["invite_user_role"] = invite_user_role
		}
		if update_status != nil{
			dictionary["update_status"] = update_status
		}
        return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         exist_user_seq = aDecoder.decodeObjectForKey("exist_user_seq") as? String
         invitation_seq = aDecoder.decodeObjectForKey("invitation_seq") as? String
         invite_user_role = aDecoder.decodeObjectForKey("invite_user_role") as? String
         update_status = aDecoder.decodeObjectForKey("update_status") as? String
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if exist_user_seq != nil{
			aCoder.encodeObject(exist_user_seq, forKey: "exist_user_seq")
		}
		if invitation_seq != nil{
			aCoder.encodeObject(invitation_seq, forKey: "invitation_seq")
		}
		if invite_user_role != nil{
			aCoder.encodeObject(invite_user_role, forKey: "invite_user_role")
		}
		if update_status != nil{
			aCoder.encodeObject(update_status, forKey: "update_status")
        }
	}

}