//
//	Data.swift
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class PhoneNumberData : NSObject, NSCoding{

	var response : Response!
	var status : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		if let responseData = dictionary["response"] as? NSDictionary{
			response = Response(fromDictionary: responseData)
		}
		status = dictionary["status"] as? Int
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if response != nil{
			dictionary["response"] = response.toDictionary()
		}
		if status != nil{
			dictionary["status"] = status
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         response = aDecoder.decodeObjectForKey("response") as? Response
         status = aDecoder.decodeObjectForKey("status") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if response != nil{
			aCoder.encodeObject(response, forKey: "response")
		}
		if status != nil{
			aCoder.encodeObject(status, forKey: "status")
		}

	}

}