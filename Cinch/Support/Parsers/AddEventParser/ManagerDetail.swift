//
//	ManagerDetail.swift
//
//	Create by Money Mahesh on 7/4/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class ManagerDetail : NSObject, NSCoding{

	var ageGroupStart : String!
	var clubSeq : String!
	var coverImage : String!
	var descriptionField : String!
	var gender : String!
	var leaguePlaying : String!
	var programSeq : String!
	var status : String!
	var teamManager : String!
	var teamName : String!
	var teamSeq : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		ageGroupStart = dictionary["age_group_start"] as? String
		clubSeq = dictionary["club_seq"] as? String
		coverImage = dictionary["cover_image"] as? String
		descriptionField = dictionary["description"] as? String
		gender = dictionary["gender"] as? String
		leaguePlaying = dictionary["league_playing"] as? String
		programSeq = dictionary["program_seq"] as? String
		status = dictionary["status"] as? String
		teamManager = dictionary["team_manager"] as? String
		teamName = dictionary["team_name"] as? String
		teamSeq = dictionary["team_seq"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if ageGroupStart != nil{
			dictionary["age_group_start"] = ageGroupStart
		}
		if clubSeq != nil{
			dictionary["club_seq"] = clubSeq
		}
		if coverImage != nil{
			dictionary["cover_image"] = coverImage
		}
		if descriptionField != nil{
			dictionary["description"] = descriptionField
		}
		if gender != nil{
			dictionary["gender"] = gender
		}
		if leaguePlaying != nil{
			dictionary["league_playing"] = leaguePlaying
		}
		if programSeq != nil{
			dictionary["program_seq"] = programSeq
		}
		if status != nil{
			dictionary["status"] = status
		}
		if teamManager != nil{
			dictionary["team_manager"] = teamManager
		}
		if teamName != nil{
			dictionary["team_name"] = teamName
		}
		if teamSeq != nil{
			dictionary["team_seq"] = teamSeq
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         ageGroupStart = aDecoder.decodeObjectForKey("age_group_start") as? String
         clubSeq = aDecoder.decodeObjectForKey("club_seq") as? String
         coverImage = aDecoder.decodeObjectForKey("cover_image") as? String
         descriptionField = aDecoder.decodeObjectForKey("description") as? String
         gender = aDecoder.decodeObjectForKey("gender") as? String
         leaguePlaying = aDecoder.decodeObjectForKey("league_playing") as? String
         programSeq = aDecoder.decodeObjectForKey("program_seq") as? String
         status = aDecoder.decodeObjectForKey("status") as? String
         teamManager = aDecoder.decodeObjectForKey("team_manager") as? String
         teamName = aDecoder.decodeObjectForKey("team_name") as? String
         teamSeq = aDecoder.decodeObjectForKey("team_seq") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if ageGroupStart != nil{
			aCoder.encodeObject(ageGroupStart, forKey: "age_group_start")
		}
		if clubSeq != nil{
			aCoder.encodeObject(clubSeq, forKey: "club_seq")
		}
		if coverImage != nil{
			aCoder.encodeObject(coverImage, forKey: "cover_image")
		}
		if descriptionField != nil{
			aCoder.encodeObject(descriptionField, forKey: "description")
		}
		if gender != nil{
			aCoder.encodeObject(gender, forKey: "gender")
		}
		if leaguePlaying != nil{
			aCoder.encodeObject(leaguePlaying, forKey: "league_playing")
		}
		if programSeq != nil{
			aCoder.encodeObject(programSeq, forKey: "program_seq")
		}
		if status != nil{
			aCoder.encodeObject(status, forKey: "status")
		}
		if teamManager != nil{
			aCoder.encodeObject(teamManager, forKey: "team_manager")
		}
		if teamName != nil{
			aCoder.encodeObject(teamName, forKey: "team_name")
		}
		if teamSeq != nil{
			aCoder.encodeObject(teamSeq, forKey: "team_seq")
		}

	}

}