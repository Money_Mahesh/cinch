//
//  AvailabilityViewController.swift
//  Cinch
//
//  Created by MONEY on 27/02/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class AvailabilityViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var availabiltyTableView: UITableView!
    @IBOutlet weak var switchForAvailabilty: UISwitch!
    @IBOutlet weak var labelForEventName: UILabel!
    @IBOutlet weak var labelForEventTime: UILabel!
    
    var eventDetail: EventDetail!
    var playerList: EventPlayerList!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Utility.setBackButton(forViewController: self, withTitle: "")
        self.title = "Availability"

        self.setEventDetail()
        self.hitAvailabiltyWebService()
        
        switchForAvailabilty.tintColor = UIColor(red: 0.0/255.0, green: 159/255, blue: 232/255, alpha: 1.0)
        switchForAvailabilty.onTintColor = UIColor(red: 0.0/255.0, green: 159/255, blue: 232/255, alpha: 1.0)
        
        //Table Footer View
        let tableFooterView = UIView(frame: CGRectMake(0, 0, MAIN_SCREEN_WIDTH, 1))
        tableFooterView.backgroundColor = UIColor(red: 238.0/255.0, green: 238.0/255.0, blue: 238.0/255.0, alpha: 1.0)
        availabiltyTableView.tableFooterView = tableFooterView

        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func setEventDetail() {
        //Set event date and Name
        if Utility.changeDateFormate(eventDetail.eventDate, inputFormat: "YYYY-MM-dd", outputFormat: "MM/dd") == "" {
            labelForEventName.text = eventDetail.eventName
        }
        else {
            labelForEventName.text = Utility.changeDateFormate(eventDetail.eventDate, inputFormat: "YYYY-MM-dd", outputFormat: "MM/dd") + " " + eventDetail.eventName
        }
        
        //Set Event time and address
        if Utility.changeDateFormate(eventDetail.startTime, inputFormat: "hh:mm:ss"
            , outputFormat: "h:mm a") == "" {
                
                labelForEventTime.text = eventDetail.teamName
        }
        else {
            labelForEventTime.text = Utility.changeDateFormate(eventDetail.startTime, inputFormat: "hh:mm:ss"
                , outputFormat: "h:mm a") + " @ " + eventDetail.teamName
        }
        
        if eventDetail.status == "Y" {
            switchForAvailabilty.on = true
        }
        else {
            switchForAvailabilty.on = false
        }
        
        switchForAvailabilty.addTarget(self, action: Selector("changeSwitchValue:"), forControlEvents: UIControlEvents.ValueChanged)

    }
    
    // MARK:- TableView Data Source and Delegates
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        var noOfSection : Int = 0
        
        if playerList == nil {
            return noOfSection
        }
        if playerList.available != nil && playerList.available.count != 0 {
            noOfSection++
        }
        
        if playerList.unavailable != nil && playerList.unavailable.count != 0 {
            noOfSection++
        }
        return noOfSection
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 22
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView(frame: CGRectMake(0, 0, MAIN_SCREEN_WIDTH, 22))
        headerView.backgroundColor = UIColor(red: 247.0/255.0, green: 247.0/255.0, blue: 247.0/255.0, alpha: 1.0)
        let label = UILabel(frame: CGRectMake(16, 0, MAIN_SCREEN_WIDTH - 16.0, 22))
        
        if playerList.available != nil && playerList.available.count != 0 && section == 0 {
            label.text = "AVAILABLE"
        }
        else if playerList.unavailable != nil && playerList.unavailable.count != 0 {
            label.text = "UNAVAILABLE"
        }
        label.font = UIFont(name: "Lato-Black", size: 12.0)
        label.textColor = UIColor.blackColor()
        
        headerView.addSubview(label)
        return headerView
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var noOfRow : Int = 0
        if playerList.available != nil && playerList.available.count != 0 && section == 0 {
            noOfRow = playerList.available.count
        }
        else if playerList.unavailable != nil && playerList.unavailable.count != 0 {
            noOfRow = playerList.unavailable.count
        }
        return noOfRow
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let availabilityEventCell = tableView.dequeueReusableCellWithIdentifier("AvailabilityTableViewCell") as! AvailabilityEventCell
        
        if playerList.available != nil && playerList.available.count != 0 && indexPath.section == 0 {
            availabilityEventCell.labelForName.text = playerList.available[indexPath.row].fname + " " + playerList.available[indexPath.row].lname
            Utility.setImageWithURl(playerList.available[indexPath.row].profilePic, inImageView: availabilityEventCell.imageViewForProfilePic)
        }
        else if playerList.unavailable != nil && playerList.unavailable.count != 0 {
            availabilityEventCell.labelForName.text = playerList.unavailable[indexPath.row].fname + " " + playerList.unavailable[indexPath.row].lname
            Utility.setImageWithURl(playerList.unavailable[indexPath.row].profilePic, inImageView: availabilityEventCell.imageViewForProfilePic)
        }
        
        availabilityEventCell.labelForStatus.text = ""
        return availabilityEventCell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    //MARK: - IBActions
    func backAction() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func changeSwitchValue(sender: UISwitch) {
        self.hitUpdateAvailabiltyWebService()
    }
    
    //MARK:- Hit Webservice
    func hitAvailabiltyWebService() {
        
        CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Fetching list...")
        
        let availabiltyObj = CommonServiceClass(httpRequestType: HTTP_REQUEST_TYPE.POST_REQUEST, requestTag: "AVAILABILTY_LIST", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
        
        availabiltyObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
            
            if errorMessage != nil
            {
                print(response)
            }
            print(response as! NSDictionary)
            
            let playerListObj =  CSPlayerList(fromDictionary: response as! NSDictionary)
            
            if playerListObj.errorCode == 0 {
                
                self.playerList = playerListObj.data
            } else {
                
                Utility.showAlert(ALERT_TITLE, message: playerListObj.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            }
            
            self.availabiltyTableView.reloadData()
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
        }
        availabiltyObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
            
            print("error ------------------------------\(error)")
        }
        
        
        var parameterDict = Dictionary <String, String>()
        
        parameterDict["cmd"] = "getEventPlayer"
        parameterDict["event_seq"] = eventDetail.eventSeq
        
        availabiltyObj.commonServiceHit(parameterDict)
    }
    
    func hitUpdateAvailabiltyWebService() {
        
        CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Updating...")
        
        let eventObj = CommonServiceClass(httpRequestType: HTTP_REQUEST_TYPE.POST_REQUEST, requestTag: "UPDATE", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
        
        eventObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
            
            if errorMessage != nil
            {
                print(response)
            }
            print(response as! NSDictionary)
            
            let eventObj =  OtpVerificationData(fromDictionary: response as! NSDictionary)
            
            if eventObj.errorCode == 0 {
                
                if eventObj.data == true {
                    self.eventDetail.status = self.eventDetail.status == "Y" ? "N" : "Y"
                }
                else {
                    self.eventDetail.status = self.eventDetail.status
                }
                
            } else {
                self.eventDetail.status = self.eventDetail.status
                Utility.showAlert(ALERT_TITLE, message: eventObj.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            }
            
            if self.eventDetail.status == "Y" {
                self.switchForAvailabilty.on = true
            }
            else {
                self.switchForAvailabilty.on = false
            }
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
        }
        eventObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
            
            print("error ------------------------------\(error)")
        }
        
        
        var parameterDict = Dictionary <String, String>()
        
        parameterDict["cmd"] = "updatePlayerEventStatus"
        parameterDict["player_seq"] = PlayerDetailManager.playerDetail?.playerSeq
        parameterDict["event_seq"] = eventDetail.eventSeq
        parameterDict["status"] = eventDetail.status == "Y" ? "N" : "Y"
        parameterDict["is_view"] = "1"
        
        
        eventObj.commonServiceHit(parameterDict)
    }

}

class AvailabilityEventCell : UITableViewCell {
    
    @IBOutlet weak var imageViewForProfilePic: UIImageView!
    @IBOutlet weak var labelForName: UILabel!
    @IBOutlet weak var labelForStatus: UILabel!
    @IBOutlet weak var labeForTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.imageViewForProfilePic.layer.cornerRadius = self.imageViewForProfilePic.frame.width / 2
        self.imageViewForProfilePic.clipsToBounds = true
        
        self.selectionStyle = .None
    }
}
