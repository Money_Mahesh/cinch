//
//  SuccessViewController.swift
//  Cinch
//
//  Created by Amit Garg on 20/02/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class SuccessViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Complete"

        self.navigationItem.leftBarButtonItem = nil
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.hidden = false
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.hidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func getStartedBtnAction(sender: AnyObject) {
        let sWRevealViewControllerObj = amitStoryBoard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
        
        Utility.saveStringInDefault(SIGN_UP_STATUS, value: "SWRevealViewController")

        ROOT_NAVVIGATION_CONROLLER.topViewController?.navigationController!.pushViewController(sWRevealViewControllerObj, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
