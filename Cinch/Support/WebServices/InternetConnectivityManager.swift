import UIKit

class InternetConnectivityManager : NSObject{
    //Class signleton object
    static var internetErrorAlert = UIAlertView(title: "No Internet Connection", message: MSG_NETWORK_FAILURE , delegate: InternetConnectivityManager.sharedManager, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
    static var isAlertShown = false
    
    static var _sharedManager : InternetConnectivityManager?
    static var sharedManager: InternetConnectivityManager!{
        get{
            if _sharedManager == nil{
                _sharedManager = InternetConnectivityManager()
            }
            
            return _sharedManager
        }
    }
    
    //Class function
    class func isInternetAvailable() -> Bool{
        if AFNetworkReachabilityManager.sharedManager().networkReachabilityStatus == AFNetworkReachabilityStatus.NotReachable {
            return false
        }else{
            return true
        }
    }
    
    override init() {
        super.init()
    }
    
    class func AFNetworkRequestFromNotification(notification : NSNotification) -> NSURLRequest? {
        var request : NSURLRequest? = nil
        
        if notification.object != nil && notification.object!.respondsToSelector(Selector("originalRequest")){
            request = (notification.object! as! NSURLSessionTask).originalRequest
        }else if notification.object != nil && notification.object!.respondsToSelector(Selector("request")){
            request = (notification.object! as! AFURLConnectionOperation).request
        }
        
        return request
    }
    
    //Instance function
    func startMonitoring() {
        stopMonitoring()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("networkRequestDidStart:"), name: AFNetworkingOperationDidStartNotification, object: nil)
    }
    
    func stopMonitoring() {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func networkRequestDidStart(notification : NSNotification) {
//        let request : NSURLRequest? = InternetConnectivityManager.AFNetworkRequestFromNotification(notification);
//        
//        if request == nil {
//            return;
//        }
        
        
        if !InternetConnectivityManager.isInternetAvailable() {
            if !InternetConnectivityManager.isAlertShown {
                InternetConnectivityManager.internetErrorAlert.show()
                InternetConnectivityManager.isAlertShown = true
            }
            
        }
    }
    
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if buttonIndex == 0 {
            InternetConnectivityManager.isAlertShown = false
        }
    }
    
    static func checkForServerError( error : NSError) {
        var localizedErrorString : String?
        switch error.code{
        case -1003, -1001, -1011, -1020:
            localizedErrorString = "Looks like we have encountered a problem. Please try again."
        case -1009, -1005:
            localizedErrorString = "Seems you are offline. Please check your network connection."
        case -1018:
            localizedErrorString = "Looks like you are Roaming. Please check your network connection."
        default:
            if error.code == 3840 {
                localizedErrorString = "Oops, something went wrong. Please try after sometime."
            }
        }
        
        
        let serverErrorAlert = UIAlertView(title: "Error", message: localizedErrorString , delegate: InternetConnectivityManager.sharedManager, cancelButtonTitle: "Ok")
        
        if !InternetConnectivityManager.isAlertShown {
            serverErrorAlert.show()
            InternetConnectivityManager.isAlertShown = true
        }
    }
}
