//
//  LocationPickerViewController.swift
//  Cinch
//
//  Created by MONEY on 16/03/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

protocol AppleMapDelegate : NSObjectProtocol {
    func selectedUserAddress(address: String, location: CLLocationCoordinate2D)
}

class LocationPickerViewController: UIViewController {

    var lastLocation : CLLocationCoordinate2D?
    var appleMapView : AppleMapView!
    weak var appleMapDelegate : AppleMapDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        appleMapView = AppleMapView(frame: CGRectMake(0, 64, MAIN_SCREEN_WIDTH, MAIN_SCREEN_HEIGHT - 64), dragablePin: true, lastLocation: lastLocation)

        self.view.addSubview(appleMapView)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func closeBtnAction(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    @IBAction func doneBtnAction(sender: AnyObject) {

        if appleMapDelegate != nil {
            appleMapDelegate?.selectedUserAddress(appleMapView.completeAddress, location: appleMapView.sourceLocation)
        }
        
        self.dismissViewControllerAnimated(true, completion: nil)

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
