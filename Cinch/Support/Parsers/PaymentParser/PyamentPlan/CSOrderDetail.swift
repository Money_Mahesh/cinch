//
//	CSOrderDetail.swift
//
//	Create by Money Mahesh on 16/4/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class CSOrderDetail : NSObject, NSCoding{

	var data : OrderDetail!
	var errorCode : Int!
	var errorMsg : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		if let dataData = dictionary["data"] as? NSDictionary{
			data = OrderDetail(fromDictionary: dataData)
		}
		errorCode = dictionary["errorCode"] as? Int
		errorMsg = dictionary["errorMsg"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if data != nil{
			dictionary["data"] = data.toDictionary()
		}
		if errorCode != nil{
			dictionary["errorCode"] = errorCode
		}
		if errorMsg != nil{
			dictionary["errorMsg"] = errorMsg
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         data = aDecoder.decodeObjectForKey("data") as? OrderDetail
         errorCode = aDecoder.decodeObjectForKey("errorCode") as? Int
         errorMsg = aDecoder.decodeObjectForKey("errorMsg") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if data != nil{
			aCoder.encodeObject(data, forKey: "data")
		}
		if errorCode != nil{
			aCoder.encodeObject(errorCode, forKey: "errorCode")
		}
		if errorMsg != nil{
			aCoder.encodeObject(errorMsg, forKey: "errorMsg")
		}

	}

}