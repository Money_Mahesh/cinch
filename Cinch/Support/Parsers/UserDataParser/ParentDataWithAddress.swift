//
//	ParentDataWithAddress.swift
//
//	Create by Amit Garg on 8/3/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class ParentDataWithAddress{

	var address : String!
	var city : String!
	var country : String!
	var createTime : String!
	var dob : String!
	var email : String!
	var gender : String!
	var mobile : String!
	var name : String!
	var profilePic : String!
	var registerationIp : String!
	var role : String!
	var seq : String!
	var signaturePic : String!
	var state : String!
	var status : String!
	var updateTime : String!
	var zipcode : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
    
    init(fromPlayerDetail playerDetail: PlayerData){
        address = playerDetail.address
        city = playerDetail.city
        country = playerDetail.country
        createTime = ""
        dob = ""
        email = ""
        gender = ""
        mobile = ""
        name = ""
        profilePic = ""
        registerationIp = ""
        role = ""
        seq = ""
        signaturePic = ""
        state = playerDetail.state
        status = playerDetail.status
        updateTime = ""
        zipcode = playerDetail.zipcode
    }
    
	init(fromDictionary dictionary: NSDictionary){
		address = dictionary["address"] as? String
		city = dictionary["city"] as? String
		country = dictionary["country"] as? String
		createTime = dictionary["create_time"] as? String
		dob = dictionary["dob"] as? String
		email = dictionary["email"] as? String
		gender = dictionary["gender"] as? String
		mobile = dictionary["mobile"] as? String
		name = dictionary["name"] as? String
		profilePic = dictionary["profile_pic"] as? String
		registerationIp = dictionary["registeration_ip"] as? String
		role = dictionary["role"] as? String
		seq = dictionary["seq"] as? String
		signaturePic = dictionary["signature_pic"] as? String
		state = dictionary["state"] as? String
		status = dictionary["status"] as? String
		updateTime = dictionary["update_time"] as? String
		zipcode = dictionary["zipcode"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if address != nil{
			dictionary["address"] = address
		}
		if city != nil{
			dictionary["city"] = city
		}
		if country != nil{
			dictionary["country"] = country
		}
		if createTime != nil{
			dictionary["create_time"] = createTime
		}
		if dob != nil{
			dictionary["dob"] = dob
		}
		if email != nil{
			dictionary["email"] = email
		}
		if gender != nil{
			dictionary["gender"] = gender
		}
		if mobile != nil{
			dictionary["mobile"] = mobile
		}
		if name != nil{
			dictionary["name"] = name
		}
		if profilePic != nil{
			dictionary["profile_pic"] = profilePic
		}
		if registerationIp != nil{
			dictionary["registeration_ip"] = registerationIp
		}
		if role != nil{
			dictionary["role"] = role
		}
		if seq != nil{
			dictionary["seq"] = seq
		}
		if signaturePic != nil{
			dictionary["signature_pic"] = signaturePic
		}
		if state != nil{
			dictionary["state"] = state
		}
		if status != nil{
			dictionary["status"] = status
		}
		if updateTime != nil{
			dictionary["update_time"] = updateTime
		}
		if zipcode != nil{
			dictionary["zipcode"] = zipcode
		}
		return dictionary
	}

}