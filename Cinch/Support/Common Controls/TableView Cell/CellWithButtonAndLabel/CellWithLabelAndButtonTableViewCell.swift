//
//  CellWithLabelAndButtonTableViewCell.swift
//  Cinch
//
//  Created by Amit Garg on 21/02/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class CellWithLabelAndButtonTableViewCell: UITableViewCell {

    @IBOutlet weak var topSeperator: UIView!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bottomSeperator: UIView!
    @IBOutlet weak var accessoryImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = UITableViewCellSelectionStyle.None
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
