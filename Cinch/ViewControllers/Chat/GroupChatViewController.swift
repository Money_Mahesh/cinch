//
//  GroupChatViewController.swift
//  Cinch
//
//  Created by Amit Garg on 14/03/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class GroupChatViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var imageViewForGroupPic: UIImageView!
    @IBOutlet weak var groupNametextField: AGFloatLabelTextField!
    @IBOutlet weak var textFieldForName: UITextField!
    @IBOutlet weak var groupChatTableView: UITableView!
    @IBOutlet weak var groupHeadShotButton: UIButton!

    var groupheadShotImageUrl = String()
    var imagePicker = UIImagePickerController()
    var nameArray = [ChatPlayerData]()

    var noOfPersonSelected = 0
    
    // *** STEP 1: STORE FIREBASE REFERENCES
    var messagesRef: Firebase!
    var groupSeq: String!
    var groupMemberSeq = String()

    //    var sender : String?
    
    func setupFirebase() {
        // *** STEP 2: SETUP FIREBASE
        messagesRef = Firebase(url: "https://cinchsports.firebaseio.com/Chat-DB/group_chat_user")
        
        let groupRef = messagesRef.childByAppendingPath("G-\(self.groupSeq)")
        
        let users = ["users": groupMemberSeq]
        groupRef.setValue(users)
        
        
        let messageViewControllerObj = amitStoryBoard.instantiateViewControllerWithIdentifier("MessageViewController") as! MessageViewController
        
        messageViewControllerObj.senderDisplayName = self.groupNametextField.text!
        messageViewControllerObj.chatNodeID = "G-\(self.groupSeq)"
        messageViewControllerObj.receiverNode = "G-\(self.groupSeq)"
        messageViewControllerObj.receiverId = self.groupSeq!
        messageViewControllerObj.profilePicUrl = groupheadShotImageUrl

        let timeStamp = Int(NSDate().timeIntervalSince1970 * 1000)
        
        let messageDetail : [String : AnyObject!] = [
            "message":"",
            "timeStamp":timeStamp,
            "imageURL": self.groupheadShotImageUrl,
            "messageImg" : "",
        ]
        
        messageViewControllerObj.setFireBaseHistory(messageDetail, withImage: false)
        
        self.navigationController?.pushViewController(messageViewControllerObj, animated: true)
            
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "New group"
        
        imageViewForGroupPic.layer.cornerRadius = 45
        imageViewForGroupPic.clipsToBounds = true
        imagePicker.delegate = self
        
        Utility.setBackButton(forViewController: self, withTitle: "")
        Utility.setRightNavigationButton(forViewController: self, withImage: "+", withTitle:"")

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- TableView Data Source and Delegates
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView(frame: CGRectMake(0, 0, MAIN_SCREEN_WIDTH, 22))
        headerView.backgroundColor = UIColor(red: 247.0/255.0, green: 247.0/255.0, blue: 247.0/255.0, alpha: 1.0)
        let label = UILabel(frame: CGRectMake(16, 0, MAIN_SCREEN_WIDTH - 16.0, 22))
        label.text = "Title \(section)"
        label.font = UIFont(name: "Lato-Black", size: 12.0)
        label.textColor = UIColor.blackColor()
        
        headerView.addSubview(label)
        return headerView
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRectMake(0, 0, MAIN_SCREEN_WIDTH, 1))
        footerView.backgroundColor = UIColor(red: 247.0/255.0, green: 247.0/255.0, blue: 247.0/255.0, alpha: 1.0)
        
        return footerView
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.nameArray.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let groupChatCell = tableView.dequeueReusableCellWithIdentifier("GroupChatCell") as! GroupChatCell
        
        groupChatCell.labelForName.text = self.nameArray[indexPath.row].name
        groupChatCell.labelForStatus.hidden = true

        Utility.setImageWithURl(self.nameArray[indexPath.row].profilePic, inImageView: groupChatCell.imageViewForProfilePic)
        
//        if self.nameArray[indexPath.row].isSelected  {
            groupChatCell.btnForStatus.selected = self.nameArray[indexPath.row].isSelected
//        }
//        else {
//            groupChatCell.btnForStatus.selected = false
//        }
        
        return groupChatCell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        self.view.endEditing(true)

        if self.nameArray[indexPath.row].isSelected == true {
            self.nameArray[indexPath.row].isSelected = false
            
            noOfPersonSelected--
            self.textFieldForName.text = ""
            
            let filterArray = self.nameArray.filter( {
//                if $0.isSelected == true {
//                    return true
//                }
                return $0.isSelected
            })
            
            let count : Int = noOfPersonSelected < 2 ? noOfPersonSelected : 2
            var index = 0
            while (index < count) {
                self.textFieldForName.text! += "\((filterArray[index].name)!), "
                index++
            }
            
            if self.textFieldForName.text!.characters.count > 0 {
                self.textFieldForName.text! = String(self.textFieldForName.text!.characters.dropLast(2))
            }
            
            if noOfPersonSelected > 2 {
                self.textFieldForName.text! += " and \(noOfPersonSelected - 2) other"
            }
        }
        else {
            self.nameArray[indexPath.row].isSelected = true
            
            noOfPersonSelected++
            
            if noOfPersonSelected < 3 {
                self.textFieldForName.text! += ", \((self.nameArray[indexPath.row].name)!)"
            }
            else {
                if self.textFieldForName.text?.containsString(" and \(noOfPersonSelected - 3) other") == true {
                    self.textFieldForName.text?.replaceRange((self.textFieldForName.text?.rangeOfString(" and \(noOfPersonSelected - 3) other"))!, with: " and \(noOfPersonSelected - 2) other")
                }
                else {
                    self.textFieldForName.text! += " and \(noOfPersonSelected - 2) other"
                }
            }
            
            if self.textFieldForName.text!.characters.first == "," {
                self.textFieldForName.text! = String(self.textFieldForName.text!.characters.dropFirst(2))
            }
        }
        
        self.groupChatTableView.reloadData()
    }

    //MARK:- IBAction
    func backAction() {
        self.view.endEditing(true)

        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func rightBtnAction() {
        
        self.view.endEditing(true)
        
        if groupNametextField.text?.characters.count == 0 {
            Utility.showAlert(nil, message: "Please enter the group name", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
        }
        else if textFieldForName.text?.characters.count == 0 {
            Utility.showAlert(nil, message: "Please select player", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
        }
        else {
            self.hitCreateGroupService()
        }
    }
    
    @IBAction func groupHeadshotTapped(sender: AnyObject) {
        self.view.endEditing(true)
        
        self.showActionSheet()
    }
    
    //MARK:- Image Picker method
    func showActionSheet() {
        
        let actionSheet = UIAlertController(title: "Player Headshot", message: "", preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        let imageGalleryAction = UIAlertAction(title: "Image Gallery", style: UIAlertActionStyle.Default) { (action) -> Void in
            self.openImageGallary()
        }
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.Default) { (action) -> Void in
            self.openCamera()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil)
        
        actionSheet.addAction(imageGalleryAction)
        actionSheet.addAction(cameraAction)
        actionSheet.addAction(cancelAction)
        
        self.presentViewController(actionSheet, animated: true, completion: nil)
    }
    
    func openImageGallary() {
        imagePicker.sourceType = .PhotoLibrary
        self.presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    func openCamera() {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
            imagePicker.sourceType = .Camera
            self.presentViewController(imagePicker, animated: true, completion: nil)
        }
        else {
            let alertController = UIAlertController(title: "Oops!!", message: "Camera is not available", preferredStyle: UIAlertControllerStyle.Alert)
            let cancelAction = UIAlertAction(title: ALERT_OK_BUTTON_TITLE, style: UIAlertActionStyle.Cancel, handler: nil)
            
            alertController.addAction(cancelAction)
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    //MARK:- Image Picker Delegate
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        if let pickerImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            print(pickerImage)
            let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
            activityIndicator.frame.size = self.groupHeadShotButton.frame.size
            
            activityIndicator.startAnimating()
            self.groupHeadShotButton.addSubview(activityIndicator)
            
            Utility.uploadImageToserver(pickerImage, resetImageViewBlock: {(selectedImage: UIImage?, headShotImageUrl: String?) in
                
                activityIndicator.removeFromSuperview()
                if selectedImage != nil {
                    
                    self.groupheadShotImageUrl = headShotImageUrl == nil ? "" : headShotImageUrl!
                    self.imageViewForGroupPic.image = selectedImage
                }
                else {
                }
            })
            
            self.groupHeadShotButton.layer.cornerRadius = 45.0
            self.groupHeadShotButton.clipsToBounds = true
        }
        
        self.imagePicker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.imagePicker.dismissViewControllerAnimated(true, completion: nil)
    }

    //MARK: - Web Service hit
    func hitCreateGroupService () {
        
        CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Creating group...")
        
        let createGroupServiceObj = CommonServiceClass(httpRequestType: HTTP_REQUEST_TYPE.POST_REQUEST, requestTag: "CREATE_GROUP_SERVICE", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
        
        createGroupServiceObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
            
            if errorMessage != nil
            {
                //print(response)
            }
            
            print(response as! NSDictionary)
            
            let responseDict = CSCreateGroup(fromDictionary: response as! NSDictionary)
            
            if responseDict.errorCode == 1 {
                
                Utility.showAlert(ALERT_TITLE, message: responseDict.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            }
            else {
                
                print(responseDict.data)
                
                self.groupSeq = responseDict.data.groupSeq
                self.groupMemberSeq = ""

                for chatPlayerData in self.nameArray {
                    
                    if chatPlayerData.isSelected == true {
                        self.groupMemberSeq += "\((chatPlayerData.userSeq)!),"
                    }
                    
                }
                
                if self.groupMemberSeq.characters.count != 0 {
                    self.groupMemberSeq = String(self.groupMemberSeq.characters.dropLast(1))
                }
                
                self.setupFirebase()
                
            }
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
            
        }
        createGroupServiceObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
            print("error ------------------------------\(error)")
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
            
        }
                
        var parameterDict = Dictionary <String, String>()
        parameterDict["cmd"] = "createChatGroup"
        parameterDict["g_name"] = groupNametextField.text
        parameterDict["img"] = groupheadShotImageUrl
        parameterDict["seq"] = UserProfileManager.userSequence

        createGroupServiceObj.commonServiceHit(parameterDict)
    }

}

class GroupChatCell : UITableViewCell {
    
    @IBOutlet weak var imageViewForProfilePic: UIImageView!
    @IBOutlet weak var labelForName: UILabel!
    @IBOutlet weak var labelForStatus: UILabel!
    @IBOutlet weak var btnForStatus: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .None
    }
}