//
//  OneButtonTableViewCell.swift
//  Cinch
//
//  Created by Amit Garg on 20/03/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class OneButtonTableViewCell: UITableViewCell {

    @IBOutlet weak var cellButton: CSCustomButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func cellButtonTapped(sender: AnyObject) {
        
        
    }
}
