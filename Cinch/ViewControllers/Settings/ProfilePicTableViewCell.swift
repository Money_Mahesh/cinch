//
//  ProfilePicTableViewCell.swift
//  Cinch
//
//  Created by Amit Garg on 20/03/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class ProfilePicTableViewCell: UITableViewCell {
    
    var mainView : UIView!
    @IBOutlet weak var profileImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setUpView()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setUpView() {
        
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "ProfilePicTableViewCell", bundle: bundle)
        mainView = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        
        mainView.frame = self.bounds
        mainView.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        
        self.profileImageView.layer.cornerRadius = 50
        self.profileImageView.clipsToBounds = true
        
        self.addSubview(mainView)
    }
    
    func bindCellWithData(imageUrl: String) {
        
        
    }
    
}
