//
//  FacebookClass.swift
//  Cinch
//
//  Created by MONEY on 23/02/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import Foundation

class FacebookClass {
    
    
    class func loginToFacebookWithSuccess(currentViewController: UIViewController, successBlock: () -> (), andFailure failureBlock: (NSError?) -> ()) {
        
        let facebookReadPermissions = ["public_profile", "email", "user_friends"]

        if FBSDKAccessToken.currentAccessToken() != nil {
            //For debugging, when we want to ensure that facebook login always happens
            //FBSDKLoginManager().logOut()
            //Otherwise do:

            successBlock()
            
            return
        }
        else {
            FBSDKLoginManager().logOut()
            
            UserProfileManager.logOut()
        }
        
        FBSDKLoginManager().logInWithReadPermissions(facebookReadPermissions, fromViewController: currentViewController, handler: { (result: FBSDKLoginManagerLoginResult!, error: NSError!) -> Void in
            
            if error != nil {
                //According to Facebook:
                //Errors will rarely occur in the typical login flow because the login dialog
                //presented by Facebook via single sign on will guide the users to resolve any errors.
                
                // Process error
                FBSDKLoginManager().logOut()
                UserProfileManager.logOut()
                
                failureBlock(error)
                
            } else if result.isCancelled {
                // Handle cancellations
                FBSDKLoginManager().logOut()
                UserProfileManager.logOut()

                failureBlock(nil)
                
            } else {
                // If you ask for multiple permissions at once, you
                // should check if specific permissions missing
                var allPermsGranted = true
                
                //result.grantedPermissions returns an array of _NSCFString pointers
                let grantedPermissions = Array(result.grantedPermissions)//.allObjects.map( {"\($0)"} )
                for permission in facebookReadPermissions {
                    if !grantedPermissions.contains(permission) {
                        allPermsGranted = false
                        break
                    }
                }
                if allPermsGranted {
                    // Do work
                    let fbToken = result.token.tokenString
                    let fbUserID = result.token.userID
                    
                    
                    let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath:  "\(fbUserID)?fields=id,name,email,gender", parameters: nil)
                    graphRequest.startWithCompletionHandler({ (connection, result, error) -> Void in
                        
                        if ((error) != nil)
                        {
                            // Process error
                            print("Error: \(error)")
                            UserProfileManager.logOut()
                            failureBlock(nil)


                        }
                        else
                        {
                            UserProfileManager.name = result.valueForKey("name") as? String
                            UserProfileManager.emailId = result.valueForKey("email") as? String
                            UserProfileManager.gender = result.valueForKey("gender") as? String
                            UserProfileManager.imageUrl = "http://graph.facebook.com/" + fbUserID! + "/picture?type=large"
                            
                            UserProfileManager.fbID = fbUserID
                            UserProfileManager.fbToken = fbToken
                            
                            successBlock()

                        }
                    })
                } else {
                    //The user did not grant all permissions requested
                    //Discover which permissions are granted
                    //and if you can live without the declined ones
                    
                    Utility.showAlert("Oops!", message: "Please grant facebook permission to Cinch App. So, it can access your facebook account", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
                    UserProfileManager.logOut()

                    failureBlock(nil)
                }
            }
        })
        
    }
}