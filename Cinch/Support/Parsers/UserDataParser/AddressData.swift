//
//	Data.swift
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class AddressData : NSObject, NSCoding{

	var fatherAdd : FatherAdd!
	var guardianAdd : FatherAdd!
	var motherAdd : FatherAdd!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		if let fatherAddData = dictionary["father_add"] as? NSDictionary{
			fatherAdd = FatherAdd(fromDictionary: fatherAddData)
		}
        if let guardianAddData = dictionary["guardian_add"] as? NSDictionary{
            guardianAdd = FatherAdd(fromDictionary: guardianAddData)
        }
		if let motherAddData = dictionary["mother_add"] as? NSDictionary{
			motherAdd = FatherAdd(fromDictionary: motherAddData)
		}
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if fatherAdd != nil{
			dictionary["father_add"] = fatherAdd.toDictionary()
		}
		if guardianAdd != nil{
			dictionary["guardian_add"] = guardianAdd.toDictionary()
		}
		if motherAdd != nil{
			dictionary["mother_add"] = motherAdd.toDictionary()
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         fatherAdd = aDecoder.decodeObjectForKey("father_add") as? FatherAdd
         guardianAdd = aDecoder.decodeObjectForKey("guardian_add") as? FatherAdd
         motherAdd = aDecoder.decodeObjectForKey("mother_add") as? FatherAdd

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if fatherAdd != nil{
			aCoder.encodeObject(fatherAdd, forKey: "father_add")
		}
		if guardianAdd != nil{
			aCoder.encodeObject(guardianAdd, forKey: "guardian_add")
		}
		if motherAdd != nil{
			aCoder.encodeObject(motherAdd, forKey: "mother_add")
		}

	}

}