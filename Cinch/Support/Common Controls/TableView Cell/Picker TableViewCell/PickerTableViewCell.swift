//
//  PickerTableViewCell.swift
//  TableViewWithPicker
//
//  Created by MONEY on 27/10/15.
//  Copyright © 2015 MONEY. All rights reserved.
//

import UIKit

@objc
protocol PickerCellDelegate: NSObjectProtocol {
    optional func pickerDoneBtnAction(selectedIndex: [Int])
}

class PickerTableViewCell: UITableViewCell, PickerViewDelegate {

    let cellHeight = CGFloat(50)
    
    var label : UILabel!
    var dropDownArrowImage : UIImageView!

    var pickerContentArray : [[String]]!
    var pickerComponentTitleArray: [String]?
    
    var selectedRowIndexArray : [Int]?

    var selectedIndex = [Int]()
    
    var customPickerViewObj : CustomPickerView?
    
    weak var cellDelegate: PickerCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    convenience init(style: UITableViewCellStyle, reuseIdentifier: String?, componentTitleArray: [String]?, arrayTobeLoadedInPicker array : [[String]], selectedRowIndexs : [Int]?, customPickerViewObj: CustomPickerView, placeHolderValue : String, mainViewDelegate: PickerCellDelegate, withSupplementaryImage supplementaryImage: String) {
        self.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = UITableViewCellSelectionStyle.None
        
        pickerContentArray = array
        pickerComponentTitleArray = componentTitleArray
        self.customPickerViewObj = customPickerViewObj
        customPickerViewObj.setUpCustomPickerView()
        self.cellDelegate = mainViewDelegate
        
        if selectedRowIndexs == nil {
            self.selectedRowIndexArray = nil
        }
        else {
            self.selectedRowIndexArray = selectedRowIndexs
        }
        
        label = UILabel(frame: CGRectMake(15, 0, UIScreen.mainScreen().bounds.width, cellHeight-1))
        dropDownArrowImage = UIImageView(frame: CGRectMake(UIScreen.mainScreen().bounds.width - CGFloat(23), (cellHeight - 9) / 2, 6, 9))
        
        label.font = UIFont(name: "Lato-Black", size: 11)
        label.textColor = UIColor(red: 119.0/255.0, green: 119.0/255.0, blue: 119.0/255.0, alpha: 1.0)
        label.textAlignment = .Left
        label.text = placeHolderValue
        self.addSubview(label)

        dropDownArrowImage.image = UIImage(named: supplementaryImage)
        dropDownArrowImage.contentMode = UIViewContentMode.Center
        self.addSubview(dropDownArrowImage)
        
        //Table Footer View
        let cellSeperatorView = UIView(frame: CGRectMake(0, cellHeight-1, MAIN_SCREEN_WIDTH, 1))
        cellSeperatorView.backgroundColor = UIColor(red: 238.0/255.0, green: 238.0/255.0, blue: 238.0/255.0, alpha: 1.0)
        self.addSubview(cellSeperatorView)
        
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    
//    func createCellWithData(componentTitleArray: [String]?, arrayTobeLoadedInPicker array : [[String]], selectedRowIndexs : [Int]?) -> UITableViewCell {
//        
//        
//    }

    
    func setPickerViewValue() {
        customPickerViewObj!.delegate = self
        customPickerViewObj!.loadPickerViewForSelected(pickerContentArray, row: self.selectedRowIndexArray, withTitle: pickerComponentTitleArray)
    }
    
    func pickerViewSelected(rowArray: [Int]) {
        
        self.selectedRowIndexArray = rowArray
        
        if self.cellDelegate != nil {
            self.cellDelegate?.pickerDoneBtnAction!(rowArray)
        }
        
        for index in 0..<rowArray.count {
            label.text = (pickerContentArray[index][rowArray[index]])
            print("Selected Element \(pickerContentArray[index][rowArray[index]])")
        }
    }
}
