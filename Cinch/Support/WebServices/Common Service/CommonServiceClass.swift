//
//  CommonServiceClass.swift
//  Cinch
//
//  Created by MONEY on 11/03/16.
//  Copyright © 2016 MONEY. All rights reserved.
//


import UIKit

class CommonServiceClass: ServerConnect {
    
    func commonServiceHit(parameters: Dictionary<String, String>?)->Void{
        
        RegisterationService.loadCookies()

        ServerConnect.requestOperationManager.POST("https://www.cinchsports.com/user/services_v2/processRequest", parameters: parameters, success: {(operation : AFHTTPRequestOperation! , responseObject: AnyObject!) in
            
            let serverResponse = self.processAfterMakingRequestWithParameters(responseObject)
            
            self.completionBlockWithSuccess?(statusCode: 0, requestType: self.requestTag, response: serverResponse, errorMessage: nil);
            
            },
            failure: {(operation: AFHTTPRequestOperation?, error:NSError) in
                
                if error.code == -1009 || error.code == -1005 {
                    InternetConnectivityManager.internetErrorAlert.show()
                }
                self.completionBlockWithFailure?(requestType: self.requestTag, error: error)
                
            }
        )
    }
    
    func weatherHit(parameters: Dictionary<String, String>?)->Void{
        
        ServerConnect.requestOperationManager.GET("http://api.openweathermap.org/data/2.5/weather?", parameters: parameters, success: {(operation : AFHTTPRequestOperation! , responseObject: AnyObject!) in
            
            let serverResponse = self.processAfterMakingRequestWithParameters(responseObject)
            
            self.completionBlockWithSuccess?(statusCode: 0, requestType: self.requestTag, response: serverResponse, errorMessage: nil);
            
            
            },
            failure: {(operation: AFHTTPRequestOperation?, error:NSError) in
                
                if error.code == -1009 || error.code == -1005 {
                    InternetConnectivityManager.internetErrorAlert.show()
                }
                
                self.completionBlockWithFailure?(requestType: self.requestTag, error: error)
                
            }
        )
    }
}