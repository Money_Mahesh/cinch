//
//	PaymentMethod.swift
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class PaymentMethod : NSObject, NSCoding{

	var cardImage : String!
	var cardType : String!
	var custName : AnyObject!
	var customerId : String!
	var expireMonth : String!
	var expireYear : String!
	var fingerprint : String!
	var isDefault : String!
	var last4 : String!
	var paymentSeq : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		cardImage = dictionary["card_image"] as? String
		cardType = dictionary["card_type"] as? String
		custName = dictionary["cust_name"]
		customerId = dictionary["customer_Id"] as? String
		expireMonth = dictionary["expire_month"] as? String
		expireYear = dictionary["expire_year"] as? String
		fingerprint = dictionary["fingerprint"] as? String
		isDefault = dictionary["is_default"] as? String
		last4 = dictionary["last4"] as? String
		paymentSeq = dictionary["payment_seq"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if cardImage != nil{
			dictionary["card_image"] = cardImage
		}
		if cardType != nil{
			dictionary["card_type"] = cardType
		}
		if custName != nil{
			dictionary["cust_name"] = custName
		}
		if customerId != nil{
			dictionary["customer_Id"] = customerId
		}
		if expireMonth != nil{
			dictionary["expire_month"] = expireMonth
		}
		if expireYear != nil{
			dictionary["expire_year"] = expireYear
		}
		if fingerprint != nil{
			dictionary["fingerprint"] = fingerprint
		}
		if isDefault != nil{
			dictionary["is_default"] = isDefault
		}
		if last4 != nil{
			dictionary["last4"] = last4
		}
		if paymentSeq != nil{
			dictionary["payment_seq"] = paymentSeq
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         cardImage = aDecoder.decodeObjectForKey("card_image") as? String
         cardType = aDecoder.decodeObjectForKey("card_type") as? String
         custName = aDecoder.decodeObjectForKey("cust_name")
         customerId = aDecoder.decodeObjectForKey("customer_Id") as? String
         expireMonth = aDecoder.decodeObjectForKey("expire_month") as? String
         expireYear = aDecoder.decodeObjectForKey("expire_year") as? String
         fingerprint = aDecoder.decodeObjectForKey("fingerprint") as? String
         isDefault = aDecoder.decodeObjectForKey("is_default") as? String
         last4 = aDecoder.decodeObjectForKey("last4") as? String
         paymentSeq = aDecoder.decodeObjectForKey("payment_seq") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if cardImage != nil{
			aCoder.encodeObject(cardImage, forKey: "card_image")
		}
		if cardType != nil{
			aCoder.encodeObject(cardType, forKey: "card_type")
		}
		if custName != nil{
			aCoder.encodeObject(custName, forKey: "cust_name")
		}
		if customerId != nil{
			aCoder.encodeObject(customerId, forKey: "customer_Id")
		}
		if expireMonth != nil{
			aCoder.encodeObject(expireMonth, forKey: "expire_month")
		}
		if expireYear != nil{
			aCoder.encodeObject(expireYear, forKey: "expire_year")
		}
		if fingerprint != nil{
			aCoder.encodeObject(fingerprint, forKey: "fingerprint")
		}
		if isDefault != nil{
			aCoder.encodeObject(isDefault, forKey: "is_default")
		}
		if last4 != nil{
			aCoder.encodeObject(last4, forKey: "last4")
		}
		if paymentSeq != nil{
			aCoder.encodeObject(paymentSeq, forKey: "payment_seq")
		}

	}

}