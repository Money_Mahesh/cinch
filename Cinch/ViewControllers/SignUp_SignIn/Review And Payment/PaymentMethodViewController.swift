//
//  PaymentMethodViewController.swift
//  Cinch
//
//  Created by MONEY on 09/03/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit
import Stripe

class PaymentMethodViewController: UIViewController, PickerViewDelegate, CardTextFieldDelegate {

    @IBOutlet weak var constraintForCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    
    let customPickerViewObj = CustomPickerView()
    let pickerContentArray = [
        ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"],
        ["2016"]
    ]
    
    var cardList = NSMutableArray()
    var customerId = String()
    var cardType = String()
    
    var expMonth = Int()
    var expYear = Int()
    var isComingFrom = ComingFrom.SignUp
    var selectedRowIndexArray = [0,0]
    weak var delegate : UpdateCellDelegate?

    @IBOutlet weak var textFieldForUserName: UITextField!
    @IBOutlet weak var textfieldForCardNo: CardTextField!
    @IBOutlet weak var textFieldForExpiryDate: UITextField!
    @IBOutlet weak var textfieldForCVC: UITextField!
    @IBOutlet weak var imageViewForCard: UIImageView!
    @IBOutlet weak var textfieldForCardholderName: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Payment Method"
        Utility.setBackButton(forViewController: self, withTitle: "")
        
        textFieldForUserName.text = UserProfileManager.name
        
        textfieldForCardNo.cardfieldDelegate = self
        
        if isComingFrom == ComingFrom.Dashboard || isComingFrom == ComingFrom.AddProgramSideMenu || isComingFrom == ComingFrom.AddPlayerSideMenu {
            customPickerViewObj.isParentNavigationBarPresent = false
        }
        else {
            customPickerViewObj.isParentNavigationBarPresent = true
        }
        
        customPickerViewObj.setUpCustomPickerView()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if cardList.count == 0 {
            constraintForCollectionViewHeight.constant = 0
        }
        
        self.navigationController?.navigationBar.hidden = false
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
//        self.navigationController?.navigationBar.hidden = true
    }
    
    //MARK:- PickerView Methods
    func setPickerViewValue() {
        
        customPickerViewObj.delegate = self
        customPickerViewObj.loadPickerViewForSelected(pickerContentArray, row: self.selectedRowIndexArray, withTitle: nil)
    }
    
    func pickerViewSelected(rowArray: [Int]) {
        
        selectedRowIndexArray = rowArray
        
        expMonth = Int(pickerContentArray[0][rowArray[0]])!
        expYear = Int((Int(pickerContentArray[1][0]))! + rowArray[1])
        
        let lastDigits = String(String(expYear).characters.dropFirst(2))
        
        textFieldForExpiryDate.text = "\(expMonth) / \(lastDigits)"
        
        

        print("Selected Element \(textFieldForExpiryDate.text)")
    }

    //OTP Delegate 
    func cardTextFieldUpdate(completeString : String) {
        print(completeString)
        
        if completeString.characters.count == 16 {
            
            let sTPPaymentCardTextFieldObj = STPPaymentCardTextField()
            sTPPaymentCardTextFieldObj.cardParams.number = completeString
            
            imageViewForCard.image = sTPPaymentCardTextFieldObj.brandImage
            textfieldForCardholderName.text = sTPPaymentCardTextFieldObj.cardParams.name
            textfieldForCVC.text = sTPPaymentCardTextFieldObj.cardParams.cvc
        }
        else {
            imageViewForCard.image = nil
        }
    }
    
    //MARK:- Textfield Delegate
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        if textField == textfieldForCVC {
            if textField.text?.characters.count == 3 && string != ""{
                return false
            }
        }
        
        if textField.text?.characters.count == 0 && string == " " {
            return false
        }
        return true
    }
    
    //MARK:- IBActions
    func backAction() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func doneBtnAction(sender: AnyObject) {
    
        self.view.endEditing(true)

        if textFieldForUserName.text!.characters.count == 0 {
            Utility.showAlert(nil, message: "Please enter you full name", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
        }
        else if textfieldForCardNo.text!.characters.count == 0 {
            Utility.showAlert(nil, message: "Please enter the card no", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
        }
        else if textFieldForExpiryDate.text!.characters.count == 0 {
            Utility.showAlert(nil, message: "Please enter the expiry date", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
        }
        else if textfieldForCVC.text!.characters.count == 0 {
            Utility.showAlert(nil, message: "Please enter CVV", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
        }
        else if textfieldForCardholderName.text!.characters.count == 0 {
            Utility.showAlert(nil, message: "Please enter cardholder name", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
        }
        else {
            if delegate != nil {
                
                let cardDetail = NSMutableDictionary()
                cardDetail.setObject(textfieldForCardholderName.text!, forKey: "CardHolderName")
                cardDetail.setObject(textfieldForCardNo.text!, forKey: "CardNumber")
                cardDetail.setObject(expMonth, forKey: "ExpiryMonth")
                cardDetail.setObject(expYear, forKey: "ExpiryYear")
                cardDetail.setObject(textfieldForCVC.text!, forKey: "CVC")
                cardDetail.setObject(customerId, forKey: "CustomerId")
                cardDetail.setObject(cardType, forKey: "CardType")
                
                delegate?.updateCell(CellToBeUpdate.PaymentMethod, withObject: cardDetail)
            }
            
            self.navigationController?.popViewControllerAnimated(true)
        }
    }
    
    @IBAction func selectExpDate(sender: AnyObject) {
        self.view.endEditing(true)

        self.setPickerViewValue()
    }
    
    //MARK: - CollectionView DataSource and Delegate
 
    func collectionView(collectionView : UICollectionView,layout collectionViewLayout:UICollectionViewLayout,insetForSectionAtIndex section:NSInteger) -> UIEdgeInsets {
        return UIEdgeInsetsMake(2, 5, 2, 0);
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cardList.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cardCell = collectionView.dequeueReusableCellWithReuseIdentifier("CardCell", forIndexPath: indexPath)
        
        let cardImg = cardCell.viewWithTag(101) as! UIImageView
        
        let imageUrlString = "http://\(((cardList.objectAtIndex(indexPath.row) as! CardDetail).card_image == nil ? "" : (cardList.objectAtIndex(indexPath.row) as! CardDetail).card_image!))"
        cardImg.image = nil
        
        if NSURL(string: imageUrlString) != nil {
            
            let image_url = NSURL(string: imageUrlString)
            let url_request = NSURLRequest(URL: image_url!)
            let placeholder = UIImage(named:"")
            //cell.activityIndicator.startAnimating()
            cardImg.image = nil
            cardImg.setImageWithURLRequest(url_request, placeholderImage: placeholder,
                success: { [weak cardImg] (request: NSURLRequest, response NSHTTPURLResponse, image: UIImage) -> Void in
                    
                    cardImg!.image = image
                    cardImg!.contentMode = UIViewContentMode.ScaleToFill
                    
                },
                failure: {[weak cardImg] (request: NSURLRequest, response: NSHTTPURLResponse?, error: NSError) -> Void in
                    cardImg!.image = UIImage(named: "")
                    cardImg!.contentMode = UIViewContentMode.Center
                })
        }
        
        return cardCell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        let cardDetail = (cardList.objectAtIndex(indexPath.row) as! CardDetail)
        let cellCardImage = (collectionView.cellForItemAtIndexPath(indexPath)!.viewWithTag(101) as! UIImageView).image
        customerId = cardDetail.customerId
        textfieldForCVC.text = "***"
        textFieldForExpiryDate.text = "\(cardDetail.expire_month) / \(cardDetail.expire_year)"
        textfieldForCardNo.text = "xxxx xxxx xxxx \(cardDetail.last4)"
        textfieldForCardholderName.text = cardDetail.custName
        imageViewForCard.image = cellCardImage
        cardType = cardDetail.cardType
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
