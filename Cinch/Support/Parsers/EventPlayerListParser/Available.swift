//
//	Available.swift
//
//	Create by Money Mahesh on 26/3/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class Available : NSObject, NSCoding{

	var dob : String!
	var dobCert : String!
	var eventName : String!
	var eventSeq : String!
	var fatherSeq : String!
	var fname : String!
	var gender : String!
	var guardianSeq : String!
	var insuranceCert : String!
	var licenceCert : String!
	var lname : String!
	var mobile : String!
	var motherSeq : String!
	var playerSeq : String!
	var profilePic : String!
	var programSeq : String!
	var status : String!
	var teamName : String!
	var teamSeq : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		dob = dictionary["dob"] as? String
		dobCert = dictionary["dob_cert"] as? String
		eventName = dictionary["event_name"] as? String
		eventSeq = dictionary["event_seq"] as? String
		fatherSeq = dictionary["father_seq"] as? String
		fname = dictionary["fname"] as? String
		gender = dictionary["gender"] as? String
		guardianSeq = dictionary["guardian_seq"] as? String
		insuranceCert = dictionary["insurance_cert"] as? String
		licenceCert = dictionary["licence_cert"] as? String
		lname = dictionary["lname"] as? String
		mobile = dictionary["mobile"] as? String
		motherSeq = dictionary["mother_seq"] as? String
		playerSeq = dictionary["player_seq"] as? String
		profilePic = dictionary["profile_pic"] as? String
		programSeq = dictionary["program_seq"] as? String
		status = dictionary["status"] as? String
		teamName = dictionary["team_name"] as? String
		teamSeq = dictionary["team_seq"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if dob != nil{
			dictionary["dob"] = dob
		}
		if dobCert != nil{
			dictionary["dob_cert"] = dobCert
		}
		if eventName != nil{
			dictionary["event_name"] = eventName
		}
		if eventSeq != nil{
			dictionary["event_seq"] = eventSeq
		}
		if fatherSeq != nil{
			dictionary["father_seq"] = fatherSeq
		}
		if fname != nil{
			dictionary["fname"] = fname
		}
		if gender != nil{
			dictionary["gender"] = gender
		}
		if guardianSeq != nil{
			dictionary["guardian_seq"] = guardianSeq
		}
		if insuranceCert != nil{
			dictionary["insurance_cert"] = insuranceCert
		}
		if licenceCert != nil{
			dictionary["licence_cert"] = licenceCert
		}
		if lname != nil{
			dictionary["lname"] = lname
		}
		if mobile != nil{
			dictionary["mobile"] = mobile
		}
		if motherSeq != nil{
			dictionary["mother_seq"] = motherSeq
		}
		if playerSeq != nil{
			dictionary["player_seq"] = playerSeq
		}
		if profilePic != nil{
			dictionary["profile_pic"] = profilePic
		}
		if programSeq != nil{
			dictionary["program_seq"] = programSeq
		}
		if status != nil{
			dictionary["status"] = status
		}
		if teamName != nil{
			dictionary["team_name"] = teamName
		}
		if teamSeq != nil{
			dictionary["team_seq"] = teamSeq
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         dob = aDecoder.decodeObjectForKey("dob") as? String
         dobCert = aDecoder.decodeObjectForKey("dob_cert") as? String
         eventName = aDecoder.decodeObjectForKey("event_name") as? String
         eventSeq = aDecoder.decodeObjectForKey("event_seq") as? String
         fatherSeq = aDecoder.decodeObjectForKey("father_seq") as? String
         fname = aDecoder.decodeObjectForKey("fname") as? String
         gender = aDecoder.decodeObjectForKey("gender") as? String
         guardianSeq = aDecoder.decodeObjectForKey("guardian_seq") as? String
         insuranceCert = aDecoder.decodeObjectForKey("insurance_cert") as? String
         licenceCert = aDecoder.decodeObjectForKey("licence_cert") as? String
         lname = aDecoder.decodeObjectForKey("lname") as? String
         mobile = aDecoder.decodeObjectForKey("mobile") as? String
         motherSeq = aDecoder.decodeObjectForKey("mother_seq") as? String
         playerSeq = aDecoder.decodeObjectForKey("player_seq") as? String
         profilePic = aDecoder.decodeObjectForKey("profile_pic") as? String
         programSeq = aDecoder.decodeObjectForKey("program_seq") as? String
         status = aDecoder.decodeObjectForKey("status") as? String
         teamName = aDecoder.decodeObjectForKey("team_name") as? String
         teamSeq = aDecoder.decodeObjectForKey("team_seq") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if dob != nil{
			aCoder.encodeObject(dob, forKey: "dob")
		}
		if dobCert != nil{
			aCoder.encodeObject(dobCert, forKey: "dob_cert")
		}
		if eventName != nil{
			aCoder.encodeObject(eventName, forKey: "event_name")
		}
		if eventSeq != nil{
			aCoder.encodeObject(eventSeq, forKey: "event_seq")
		}
		if fatherSeq != nil{
			aCoder.encodeObject(fatherSeq, forKey: "father_seq")
		}
		if fname != nil{
			aCoder.encodeObject(fname, forKey: "fname")
		}
		if gender != nil{
			aCoder.encodeObject(gender, forKey: "gender")
		}
		if guardianSeq != nil{
			aCoder.encodeObject(guardianSeq, forKey: "guardian_seq")
		}
		if insuranceCert != nil{
			aCoder.encodeObject(insuranceCert, forKey: "insurance_cert")
		}
		if licenceCert != nil{
			aCoder.encodeObject(licenceCert, forKey: "licence_cert")
		}
		if lname != nil{
			aCoder.encodeObject(lname, forKey: "lname")
		}
		if mobile != nil{
			aCoder.encodeObject(mobile, forKey: "mobile")
		}
		if motherSeq != nil{
			aCoder.encodeObject(motherSeq, forKey: "mother_seq")
		}
		if playerSeq != nil{
			aCoder.encodeObject(playerSeq, forKey: "player_seq")
		}
		if profilePic != nil{
			aCoder.encodeObject(profilePic, forKey: "profile_pic")
		}
		if programSeq != nil{
			aCoder.encodeObject(programSeq, forKey: "program_seq")
		}
		if status != nil{
			aCoder.encodeObject(status, forKey: "status")
		}
		if teamName != nil{
			aCoder.encodeObject(teamName, forKey: "team_name")
		}
		if teamSeq != nil{
			aCoder.encodeObject(teamSeq, forKey: "team_seq")
		}

	}

}