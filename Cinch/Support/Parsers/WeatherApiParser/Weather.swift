//
//	Weather.swift
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class Weather : NSObject, NSCoding{

	var descriptionField : String!
	var icon : String!
	var id : Int!
	var main : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		descriptionField = dictionary["description"] as? String
		icon = dictionary["icon"] as? String
		id = dictionary["id"] as? Int
		main = dictionary["main"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if descriptionField != nil{
			dictionary["description"] = descriptionField
		}
		if icon != nil{
			dictionary["icon"] = icon
		}
		if id != nil{
			dictionary["id"] = id
		}
		if main != nil{
			dictionary["main"] = main
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         descriptionField = aDecoder.decodeObjectForKey("description") as? String
         icon = aDecoder.decodeObjectForKey("icon") as? String
         id = aDecoder.decodeObjectForKey("id") as? Int
         main = aDecoder.decodeObjectForKey("main") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if descriptionField != nil{
			aCoder.encodeObject(descriptionField, forKey: "description")
		}
		if icon != nil{
			aCoder.encodeObject(icon, forKey: "icon")
		}
		if id != nil{
			aCoder.encodeObject(id, forKey: "id")
		}
		if main != nil{
			aCoder.encodeObject(main, forKey: "main")
		}

	}

}