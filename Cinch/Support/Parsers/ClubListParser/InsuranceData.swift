//
//	InsuranceData.swift
//
//	Create by Money Mahesh on 6/3/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class InsuranceData : NSObject, NSCoding{

	var affilateSeq : String!
	var clubSeq : String!
	var content : String!
	var createTime : String!
	var descriptionField : String!
	var isCustom : String!
	var programSeq : String!
	var status : String!
	var updateTime : String!
	var waiverSeq : String!
    var waiverImg : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		affilateSeq = dictionary["affilate_seq"] as? String
		clubSeq = dictionary["club_seq"] as? String
		content = dictionary["content"] as? String
		createTime = dictionary["create_time"] as? String
		descriptionField = dictionary["description"] as? String
		isCustom = dictionary["is_custom"] as? String
		programSeq = dictionary["program_seq"] as? String
		status = dictionary["status"] as? String
		updateTime = dictionary["update_time"] as? String
		waiverSeq = dictionary["waiver_seq"] as? String
        waiverImg = dictionary["waiver_image"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if affilateSeq != nil{
			dictionary["affilate_seq"] = affilateSeq
		}
		if clubSeq != nil{
			dictionary["club_seq"] = clubSeq
		}
		if content != nil{
			dictionary["content"] = content
		}
		if createTime != nil{
			dictionary["create_time"] = createTime
		}
		if descriptionField != nil{
			dictionary["description"] = descriptionField
		}
		if isCustom != nil{
			dictionary["is_custom"] = isCustom
		}
		if programSeq != nil{
			dictionary["program_seq"] = programSeq
		}
		if status != nil{
			dictionary["status"] = status
		}
		if updateTime != nil{
			dictionary["update_time"] = updateTime
		}
		if waiverSeq != nil{
			dictionary["waiver_seq"] = waiverSeq
		}
        if waiverImg != nil{
            dictionary["waiver_image"] = waiverImg
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         affilateSeq = aDecoder.decodeObjectForKey("affilate_seq") as? String
         clubSeq = aDecoder.decodeObjectForKey("club_seq") as? String
         content = aDecoder.decodeObjectForKey("content") as? String
         createTime = aDecoder.decodeObjectForKey("create_time") as? String
         descriptionField = aDecoder.decodeObjectForKey("description") as? String
         isCustom = aDecoder.decodeObjectForKey("is_custom") as? String
         programSeq = aDecoder.decodeObjectForKey("program_seq") as? String
         status = aDecoder.decodeObjectForKey("status") as? String
         updateTime = aDecoder.decodeObjectForKey("update_time") as? String
         waiverSeq = aDecoder.decodeObjectForKey("waiver_seq") as? String
         waiverImg = aDecoder.decodeObjectForKey("waiver_image") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if affilateSeq != nil{
			aCoder.encodeObject(affilateSeq, forKey: "affilate_seq")
		}
		if clubSeq != nil{
			aCoder.encodeObject(clubSeq, forKey: "club_seq")
		}
		if content != nil{
			aCoder.encodeObject(content, forKey: "content")
		}
		if createTime != nil{
			aCoder.encodeObject(createTime, forKey: "create_time")
		}
		if descriptionField != nil{
			aCoder.encodeObject(descriptionField, forKey: "description")
		}
		if isCustom != nil{
			aCoder.encodeObject(isCustom, forKey: "is_custom")
		}
		if programSeq != nil{
			aCoder.encodeObject(programSeq, forKey: "program_seq")
		}
		if status != nil{
			aCoder.encodeObject(status, forKey: "status")
		}
		if updateTime != nil{
			aCoder.encodeObject(updateTime, forKey: "update_time")
		}
		if waiverSeq != nil{
			aCoder.encodeObject(waiverSeq, forKey: "waiver_seq")
		}
        if waiverImg != nil{
            aCoder.encodeObject(waiverImg, forKey: "waiver_image")
        }
	}

}