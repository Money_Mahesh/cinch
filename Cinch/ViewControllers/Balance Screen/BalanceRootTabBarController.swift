//
//  BalanceRootTabBarController.swift
//  Cinch
//
//  Created by Amit Garg on 17/03/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class BalanceRootTabBarController: UITabBarController, UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBar.selectedItem?.imageInsets = UIEdgeInsets(top: 10, left: 0, bottom: -10, right: 0)
        
    }
    
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        
        for tabItem in tabBarController.tabBar.items! {
            tabItem.imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
        
        tabBarController.tabBar.selectedItem?.imageInsets = UIEdgeInsets(top: 10, left: 0, bottom: -10, right: 0)
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
