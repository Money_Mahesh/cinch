//
//  AddEventViewController.swift
//  Cinch
//
//  Created by MONEY on 28/02/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class AddEventViewController: UIViewController, CalendarDelegate, PickerViewDelegate, GooglePlacesAutocompleteDelegate {

    @IBOutlet weak var addEventTableView: UITableView!
    var tableComponentArray = [[UITableViewCell]]()
    var calendar : CustomCalendarView!
    var selectedRepeatDate = [String]()
    var uniformValue = "Home"
    
    let customPickerViewObj = CustomPickerView()
    var pickerContentArray : [[String]]!
    var selectedRowIndexArray = [[0],[0]]
    var selectedRowIndexPath : NSIndexPath!
    
    var selectedPickerNo = 0
    
    var managerArray = [ManagerDetail]()
    var selectedArriveEarly = String()
    
    var selectedLocationCoordinate : CLLocationCoordinate2D!

    override func viewDidLoad() {
        super.viewDidLoad()
        Utility.setBackButton(forViewController: self, withTitle: "")
        
        customPickerViewObj.isParentNavigationBarPresent = false
        customPickerViewObj.setUpCustomPickerView()

        self.title = "Add Event"

        self.hitGetManagerWebService()
        self.createTableComponent()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
//        self.navigationController?.navigationBar.hidden = false
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
//        self.navigationController?.navigationBar.hidden = true
    }
    
    func createTableComponent() {
        
        var sectionCell = [UITableViewCell]()
        let teamsCell = TableViewCellWithTitle(style: UITableViewCellStyle.Default, reuseIdentifier: "TeamTextfield")
        teamsCell.bindCellWithData("Teams", description: "", behaveAsPicker: true, withAccessoryImage: true, isPickerTypeDate : false)
        sectionCell.append(teamsCell)
        
        let nameCell = TableViewCellWithTitle(style: UITableViewCellStyle.Default, reuseIdentifier: "NameTextfield")
        nameCell.bindCellWithData("Enter Name", description: "", behaveAsPicker: false, withAccessoryImage: false, isPickerTypeDate : false)
        sectionCell.append(nameCell)
        
        let locationCell = TableViewCellWithTitle(style: UITableViewCellStyle.Default, reuseIdentifier: "LocationTextfield")
        locationCell.bindCellWithData("Location", description: "", behaveAsPicker: true, withAccessoryImage: false, isPickerTypeDate : false)
        sectionCell.append(locationCell)

        let locationDetail = TableViewCellWithTitle(style: UITableViewCellStyle.Default, reuseIdentifier: "LocationDetailTextfield")
        locationDetail.bindCellWithData("Location Detail", description: "", behaveAsPicker: false, withAccessoryImage: false, isPickerTypeDate : false)
        sectionCell.append(locationDetail)
        
        let eventTypeCell = TableCellWithCheckBox(style: UITableViewCellStyle.Default, reuseIdentifier: "EventTypeSelectorcell")
        eventTypeCell.bindCellWithTitle("Event Type", buttonTitle: ["Game", "Practice"])
        sectionCell.append(eventTypeCell)
        tableComponentArray.append(sectionCell)

        
        sectionCell = [UITableViewCell]()
        let startsCell = TableViewCellWithTitle(style: UITableViewCellStyle.Default, reuseIdentifier: "StartTextfield")
        startsCell.bindCellWithData("Starts", description: "", behaveAsPicker: true, withAccessoryImage: false, isPickerTypeDate : true)
        sectionCell.append(startsCell)

        let endsCell = TableViewCellWithTitle(style: UITableViewCellStyle.Default, reuseIdentifier: "EndTextfield")
        endsCell.bindCellWithData("Ends", description: "", behaveAsPicker: true, withAccessoryImage: false, isPickerTypeDate : true)
        sectionCell.append(endsCell)
        tableComponentArray.append(sectionCell)

        
        sectionCell = [UITableViewCell]()
        let repeatCell = TableViewCellWithTitle(style: UITableViewCellStyle.Default, reuseIdentifier: "RepeatTextfield")
        repeatCell.bindCellWithData("Repeat", description: "", behaveAsPicker: true, withAccessoryImage: true, isPickerTypeDate : false)
        sectionCell.append(repeatCell)
        
        let arriveEarlyCell = TableViewCellWithTitle(style: UITableViewCellStyle.Default, reuseIdentifier: "ArriveEarlyTextfield")
        arriveEarlyCell.bindCellWithData("Arrive Early", description: "", behaveAsPicker: true, withAccessoryImage: true, isPickerTypeDate : false)
        sectionCell.append(arriveEarlyCell)
        tableComponentArray.append(sectionCell)

        
        sectionCell = [UITableViewCell]()
        let uniformCell = TableCellWithCheckBox(style: UITableViewCellStyle.Default, reuseIdentifier: "uniformSelectorcell")
        uniformCell.bindCellWithTitle("Uniform", buttonTitle: ["Home", "Away"])
        sectionCell.append(uniformCell)
        
        let alertCell = TableViewCellWithTitle(style: UITableViewCellStyle.Default, reuseIdentifier: "AlertTextfield")
        alertCell.bindCellWithData("Alert", description: "", behaveAsPicker: false, withAccessoryImage: true, isPickerTypeDate : false)
        alertCell.textfieldForDescription.placeholder = "HH:MM"
        sectionCell.append(alertCell)
        tableComponentArray.append(sectionCell)
        
        addEventTableView.reloadData()

    }
    
    // MARK:- TableView Data Source and Delegates
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return tableComponentArray.count
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        }
        
        return 35
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView(frame: CGRectMake(0, 0, MAIN_SCREEN_WIDTH, 35))
        headerView.backgroundColor = UIColor(red: 247.0/255.0, green: 247.0/255.0, blue: 247.0/255.0, alpha: 1.0)
        
        return headerView
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 52
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        return tableComponentArray[section].count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        return tableComponentArray[indexPath.section][indexPath.row]
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        selectedRowIndexPath = indexPath
        
        if tableView.cellForRowAtIndexPath(indexPath)?.reuseIdentifier == "RepeatTextfield" {
            
            calendar = CustomCalendarView(frame: CGRectMake(0, 0, MAIN_SCREEN_WIDTH, MAIN_SCREEN_HEIGHT), selectedDates: [Date(date: NSDate())])
            calendar.delegate = self
            
            UIApplication.sharedApplication().keyWindow!.addSubview(calendar)
        }
        else if tableView.cellForRowAtIndexPath(indexPath)?.reuseIdentifier == "LocationTextfield" {
            
            let gpaViewController = GooglePlacesAutocomplete(
                apiKey: "AIzaSyDwh3zAqZ4BjqVfpO6muRz_wHu7pbIUtvU",
                placeType: .Geocode
            )
            
            gpaViewController.placeDelegate = self
            
            self.presentViewController(gpaViewController, animated: true, completion: nil)
            
        }
        else if tableView.cellForRowAtIndexPath(indexPath)?.reuseIdentifier == "ArriveEarlyTextfield" {
            
            selectedPickerNo = 1
            pickerContentArray = [
                ["15 min", "30 min", "45 min"]
            ]
            self.setPickerViewValue()
        }
        else if tableView.cellForRowAtIndexPath(indexPath)?.reuseIdentifier == "TeamTextfield" {
            
            if self.managerArray.count == 0 {
                Utility.showAlert(ALERT_TITLE, message: "No Team Available", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            }
            else {
                                
                var teamArray = [String]()
                for managerDetail in managerArray {
                    teamArray.append(managerDetail.teamName)
                }
                
                pickerContentArray = [teamArray]
                selectedPickerNo = 0
                self.setPickerViewValue()
            }
        }
        
    }
    
    //MARK: - IBActions
    func backAction() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func doneBtnAction(sender: AnyObject) {
        if self.checkValidation() == true {
            self.hitAddEventWebService()
        }
    }
    
    //MARK:- PickerView Methods
    func pickerViewSelected(rowArray: [Int]) {
        self.selectedRowIndexArray[selectedPickerNo] = rowArray
        (tableComponentArray[selectedRowIndexPath.section][selectedRowIndexPath.row] as! TableViewCellWithTitle).textfieldForDescription.text = pickerContentArray[0][self.selectedRowIndexArray[selectedPickerNo][0]]
        
        if selectedPickerNo == 1 {
            
            switch self.selectedRowIndexArray[selectedPickerNo][0] {
            case 1:
                selectedArriveEarly = "00:15"
            case 2:
                selectedArriveEarly = "00:15"
            default:
                selectedArriveEarly = "00:15"
            }
        }
    }
    
    func setPickerViewValue() {
        self.view.endEditing(true)
        
        customPickerViewObj.delegate = self
        customPickerViewObj.loadPickerViewForSelected(pickerContentArray, row: self.selectedRowIndexArray[selectedPickerNo], withTitle: nil)
    }

    //MARK:- Calendar Delegate
    func selectedDate(date: [Date]) {
        let cell = self.addEventTableView.cellForRowAtIndexPath(selectedRowIndexPath) as! TableViewCellWithTitle
        
        cell.textfieldForDescription.text = ""
        
        for dateElement in date {
            selectedRepeatDate.append("\(dateElement.year)-\(dateElement.month)-\(dateElement.day)")
        }
        
        var dateString = String()
        for tempDate in date {
            dateString += "\(tempDate.day)/\(tempDate.month), "
        }
        
        if dateString != "" {
            dateString = String(dateString.characters.dropLast(2))
        }
        
        cell.textfieldForDescription.text = dateString

        calendar.removeFromSuperview()
    }
    
    //MARK:- GooglePlacesAutocomplete Delegate
    func placeSelected(place: Place) {
        print(place.description)
        //modify this delegate
        

        let cell = (self.getCellWithIdentifier("LocationTextfield") as! TableViewCellWithTitle)
        
        let address = place.description
        cell.textfieldForDescription.text = address
        
        place.getDetails { (placeDetail: PlaceDetails) -> () in
            self.selectedLocationCoordinate = CLLocationCoordinate2D(latitude: placeDetail.latitude, longitude: placeDetail.longitude)
        }
        
        dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    func placeViewClosed() {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func checkValidation() -> Bool{
        for sectionCellArray in tableComponentArray {
            for cell in sectionCellArray {
                if cell.isKindOfClass(TableViewCellWithTitle) == true {
                    if (cell as! TableViewCellWithTitle).textfieldForDescription.text?.characters.count == 0 {
                        Utility.showAlert(ALERT_TITLE, message: "Please provide all the detail mention above to create the event", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
                        return false
                    }
                }
            }
        }
        return true
    }
    
    //MARK:- Hit Webservice
    func hitAddEventWebService() {
        
        CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Adding event...")
        
        let addEventObj = CommonServiceClass(httpRequestType: HTTP_REQUEST_TYPE.POST_REQUEST, requestTag: "ADD_EVENT", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
        
        addEventObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
            
            if errorMessage != nil
            {
                print(response)
            }
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)

            let addEventResponseObj =  OtpVerificationData(fromDictionary: response as! NSDictionary)
            
            if addEventResponseObj.errorCode == 0 {
                
                if addEventResponseObj.data == true {
                    self.navigationController?.popViewControllerAnimated(true)
                    
                    Utility.showAlert(ALERT_TITLE, message: "Event successfully created", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
                }
                else {
                    Utility.showAlert(ALERT_TITLE, message: "Provided details are not valid", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
                }
            } else {
                
                Utility.showAlert(ALERT_TITLE, message: addEventResponseObj.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            }
            
        }
        addEventObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
            
            print("error ------------------------------\(error)")
        }
        
        
        var parameterDict = Dictionary <String, String>()
        
        parameterDict["cmd"] = "addEvent"
        parameterDict["event_name"] = (self.getCellWithIdentifier("NameTextfield") as! TableViewCellWithTitle).textfieldForDescription.text
        parameterDict["team_seq"] = self.managerArray[selectedRowIndexArray[0][0]].teamSeq
        parameterDict["location_seq"] = "0"
        parameterDict["event_type"] = (self.getCellWithIdentifier("EventTypeSelectorcell") as! TableCellWithCheckBox).selectedBtn.title == "Game" ? "G" : "P"
        parameterDict["event_date"] = selectedRepeatDate.joinWithSeparator(",")//(self.getCellWithIdentifier("RepeatTextfield") as! TableViewCellWithTitle).textfieldForDescription.text
        parameterDict["start_time"] = (self.getCellWithIdentifier("StartTextfield") as! TableViewCellWithTitle).selectedDate
        parameterDict["end_time"] = (self.getCellWithIdentifier("EndTextfield") as! TableViewCellWithTitle).selectedDate
        parameterDict["status"] = "A"
        parameterDict["arrive_early"] = selectedArriveEarly
        parameterDict["uniform"] = (self.getCellWithIdentifier("uniformSelectorcell") as! TableCellWithCheckBox).selectedBtn.title == "Home" ? "H" : "A"
        parameterDict["alert_time"] = (self.getCellWithIdentifier("AlertTextfield") as! TableViewCellWithTitle).textfieldForDescription.text
        parameterDict["location_details"] = (self.getCellWithIdentifier("LocationDetailTextfield") as! TableViewCellWithTitle).textfieldForDescription.text
        parameterDict["location"] = (self.getCellWithIdentifier("LocationTextfield") as! TableViewCellWithTitle).textfieldForDescription.text
        parameterDict["lat"] = "\(self.selectedLocationCoordinate.latitude)"
        parameterDict["lng"] = "\(self.selectedLocationCoordinate.longitude)"

        print(parameterDict)
        addEventObj.commonServiceHit(parameterDict)
    }

    func hitGetManagerWebService() {
        
        CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Fetching manager list...")
        
        let managerObj = CommonServiceClass(httpRequestType: HTTP_REQUEST_TYPE.POST_REQUEST, requestTag: "GET_MANAGER", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
        
        managerObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
            
            if errorMessage != nil
            {
                print(response)
            }
            print(response as! NSDictionary)
            
            let managerListObj =  CGManager(fromDictionary: response as! NSDictionary)
            
            if managerListObj.errorCode == 0 {
                
                self.managerArray = managerListObj.data
            } else {
                
                Utility.showAlert(ALERT_TITLE, message: managerListObj.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            }
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
        }
        managerObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
            
            print("error ------------------------------\(error)")
        }
        
        var parameterDict = Dictionary <String, String>()
        parameterDict["cmd"] = "getManagerTeams"
        parameterDict["user_seq"] = UserProfileManager.userSequence
        
        managerObj.commonServiceHit(parameterDict)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func getCellWithIdentifier(cellIdentifier : String) -> UITableViewCell? {
        
        for sectionCellArray in tableComponentArray {
            for cell in sectionCellArray {
                if cell.reuseIdentifier == cellIdentifier {
                    return cell
                }
            }
        }
        return nil
    }

}
