//
//	EventPlayerList.swift
//
//	Create by Money Mahesh on 26/3/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class EventPlayerList : NSObject, NSCoding{

	var available : [Available]!
	var unavailable : [Available]!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		available = [Available]()
		if let availableArray = dictionary["available"] as? [NSDictionary]{
			for dic in availableArray{
				let value = Available(fromDictionary: dic)
				available.append(value)
			}
		}
		unavailable = [Available]()
		if let unavailableArray = dictionary["unavailable"] as? [NSDictionary]{
			for dic in unavailableArray{
				let value = Available(fromDictionary: dic)
				unavailable.append(value)
			}
		}
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if available != nil{
			var dictionaryElements = [NSDictionary]()
			for availableElement in available {
				dictionaryElements.append(availableElement.toDictionary())
			}
			dictionary["available"] = dictionaryElements
		}
		if unavailable != nil{
			var dictionaryElements = [NSDictionary]()
			for unavailableElement in unavailable {
				dictionaryElements.append(unavailableElement.toDictionary())
			}
			dictionary["unavailable"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         available = aDecoder.decodeObjectForKey("available") as? [Available]
         unavailable = aDecoder.decodeObjectForKey("unavailable") as? [Available]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if available != nil{
			aCoder.encodeObject(available, forKey: "available")
		}
		if unavailable != nil{
			aCoder.encodeObject(unavailable, forKey: "unavailable")
		}

	}

}