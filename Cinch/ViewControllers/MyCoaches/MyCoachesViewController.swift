//
//  MyCoachesViewController.swift
//  Cinch
//
//  Created by Amit Garg on 29/02/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class MyCoachesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "My Coaches"
        
        if revealViewController() != nil {
            Utility.setMenuButton(forViewController: self, revealController: revealViewController())
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }


        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        
       return 3
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return 75.0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let myCoachesCell = tableView.dequeueReusableCellWithIdentifier("myCoachesCell", forIndexPath: indexPath) as! MyCoachesTableViewCell
        myCoachesCell.selectionStyle = .None
        myCoachesCell.followButton.tag = indexPath.row
        myCoachesCell.followButton.addTarget(self, action: Selector("followButtonTapped:"), forControlEvents: .TouchUpInside)
        return myCoachesCell
    }
    
    // MARK: - Table view delegate
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        
    }
    
    func rightBtnAction () {
        
    }
    
    
    func followButtonTapped(sender:UIButton) {
        
        if sender.titleLabel?.text == "Follow" {
            
            sender.setTitle("Following", forState: .Normal)
            sender.backgroundColor = UIColor(red: 0, green: 159/255, blue: 232/255, alpha: 1.0)
            sender.setTitleColorForAllState(UIColor.whiteColor())
            sender.layer.borderColor = UIColor.clearColor().CGColor
            sender.layer.borderWidth = 0.0
            
        } else {
            
            sender.setTitle("Follow", forState: .Normal)
            sender.backgroundColor = UIColor.whiteColor()
            sender.layer.borderColor = UIColor(red: 151/255, green: 151/255, blue: 151/255, alpha: 1.0).CGColor
            sender.layer.borderWidth = 0.5
            sender.setTitleColorForAllState(UIColor(red: 0, green: 159/255, blue: 232/255, alpha: 1.0))
        }
        
        
    }

    //MARK: - Web Service hit
    func getCoachListService () {
        
        CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Fetching Coach List...")
        
        let coachListServiceObj = CommonServiceClass(httpRequestType: HTTP_REQUEST_TYPE.POST_REQUEST, requestTag: "GET_COACH_LIST_SERVICE", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
        
        coachListServiceObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
            
            if errorMessage != nil
            {
                //print(response)
            }
            
            print(response as! NSDictionary)
            let responseDict = ChatListData(fromDictionary: response as! NSDictionary)
            
            if responseDict.errorCode == 1 {
                
                Utility.showAlert(ALERT_TITLE, message: responseDict.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            }
            else {
                
//                print(responseDict.data)
//                
//                self.nameArray.appendContentsOf(responseDict.data)
//                self.filteredArray = self.nameArray
//                
//                if responseDict.data.count > 0 {
//                    
//                    self.newChatListTableView.reloadData()
//                    
//                } else {
//                    
//                    Utility.showAlert(ALERT_TITLE, message: "Oops! No player added", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
//                }
                
                
            }
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
            
        }
        coachListServiceObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
            print("error ------------------------------\(error)")
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
            
        }
        
        
        var parameterDict = Dictionary <String, String>()
        parameterDict["cmd"] = "getUserListForChat"
        parameterDict["team_seq"] = "5"
        
        coachListServiceObj.commonServiceHit(parameterDict)
    }

}
