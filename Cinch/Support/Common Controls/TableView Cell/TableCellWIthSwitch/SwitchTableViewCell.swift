//
//  SwitchTableViewCell.swift
//  Cinch
//
//  Created by MONEY on 21/02/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class SwitchTableViewCell: UITableViewCell {

    @IBOutlet weak var switchRef: UISwitch!
    @IBOutlet weak var title: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = UITableViewCellSelectionStyle.None
        switchRef.tintColor = UIColor(red: 0.0/255.0, green: 159/255, blue: 232/255, alpha: 1.0)
        switchRef.onTintColor = UIColor(red: 0.0/255.0, green: 159/255, blue: 232/255, alpha: 1.0)
        
        
        
////        switchRef.backgroundColor = UIColor(red: 217.0/255.0, green: 217.0/255, blue: 217.0/255, alpha: 1.0)
//        switchRef.tintColor = UIColor(red: 23.0/255.0, green: 22/255, blue: 22/255, alpha: 1.0)
//        switchRef.thumbTintColor = UIColor(red: 119.0/255.0, green: 119.0/255, blue: 119.0/255, alpha: 1.0)
//        switchRef.onTintColor = UIColor(red: 23.0/255.0, green: 22/255, blue: 22/255, alpha: 1.0)
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func switchTapped(sender: AnyObject) {
        
    }
}
