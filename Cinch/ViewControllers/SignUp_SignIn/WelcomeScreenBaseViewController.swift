//
//  WelcomeScreenBaseViewController.swift
//  Cinch
//
//  Created by MONEY on 14/02/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class WelcomeScreenBaseViewController: UIViewController {
    
    @IBOutlet weak var constraintOfUpperViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var btnForSignIn: UIButton!
    @IBOutlet weak var btnForSignUp: UIButton!
    
    @IBOutlet weak var imageViewForSelector: UIImageView!
    @IBOutlet weak var scrollViewForMenuContent: UIScrollView!
    
    @IBOutlet weak var constraintForSelectorLeading: NSLayoutConstraint!
    var menuItems = [UIViewController]()
    var selectedMenuItem : Int!
    
    var signUpViewControllerObj : SignUpViewController!
    var signInViewControllerObj : SignInViewController!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Complete"

        signInViewControllerObj = self.storyboard?.instantiateViewControllerWithIdentifier("SignInViewController") as! SignInViewController
        signInViewControllerObj.parentScrollViewRef = scrollViewForMenuContent
        signInViewControllerObj.parentController = self
        menuItems.append(signInViewControllerObj)
        
        signUpViewControllerObj = self.storyboard?.instantiateViewControllerWithIdentifier("SignUpViewController") as! SignUpViewController
        menuItems.append(signUpViewControllerObj)
        
        

        self.setMenuItems()
        
        let _ = NSTimer.scheduledTimerWithTimeInterval(0.0, target: self, selector: Selector("scrollToSignIn"), userInfo: nil, repeats: false)
        // Do any additional setup after loading the view.
    }

    func scrollToSignIn() {
//        scrollViewForMenuContent.setContentOffset(CGPointMake(MAIN_SCREEN_WIDTH, 0), animated: true)

    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.hidden = true
        
        //Add keyboard notifications
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillShown:", name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillHidden:", name: UIKeyboardWillHideNotification, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidDisappear(animated: Bool) {
        //Removing notifies on keyboard appearing
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
    }
    
    //MARK:- SetUp MenuItems
    func setMenuItems() {
        
        for index in 0..<menuItems.count {
            
            menuItems[index].view.frame = CGRectMake(CGFloat(index) * MAIN_SCREEN_WIDTH, 0, 375, scrollViewForMenuContent.frame.height)
            
            scrollViewForMenuContent.addSubview(menuItems[index].view)
         
            let topConstraint = NSLayoutConstraint(item: menuItems[index].view , attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: scrollViewForMenuContent, attribute: NSLayoutAttribute.Top, multiplier: 1, constant: 0)
            scrollViewForMenuContent.addConstraint(topConstraint)
            
            let bottomConstraint = NSLayoutConstraint(item: menuItems[index].view, attribute: NSLayoutAttribute.Bottom, relatedBy: NSLayoutRelation.Equal, toItem: scrollViewForMenuContent, attribute: NSLayoutAttribute.Bottom, multiplier: 1, constant: 0)
            scrollViewForMenuContent.addConstraint(bottomConstraint)
            
            let leadingConstraint = NSLayoutConstraint(item: menuItems[index].view, attribute: NSLayoutAttribute.Leading, relatedBy: NSLayoutRelation.Equal, toItem: scrollViewForMenuContent, attribute: NSLayoutAttribute.Leading, multiplier: 1, constant: CGFloat(index) *  menuItems[index].view.frame.size.width)
            scrollViewForMenuContent.addConstraint(leadingConstraint)
            
            if index == menuItems.count - 1 {
                let trailingConstraint = NSLayoutConstraint(item: menuItems[index].view, attribute: NSLayoutAttribute.Trailing, relatedBy: NSLayoutRelation.Equal, toItem: scrollViewForMenuContent, attribute: NSLayoutAttribute.Trailing, multiplier: 1, constant: 0)
                scrollViewForMenuContent.addConstraint(trailingConstraint)
            }
        }
        
        scrollViewForMenuContent.contentSize.width = MAIN_SCREEN_WIDTH * CGFloat(menuItems.count)
        scrollViewForMenuContent.pagingEnabled = true
        selectedMenuItem = 0
        btnForSignIn.selected = true
        constraintForSelectorLeading.constant = 0
    }
    
    
    //MARK:- ScrollView Delegate 
    func scrollViewDidScroll(scrollView: UIScrollView) {
        
        constraintForSelectorLeading.constant = scrollView.contentOffset.x / 2
        
        if scrollView.contentOffset.x > MAIN_SCREEN_WIDTH/2 {
            selectedMenuItem = 1
            btnForSignUp.selected = true
            btnForSignIn.selected = false
        }
        else {
            selectedMenuItem = 0
            btnForSignIn.selected = true
            btnForSignUp.selected = false
        }
    }
    
    //MARK:- IBAction
    @IBAction func menuBtnAction(sender: UIButton) {
        
        if sender == btnForSignIn && selectedMenuItem == 1 {
            scrollViewForMenuContent.setContentOffset(CGPointMake(0, 0), animated: true)
        }
        
        if sender == btnForSignUp && selectedMenuItem == 0 {
            scrollViewForMenuContent.setContentOffset(CGPointMake(MAIN_SCREEN_WIDTH, 0), animated: true)
        }
    }
    
    //MARK: Keyboard Method
    func keyboardWillShown(notification: NSNotification) {
        
        constraintOfUpperViewHeight.constant = 20
        
        UIView.animateWithDuration(0.3, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
        
    }
    
    func keyboardWillHidden(notification: NSNotification) {
        constraintOfUpperViewHeight.constant = 236
        
//        scrollViewForMenuContent.contentSize.height = 374
        signUpViewControllerObj.containerView.contentSize.height = 374
        signInViewControllerObj.containerView.contentSize.height = 374

        UIView.animateWithDuration(0.3, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
