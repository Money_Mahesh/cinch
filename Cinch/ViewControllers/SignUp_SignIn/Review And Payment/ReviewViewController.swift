//
//  ReviewViewController.swift
//  Cinch
//
//  Created by Amit Garg on 21/02/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit
import Stripe

public enum CellToBeUpdate : Int {
    
    case PaymentMethod = 0
    case PaymentPlan = 1

}

//enum paymentPlan: Int {
//    case OneTime = 1
//    case Monthly = 12
//    case Quarterly = 4
//    case Semiannual = 2
//}

enum STPBackendChargeResult {
    case Success, Failure
}

typealias STPTokenSubmissionHandler = (STPBackendChargeResult?, NSError?) -> Void

protocol UpdateCellDelegate : NSObjectProtocol {
    func updateCell(cellType: CellToBeUpdate, withObject value: AnyObject?)
}

class ReviewViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UpdateCellDelegate {

    let SelectedColor = UIColor(red: 126.0/255, green: 211.0/255, blue: 33.0/255, alpha: 1.0)
    let UnSelectedColor = UIColor(red: 208/255, green: 2/255, blue: 27/255, alpha: 1.0)

    @IBOutlet weak var tableView: UITableView!
    
    var selectedPlan : PaymentPlanDetail?
    var selectedPaymentMethod : NSDictionary?
    var cardList = NSMutableArray()
    
    var dueNowString = "DUE NOW"
    let processingFee = 10

    var isComingFrom = ComingFrom.SignUp

    var orderReviewDetail: OrderDetail?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Stripe.setDefaultPublishableKey(StripePublishableKey)
        
        Utility.setBackButton(forViewController: self, withTitle: "")
        self.title = "Review"

        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        self.tableView.registerNib(UINib(nibName: "CellWithLabelAndButtonTableViewCell", bundle: nil), forCellReuseIdentifier: "buttonLabelCell")
        
        tableView.registerNib(UINib(nibName: "SectionHeaderWithLabelAndButton", bundle: nil), forHeaderFooterViewReuseIdentifier: "SectionHeaderWithLabelAndButton")
        
        getPlayerOrderReview()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.hidden = false
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.hidden = true
    }
    
    //MARK: - TableViewDataSource Methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        if orderReviewDetail == nil {
            return 0
        }
        else {
            return 3
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0  {
            return 1
            
        } else if section == 1 {
            return 3
        }
        else {
            return 4
        }
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 0 || section == 1 {
            
            return 50
        }
        return 0
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 0 || section == 1 {
           
            let headerView = tableView.dequeueReusableHeaderFooterViewWithIdentifier("SectionHeaderWithLabelAndButton") as! SectionHeaderWithLabelAndButton
            headerView.label.font = UIFont(name: "Lato-Black", size: 11.0)
            headerView.label.textColor = UIColor.blackColor()
            headerView.button.setTitleColorForAllState(UIColor(red: 0/255, green: 159/255, blue: 232/255, alpha: 1.0))
            headerView.button.titleLabel?.font = UIFont(name: "Lato-Regular", size: 14.0)
            headerView.button.setTitleForAllState("Edit")
            headerView.button.contentHorizontalAlignment = .Right
            headerView.button.hidden = true
            headerView.button.enabled = false

            switch(section) {
                
            case 0:
                if PlayerDetailManager.playerDetail != nil && PlayerDetailManager.playerDetail?.fname != "" {
                    headerView.label.text = (PlayerDetailManager.playerDetail?.fname)! + (PlayerDetailManager.playerDetail?.lname == nil ? "" : (PlayerDetailManager.playerDetail?.lname)!)
                }
                else {
                    headerView.label.text = "PLAYER NAME"
                }
                
            case 1:
                    headerView.label.text = PlayerDetailManager.clubSelected?.clubName == nil ? "YOUR ORDER" : PlayerDetailManager.clubSelected!.clubName
                
            default:
                print("Something is wrong")
                
            }
            
            
            return headerView
        }
        
        return nil
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        if indexPath.section == 1 {
            return 30.0
        } else if indexPath.section == 2 {
            return 50.0
        } else {
            return UITableViewAutomaticDimension
        }
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 20
    }
    

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        if indexPath.section == 0 {
            
            let cell = tableView.dequeueReusableCellWithIdentifier("addressCell", forIndexPath: indexPath) as UITableViewCell 
            cell.textLabel?.text = PlayerDetailManager.playerAddress
            cell.textLabel?.font = UIFont(name: "Lato-Regular", size: 14.0)
            cell.textLabel?.lineBreakMode = .ByWordWrapping
            cell.textLabel?.numberOfLines = 0
            
            return cell
            
        } else if indexPath.section == 1 {
            
            let cell = tableView.dequeueReusableCellWithIdentifier("buttonLabelCell", forIndexPath: indexPath) as! CellWithLabelAndButtonTableViewCell
            cell.actionButton.enabled = false
            cell.actionButton.setTitleColorForAllState(UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0))
            cell.titleLabel.textColor = UIColor(red: 119/255, green: 119/255, blue: 119/255, alpha: 1.0)
            cell.topSeperator.hidden = true
            cell.bottomSeperator.hidden = true
            
            switch(indexPath.row) {
                
            case 0:
                cell.titleLabel.text = PlayerDetailManager.programSelected?.name
                
                let price = PlayerDetailManager.programSelected?.fee_amount == nil ? "0" : (orderReviewDetail!.feeAmount == "" ? "0" : orderReviewDetail!.feeAmount)!
                
                cell.actionButton.setTitle("$\(price)", forState: .Normal)
                
                
            case 1:
                cell.titleLabel.text = "PROCESSING FEES"
                cell.actionButton.setTitle("$\(orderReviewDetail?.procesingAmt == nil ? "0" : (orderReviewDetail!.procesingAmt == "" ? "0" : orderReviewDetail!.procesingAmt))", forState: .Normal)
                
            case 2:
                cell.titleLabel.text = "CLUB REGISTERATION FEES"
                cell.actionButton.setTitle("$\(orderReviewDetail?.clubFee == nil ? "0" : (orderReviewDetail!.clubFee == "" ? "0" : orderReviewDetail!.clubFee))", forState: .Normal)
                
            default:
                print("Something is broken")
                
            }
            
            return cell
            
        }  else {
            
            let cell = tableView.dequeueReusableCellWithIdentifier("buttonLabelCell", forIndexPath: indexPath) as! CellWithLabelAndButtonTableViewCell
            
            switch(indexPath.row) {
                
            case 0:
                cell.titleLabel.text = "PAYMENT METHOD"
                cell.actionButton.enabled = true
                cell.actionButton.addTarget(self, action: Selector("selectPlaymentMethod"), forControlEvents: UIControlEvents.TouchUpInside)

                if selectedPaymentMethod == nil {
                    cell.actionButton.setTitleForAllState("CHOOSE")
                    cell.actionButton.setTitleColorForAllState(UnSelectedColor)
                }
                else {
                    cell.actionButton.setTitleForAllState("DONE")
                    cell.actionButton.setTitleColorForAllState(SelectedColor)
                }
                
            case 1:
                cell.titleLabel.text = "PAYMENT PLAN"
                cell.actionButton.enabled = true
                cell.actionButton.addTarget(self, action: Selector("selectPlan"), forControlEvents: UIControlEvents.TouchUpInside)
                
                if selectedPlan == nil {
                    cell.actionButton.setTitleForAllState("CHOOSE")
                    cell.actionButton.setTitleColorForAllState(UnSelectedColor)
                }
                else {
                    cell.actionButton.setTitleForAllState((selectedPlan?.paymentPlanName)!)
                    cell.actionButton.setTitleColorForAllState(SelectedColor)
                }
                
            case 2:
                cell.titleLabel.text = "TOTAL PRICE"
                
                if selectedPlan == nil {
                    cell.actionButton.setTitle("$\(orderReviewDetail?.totalPrice == nil ? 0 : orderReviewDetail!.totalPrice)", forState: .Normal)
                }else {
                    cell.actionButton.setTitle("$\(orderReviewDetail?.totalPrice == nil ? 0 : orderReviewDetail!.totalPrice)", forState: .Normal)
                }
                
                cell.actionButton.setTitleColorForAllState(UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0))
                cell.actionButton.enabled = false
                
            case 3:
                cell.titleLabel.text = dueNowString
                
                cell.actionButton.setTitle("$\(orderReviewDetail?.dueNow == nil ? 0 : orderReviewDetail!.dueNow)", forState: .Normal)
                cell.actionButton.setTitleColorForAllState(UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0))
                cell.contentView.backgroundColor = UIColor(red: 249/255, green: 249/255, blue: 249/255, alpha: 1.0)
                cell.actionButton.enabled = false

                
            default:
                print("Something is broken")
                
            }
            
            return cell
        }
    }
    
    //MARK: - TableViewDelegate Methods
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    func updateCell(cellType: CellToBeUpdate, withObject value: AnyObject?) {
        if cellType == CellToBeUpdate.PaymentMethod {
            selectedPaymentMethod = value as? NSDictionary
        }
        
        if cellType == CellToBeUpdate.PaymentPlan {
            selectedPlan = value as? PaymentPlanDetail
            PlayerDetailManager.planSelected = selectedPlan
            
            dueNowString = "AMOUNT"
            
            getPlayerOrderReview()
        }
        
        self.tableView.reloadData()
    }
    
    //MARK:- IBActions
    func backAction() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func selectPlan() {
        
        let paymentPlanViewControllerObj = mainStoryBoard.instantiateViewControllerWithIdentifier("PaymentPlanViewController") as! PaymentPlanViewController
        paymentPlanViewControllerObj.delegate = self
        paymentPlanViewControllerObj.paymentPlanDetail = PlayerDetailManager.planSelected
        
        self.navigationController?.pushViewController(paymentPlanViewControllerObj, animated: true)
    }

    func selectPlaymentMethod() {
        
        self.getPaymentMethodListService()
    }
    
    @IBAction func completeBtnAction(sender: AnyObject) {
        
        if selectedPlan == nil {
            Utility.showAlert(nil, message: "Please enter select payment plan", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
        }
        else if selectedPaymentMethod == nil {
            Utility.showAlert(nil, message: "Please enter select payment method", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
        }
        else {
            if (selectedPaymentMethod?.objectForKey("CustomerId") as! String) == "" {
                self.getStripeToken()
            }
            else {
                
                CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Processing transaction...")

                self.createBackendChargeWithToken(nil, completion: { (result: STPBackendChargeResult?, error: NSError?) -> Void in
                    if error != nil {
                        Utility.showAlert(nil, message: (error?.localizedDescription)!, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
                        
                        CSActivityIndicator.hideAllFromMainWindow()
                        
                        return
                    }
                    
                })
            }
        }
//        let successViewControllerObj = amitStoryBoard.instantiateViewControllerWithIdentifier("SuccessViewController") as! SuccessViewController
//        ROOT_NAVVIGATION_CONROLLER.topViewController?.navigationController!.pushViewController(successViewControllerObj, animated: true)
    }
    
    //MARK: - Web service hit methods
    func getPaymentMethodListService () {
        
        CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Getting payment method list. Please wait..")
        
        let paymentMethodListServiceObj = CommonServiceClass(httpRequestType: HTTP_REQUEST_TYPE.POST_REQUEST, requestTag: "GET_PAYMENT_METHOD_LIST_SERVICE", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
        
        paymentMethodListServiceObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
            
            
            if errorMessage != nil
            {
                //print(response)
            }
            
            print(response as! NSDictionary)
            let responseDict = CSCardList(fromDictionary: response as! NSDictionary)
            
            if responseDict.errorCode == 1 {
                Utility.showAlert(ALERT_TITLE, message: responseDict.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            }
            else {
                
                print(responseDict.data)
                
                if self.cardList.count > 0 {
                    self.cardList.removeAllObjects()
                }
                self.cardList.addObjectsFromArray(responseDict.data)
                
                let paymentMethodViewControllerObj = mainStoryBoard.instantiateViewControllerWithIdentifier("PaymentMethodViewController") as! PaymentMethodViewController
                paymentMethodViewControllerObj.cardList = self.cardList
                paymentMethodViewControllerObj.delegate = self
                paymentMethodViewControllerObj.isComingFrom = self.isComingFrom

                self.navigationController?.pushViewController(paymentMethodViewControllerObj, animated: true)
                
                
                //tableContentArray.addObject(self.createCellContentDict("SELECT PROGRAM", withAsseccoryImage: true, cell: collectionGridCell))
            }
            
            CSActivityIndicator.hideAllFromMainWindow()

        }
        paymentMethodListServiceObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
            print("error ------------------------------\(error)")
            CSActivityIndicator.hideAllFromMainWindow()
        }
        
        
        var parameterDict = Dictionary <String, String>()
        parameterDict["cmd"] = "getUserCardInfo"
        
        paymentMethodListServiceObj.commonServiceHit(parameterDict)
    }
    
    //MARK:- Stripe service method 
    func getStripeToken() {

        CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Processing transaction...")

        let cardParam = STPCardParams()
        cardParam.number = selectedPaymentMethod?.objectForKey("CardNumber") as? String
        cardParam.expMonth = (selectedPaymentMethod?.objectForKey("ExpiryMonth") as? UInt)!
        cardParam.expYear = (selectedPaymentMethod?.objectForKey("ExpiryYear") as? UInt)!
        cardParam.cvc = selectedPaymentMethod?.objectForKey("CVC") as? String
        cardParam.name = selectedPaymentMethod?.objectForKey("CardHolderName") as? String

        STPAPIClient.sharedClient().createTokenWithCard(cardParam) { (token : STPToken?, error : NSError?) -> Void in
            
            if error != nil {
                Utility.showAlert(nil, message: (error?.localizedDescription)!, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
                CSActivityIndicator.hideAllFromMainWindow()
            }
            else {
            
                self.createBackendChargeWithToken(token!, completion: { (result: STPBackendChargeResult?, error: NSError?) -> Void in
                    if error != nil {
                        Utility.showAlert(nil, message: (error?.localizedDescription)!, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
                        
                        CSActivityIndicator.hideAllFromMainWindow()

                        return
                    }
                    
                })
            }
        }
    }
    
    func createBackendChargeWithToken(token: STPToken?, completion: STPTokenSubmissionHandler)
    {
        let stripePaymentServiceObj = CommonServiceClass(httpRequestType: HTTP_REQUEST_TYPE.POST_REQUEST, requestTag: "PAYMENT_SERVICE", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
        
        stripePaymentServiceObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
            
            
            if errorMessage != nil
            {
                //print(response)
            }
            
            print(response as! NSDictionary)
            let responseDict = CSCardList(fromDictionary: response as! NSDictionary)
            
            if responseDict.errorCode == 0 {
                
                print(responseDict.data)

                if self.isComingFrom == ComingFrom.Dashboard || self.isComingFrom == ComingFrom.AddPlayerSideMenu || self.isComingFrom == ComingFrom.AddProgramSideMenu {
                    
                    let sWRevealViewControllerObj = amitStoryBoard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
                    
                    ROOT_NAVVIGATION_CONROLLER.topViewController?.navigationController!.setViewControllers([sWRevealViewControllerObj], animated: true)
                    
                }
                else {
                    let successViewControllerObj = amitStoryBoard.instantiateViewControllerWithIdentifier("SuccessViewController") as! SuccessViewController
                    
                    self.navigationController?.pushViewController(successViewControllerObj, animated: true)
                }
                
                
                Utility.showAlert(ALERT_TITLE, message: "Payment successfully created!", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)

                CSActivityIndicator.hideAllFromMainWindow()
                    
            }
            else {
                CSActivityIndicator.hideAllFromMainWindow()

                Utility.showAlert(ALERT_TITLE, message: responseDict.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)

            }
        }
        stripePaymentServiceObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
            print("error ------------------------------\(error)")
            CSActivityIndicator.hideAllFromMainWindow()
        }
        
        var parameterDict = Dictionary <String, String>()
                
        parameterDict["cmd"] = "make_stripe_payment"
        parameterDict["pSeq"] = PlayerDetailManager.playerDetail?.playerSeq
        parameterDict["programSeq"] = PlayerDetailManager.programSelected?.programSeq
        parameterDict["paymentPlan"] = String(PlayerDetailManager.planSelected!.paymentPlan)
        parameterDict["paymentScheduleSeq"] = ""
        parameterDict["paymentTerms"] = "Y"

        if (token == nil) {
            parameterDict["cust_id"] = selectedPaymentMethod?.objectForKey("CustomerId") as? String
        }
        else {
            parameterDict["stripeToken"] = token!.tokenId
            parameterDict["name"] = token!.card!.name
            parameterDict["cust_id"] = ""
        }
        
        
        
        print("parameterDict \(parameterDict)")

        stripePaymentServiceObj.commonServiceHit(parameterDict)
    }
    
    
    func getPlayerOrderReview()
    {
        
        CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Getting order detail. Please wait..")

        let orderReviewServiceObj = CommonServiceClass(httpRequestType: HTTP_REQUEST_TYPE.POST_REQUEST, requestTag: "PAYMENT_SERVICE", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
        
        orderReviewServiceObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
            
            
            if errorMessage != nil
            {
                //print(response)
            }
            
            print(response as! NSDictionary)
            let responseDict = CSOrderDetail(fromDictionary: response as! NSDictionary)

            if responseDict.errorCode == 0 {
                
                print(responseDict.data)
                
                self.orderReviewDetail = responseDict.data
                
                self.tableView.reloadData()
                CSActivityIndicator.hideAllFromMainWindow()
                
            }
            else {
                CSActivityIndicator.hideAllFromMainWindow()
                
                Utility.showAlert(ALERT_TITLE, message: responseDict.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
                
            }
        }
        orderReviewServiceObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
            print("error ------------------------------\(error)")
            CSActivityIndicator.hideAllFromMainWindow()
        }
        
//        cmd:getPlayerOrderReview, payment_plan:2, club_seq:1,div_seq:3,player_seq:5

        var parameterDict = Dictionary <String, String>()
        
        parameterDict["cmd"] = "getPlayerOrderReview"
        parameterDict["payment_plan"] = String(orderReviewDetail == nil ? "1" : PlayerDetailManager.planSelected!.paymentPlan)
        parameterDict["club_seq"] = PlayerDetailManager.clubSelected!.clubSeq
        parameterDict["div_seq"] = PlayerDetailManager.programSelected!.divisionSeq
        parameterDict["player_seq"] = PlayerDetailManager.playerDetail?.playerSeq

        print("parameterDict \(parameterDict)")
        
        orderReviewServiceObj.commonServiceHit(parameterDict)
    }

}
