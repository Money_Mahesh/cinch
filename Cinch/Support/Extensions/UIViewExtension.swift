//
//  UIViewExtension.swift
//  Cinch
//
//  Created by MONEY on 21/02/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import Foundation

extension CALayer {

    func setLayerBorderColor(color: UIColor) {
        self.borderColor = color.CGColor
    }
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
}
