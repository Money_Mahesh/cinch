//
//  OTP_View.swift
//  Cinch
//
//  Created by MONEY on 19/02/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class OTP_View: UIView, UITextFieldDelegate {

    @IBInspectable var noOfDigit : Int = 0
    @IBInspectable var noOfDigitPerField : Int = 1
    @IBInspectable var interspace : CGFloat = 5
    @IBInspectable var ShowPlaceholder : Bool = false
    
    @IBInspectable var fontSize: CGFloat = 10.0
    @IBInspectable var fontFamily: String = "Lato-Regular"

    @IBInspectable var lineColor : UIColor = UIColor.clearColor()
    @IBInspectable var textColor : UIColor = UIColor.blackColor()

    weak var delegate: TextFieldDelegate?
    
    var isDrew : Bool = false
    
    var textAlignment: NSTextAlignment = .Center
    var completeString = String()
    var oTP_TextField = [UITextField]()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.build_OTPView()
    }

    func build_OTPView() {
        
        if isDrew {return}
        isDrew = true
        
        let otp_TextfieldView = UIView(frame: self.bounds)
        
        for position in 0..<noOfDigit {
            
            let viewWidthAfterRemovingInterSpace = self.bounds.size.width - interspace * CGFloat(noOfDigit-1)
            let textfield = UITextField(frame: CGRectMake((CGFloat(position) * (viewWidthAfterRemovingInterSpace / CGFloat(noOfDigit))) + (CGFloat(position) * interspace), 0, (viewWidthAfterRemovingInterSpace / CGFloat(noOfDigit)), self.bounds.size.height-2))
            
            textfield.tag =  position
            textfield.font =  UIFont(name: self.fontFamily, size: self.fontSize)!
            textfield.textColor =  textColor
            textfield.keyboardType = UIKeyboardType.NumberPad
            textfield.textAlignment = textAlignment
            textfield.delegate = self
        
            if ShowPlaceholder {
 
                var placeholderString = ""
                
                for _ in 0..<noOfDigitPerField {
                    placeholderString += "x"
                }
                textfield.placeholder = placeholderString
            }

            oTP_TextField.append(textfield)
            
            let bottomLine = UIView(frame: CGRectMake(textfield.frame.origin.x, textfield.frame.size.height + 1, textfield.frame.size.width, 1))
            bottomLine.backgroundColor = lineColor
            
            otp_TextfieldView.addSubview(oTP_TextField[position])
            otp_TextfieldView.addSubview(bottomLine)
            
            self.addSubview(otp_TextfieldView)
        }
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        let returnStatus : Bool!
        
        if string == "" {
            //character deleted
            if textField.tag == 0 {
                returnStatus = true
            }
            else {
                if textField.text?.characters.count == 1 {
                    oTP_TextField[textField.tag].text! = string
                    oTP_TextField[textField.tag - 1].becomeFirstResponder()
                    returnStatus = false
                }
                else {
                    returnStatus = true
                }
            }
        }
        else {
            //character added
            if textField.text?.characters.count < noOfDigitPerField - 1 {
                returnStatus = true
            }
            else {
                
                if range.location == 0 {
                    if oTP_TextField[textField.tag].text?.characters.count == 0 {
                        oTP_TextField[textField.tag].text = string
                        oTP_TextField[textField.tag].becomeFirstResponder()
                    }
                    else {
                        oTP_TextField[textField.tag].text?.replaceRange(
                            (oTP_TextField[textField.tag].text?.startIndex.advancedBy(
                                range.location // start position
                                ))!..<(oTP_TextField[textField.tag].text?.startIndex.advancedBy(
                                    1 // end position
                                    ))!, with: string)
                    }
                }
                else {
                    if textField.tag == noOfDigit - 1 {
                        self.endEditing(true)
                    }
                    else {
                        if oTP_TextField[textField.tag + 1].text?.characters.count == 0 {
                            oTP_TextField[textField.tag + 1].text = string
                            oTP_TextField[textField.tag + 1].becomeFirstResponder()
                        }
                        else {
                            oTP_TextField[textField.tag + 1].text?.replaceRange(
                                (oTP_TextField[textField.tag + 1].text?.startIndex.advancedBy(
                                    range.location - 1 // start position
                                    ))!..<(oTP_TextField[textField.tag].text?.startIndex.advancedBy(
                                        range.location // end position
                                        ))!, with: string)
                        }
                        
                        oTP_TextField[textField.tag + 1].becomeFirstResponder()
                        
                    }
                }
                returnStatus = false
            }
            
        }
        
        let _ = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: Selector("callUpdate"), userInfo: nil, repeats: false)
        
        return returnStatus
    }
    
    func callUpdate() {
        completeString = ""
        for index in 0..<noOfDigit {
            completeString += oTP_TextField[index].text!
        }
        
        print("completeString \(completeString)")
        
        if delegate != nil {
            delegate?.textFieldUpdate!(String(completeString))
        }
    }

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}

@objc protocol TextFieldDelegate : NSObjectProtocol {
    optional func textFieldUpdate(completeString : String)
}
