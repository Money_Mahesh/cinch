//
//  RosterViewController.swift
//  Cinch
//
//  Created by MONEY on 12/03/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class RosterViewController: UIViewController, UITableViewDelegate {

    @IBOutlet weak var searchTextField: UITextField!
    
    @IBOutlet weak var rosterTableView: UITableView!
    
    var playerArray = [PlayerData]()
    var filteredArray = [PlayerData]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if revealViewController() != nil {
            Utility.setMenuButton(forViewController: self, revealController: revealViewController())
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            
        }
        
//        filteredArray = nameArray
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hitRosterWebService()

        self.navigationController?.navigationBar.topItem?.title = "Roster"        
        self.tabBarController?.navigationItem.rightBarButtonItem = nil

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- TableView Data Source and Delegates
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRectMake(0, 0, MAIN_SCREEN_WIDTH, 1))
        footerView.backgroundColor = UIColor(red: 247.0/255.0, green: 247.0/255.0, blue: 247.0/255.0, alpha: 1.0)
        
        return footerView
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filteredArray.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let availabilityEventCell = tableView.dequeueReusableCellWithIdentifier("AvailabilityTableViewCell") as! AvailabilityEventCell
        
        availabilityEventCell.labelForName.text = "\((self.filteredArray[indexPath.row].fname)!) \((self.filteredArray[indexPath.row].lname)!)"
        
        Utility.setImageWithURl((self.filteredArray[indexPath.row].profilePic), inImageView: availabilityEventCell.imageViewForProfilePic)
        return availabilityEventCell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let playerDetailedProfileViewControllerObj = mainStoryBoard.instantiateViewControllerWithIdentifier("PlayerDetailedProfileViewController") as! PlayerDetailedProfileViewController
        playerDetailedProfileViewControllerObj.title = "\((self.filteredArray[indexPath.row].fname)!) \((self.filteredArray[indexPath.row].lname)!)"
        playerDetailedProfileViewControllerObj.playerDetail = filteredArray[indexPath.row]
        self.navigationController?.pushViewController(playerDetailedProfileViewControllerObj, animated: true)
    }

    //MARK:- Textfield Delegate
    @IBAction func textfieldDidChanged(sender: UITextField) {
        
        if sender.text == "" {
            filteredArray = playerArray
        }
        else {
            filteredArray = playerArray.filter() {
                return String("\($0.fname.lowercaseString) \($0.lname.lowercaseString)").rangeOfString(String(sender.text!).lowercaseString) == nil ? false : true
            }
        }
        rosterTableView.reloadData()
    }
    
    //MARK:- Hit Webservice
    func hitRosterWebService() {
        
        CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Fetching List...")
        
        let rosterListObj = CommonServiceClass(httpRequestType: HTTP_REQUEST_TYPE.POST_REQUEST, requestTag: "ROSTER_LIST", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
        
        rosterListObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
            
            if errorMessage != nil
            {
                print(response)
            }
            print(response as! NSDictionary)
            
            let playerDataObj =  CSPlayerDetailData(fromDictionary: response as! NSDictionary)
            
            if playerDataObj.errorCode == 0 {
                
                self.playerArray = playerDataObj.data
                self.filteredArray = self.playerArray
            } else {
                
                Utility.showAlert(ALERT_TITLE, message: playerDataObj.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            }
            
            self.rosterTableView.reloadData()
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
        }
        rosterListObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
            
            print("error ------------------------------\(error)")
        }
        
        
        var parameterDict = Dictionary <String, String>()

        parameterDict["cmd"] = "getPlayersByPrograms"
        parameterDict["program_seq"] = PlayerDetailManager.programSelected!.programSeq
        
        rosterListObj.commonServiceHit(parameterDict)
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


