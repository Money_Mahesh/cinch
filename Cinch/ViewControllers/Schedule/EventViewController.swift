//
//  EventViewController.swift
//  Cinch
//
//  Created by MONEY on 25/02/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class EventViewController: UIViewController {

    @IBOutlet weak var eventTableView: UITableView!
    
    var mapView = AppleMapView(frame: CGRectMake(0, 0, MAIN_SCREEN_WIDTH, 160),dragablePin: false, lastLocation: nil)
    var selectedCellIndex : Int = -1
    
    var eventArray = [EventDetail]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if revealViewController() != nil {
            Utility.setMenuButton(forViewController: self, revealController: revealViewController())
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            
        }
        
        eventTableView.estimatedRowHeight = 57
        
        //Table Footer View
        let tableFooterView = UIView(frame: CGRectMake(0, 0, MAIN_SCREEN_WIDTH, 1))
        tableFooterView.backgroundColor = UIColor(red: 238.0/255.0, green: 238.0/255.0, blue: 238.0/255.0, alpha: 1.0)
        eventTableView.tableFooterView = tableFooterView
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hitEventWebService()

        self.navigationController?.navigationBar.topItem?.title = "Upcoming Events"
        //        Utility.setRightNavigationButton(forViewController: self, withImage: "Edit Icon", withTitle:"")
        
        let rightButton: UIButton = UIButton()
        rightButton.setImage(UIImage(named: "+"), forState: .Normal)
        rightButton.frame = CGRectMake(0.0, 0.0, 38.0, 44.0)
        
        //Adding Action Left Navigation Bar Button Item.
        rightButton.addTarget(self, action: "rightBtnAction", forControlEvents: .TouchUpInside)
        rightButton.setTitle("", forState: .Normal)
        rightButton.titleLabel?.font = UIFont(name: "Lato-Regular", size: 12)
        rightButton.setTitleColorForAllState(UIColor(red: 160/255, green: 160/255, blue: 160/255, alpha: 1.0))
        
        let rightItem:UIBarButtonItem = UIBarButtonItem()
        rightItem.customView = rightButton
        
        
        self.tabBarController?.navigationItem.rightBarButtonItem = rightItem
        //self.navigationController?.navigationBar.hidden = false
    }

//    override func viewWillAppear(animated: Bool) {
//        super.viewWillAppear(animated)
//        
//        Utility.setBackButton(forViewController: self, withTitle: "")
//        Utility.setRightNavigationButton(forViewController: self, withImage: "+", withTitle:"")
//        self.title = "Event"
//        //self.navigationController?.navigationBar.hidden = false
//    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        //self.navigationController?.navigationBar.hidden = true
    }
    
    // MARK:- TableView Data Source and Delegates
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if selectedCellIndex == indexPath.row {
            return UITableViewAutomaticDimension
        }
        return 57
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return eventArray.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let eventCell = tableView.dequeueReusableCellWithIdentifier("EventTableViewCell") as! EventTableViewCell
        eventCell.buttonForCellMainView.addTarget(self, action: Selector("animateCell:"), forControlEvents: UIControlEvents.TouchUpInside)
        
        eventCell.bindCellWithData(eventArray[indexPath.row])
        
        eventCell.availabiltyDetailBtnRef.addTarget(self, action: Selector("showAvailability:"), forControlEvents: UIControlEvents.TouchUpInside)
        eventCell.btnRefForCreatePath.addTarget(self, action: Selector("createPath:"), forControlEvents: UIControlEvents.TouchUpInside)
        eventCell.switchForAvailabilty.addTarget(self, action: Selector("changeSwitchValue:"), forControlEvents: UIControlEvents.ValueChanged)

        return eventCell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }

    //MARK:- Cell Button Action
    func showAvailability(sender: UIButton) {
        let point = sender.convertPoint(CGPointZero, toView:self.eventTableView)
        let cellIndexPath = self.eventTableView.indexPathForRowAtPoint(point)!
        
        let availabilityViewControllerObj = mainStoryBoard.instantiateViewControllerWithIdentifier("AvailabilityViewController") as! AvailabilityViewController
        availabilityViewControllerObj.eventDetail = eventArray[cellIndexPath.row]
        
        self.navigationController?.pushViewController(availabilityViewControllerObj, animated: true)
    }
    
    func createPath(sender: UIButton) {
        let point = sender.convertPoint(CGPointZero, toView:self.eventTableView)
        let cellIndexPath = self.eventTableView.indexPathForRowAtPoint(point)!
        
        if eventArray[cellIndexPath.row].lat != nil && eventArray[cellIndexPath.row].lng != nil {
            mapView.getPath()
        }
        else {
            Utility.showAlert(ALERT_TITLE, message: "Event location is not available", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
        }
    }
    
    func changeSwitchValue(sender: UISwitch) {
        
        let point = sender.convertPoint(CGPointZero, toView:self.eventTableView)
        let cellIndexPath = self.eventTableView.indexPathForRowAtPoint(point)!
        
        self.hitUpdateAvailabiltyWebService(cellIndexPath.row)
    }
    
    //MARK: - Cell Expand & Collapse Methods
    func animateCell(sender: UIButton) {
        
        let point = sender.convertPoint(CGPointZero, toView:self.eventTableView)
        let cellIndexPath = self.eventTableView.indexPathForRowAtPoint(point)!
        
        if selectedCellIndex == cellIndexPath.row {
            selectedCellIndex = -1
            
            let lastSelectedCell = (self.eventTableView.cellForRowAtIndexPath(cellIndexPath) as! EventTableViewCell)
            lastSelectedCell.constraintForCellMainViewBottom.priority = 800
            
            let _ = NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: Selector("collapseCell"), userInfo: nil, repeats: false)
        }
        else {
            
            let lastSelectedCell = (self.eventTableView.cellForRowAtIndexPath(NSIndexPath(forRow: selectedCellIndex, inSection: 0)) as? EventTableViewCell)
            
            if lastSelectedCell != nil {
                //When there is no expanded cell
                lastSelectedCell!.constraintForCellMainViewBottom.priority = 800
                
                selectedCellIndex = -1
                let _ = NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: Selector("expandCell:"), userInfo: cellIndexPath, repeats: false)
            }
            else {
                //When there is a expanded cell
                let selectedCell = (self.eventTableView.cellForRowAtIndexPath(cellIndexPath) as! EventTableViewCell)
                
                selectedCellIndex = cellIndexPath.row
                
                if eventArray[selectedCellIndex].lat != nil && eventArray[selectedCellIndex].lng != nil {
                    mapView.loadMapWithLatitude(eventArray[selectedCellIndex].lat, longitudeStr: eventArray[selectedCellIndex].lng)
                }
                else {
                    mapView.unloadMap()
                }
                
                selectedCell.viewForMap.addSubview(mapView)
                selectedCell.constraintForCellMainViewBottom.priority = 1
            }
        }
        
        self.eventTableView.beginUpdates()
        self.eventTableView.endUpdates()
    }
    
    func expandCell(timer: NSTimer) {
        
        let selectedCell = (self.eventTableView.cellForRowAtIndexPath(timer.userInfo as! NSIndexPath) as! EventTableViewCell)
        
        selectedCellIndex = (timer.userInfo as! NSIndexPath).row
        
        if eventArray[selectedCellIndex].lat != nil && eventArray[selectedCellIndex].lng != nil {
            mapView.loadMapWithLatitude(eventArray[selectedCellIndex].lat, longitudeStr: eventArray[selectedCellIndex].lng)
        }
        else {
            mapView.unloadMap()
        }
        
        selectedCell.viewForMap.addSubview(mapView)
        selectedCell.constraintForCellMainViewBottom.priority = 1
        
        self.eventTableView.beginUpdates()
        self.eventTableView.endUpdates()
        
    }
    
    func collapseCell() {
        mapView.removeFromSuperview()
    }
    
    //MARK: - IBActions
//    func backAction() {
//        self.navigationController?.popViewControllerAnimated(true)
//    }
    
    func rightBtnAction() {
        let addEventViewControllerObj = mainStoryBoard.instantiateViewControllerWithIdentifier("AddEventViewController")
        
        self.navigationController!.pushViewController(addEventViewControllerObj , animated: true)
    }
    
    //MARK:- Hit Webservice
    func hitEventWebService() {
        
        CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Fetching events list...")
        
        let eventListObj = CommonServiceClass(httpRequestType: HTTP_REQUEST_TYPE.POST_REQUEST, requestTag: "EVENT_LIST", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
        
        eventListObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
            
            if errorMessage != nil
            {
                print(response)
            }
            print(response as! NSDictionary)
            
            let eventListObj =  CSEventList(fromDictionary: response as! NSDictionary)
            
            if eventListObj.errorCode == 0 {
                
                self.eventArray = eventListObj.data
            } else {
                
                Utility.showAlert(ALERT_TITLE, message: eventListObj.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            }
            
            self.eventTableView.reloadData()
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
        }
        eventListObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
            
            print("error ------------------------------\(error)")
        }
        
        
        var parameterDict = Dictionary <String, String>()
        
        parameterDict["cmd"] = "getEventsByPrograms"
        parameterDict["program_seq"] = PlayerDetailManager.programSelected!.programSeq
        parameterDict["player_seq"] = PlayerDetailManager.playerDetail?.playerSeq

        eventListObj.commonServiceHit(parameterDict)
    }
    
    func hitUpdateAvailabiltyWebService(eventIndex : Int) {
    
        CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Updating...")
        
        let eventObj = CommonServiceClass(httpRequestType: HTTP_REQUEST_TYPE.POST_REQUEST, requestTag: "UPDATE", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
        
        eventObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
            
            if errorMessage != nil
            {
                print(response)
            }
            print(response as! NSDictionary)
            
            let eventObj =  OtpVerificationData(fromDictionary: response as! NSDictionary)
            
            if eventObj.errorCode == 0 {
                
                if eventObj.data == true {
                    self.eventArray[eventIndex].status = self.eventArray[eventIndex].status == "Y" ? "N" : "Y"
                }
                else {
                    self.eventArray[eventIndex].status = self.eventArray[eventIndex].status
                }
                
            } else {
                self.eventArray[eventIndex].status = self.eventArray[eventIndex].status
                Utility.showAlert(ALERT_TITLE, message: eventObj.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            }
            
            self.eventTableView.reloadData()
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
        }
        eventObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
            
            print("error ------------------------------\(error)")
        }
        
        
        var parameterDict = Dictionary <String, String>()
        
        parameterDict["cmd"] = "updatePlayerEventStatus"
        parameterDict["player_seq"] = PlayerDetailManager.playerDetail?.playerSeq
        parameterDict["event_seq"] = eventArray[eventIndex].eventSeq
        parameterDict["status"] = eventArray[eventIndex].status == "Y" ? "N" : "Y"
        parameterDict["is_view"] = "1"


        eventObj.commonServiceHit(parameterDict)
    }

}

class EventTableViewCell: UITableViewCell {
    
    @IBOutlet weak var constraintForCellMainViewBottom: NSLayoutConstraint!
    
    @IBOutlet weak var buttonForCellMainView: UIButton!
    @IBOutlet weak var imageViewForBigMapPin: UIImageView!
    @IBOutlet weak var labelForEventName: UILabel!
    @IBOutlet weak var labelForEventTime: UILabel!
    @IBOutlet weak var switchForAvailabilty: UISwitch!
    
    @IBOutlet weak var viewForMap: UIView!
    @IBOutlet weak var labelForAddress: UILabel!
    
    @IBOutlet weak var labelForYesCount: UILabel!
    @IBOutlet weak var labelForNoCount: UILabel!
    @IBOutlet weak var labelForMayBeCount: UILabel!
    @IBOutlet weak var availabiltyDetailBtnRef: UIButton!
    @IBOutlet weak var btnRefForCreatePath: UIButton!
    
    override func awakeFromNib() {
        
        switchForAvailabilty.tintColor = UIColor(red: 0.0/255.0, green: 159/255, blue: 232/255, alpha: 1.0)
        switchForAvailabilty.onTintColor = UIColor(red: 0.0/255.0, green: 159/255, blue: 232/255, alpha: 1.0)
    }
    
    func bindCellWithData(eventDetail: EventDetail) {
        
        //Set event date and Name
        if Utility.changeDateFormate(eventDetail.eventDate, inputFormat: "YYYY-MM-dd", outputFormat: "MM/dd") == "" {
            labelForEventName.text = eventDetail.eventName
        }
        else {
            labelForEventName.text = Utility.changeDateFormate(eventDetail.eventDate, inputFormat: "YYYY-MM-dd", outputFormat: "MM/dd") + " " + eventDetail.eventName
        }
        
        //Set Event time and address
        if Utility.changeDateFormate(eventDetail.startTime, inputFormat: "hh:mm:ss"
            , outputFormat: "h:mm a") == "" {
                
            labelForEventTime.text = eventDetail.teamName
        }
        else {
            labelForEventTime.text = Utility.changeDateFormate(eventDetail.startTime, inputFormat: "hh:mm:ss"
                , outputFormat: "h:mm a") + " @ " + eventDetail.teamName
        }
        
        labelForAddress.text = eventDetail.address + ", " + (eventDetail.location == nil ? "" : eventDetail.location)
        labelForYesCount.text = String(eventDetail.yes)
        labelForNoCount.text = String(eventDetail.no)
        
        if eventDetail.status == "Y" {
            switchForAvailabilty.on = true
        }
        else {
            switchForAvailabilty.on = false
        }

    }
    
    //MARK:- IBAction
    @IBAction func switchValueChanged(sender: AnyObject) {
        print("switchValueChanged")

    }
    
    @IBAction func createPathAction(sender: AnyObject) {
        print("createPathAction")
    }
}
