//
//	Event.swift
//
//	Create by Money Mahesh on 20/3/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class EventDetail : NSObject, NSCoding{
    
    var ageGroupEnd : String!
    var ageGroupStart : String!
    var eventStatus : String!
    var gender : String!
    var programName : String!
    var programImage : String!
    var programSeq : String!
    var address : String!
    var createTime : String!
    var creatorSeq : String!
    var eventName : String!
    var endTime : String!
    var eventDate : String!
    var eventSeq : String!
    var eventType : String!
    var isDeleted : String!
    var lat : String!
    var lng : String!
    var location : String!
    var locationSeq : String!
    var no : Int!
    var role : String!
    var startTime : String!
    var status : String!
    var teamName : String!
    var teamSeq : String!
    var updateTime : String!
    var yes : Int!
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    
    override init() {
        ageGroupEnd = ""
        ageGroupStart = ""
        eventStatus = ""
        gender = ""
        programName = ""
        programImage = ""
        programSeq = ""
        address = ""
        createTime = ""
        creatorSeq = ""
        eventName = ""
        endTime = ""
        eventDate = ""
        eventSeq = ""
        eventType = ""
        isDeleted = ""
        lat = ""
        lng = ""
        location = ""
        locationSeq = ""
        no = 0
        role = ""
        startTime = ""
        status = ""
        teamName = ""
        teamSeq = ""
        updateTime = ""
        yes = 0
    }
    
    init(fromDictionary dictionary: NSDictionary){
        ageGroupEnd = dictionary["age_group_end"] as? String
        ageGroupStart = dictionary["age_group_start"] as? String
        eventStatus = dictionary["event_status"] as? String
        gender = dictionary["gender"] as? String
        programName = dictionary["name"] as? String
        programImage = dictionary["program_image"] as? String
        programSeq = dictionary["program_seq"] as? String
        address = dictionary["address"] as? String
        createTime = dictionary["create_time"] as? String
        creatorSeq = dictionary["creator_seq"] as? String
        eventName = dictionary["event_name"] as? String
        endTime = dictionary["end_time"] as? String
        eventDate = dictionary["event_date"] as? String
        eventSeq = dictionary["event_seq"] as? String
        eventType = dictionary["event_type"] as? String
        isDeleted = dictionary["is_deleted"] as? String
        location = dictionary["location"] as? String
        lat = dictionary["lat"] as? String
        lng = dictionary["lng"] as? String
        locationSeq = dictionary["location_seq"] as? String
        no = dictionary["no"] as? Int
        role = dictionary["role"] as? String
        startTime = dictionary["start_time"] as? String
        status = dictionary["status"] as? String
        teamName = dictionary["team_name"] as? String
        teamSeq = dictionary["team_seq"] as? String
        updateTime = dictionary["update_time"] as? String
        yes = dictionary["yes"] as? Int
    }
    
    /**
     * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> NSDictionary
    {
        let dictionary = NSMutableDictionary()
        if ageGroupStart != nil{
            dictionary["age_group_start"] = ageGroupStart
        }
        if ageGroupEnd != nil{
            dictionary["age_group_end"] = ageGroupEnd
        }
        if eventStatus != nil{
            dictionary["event_status"] = eventStatus
        }
        if gender != nil{
            dictionary["gender"] = eventName
        }
        if programName != nil{
            dictionary["name"] = programName
        }
        if programImage != nil{
            dictionary["program_image"] = programImage
        }
        if programSeq != nil{
            dictionary["program_seq"] = programSeq
        }
        if address != nil{
            dictionary["address"] = address
        }
        if createTime != nil{
            dictionary["create_time"] = createTime
        }
        if creatorSeq != nil{
            dictionary["creator_seq"] = creatorSeq
        }
        if eventName != nil{
            dictionary["event_name"] = eventName
        }
        if endTime != nil{
            dictionary["end_time"] = endTime
        }
        if eventDate != nil{
            dictionary["event_date"] = eventDate
        }
        if eventSeq != nil{
            dictionary["event_seq"] = eventSeq
        }
        if eventType != nil{
            dictionary["event_type"] = eventType
        }
        if isDeleted != nil{
            dictionary["is_deleted"] = isDeleted
        }
        if location != nil{
            dictionary["location"] = location
        }
        if lat != nil{
            dictionary["lat"] = lat
        }
        if lng != nil{
            dictionary["lng"] = lng
        }
        if locationSeq != nil{
            dictionary["location_seq"] = locationSeq
        }
        if no != nil{
            dictionary["no"] = no
        }
        if role != nil{
            dictionary["role"] = role
        }
        if startTime != nil{
            dictionary["start_time"] = startTime
        }
        if status != nil{
            dictionary["status"] = status
        }
        if teamName != nil{
            dictionary["team_name"] = teamName
        }
        if teamSeq != nil{
            dictionary["team_seq"] = teamSeq
        }
        if updateTime != nil{
            dictionary["update_time"] = updateTime
        }
        if yes != nil{
            dictionary["yes"] = yes
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        ageGroupEnd = aDecoder.decodeObjectForKey("age_group_end") as? String
        ageGroupStart = aDecoder.decodeObjectForKey("age_group_start") as? String
        eventStatus = aDecoder.decodeObjectForKey("event_status") as? String
        gender = aDecoder.decodeObjectForKey("gender") as? String
        programName = aDecoder.decodeObjectForKey("name") as? String
        programImage = aDecoder.decodeObjectForKey("program_image") as? String
        programSeq = aDecoder.decodeObjectForKey("program_seq") as? String
        address = aDecoder.decodeObjectForKey("address") as? String
        createTime = aDecoder.decodeObjectForKey("create_time") as? String
        creatorSeq = aDecoder.decodeObjectForKey("creator_seq") as? String
        eventName = aDecoder.decodeObjectForKey("event_name") as? String
        endTime = aDecoder.decodeObjectForKey("end_time") as? String
        eventDate = aDecoder.decodeObjectForKey("event_date") as? String
        eventSeq = aDecoder.decodeObjectForKey("event_seq") as? String
        eventType = aDecoder.decodeObjectForKey("event_type") as? String
        isDeleted = aDecoder.decodeObjectForKey("is_deleted") as? String
        location = aDecoder.decodeObjectForKey("location") as? String
        lat = aDecoder.decodeObjectForKey("lat") as? String
        lng = aDecoder.decodeObjectForKey("lng") as? String
        locationSeq = aDecoder.decodeObjectForKey("location_seq") as? String
        no = aDecoder.decodeObjectForKey("no") as? Int
        role = aDecoder.decodeObjectForKey("role") as? String
        startTime = aDecoder.decodeObjectForKey("start_time") as? String
        status = aDecoder.decodeObjectForKey("status") as? String
        teamName = aDecoder.decodeObjectForKey("team_name") as? String
        teamSeq = aDecoder.decodeObjectForKey("team_seq") as? String
        updateTime = aDecoder.decodeObjectForKey("update_time") as? String
        yes = aDecoder.decodeObjectForKey("yes") as? Int
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encodeWithCoder(aCoder: NSCoder)
    {
        if ageGroupStart != nil{
            aCoder.encodeObject(ageGroupStart, forKey: "age_group_start")
        }
        if ageGroupEnd != nil{
            aCoder.encodeObject(ageGroupEnd, forKey: "age_group_end")
        }
        if eventStatus != nil{
            aCoder.encodeObject(eventStatus, forKey: "event_status")
        }
        if gender != nil{
            aCoder.encodeObject(gender, forKey: "gender")
        }
        if programName != nil{
            aCoder.encodeObject(programName, forKey: "name")
        }
        if programImage != nil{
            aCoder.encodeObject(programImage, forKey: "program_image")
        }
        if programSeq != nil{
            aCoder.encodeObject(programSeq, forKey: "program_seq")
        }
        if address != nil{
            aCoder.encodeObject(address, forKey: "address")
        }
        if createTime != nil{
            aCoder.encodeObject(createTime, forKey: "create_time")
        }
        if creatorSeq != nil{
            aCoder.encodeObject(creatorSeq, forKey: "creator_seq")
        }
        if eventName != nil{
            aCoder.encodeObject(eventName, forKey: "event_name")
        }
        if endTime != nil{
            aCoder.encodeObject(endTime, forKey: "end_time")
        }
        if eventDate != nil{
            aCoder.encodeObject(eventDate, forKey: "event_date")
        }
        if eventSeq != nil{
            aCoder.encodeObject(eventSeq, forKey: "event_seq")
        }
        if eventType != nil{
            aCoder.encodeObject(eventType, forKey: "event_type")
        }
        if isDeleted != nil{
            aCoder.encodeObject(isDeleted, forKey: "is_deleted")
        }
        if lat != nil{
            aCoder.encodeObject(lat, forKey: "lat")
        }
        if lng != nil{
            aCoder.encodeObject(lng, forKey: "lng")
        }
        if location != nil{
            aCoder.encodeObject(location, forKey: "location")
        }
        if locationSeq != nil{
            aCoder.encodeObject(locationSeq, forKey: "location_seq")
        }
        if no != nil{
            aCoder.encodeObject(no, forKey: "no")
        }
        if role != nil{
            aCoder.encodeObject(role, forKey: "role")
        }
        if startTime != nil{
            aCoder.encodeObject(startTime, forKey: "start_time")
        }
        if status != nil{
            aCoder.encodeObject(status, forKey: "status")
        }
        if teamName != nil{
            aCoder.encodeObject(teamName, forKey: "team_name")
        }
        if teamSeq != nil{
            aCoder.encodeObject(teamSeq, forKey: "team_seq")
        }
        if updateTime != nil{
            aCoder.encodeObject(updateTime, forKey: "update_time")
        }
        if yes != nil{
            aCoder.encodeObject(yes, forKey: "yes")
        }
        
    }
    
}