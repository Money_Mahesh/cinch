//
//  Strings.swift
//  Cinch
//
//  Created by MONEY on 14/02/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import Foundation

let ALERT_TITLE = "Cinch Sports"
let ALERT_OK_BUTTON_TITLE = "Ok"
let ALERT_CANCEL_BUTTON_TITLE = "Cancel"

let USER_SEQUENCE = "UserSequence"
let USER_NAME = "UserName"
let USER_GENDER = "UserGender"
let USER_ACCOUNT_TYPE = "UserAccountType"
let USER_COUNTRY = "UserCountry"
let USER_PHONE_NO = "UserPhoneNo"
let USER_EMAIL = "UserEmail"
let USER_ROLE = "UserRole"
let USER_STATUS = "UserStatus"
let USER_INVITATION_SEQ = "InvitationSeq"

let USER_IMAGE_URL = "UserImageUrl"

let FACEBOOK_LOGIN = "IsFacebook"
let USER_FB_ID = "UserFBID"
let FACEBOOK_TOKEN_STRING = "FacebookTokenString"

let SIGN_UP_STATUS = "signUpStatus"
let BIRTH_CERT_REQ = "birthCertReq"

let AGREEMENT_STATUS = "AgreementStatus"
let BIRTH_CERTIFICATE_STATUS = "BirthCertificateStatus"


//Player Detail Strings
let WAIVER_SEQUENCE = "WaiverSeq"
let INSURANCE_SIGNATURE_URL = "InsuranceSignatureUrl"
let BIRTH_CERTIFICATE_URL = "BirthCertificateUrl"
let PROGRAM_SELECTED = "ProgramSelected"
let PLAN_SELECTED = "PlanSelected"
let CLUB_SELECTED = "ClubSelected"
let EVENT_SELECTED = "EventSelected"
let PLAYER_ADDRESS = "PlayerAddress"
let PLAYER_DETAIL = "PlayerDetail"

