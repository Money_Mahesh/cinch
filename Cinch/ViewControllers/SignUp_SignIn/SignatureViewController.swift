//
//  SignatureViewController.swift
//  Cinch
//
//  Created by Amit Garg on 20/02/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class SignatureViewController: UIViewController {

    @IBOutlet weak var drawSignatureView: YPDrawSignatureView!
    
    var signatureUrl: String?
    var waiverSeq = String()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.hidden = true
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.hidden = true
    }

    @IBAction func clearSignatureTapped(sender: AnyObject) {
        
        self.drawSignatureView.clearSignature()
        
    }

    @IBAction func doneBtnAction(sender: AnyObject) {
        
        let sigatureImage = self.drawSignatureView.getSignature()
        
        if sigatureImage == nil {
            
            Utility.showAlert(ALERT_TITLE, message: "Please sign the document", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            
        } else {
            
            CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Uploading signature...")
            
            Utility.uploadImageToserver(sigatureImage!) { (image: UIImage?, headShotImageUrl: String?) -> Void in
                
                if image != nil {
                    self.signatureUrl = headShotImageUrl
                    
                    self.hitSignatureWebService()
                }
                else {
                    CSActivityIndicator.hideAllFromMainWindow()
                    
                    self.signatureUrl = nil
                    Utility.saveBoolInDefault(AGREEMENT_STATUS, value: false)
                    
                    ROOT_NAVVIGATION_CONROLLER.topViewController!.dismissViewControllerAnimated(true, completion: nil)
                    
                    Utility.showAlert(ALERT_TITLE, message: "Something went wrong. Please try after sometime", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
                }
                
            }
            
        }
        
    }
    
    //MARK:- Hit Webservice
    func hitSignatureWebService() {
        
        CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Updating signature...")
        
        let saveSignatureObj = ClubListServices(httpRequestType: HTTP_REQUEST_TYPE.POST_REQUEST, requestTag: "SAVE_SIGNATURE", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
        
        saveSignatureObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
            
            if errorMessage != nil
            {
                print(response)
            }
            print(response as! NSDictionary)
            
            let signatureObj =  CSCommonParser(fromDictionary: response as! NSDictionary)
            
            if signatureObj.errorCode == 0 {
                
                Utility.saveBoolInDefault(AGREEMENT_STATUS, value: true)
                
                PlayerDetailManager.waiverSeq = self.waiverSeq
                PlayerDetailManager.insuranceSignatureUrl = self.signatureUrl

                ROOT_NAVVIGATION_CONROLLER.topViewController!.dismissViewControllerAnimated(true, completion: nil)


            } else {
                
                Utility.saveBoolInDefault(AGREEMENT_STATUS, value: false)

                Utility.showAlert(ALERT_TITLE, message: signatureObj.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            }
            

            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
        }
        saveSignatureObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
            
            print("error ------------------------------\(error)")
        }
        
        
        var parameterDict = Dictionary <String, String>()
        
        parameterDict["cmd"] = "updateUserSignature"
        parameterDict["user_seq"] = UserProfileManager.userSequence
        parameterDict["role"] = UserProfileManager.role
        parameterDict["signature_url"] = signatureUrl

        print(parameterDict)
        saveSignatureObj.signatureHit(parameterDict)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
