import Foundation


/**
HTTP Request Type Enum

- GET_REQUEST:  To mark request of HTTP GET type
- POST_REQUEST: To mark request of HTTP POST type
*/
enum HTTP_REQUEST_TYPE{
    case GET_REQUEST
    case POST_REQUEST
}

/**
Enum to define Web Request Priorty

- LOW_PRIORITY:      To mark request as Low Priority
- MODERATE_PRIORITY: To mark request as Moderate Priority
- HIGH_PRIORITY:     To mark request as High Priority
*/
enum REQUEST_PRIORITY{
    case LOW_PRIORITY
    case MODERATE_PRIORITY
    case HIGH_PRIORITY
}


/**
Base class for all web services to control server communication.
*/
class ServerConnect{
    /**
    Http Request Type for the web service call
    */
    var httpRequestType: HTTP_REQUEST_TYPE?
    
    /**
    Request Identifier to distinguish various requests
    */
    var requestTag: String
    
    /**
    Priorty for request
    */
    var requestPriority: REQUEST_PRIORITY
    
    var completionBlockWithSuccess:((statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?)->Void)?
    var completionBlockWithFailure:((requestType: String!, error: NSError!)->Void)?

    static var requestOperationManagerSingleton : ServiceRequestManager!
    
    static var requestOperationManager: ServiceRequestManager!{
        get
    {
            if requestOperationManagerSingleton == nil
    {
            requestOperationManagerSingleton = ServiceRequestManager(baseURL: BASE_URL)
            requestOperationManagerSingleton.responseSerializer = AFJSONResponseSerializer();
            
            
            let contentTypes : NSMutableSet = NSMutableSet(set: requestOperationManagerSingleton.responseSerializer.acceptableContentTypes!)
            contentTypes.addObject("text/html")
            requestOperationManagerSingleton.responseSerializer.acceptableContentTypes = contentTypes as Set<NSObject>
            
            self.setCommonRequestHeaders()
            }
        
            return requestOperationManagerSingleton
        }
        set{}
    }
    
    
    /**
    Place common request headers in each and every request made to server
    */
    static func setCommonRequestHeaders()
    {
        requestOperationManagerSingleton.requestSerializer.setValue("application/json", forHTTPHeaderField: "Accept")
        requestOperationManagerSingleton.requestSerializer.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        requestOperationManagerSingleton.requestSerializer.setValue("iOS", forHTTPHeaderField: "platform")
    }
    
    init(httpRequestType: HTTP_REQUEST_TYPE, requestTag: String, requestPriority: REQUEST_PRIORITY){
        self.httpRequestType = httpRequestType
        self.requestTag = requestTag
        self.requestPriority = requestPriority
    }
    
    init(requestTag: String, requestPriority: REQUEST_PRIORITY){
        self.requestTag = requestTag
        self.requestPriority = requestPriority
    }
    
    func processBeforeMakingRequestWithParameters(parameters:Dictionary<String,String?>!) -> Dictionary<String, String>!{
        if parameters == nil {
            return nil;
        }
        
        var sanitizeRequestParameters = Dictionary<String, String>()
        for (key, value) in parameters {
            if value != nil{
                sanitizeRequestParameters[key] = value
            }
        }

        return sanitizeRequestParameters
    }
    
    /**
    Process response after making the web request
    @param response The server response object of the current request
    @returns response object after performing required processing 
    */
    func processAfterMakingRequestWithParameters(response: AnyObject) -> AnyObject!{
        
//        if ERROR_CONDITION
//        {
//            self.completionBlockWithSuccess?(statusCode: 0, requestType: self.requestTag, response: response, errorMessage: "ERROR MESSAGE");
//
//            return nil
//        }
        
        return response
    }
 
}