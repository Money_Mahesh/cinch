//
//  ParentViewController.swift
//  Cinch
//
//  Created by Amit Garg on 12/03/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class ParentViewController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        if Utility.fetchStringFromDefault(SIGN_UP_STATUS) == nil {
            self.setRootViewControllerBasedOnLoginStatus("WelcomeScreenBaseViewController")
        }
        else {
            //return Utility.fetchStringFromDefault(SIGN_UP_STATUS) as! String
            self.setRootViewControllerBasedOnLoginStatus(Utility.fetchStringFromDefault(SIGN_UP_STATUS) as! String)
            
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    
    func setRootViewControllerBasedOnLoginStatus (viewController : String) {
        
        self.navigationBarHidden = false

        switch viewController {
            
        case "WelcomeScreenBaseViewController":
            
            let welcomeScreenBaseViewControllerObj = mainStoryBoard.instantiateViewControllerWithIdentifier("WelcomeScreenBaseViewController")
            self.setViewControllers([welcomeScreenBaseViewControllerObj], animated: false)
            break
            
        case "CreateAccountViewController":
            let createAccountViewControllerObj = amitStoryBoard.instantiateViewControllerWithIdentifier("CreateAccountViewController")
            self.setViewControllers([createAccountViewControllerObj], animated: false)
            
            break
            
        case "PhoneNoVerificationViewController":
            let phoneNoVerificationViewControllerObj = mainStoryBoard.instantiateViewControllerWithIdentifier("PhoneNoVerificationViewController")
            self.setViewControllers([phoneNoVerificationViewControllerObj], animated: false)
            break
            
        case "AddPlayerViewController":
            let addPlayerViewControllerObj = amitStoryBoard.instantiateViewControllerWithIdentifier("AddPlayerViewController")
            self.setViewControllers([addPlayerViewControllerObj], animated: false)
            break
        
            
        case "FormsViewController":
            let addFormViewControllerObj = amitStoryBoard.instantiateViewControllerWithIdentifier("FormsViewController")
            self.setViewControllers([addFormViewControllerObj], animated: false)
            break
            
        case "ReviewViewController":
            let reviewViewControllerObj = amitStoryBoard.instantiateViewControllerWithIdentifier("ReviewViewController")
            self.setViewControllers([reviewViewControllerObj], animated: false)
            break
            
        case "SuccessViewController":
            let successViewControllerObj = amitStoryBoard.instantiateViewControllerWithIdentifier("SuccessViewController")
            self.setViewControllers([successViewControllerObj], animated: false)
            break
            
        case "SWRevealViewController":
            let sWRevealViewControllerObj = amitStoryBoard.instantiateViewControllerWithIdentifier("SWRevealViewController")
            self.navigationBarHidden = true
            self.setViewControllers([sWRevealViewControllerObj], animated: false)
            break
            
        default:
            print("something broke viewController \(viewController)")
            break
            
        }
        
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
