//
//	PlayerDashboardDetail.swift
//
//	Create by Money Mahesh on 20/3/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class PlayerSportDetail : NSObject, NSCoding{

	var events : EventDetail!
	var paymentSchedule : PaymentSchedule!
	var programName : String!
	var programSeq : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		if let eventsData = dictionary["events"] as? NSDictionary{
			events = EventDetail(fromDictionary: eventsData)
		}
		if let paymentScheduleData = dictionary["payment_schedule"] as? NSDictionary{
			paymentSchedule = PaymentSchedule(fromDictionary: paymentScheduleData)
		}
		programName = dictionary["program_name"] as? String
		programSeq = dictionary["program_seq"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if events != nil{
			dictionary["events"] = events.toDictionary()
		}
		if paymentSchedule != nil{
			dictionary["payment_schedule"] = paymentSchedule.toDictionary()
		}
		if programName != nil{
			dictionary["program_name"] = programName
		}
		if programSeq != nil{
			dictionary["program_seq"] = programSeq
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         events = aDecoder.decodeObjectForKey("events") as? EventDetail
         paymentSchedule = aDecoder.decodeObjectForKey("payment_schedule") as? PaymentSchedule
         programName = aDecoder.decodeObjectForKey("program_name") as? String
         programSeq = aDecoder.decodeObjectForKey("program_seq") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if events != nil{
			aCoder.encodeObject(events, forKey: "events")
		}
		if paymentSchedule != nil{
			aCoder.encodeObject(paymentSchedule, forKey: "payment_schedule")
		}
		if programName != nil{
			aCoder.encodeObject(programName, forKey: "program_name")
		}
		if programSeq != nil{
			aCoder.encodeObject(programSeq, forKey: "program_seq")
		}

	}

}