//
//  PlayerDashboardViewController.swift
//  Cinch
//
//  Created by Amit Garg on 16/03/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class PlayerDashboardViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var playerList = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if revealViewController() != nil {
            Utility.setMenuButton(forViewController: self, revealController: revealViewController())
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
//        UIBarButtonItem *backBtn = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
//        self.navigationItem.backBarButtonItem = backBtn;
        
        let backbutton = UIBarButtonItem(title: "", style: .Plain, target: nil
            , action: nil)
        self.navigationItem.backBarButtonItem = backbutton
        
        self.getPlayerListService()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = "Dashboard"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        
        if self.playerList.count > 0 {
            return self.playerList.count
        }
        return 0
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return 75.0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let myPlayerCell = tableView.dequeueReusableCellWithIdentifier("myPlayerCell", forIndexPath: indexPath) as! PlayerListCell
        myPlayerCell.selectionStyle = .None
        
        let playerObj = self.playerList.objectAtIndex(indexPath.row) as! PlayerData
        myPlayerCell.playerNameLabel.text = playerObj.fname + " " + playerObj.lname
        
        Utility.setImageWithURl(playerObj.profilePic, inImageView: myPlayerCell.playerImageView)
        
        return myPlayerCell
    }
    
    // MARK: - Table view delegate
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let mainTabBarControllerObj = amitStoryBoard.instantiateViewControllerWithIdentifier("MainTabBarViewController") as! MainTabBarViewController
        
        PlayerDetailManager.playerDetail = playerList.objectAtIndex(indexPath.row) as? PlayerData
        
        self.navigationController?.pushViewController(mainTabBarControllerObj, animated: true)
        
    }
    
    //MARK: - Web Service hit
    
    func getPlayerListService () {
        
        CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Fetching Player List...")
        
        let playerListServiceObj = CommonServiceClass(httpRequestType: HTTP_REQUEST_TYPE.POST_REQUEST, requestTag: "GET_PLAYER_LIST_SERVICE", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
        
        playerListServiceObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
            
            if errorMessage != nil
            {
                //print(response)
            }
            
            print(response as! NSDictionary)
            let responseDict = CSPlayerDetailData(fromDictionary: response as! NSDictionary)
            
            if responseDict.errorCode == 1 {
                self.playerList.removeAllObjects()
                
                Utility.showAlert(ALERT_TITLE, message: responseDict.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            }
            else {
                
                print(responseDict.data)
                
                
                if responseDict.data.count > 0 {
                    
                    if self.playerList.count > 0 {
                        self.playerList.removeAllObjects()
                    }
                    
                    self.playerList.addObjectsFromArray(responseDict.data)
                    PlayerDetailManager.playerDetail = self.playerList.objectAtIndex(0) as? PlayerData
                    
                    self.tableView.reloadData()
                    
                } else {
                    
                    Utility.showAlert(ALERT_TITLE, message: "Oops! No player added", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
                }
                
                
            }
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)

        }
        playerListServiceObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
            print("error ------------------------------\(error)")
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)

        }
        
        
        var parameterDict = Dictionary <String, String>()
        parameterDict["cmd"] = "getPlayersDetailsByParents"
        parameterDict["parent_seq"] = UserProfileManager.userSequence!
        
        playerListServiceObj.commonServiceHit(parameterDict)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

class PlayerListCell : UITableViewCell {
    
    @IBOutlet weak var playerImageView: UIImageView!
    @IBOutlet weak var playerNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.playerImageView.layer.cornerRadius = self.playerImageView.frame.width / 2
        self.playerImageView.clipsToBounds = true
    }
}

