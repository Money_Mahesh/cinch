//
//  ClubListServices.swift
//  Cinch
//
//  Created by Amit Garg on 05/03/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class ClubListServices: ServerConnect {
    
    func clubListHit(parameters: Dictionary<String, String>?)->Void{
        
        ServerConnect.requestOperationManager.POST("https://www.cinchsports.com/user/services_v2/processRequest", parameters: parameters, success: {(operation : AFHTTPRequestOperation! , responseObject: AnyObject!) in
            
            let serverResponse = self.processAfterMakingRequestWithParameters(responseObject)
            
            self.completionBlockWithSuccess?(statusCode: 0, requestType: self.requestTag, response: serverResponse, errorMessage: nil);
            
            },
            failure: {(operation: AFHTTPRequestOperation?, error:NSError) in
                
                if error.code == -1009 || error.code == -1005 {
                    InternetConnectivityManager.internetErrorAlert.show()
                }
                
                self.completionBlockWithFailure?(requestType: self.requestTag, error: error)
                
            }
        )
    }
    
    func programListHit(parameters: Dictionary<String, String>?)->Void{
        
        ServerConnect.requestOperationManager.POST("https://www.cinchsports.com/user/services_v2/processRequest", parameters: parameters, success: {(operation : AFHTTPRequestOperation! , responseObject: AnyObject!) in
            
            let serverResponse = self.processAfterMakingRequestWithParameters(responseObject)
            
            self.completionBlockWithSuccess?(statusCode: 0, requestType: self.requestTag, response: serverResponse, errorMessage: nil);
            
            },
            failure: {(operation: AFHTTPRequestOperation?, error:NSError) in
                
                if error.code == -1009 || error.code == -1005 {
                    InternetConnectivityManager.internetErrorAlert.show()
                }
                
                self.completionBlockWithFailure?(requestType: self.requestTag, error: error)
                
            }
        )
    }

    func addProgramHit(parameters: NSMutableDictionary)->Void{
        
        ServerConnect.requestOperationManager.POST("https://www.cinchsports.com/user/services_v2/processRequest", parameters: parameters, success: {(operation : AFHTTPRequestOperation! , responseObject: AnyObject!) in
            
            let serverResponse = self.processAfterMakingRequestWithParameters(responseObject)
            
            self.completionBlockWithSuccess?(statusCode: 0, requestType: self.requestTag, response: serverResponse, errorMessage: nil);
            
            },
            failure: {(operation: AFHTTPRequestOperation?, error:NSError) in
                
                if error.code == -1009 || error.code == -1005 {
                    InternetConnectivityManager.internetErrorAlert.show()
                }
                
                self.completionBlockWithFailure?(requestType: self.requestTag, error: error)
                
            }
        )
    }
    
    func getInsuranceFormHit(parameters: Dictionary<String, String>?)->Void{
        
        ServerConnect.requestOperationManager.POST("https://www.cinchsports.com/user/services_v2/processRequest", parameters: parameters, success: {(operation : AFHTTPRequestOperation! , responseObject: AnyObject!) in
            
            let serverResponse = self.processAfterMakingRequestWithParameters(responseObject)
            
            self.completionBlockWithSuccess?(statusCode: 0, requestType: self.requestTag, response: serverResponse, errorMessage: nil);
            
            },
            failure: {(operation: AFHTTPRequestOperation?, error:NSError) in
                
                if error.code == -1009 || error.code == -1005 {
                    InternetConnectivityManager.internetErrorAlert.show()
                }
                
                self.completionBlockWithFailure?(requestType: self.requestTag, error: error)
                
            }
        )
    }
    
    func signatureHit(parameters: Dictionary<String, String>?)->Void{
        
        ServerConnect.requestOperationManager.POST("https://www.cinchsports.com/user/services_v2/processRequest", parameters: parameters, success: {(operation : AFHTTPRequestOperation! , responseObject: AnyObject!) in
            
            let serverResponse = self.processAfterMakingRequestWithParameters(responseObject)
            
            self.completionBlockWithSuccess?(statusCode: 0, requestType: self.requestTag, response: serverResponse, errorMessage: nil);
            
            },
            failure: {(operation: AFHTTPRequestOperation?, error:NSError) in
                
                if error.code == -1009 || error.code == -1005 {
                    InternetConnectivityManager.internetErrorAlert.show()
                }
                
                self.completionBlockWithFailure?(requestType: self.requestTag, error: error)
                
            }
        )
    }
}
