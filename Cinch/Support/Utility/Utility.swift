//
//  Utility.swift
//  Cinch
//
//  Created by Money Mahesh on 14/02/16.
//  Copyright © 2016 tbsl. All rights reserved.
//

import UIKit

class Utility: NSObject {
    class func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRectMake(0, 0, width, CGFloat.max))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.ByWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
    
    class func widthForView(text:String, font:UIFont, height:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRectMake(0, 0, CGFloat.max, height))
        label.numberOfLines = 1
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.width
    }
    
    class func sizeForView(text:String, font:UIFont) -> CGSize{
        let label:UILabel = UILabel(frame: CGRectMake(0, 0, CGFloat.max, CGFloat.max))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.ByWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        
        return label.frame.size
    }
    
    //SHOW ALERT VIEW
    class func showAlert(title:String?, message:String, cancelButtonTitle:String) -> Void {
        
        let alert = UIAlertController(title:title, message:message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title:cancelButtonTitle, style: UIAlertActionStyle.Default, handler: nil))
        
        
        getCurrentViewController().presentViewController(alert, animated: true, completion: nil)
    }
    
    class func showFunctionalityPendingAlert() -> Void {
        
        let alert = UIAlertController(title:"Oops!", message:"Functionality Pending", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title:ALERT_OK_BUTTON_TITLE, style: UIAlertActionStyle.Default, handler: nil))
        
        getCurrentViewController().presentViewController(alert, animated: true, completion: nil)
    }
    
    //UIViewController Presented or pushed
    class func isModal(viewController : UIViewController) -> Bool {
        if((viewController.presentingViewController) != nil) {
            return true
        }
        
        if(viewController.presentingViewController?.presentedViewController == viewController) {
            return true
        }
        if(viewController.navigationController?.presentingViewController?.presentedViewController == viewController.navigationController) {
            return true
        }
        if((viewController.tabBarController?.presentingViewController?.isKindOfClass(UITabBarController)) != nil) {
            return true
        }
        return false
    }
    
    // FETCH COLOR FROM HEXSTRING
    class func colorWithHexString (hex:String) -> UIColor
    {
        var cString:String = hex.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()).uppercaseString
        
        if (cString.hasPrefix("#")) {
            cString = (cString as NSString).substringFromIndex(1)
        }
        
        if (cString.characters.count != 6) {
            return UIColor.grayColor()
        }
        
        let rString = (cString as NSString).substringToIndex(2)
        let gString = ((cString as NSString).substringFromIndex(2) as NSString).substringToIndex(2)
        let bString = ((cString as NSString).substringFromIndex(4) as NSString).substringToIndex(2)
        
        var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
        NSScanner(string: rString).scanHexInt(&r)
        NSScanner(string: gString).scanHexInt(&g)
        NSScanner(string: bString).scanHexInt(&b)
        
        return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(1))
    }
    
    //MARK: Date Formatter
    class func changeDateFormate(dateStr: String?, inputFormat: String, outputFormat: String) -> String {
        if dateStr == nil {
            return ""
        }
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = inputFormat
        
        let date = dateFormatter.dateFromString(dateStr!)
        print(date)
        dateFormatter.dateFormat = outputFormat
        
        print(dateFormatter.stringFromDate(date!))
        
        return dateFormatter.stringFromDate(date!) + " "
    }
    
    //MARK: Utilities For NSUserDefaults
    
    //SAVE STRING IN USER DEFAULT
    class func saveStringInDefault(key:String,value:String) {
        NSUserDefaults.standardUserDefaults().setObject(value, forKey: key);
        NSUserDefaults.standardUserDefaults().synchronize();
    }
    //FETCH STRING FROM USER DEFAULT
    class func fetchStringFromDefault(key:String)->AnyObject? {
        return NSUserDefaults.standardUserDefaults().objectForKey(key);
    }
    
    
    //SAVE OBJECT IN USER DEFAULT
    class func saveObjectInDefault(key:String,value:AnyObject?) {
        let encodedData = NSKeyedArchiver.archivedDataWithRootObject(value!)
        
        NSUserDefaults.standardUserDefaults().setObject(encodedData, forKey: key);
        NSUserDefaults.standardUserDefaults().synchronize();
    }
    //FETCH OBJECT FROM USER DEFAULT
    class func fetchObjectFromDefault(key:String)->AnyObject? {
        let decoded  = NSUserDefaults.standardUserDefaults().objectForKey(key) as? NSData
        
        if decoded == nil {
            return nil
        }
        
        let decodedObject = NSKeyedUnarchiver.unarchiveObjectWithData(decoded!)
        return decodedObject
    }
    
    class func saveCustomArrayInDefault(key:String, object:NSMutableArray!) {
        let encodedData = NSKeyedArchiver.archivedDataWithRootObject(object)
        NSUserDefaults.standardUserDefaults().setObject(encodedData, forKey: key)
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    //SAVE BOOL IN USER DEFAULT
    class func saveBoolInDefault(key:String,value:Bool) {
        NSUserDefaults.standardUserDefaults().setBool(value, forKey: key);
        NSUserDefaults.standardUserDefaults().synchronize();
    }
    
    //FETCH BOOL FROM USER DEFAULT
    class func fetchBoolFromDefault(key:String)->Bool? {
        return NSUserDefaults.standardUserDefaults().boolForKey(key);
    }
    
    
    //REMOVE NSUSER DEFAULT
    class func removeParameterFromUserDefault(key:String) {
        NSUserDefaults.standardUserDefaults().removeObjectForKey(key);
        NSUserDefaults.standardUserDefaults().synchronize();
    }
    
    //MARK: - Get Current ViewController
    class func getCurrentViewController() -> UIViewController {
        
        var currentViewController: UIViewController!
        
        if ROOT_NAVVIGATION_CONROLLER.topViewController!.presentedViewController == nil {
            
            currentViewController = ROOT_NAVVIGATION_CONROLLER.topViewController!
            ROOT_NAVVIGATION_CONROLLER.topViewController!
        }
        else {
            
            if ROOT_NAVVIGATION_CONROLLER.topViewController!.presentedViewController?.isKindOfClass(UINavigationController) == true {
                
                currentViewController = (ROOT_NAVVIGATION_CONROLLER.topViewController!.presentedViewController as! UINavigationController).topViewController!
                
            }
            else {
                
                currentViewController = ROOT_NAVVIGATION_CONROLLER.topViewController!.presentedViewController!
                
            }
        }
        
        return currentViewController
    }
    
    //MARK: - Get Next ViewController Description To SignIn
    class func getNextViewControllerDescriptionToSignIn() -> String {
        
        if self.fetchStringFromDefault(SIGN_UP_STATUS) == nil {
            return "CreateAccountViewController"
        }
        else {
            return self.fetchStringFromDefault(SIGN_UP_STATUS) as! String
        }
    }
    
    
    //MARK: - CREATE NAVIGATION BAR BUTTON
    class func setBackButton(forViewController viewController:UIViewController?, withTitle title: String) {

        //Set Back and map button
        let backButton: UIButton = UIButton()
        backButton.setImage(UIImage(named: "Back Arrow"), forState: .Normal)
        backButton.frame = CGRectMake(-30.0, 0.0, 38.0, 44.0)
        
        //Adding Action Left Navigation Bar Button Item.
        backButton.addTarget(viewController, action: "backAction", forControlEvents: .TouchUpInside)
        backButton.imageEdgeInsets = UIEdgeInsetsMake(12, 0, 13, 25)
        
        let leftItem:UIBarButtonItem = UIBarButtonItem()
        leftItem.customView = backButton

        viewController!.navigationItem.leftBarButtonItem = leftItem
    }
    
 class func setMenuButton(forViewController viewController:UIViewController, revealController: SWRevealViewController) {
    
        let menuItem:UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "Menu"), style: UIBarButtonItemStyle.Done, target: revealController, action: Selector("revealToggle:"))
        menuItem.tintColor = UIColor(red: 0, green: 169/255, blue: 255255, alpha: 1.0)
        
        viewController.navigationItem.leftBarButtonItem = menuItem
    }
    
    class func setLeftNavigationButton(forViewController viewController:UIViewController, withImage imageName : String) {
        
        //Set Back and map button
        let backButton: UIButton = UIButton()
        backButton.setImage(UIImage(named: imageName), forState: .Normal)
        backButton.frame = CGRectMake(-30.0, 0.0, 38.0, 44.0)
        
        //Adding Action Left Navigation Bar Button Item.
        backButton.addTarget(viewController, action: "leftBtnAction", forControlEvents: .TouchUpInside)
        backButton.imageEdgeInsets = UIEdgeInsetsMake(12, 0, 13, 25)
        
        let leftItem:UIBarButtonItem = UIBarButtonItem()
        leftItem.customView = backButton
        
        viewController.navigationItem.leftBarButtonItem = leftItem
    }

    class func setRightNavigationButton(forViewController viewController:UIViewController, withImage imageName : String, withTitle: String) {
        
        //Set Back and map button
        let rightButton: UIButton = UIButton()
        rightButton.setImage(UIImage(named: imageName), forState: .Normal)
        rightButton.frame = CGRectMake(-38.0, 0.0, 38.0, 44.0)

        //Adding Action Left Navigation Bar Button Item.
        rightButton.addTarget(viewController, action: "rightBtnAction", forControlEvents: .TouchUpInside)
        rightButton.imageEdgeInsets = UIEdgeInsetsMake(13, 20, 13, 0)
        rightButton.setTitle(withTitle, forState: .Normal)
        rightButton.titleLabel?.font = UIFont(name: "Lato-Regular", size: 12)
        rightButton.titleLabel?.adjustsFontSizeToFitWidth = true
        rightButton.setTitleColorForAllState(UIColor(red: 160/255, green: 160/255, blue: 160/255, alpha: 1.0))
        
        let rightItem:UIBarButtonItem = UIBarButtonItem()
        rightItem.customView = rightButton
        
        viewController.navigationItem.rightBarButtonItem = rightItem
    }
    
    //Mark:- TopViewController
    class func topViewController() -> UIViewController? {
        
        if ROOT_NAVVIGATION_CONROLLER.topViewController?.isKindOfClass(UIViewController) == true {
            
            return ROOT_NAVVIGATION_CONROLLER.topViewController!
            
        }
        else if ROOT_NAVVIGATION_CONROLLER.topViewController?.isKindOfClass(SWRevealViewController) == true {
            
            return ((ROOT_NAVVIGATION_CONROLLER.topViewController as! SWRevealViewController).frontViewController as! UINavigationController).topViewController!
        }
        return nil
    }
    
    //MARK:- Get Image From URL
    class func setImageWithURl(imageUrl: String?, inImageView imgView: UIImageView) {
        
        if imageUrl == nil {
            imgView.image = UIImage(named: "no_image_icon")
            imgView.contentMode = UIViewContentMode.ScaleToFill
            
            imgView.layer.cornerRadius = (imgView.frame.width)/2
            imgView.clipsToBounds = true
            return
        }
        
        let image_url = NSURL(string: imageUrl!.stringByReplacingOccurrencesOfString(" ", withString: ""))
        let url_request = NSURLRequest(URL: image_url!)
        let placeholder = UIImage(named:"")
        //cell.activityIndicator.startAnimating()
        imgView.image = nil
        imgView.setImageWithURLRequest(url_request, placeholderImage: placeholder,
            success: { [weak imgView] (request: NSURLRequest, response NSHTTPURLResponse, image: UIImage) -> Void in
                
                imgView!.image = image
                imgView!.contentMode = UIViewContentMode.ScaleToFill
                
                imgView?.layer.cornerRadius = (imgView?.frame.width)! / 2
                imgView?.clipsToBounds = true
                
            },
            failure: {[weak imgView] (request: NSURLRequest, response: NSHTTPURLResponse?, error: NSError) -> Void in
                imgView!.image = UIImage(named: "no_image_icon")
                imgView!.contentMode = UIViewContentMode.ScaleToFill
                
                imgView?.layer.cornerRadius = (imgView?.frame.width)! / 2
                imgView?.clipsToBounds = true
            })
    }
    
    //MARK:- UPLOAD IMAGE TO SERVER
    class func uploadImageToserver(image: UIImage, resetImageViewBlock:((UIImage?, headShotImageUrl: String?)->Void)) {
        
        let uploadImageObj = UploadImage(httpRequestType: HTTP_REQUEST_TYPE.POST_REQUEST, requestTag: "UPLOAD_IMAGE_SERVICE", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
        
        uploadImageObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
            
            if errorMessage != nil
            {
                //print(response)
            }
            
            print(response as! NSDictionary)

            let uploadImageObj = CSUploadImage(fromDictionary: (response as! NSDictionary))
            
            if uploadImageObj.errorCode == 1 {
                Utility.showAlert(ALERT_TITLE, message: uploadImageObj.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
                
                resetImageViewBlock(nil, headShotImageUrl: nil)
            }
            else {
                resetImageViewBlock(image, headShotImageUrl: uploadImageObj.data.imagejpg.imgUrl)
            }
        }
        uploadImageObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
            print("error ------------------------------\(error)")
            resetImageViewBlock(nil, headShotImageUrl: nil)
        }
        
        var parameterDict = Dictionary <String, AnyObject>()
        
        parameterDict["cmd"] = "uploadImage"
        parameterDict["0"] = ["main", "200", "130", "90"]
        
        print(parameterDict)
        uploadImageObj.uploadingImage(parameterDict, image: image)
    }
}

