//
//  Enum.swift
//  Cinch
//
//  Created by MONEY on 14/02/16.
//  Copyright © 2016 MONEY. All rights reserved.
//


import Foundation

public enum CSAccountType: Int {
    case Simple = 0
    case Facebook = 1
}

//MARK: - Search Type Enum
public enum Example : Int {
    case Case1 = 1
    case Case2 = 2
}

public enum ComingFrom: Int {
    case SignUp = 0
    case Dashboard = 1
    case AddPlayerSideMenu = 2
    case AddProgramSideMenu = 3
}


