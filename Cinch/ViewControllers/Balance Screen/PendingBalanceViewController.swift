//
//  PendingBalanceViewController.swift
//  Cinch
//
//  Created by Amit Garg on 17/03/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit
import Stripe

class PendingBalanceViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,UpdateCellDelegate {

    @IBOutlet weak var tableView: UITableView!
    var pendingPayments : [PaymentData]?
    @IBOutlet weak var topAmountLabel: UILabel!
    var cardList = NSMutableArray()
    var selectedPaymentMethod : NSDictionary?
    var selectedPendingTransaction: PaymentData!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getTransactionListService()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBarHidden = false
        self.tabBarController?.navigationItem.title = "Balance"
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        
        if self.pendingPayments?.count > 0 {
            return 1
        }
        return 0
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        
        if self.pendingPayments?.count > 0 {
           return (self.pendingPayments?.count)!
        }
        
        return 0
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return 75.0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let pendingBalanceCell = tableView.dequeueReusableCellWithIdentifier("pendingBalanceTableCell", forIndexPath: indexPath) as! PendingBalanceTableCell
        pendingBalanceCell.selectionStyle = .None
        let pendingObj = self.pendingPayments![indexPath.row]
        pendingBalanceCell.programName.text = pendingObj.programName
        pendingBalanceCell.dueCost.text = "$\(pendingObj.dueDateCost) Scheduled on \(pendingObj.dueDate)"
        pendingBalanceCell.payButton.tag = indexPath.row
        pendingBalanceCell.payButton.addTarget(self, action: "payButtonTapped:", forControlEvents: .TouchUpInside)
        Utility.setImageWithURl(pendingObj.programImage, inImageView: pendingBalanceCell.programImageView)
        
        return pendingBalanceCell
    }
    
    // MARK: - Table view delegate
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        
    }
    
    //MARK: - PAY BUTTON TAPPED
    func payButtonTapped(sender: UIButton){
        
        let point = sender.convertPoint(CGPointZero, toView: self.tableView)
        let indexPath = self.tableView.indexPathForRowAtPoint(point)
        
        selectedPendingTransaction = self.pendingPayments![(indexPath?.row)!]
        self.getPaymentMethodListService()
        
    }
    
    func updateCell(cellType: CellToBeUpdate, withObject value: AnyObject?) {
        if cellType == CellToBeUpdate.PaymentMethod {
            selectedPaymentMethod = value as? NSDictionary
            
            if (selectedPaymentMethod?.objectForKey("CustomerId") as! String) == "" {
                self.getStripeToken()
            }
            else {
                
                CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Processing transaction...")
                
                self.createBackendChargeWithToken(nil, completion: { (result: STPBackendChargeResult?, error: NSError?) -> Void in
                    if error != nil {
                        Utility.showAlert(nil, message: (error?.localizedDescription)!, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
                        
                        CSActivityIndicator.hideAllFromMainWindow()
                        
                        return
                    }
                    
                })
            }
        }
    }
    
    //MARK: - Web Service hit
    func getTransactionListService () {
        
        CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Fetching Balance Data...")
        
        let playerListServiceObj = CommonServiceClass(httpRequestType: HTTP_REQUEST_TYPE.POST_REQUEST, requestTag: "GET_BALANCE_SERVICE", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
        
        playerListServiceObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
            
            if errorMessage != nil
            {
                //print(response)
            }
            
            print(response as! NSDictionary)
            let responseDict = PaymentListData(fromDictionary: response as! NSDictionary)
            
            if responseDict.errorCode == 1 {
                
                Utility.showAlert(ALERT_TITLE, message: responseDict.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            }
            else {
                
                
                self.pendingPayments = []
                
                let tempArray = responseDict.data
                
                print(tempArray)
                
                for paymentObj in tempArray {
                    
                    print(paymentObj.dueDateCost)
                    print(paymentObj.programName)
                    
                    if paymentObj.isPaid == "N" {
                        
                        self.pendingPayments?.append(paymentObj)
                    }
                }
                
                let dollarAttributes = [ NSForegroundColorAttributeName: UIColor.whiteColor(),
                    NSFontAttributeName: UIFont(name: "AvenirNext-Regular", size: 40.0)! ]
                
                let dueCostAttributes = [ NSForegroundColorAttributeName: UIColor.whiteColor(),
                    NSFontAttributeName: UIFont(name: "AvenirNext-Regular", size: 70.0)! ]
                
                if self.pendingPayments?.count > 0 {
    
                    let attributedString = NSMutableAttributedString(string: "$", attributes: dollarAttributes)
                    
                    let attributedString1 = NSMutableAttributedString(string: "\(self.pendingPayments![0].dueDateCost)", attributes: dueCostAttributes)
                    
                    attributedString.appendAttributedString(attributedString1)
                    
                    self.topAmountLabel.attributedText = attributedString
                    
                    
                } else  {
                    
                    let attributedString = NSMutableAttributedString(string: "$", attributes: dollarAttributes)
                    
                    let attributedString1 = NSMutableAttributedString(string: "0.0", attributes: dueCostAttributes)
                    
                    attributedString.appendAttributedString(attributedString1)
                    
                    self.topAmountLabel.attributedText = attributedString
                    
                    Utility.showAlert(ALERT_TITLE, message: "No Pending Payments", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
                    
                }
                
                self.tableView.reloadData()

                
            }
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
            
        }
        playerListServiceObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
            print("error ------------------------------\(error)")
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
            
        }
        
        var parameterDict = Dictionary <String, String>()
        parameterDict["cmd"] = "getPlayerPaymentScheduleByDivision"
        parameterDict["player_seq"] = PlayerDetailManager.playerDetail?.playerSeq!
        print("parameterDict \(parameterDict)")

        playerListServiceObj.commonServiceHit(parameterDict)
    }
    
    func getPaymentMethodListService () {
        
        CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Getting payment method list. Please wait..")
        
        let paymentMethodListServiceObj = CommonServiceClass(httpRequestType: HTTP_REQUEST_TYPE.POST_REQUEST, requestTag: "GET_PAYMENT_METHOD_LIST_SERVICE", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
        
        paymentMethodListServiceObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
            
            
            if errorMessage != nil
            {
                //print(response)
            }
            
            print(response as! NSDictionary)
            let responseDict = CSCardList(fromDictionary: response as! NSDictionary)
            
            if responseDict.errorCode == 1 {
                Utility.showAlert(ALERT_TITLE, message: responseDict.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            }
            else {
                
                print(responseDict.data)
                
                if self.cardList.count > 0 {
                    self.cardList.removeAllObjects()
                }
                self.cardList.addObjectsFromArray(responseDict.data)
                
                let paymentMethodViewControllerObj = mainStoryBoard.instantiateViewControllerWithIdentifier("PaymentMethodViewController") as! PaymentMethodViewController
                paymentMethodViewControllerObj.cardList = self.cardList
                paymentMethodViewControllerObj.delegate = self
                
                self.navigationController?.pushViewController(paymentMethodViewControllerObj, animated: true)
                
                CSActivityIndicator.hideAllFromMainWindow()
                
            }
        }
        paymentMethodListServiceObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
            print("error ------------------------------\(error)")
            CSActivityIndicator.hideAllFromMainWindow()
        }
        
        
        var parameterDict = Dictionary <String, String>()
        parameterDict["cmd"] = "getUserCardInfo"
        
        paymentMethodListServiceObj.commonServiceHit(parameterDict)
    }
    
    //MARK:- Stripe service method
    func getStripeToken() {
        
        CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Processing transaction...")
        
        let cardParam = STPCardParams()
        cardParam.number = selectedPaymentMethod?.objectForKey("CardNumber") as? String
        cardParam.expMonth = (selectedPaymentMethod?.objectForKey("ExpiryMonth") as? UInt)!
        cardParam.expYear = (selectedPaymentMethod?.objectForKey("ExpiryYear") as? UInt)!
        cardParam.cvc = selectedPaymentMethod?.objectForKey("CVC") as? String
        cardParam.name = selectedPaymentMethod?.objectForKey("CardHolderName") as? String
        
        STPAPIClient.sharedClient().createTokenWithCard(cardParam) { (token : STPToken?, error : NSError?) -> Void in
            
            if error != nil {
                Utility.showAlert(nil, message: (error?.localizedDescription)!, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
                CSActivityIndicator.hideAllFromMainWindow()
            }
            else {
                
                self.createBackendChargeWithToken(token!, completion: { (result: STPBackendChargeResult?, error: NSError?) -> Void in
                    if error != nil {
                        Utility.showAlert(nil, message: (error?.localizedDescription)!, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
                        
                        CSActivityIndicator.hideAllFromMainWindow()
                        
                        return
                    }
                    
                })
            }
        }
    }
    
    func createBackendChargeWithToken(token: STPToken?, completion: STPTokenSubmissionHandler)
    {
        let stripePaymentServiceObj = CommonServiceClass(httpRequestType: HTTP_REQUEST_TYPE.POST_REQUEST, requestTag: "PAYMENT_SERVICE", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
        
        stripePaymentServiceObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
            
            
            if errorMessage != nil
            {
                //print(response)
            }
            
            print(response as! NSDictionary)
            let responseDict = CSCardList(fromDictionary: response as! NSDictionary)
            
            if responseDict.errorCode == 0 {
                
                print(responseDict.data)
                
                Utility.showAlert(ALERT_TITLE, message: "Payment successfully created!", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
                
                CSActivityIndicator.hideAllFromMainWindow()
                
                self.pendingPayments?.removeAll()
                self.getTransactionListService()
                
            }
            else {
                CSActivityIndicator.hideAllFromMainWindow()
                
                Utility.showAlert(ALERT_TITLE, message: responseDict.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
                
            }
        }
        stripePaymentServiceObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
            print("error ------------------------------\(error)")
            CSActivityIndicator.hideAllFromMainWindow()
        }
        
        
        //        let price = PlayerDetailManager.programSelected?.fee_amount == nil ? "" : (PlayerDetailManager.programSelected?.fee_amount)!
        //        let total = Int(price)! + processingFee
        
        var parameterDict = Dictionary <String, String>()
        
        parameterDict["cmd"] = "make_stripe_payment"
//        parameterDict["amount"] = selectedPendingTransaction.dueDateCost!
        parameterDict["pSeq"] = PlayerDetailManager.playerDetail?.playerSeq
        parameterDict["programSeq"] = selectedPendingTransaction.programSeq!
        parameterDict["paymentPlan"] = selectedPendingTransaction.instalmentType
        parameterDict["paymentScheduleSeq"] = selectedPendingTransaction.paymentScheduleSeq!
        parameterDict["paymentTerms"] = "Y"
        
        if (token == nil) {
            parameterDict["cust_id"] = selectedPaymentMethod?.objectForKey("CustomerId") as? String
        }
        else {
            parameterDict["stripeToken"] = token!.tokenId
            parameterDict["name"] = token!.card!.name
            parameterDict["cust_id"] = ""
        }

        
        print("parameterDict \(parameterDict)")
        
        stripePaymentServiceObj.commonServiceHit(parameterDict)
    }
    
}


class PendingBalanceTableCell : UITableViewCell {
    
    @IBOutlet weak var programImageView: UIImageView!
    @IBOutlet weak var programName: UILabel!
    @IBOutlet weak var dueCost: UILabel!
    @IBOutlet weak var payButton: UIButton!
    
}