//
//  AppleMapView.swift
//  Cinch
//
//  Created by MONEY on 27/02/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit
import MapKit

class AppleMapView: UIView, MKMapViewDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var mapView: MKMapView!
    var locationManager:CLLocationManager!

    var destinationLocation: CLLocationCoordinate2D!
    var source, destination : MKMapItem!
    var directionsResponse : MKDirectionsResponse!
    var isDragablePin: Bool!
    
    var completeAddress = String()
    var sourceLocation: CLLocationCoordinate2D!
    
    var gesture : UILongPressGestureRecognizer!
    var label : UILabel!
    
    convenience init(frame: CGRect, dragablePin : Bool, lastLocation: CLLocationCoordinate2D?) {
        self.init(frame: frame)
        setUpView()
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        self.isDragablePin = dragablePin
        
        if dragablePin {
            
            label = UILabel(frame: CGRectMake(0, self.frame.size.height - 40, MAIN_SCREEN_WIDTH, 40))
            label.backgroundColor = UIColor(red: 23.0/255.0, green: 22/255, blue: 22/255, alpha: 1.0)
            label.font = UIFont(name: "Lato-Regular", size: 16.0)
            label.textColor = UIColor.whiteColor()
            label.textAlignment = .Center

            if lastLocation != nil || locationManager.location != nil {
                label.text = "Drag pin to change location"
                self.addUserLocationPin(lastLocation)
            }
            else {
                gesture = UILongPressGestureRecognizer(target: self, action: Selector("revealRegionDetailsWithLongPressOnMap:"))
                mapView.addGestureRecognizer(gesture)
                
                label.text = "Long press on map to add your location"
            }
            
            self.addSubview(label)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setUpView() {
        
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "AppleMapView", bundle: bundle)
        let mapView = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        
        mapView.frame = self.bounds
        mapView.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        
        self.addSubview(mapView)
    }
    
    //MARK:- Load Map Method
    func loadMapWithLatitude(latitudeStr: String, longitudeStr: String) {
        
        mapView.removeAnnotations(mapView.annotations)
        
        let latitude = Double(latitudeStr)
        let longitude = Double(longitudeStr)
        destinationLocation = CLLocationCoordinate2D(latitude: latitude!, longitude: longitude!)

        let location = CLLocation(latitude: latitude!, longitude: longitude!)

        let regionRadius: CLLocationDistance = 1000

        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
            regionRadius * 2.0, regionRadius * 2.0)
        
        let mapAnnotation = MKPointAnnotation()
        mapAnnotation.coordinate = CLLocationCoordinate2D(latitude: latitude!, longitude: longitude!)
        self.getAddress(mapAnnotation)

        mapView.addAnnotation(mapAnnotation)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    //MARK:- UnLoad Map Method
    func unloadMap() {
        mapView.removeAnnotations(mapView.annotations)
    }
    
    //MARK:- Draw path methods
    func getPath() {
        
        destination = MKMapItem(placemark: MKPlacemark(coordinate: destinationLocation, addressDictionary: nil))
        source = MKMapItem.mapItemForCurrentLocation()
        
        let mapSourceAnnotation = MKPointAnnotation()
        mapSourceAnnotation.coordinate = source.placemark.coordinate
        self.getAddress(mapSourceAnnotation)
        mapView.addAnnotation(mapSourceAnnotation)
        
        let mapDestinationAnnotation = MKPointAnnotation()
        mapDestinationAnnotation.coordinate = destination.placemark.coordinate
        self.getAddress(mapDestinationAnnotation)
        mapView.addAnnotation(mapDestinationAnnotation)
        
        mapView.showAnnotations([mapSourceAnnotation, mapDestinationAnnotation], animated: true)
        
        let request = MKDirectionsRequest()
        request.source = source
        request.destination = destination
        request.requestsAlternateRoutes = false
        
        let directions = MKDirections(request: request)
        
        directions.calculateDirectionsWithCompletionHandler { (response: MKDirectionsResponse?, error: NSError?) -> Void in
            
            if error != nil {
                print(error)
            }
            else {
                self.showRoute(response!)
            }
        }
    }
    
    func showRoute(response: MKDirectionsResponse) {
        for route in response.routes {
            mapView.addOverlay(route.polyline, level: MKOverlayLevel.AboveRoads)
        }
    }
    
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
        
        let render = MKPolylineRenderer(overlay: overlay)
        
        render.strokeColor = UIColor.redColor()
        render.lineWidth = 5.0
        
        return render
    }
    

    //MARK:- MapViewDelegate
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        
        let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "DestinationAnnotation")
        annotationView.image = UIImage(named: "Selected Pin")
        annotationView.canShowCallout = true
        annotationView.draggable = isDragablePin
        
        return annotationView
    }
    
    func mapView(mapView: MKMapView, annotationView view: MKAnnotationView, didChangeDragState newState: MKAnnotationViewDragState, fromOldState oldState: MKAnnotationViewDragState) {
        
        print(oldState.rawValue)
        print((view.annotation?.coordinate)!)
        if oldState == .Starting {
            
            print((view.annotation?.coordinate)!)
            
            let  selectedLocationAnnotation = MKPointAnnotation()
            selectedLocationAnnotation.coordinate = (view.annotation?.coordinate)!
            
            self.mapView.removeAnnotation(view.annotation!)
            self.mapView.addAnnotation(selectedLocationAnnotation)
            self.getAddress(selectedLocationAnnotation)
        }
        
    }

    
    //MARK: Long Press Gesture to add new annotation in MapView
    func revealRegionDetailsWithLongPressOnMap(sender: UILongPressGestureRecognizer) {
        if sender.state != UIGestureRecognizerState.Began { return }
        let touchLocation = sender.locationInView(mapView)
        let locationCoordinate = mapView.convertPoint(touchLocation, toCoordinateFromView: mapView)
        print("Tapped at lat: \(locationCoordinate.latitude) long: \(locationCoordinate.longitude)")
        
        let location = CLLocation(latitude: locationCoordinate.latitude, longitude: locationCoordinate.longitude)
        
        let  selectedLocationAnnotation = MKPointAnnotation()
        selectedLocationAnnotation.coordinate = location.coordinate
        self.mapView.addAnnotation(selectedLocationAnnotation)
        
        self.getAddress(selectedLocationAnnotation)
        
        label.text = "Drag pin to change location"

        mapView.removeGestureRecognizer(gesture)
    }
    
    func addUserLocationPin(lastLocation: CLLocationCoordinate2D?) {
        
        if lastLocation == nil {
            source = MKMapItem.mapItemForCurrentLocation()
        }
        else {
            source = MKMapItem(placemark: MKPlacemark(coordinate: lastLocation!, addressDictionary: nil))
        }
        let mapSourceAnnotation = MKPointAnnotation()
        mapSourceAnnotation.coordinate = source.placemark.coordinate
        self.getAddress(mapSourceAnnotation)
        mapView.addAnnotation(mapSourceAnnotation)
    }
    
    
    
    //MARK:- Get Address for Annotaion
    func getAddress(selectedLocationAnnotation: MKPointAnnotation) {
        
        // Add below code to get address for touch coordinates.
        completeAddress = ""
        sourceLocation = selectedLocationAnnotation.coordinate

        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: selectedLocationAnnotation.coordinate.latitude, longitude: selectedLocationAnnotation.coordinate.longitude)
        
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            
            // Place details
            var placeMark: CLPlacemark!
            placeMark = placemarks?[0]
            
            // Address dictionary
            if placeMark != nil {
                print(placeMark.addressDictionary)
                
                var address = String()
                if let locationName = placeMark.addressDictionary!["Name"] as? NSString {
                    
                    address = "\(locationName), "
                    self.completeAddress = address
                    print(locationName)
                }
                
                // Street address
                if let street = placeMark.addressDictionary!["Thoroughfare"] as? NSString {
                    self.completeAddress += "\(street), "
                }
                
                // City
                if let city = placeMark.addressDictionary!["City"] as? NSString {
                    self.completeAddress += "\(city), "
                }
                
                // Zip code
                if let zip = placeMark.addressDictionary!["ZIP"] as? NSString {
                    self.completeAddress += "\(zip), "
                }
                
                // Country
                if let country = placeMark.addressDictionary!["Country"] as? NSString {
                    selectedLocationAnnotation.title = country as String
                    self.completeAddress += "\(country), "
                    print(country)
                }
                
                if address != "" {
                    address = String(address.characters.dropLast(2))
                    selectedLocationAnnotation.subtitle = address
                    
                    //                self.completeAddress = String(self.completeAddress.characters.dropFirst(2))
                }
            }
            else {
                selectedLocationAnnotation.title = ""
                selectedLocationAnnotation.subtitle = ""
                self.completeAddress = ""
            }
            
        })
    }

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
