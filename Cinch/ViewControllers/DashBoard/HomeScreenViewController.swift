//
//  HomeScreenViewController.swift
//  Cinch
//
//  Created by Amit Garg on 28/02/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class HomeScreenViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    //Segmented Control View
    @IBInspectable var noOfDigit : Int = 4
    @IBInspectable var interspace : CGFloat = 1
    
    @IBOutlet weak var sportsListCollection: UICollectionView!
    @IBInspectable var fontSize: CGFloat = 10.0
    @IBInspectable var fontFamily: String = "Lato-Bold"
    var segmentedButtons = [UIButton]()
    
    
    // User Detail
    @IBOutlet weak var imageViewForProfilePic: UIImageView!
    @IBOutlet weak var labelForName: UILabel!
    @IBOutlet weak var labelForAddress: UILabel!
    
    @IBOutlet weak var labelFoEventName: UILabel!
    @IBOutlet weak var labelForEventAddress: UILabel!
    
    @IBOutlet weak var labelForTemperature: UILabel!
    @IBOutlet weak var labelForFeelingTemp: UILabel!
    @IBOutlet weak var labelForVisibilty: UILabel!

    @IBOutlet weak var labelForBalanceAmt: UILabel!
    @IBOutlet weak var labelForNext: UILabel!
    @IBOutlet weak var labelForNextPaymentSchedule: UILabel!
    
    @IBOutlet weak var labelForMessageCount: UILabel!
    
    let selectedProgram = ProgramData()

    var playerDashboardData = [PlayerSportDetail]()
    var buttonTitle = ["ALL", "SPORT 1", "SPORT 2", "SPORT 3", "SOCCER PROGRAM"]
    var cellSelectedIndex = 0
    
    dynamic var selectedPlayer : PlayerData?

    override func viewDidLoad() {
        super.viewDidLoad()

        selectedPlayer = PlayerDetailManager.playerDetail
        
        imageViewForProfilePic.layer.cornerRadius = imageViewForProfilePic.frame.width / 2
        imageViewForProfilePic.clipsToBounds = true
        
        Utility.setImageWithURl((selectedPlayer?.profilePic)!, inImageView: imageViewForProfilePic)
        labelForName.text = (selectedPlayer?.fname)! + " " +  (selectedPlayer?.lname == nil ? "" : (selectedPlayer?.lname)!)
        labelForAddress.text = PlayerDetailManager.playerAddress
        
        let backbutton = UIBarButtonItem(title: "", style: .Plain, target: nil
            , action: nil)
        self.tabBarController!.navigationItem.backBarButtonItem = backbutton
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        Utility.setBackButton(forViewController: self, withTitle: "")
        self.tabBarController?.navigationItem.title = "Home"
        self.tabBarController?.navigationItem.rightBarButtonItem = nil
        
        self.hitPlayerDashboardWebService()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setSelected(index: Int) {
        cellSelectedIndex = index
        
        self.selectedProgram.programSeq = playerDashboardData[index].programSeq
        self.selectedProgram.name = playerDashboardData[index].programName
        PlayerDetailManager.programSelected = self.selectedProgram
        
        func dateWithDayAndMonth(dateStr: String?) -> String {
            if dateStr == nil {
                return ""
            }
            
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "YYYY-MM-dd"
            
            let date = dateFormatter.dateFromString(dateStr!)
            print(date)
            dateFormatter.dateFormat = "MM/dd"
            
            print(dateFormatter.stringFromDate(date!))
            
            return dateFormatter.stringFromDate(date!) + " "
        }
        
        func dateWithDayMonthYear(dateStr: String?) -> String {
            if dateStr == nil {
                return ""
            }
            
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "YYYY-MM-dd"
            
            let date = dateFormatter.dateFromString(dateStr!)
            print(date)
            dateFormatter.dateStyle = .ShortStyle

            print(dateFormatter.stringFromDate(date!))

            return dateFormatter.stringFromDate(date!)

        }
        
        func timeWithHourMin(timeStr: String?) -> String {
            if timeStr == nil {
                return ""
            }
            
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "HH:mm:ss"
            
            let date = dateFormatter.dateFromString(timeStr!)
            print(date)
            dateFormatter.dateFormat = "h:mm a"
            
            print(dateFormatter.stringFromDate(date!))
            
            return dateFormatter.stringFromDate(date!) + " @ "
            
        }
        
        var eventName = String()
        var eventAddress = String()
        if playerDashboardData[index].events != nil {
            eventName = dateWithDayAndMonth(playerDashboardData[index].events.eventDate) + (playerDashboardData[index].events.eventName == nil ? "" : playerDashboardData[index].events.eventName)
            
            eventAddress = timeWithHourMin(playerDashboardData[index].events.startTime) + (playerDashboardData[index].events.address == nil ? "" : playerDashboardData[index].events.address!)
        }
        
        labelFoEventName.text = eventName
        labelForEventAddress.text = eventAddress
        
//        labelForTemperature.text = ""
//        labelForFeelingTemp.text = ""
//        labelForVisibilty.text = ""
        
        if playerDashboardData[index].paymentSchedule != nil {
            labelForBalanceAmt.text = (playerDashboardData[index].paymentSchedule.dueDateCost == nil ? "" : playerDashboardData[index].paymentSchedule.dueDateCost!)
            labelForNextPaymentSchedule.text = "Schedule: " + dateWithDayMonthYear(playerDashboardData[index].paymentSchedule.dueDate)
        }
        else {
            labelForBalanceAmt.text = ""
            labelForNextPaymentSchedule.text = ""
        }
        
//        labelForMessageCount.text = 

        
        self.sportsListCollection.reloadData()
    }
    
    // MARK: - Collection view datasource
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return playerDashboardData.count
    }
    
    // MARK: - Collection view delegate
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
//        if playerDashboardData.count == 1 {
//            let frame = CGSizeMake(MAIN_SCREEN_WIDTH ,40.0)
//            return frame
//        }
        
        let programTitle = playerDashboardData[indexPath.row].programName
        let size = programTitle.sizeWithAttributes([NSFontAttributeName: UIFont(name: "Lato-Bold", size: 10.0)!])
        
        var cellWidth:CGFloat = 93.75
        
        if size.width > 93.75  {
            cellWidth = size.width + 20
        }
        
        let frame = CGSizeMake(cellWidth ,40.0)
        
        return frame
    }
    
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("programCell", forIndexPath: indexPath) as! ProgramCollectionCell
        
        cell.titleLabel.text = playerDashboardData[indexPath.row].programName!
        cell.layer.borderColor = UIColor(red: 151/255, green: 151/255, blue: 151/255, alpha: 1.0).CGColor
        cell.layer.borderWidth = 0.5
        
        if cellSelectedIndex == indexPath.row {
            cell.titleLabel.backgroundColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1.0)
        } else {
            cell.titleLabel.backgroundColor = UIColor.whiteColor()
        }
        
        return cell
        
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        setSelected(indexPath.row)
        
        if playerDashboardData[indexPath.row].events != nil {
            
            PlayerDetailManager.eventSelected = playerDashboardData[indexPath.row].events
            self.hitWeatherWebService(playerDashboardData[indexPath.row].events)
        }
    }
    
    //MARK: - Button Actions
    @IBAction func newProgramButtonAction(sender: AnyObject) {
        
        let addProgramVC = mainStoryBoard.instantiateViewControllerWithIdentifier("AddProgramViewController") as! AddProgramViewController

        addProgramVC.isComingFrom = ComingFrom.Dashboard
        addProgramVC.playerList = [selectedPlayer!]
        addProgramVC.addressDetail = FatherAdd(fromPlayerDetail: selectedPlayer!)
        addProgramVC.mSeq = (selectedPlayer?.motherSeq)!
        addProgramVC.fSeq = (selectedPlayer?.fatherSeq)!
        addProgramVC.gSeq = (selectedPlayer?.guardianSeq)!
        addProgramVC.selectedPlayer = selectedPlayer

        self.navigationController?.pushViewController(addProgramVC, animated: true)
        
    }
    
    @IBAction func chatBtnAction(sender: AnyObject) {
//        let messageViewControllerObj = amitStoryBoard.instantiateViewControllerWithIdentifier("MessageViewController") as! MessageViewController
//        
//        messageViewControllerObj.senderDisplayName = labelForName.text
//        
//        let chatNodeID: String!
//        if Int(UserProfileManager.userSequence!) < Int((PlayerDetailManager.playerDetail?.playerSeq!)!) {
//            chatNodeID = ("\(UserProfileManager.userSequence!)-\((PlayerDetailManager.playerDetail?.playerSeq!)!)")
//        }
//        else {
//            chatNodeID = ("\((PlayerDetailManager.playerDetail?.playerSeq!)!) - \(UserProfileManager.userSequence!)")
//        }
//        
//        messageViewControllerObj.chatNodeID = chatNodeID
//        messageViewControllerObj.receiverNode = (PlayerDetailManager.playerDetail?.playerSeq!)!
//        messageViewControllerObj.receiverId = (PlayerDetailManager.playerDetail?.playerSeq!)!
//        messageViewControllerObj.profilePicUrl = (selectedPlayer?.profilePic)!
//        
//        self.navigationController?.pushViewController(messageViewControllerObj, animated: true)
        
        self.tabBarController?.selectedIndex = 3
        for tabItem in self.tabBarController!.tabBar.items! {
            tabItem.imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
        self.tabBarController!.tabBar.selectedItem?.imageInsets = UIEdgeInsets(top: 10, left: 0, bottom: -10, right: 0)

    }
    
    
    func backAction() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func locationBtnAction(sender: AnyObject) {
        
        self.tabBarController?.selectedIndex = 1
        for tabItem in self.tabBarController!.tabBar.items! {
            tabItem.imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
        self.tabBarController!.tabBar.selectedItem?.imageInsets = UIEdgeInsets(top: 10, left: 0, bottom: -10, right: 0)
    }
    
    @IBAction func temperatureBtnAction(sender: AnyObject) {
    }
    
    @IBAction func balanceBtnAction(sender: AnyObject) {
        
        let balanceRootViewObj = mainStoryBoard.instantiateViewControllerWithIdentifier("BalanceRootTabBarController")
        
        self.navigationController?.pushViewController(balanceRootViewObj, animated: true)
        
    }
    
    @IBAction func messageBtnAction(sender: AnyObject) {
        self.tabBarController?.selectedIndex = 3
        for tabItem in self.tabBarController!.tabBar.items! {
            tabItem.imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
        self.tabBarController!.tabBar.selectedItem?.imageInsets = UIEdgeInsets(top: 10, left: 0, bottom: -10, right: 0)
    }
    
    //MARK:- Hit Webservice
    func hitPlayerDashboardWebService() {
        
        CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Fetching Details...")
        
        let playerDashboardObj = CommonServiceClass(httpRequestType: HTTP_REQUEST_TYPE.POST_REQUEST, requestTag: "PLAYER_DASHBOARD", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
        
        playerDashboardObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
            
            if errorMessage != nil
            {
                print(response)
            }
            print(response as! NSDictionary)
            
            let playerDashboardDataObj =  CSPlayerDashboard(fromDictionary: response as! NSDictionary)
            
            
            if playerDashboardDataObj.errorCode == 0 {
                
                self.playerDashboardData = playerDashboardDataObj.data
                self.setSelected(0)

            } else {
                
                Utility.showAlert(ALERT_TITLE, message: playerDashboardDataObj.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            }
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
        }
        playerDashboardObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
            
            print("error ------------------------------\(error)")
        }
        
        
        var parameterDict = Dictionary <String, String>()

        parameterDict["cmd"] = "getPlayerDashboardData"
        parameterDict["player_seq"] = PlayerDetailManager.playerDetail?.playerSeq
        
        playerDashboardObj.commonServiceHit(parameterDict)
    }
    
    func hitWeatherWebService(eventDetail: EventDetail) {
        
        CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Fetching Details...")
        
        let weatherObj = CommonServiceClass(httpRequestType: HTTP_REQUEST_TYPE.GET_REQUEST, requestTag: "WEATHER_DATA", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
        
        weatherObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
            
            if errorMessage != nil
            {
                print(response)
            }
            print(response as! NSDictionary)
            
            let weatherDataObj =  WeatherData(fromDictionary: response as! NSDictionary)
            
            self.labelForTemperature.text = String(weatherDataObj.main.temp) + "\u{00B0}"
            self.labelForFeelingTemp.text = "FEELS LIKE " + String(weatherDataObj.main.tempMax)
            self.labelForVisibilty.hidden = true
//                Utility.showAlert(ALERT_TITLE, message: playerDashboardDataObj.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
        }
        weatherObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
            
            print("error ------------------------------\(error)")
        }
        
        
        var parameterDict = Dictionary <String, String>()

        parameterDict["APPID"] = "dcd34d1fef9226c87bc60137ae0b4399"
        parameterDict["lat"] = eventDetail.lat
        parameterDict["lon"] = eventDetail.lng
        parameterDict["units"] = "metric"
        weatherObj.weatherHit(parameterDict)
    }
}

class ProgramCollectionCell : UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
        
}


