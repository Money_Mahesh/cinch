//
//	PaymentData.swift
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class PaymentData{

	var divisionSeq : String!
	var dueDate : String!
	var dueDateCost : String!
	var instalmentType : String!
	var isPaid : String!
	var paidDate : String!
	var paymentScheduleSeq : String!
	var programImage : String!
	var programName : String!
	var programSeq : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		divisionSeq = dictionary["division_seq"] as? String
		dueDate = dictionary["due_date"] as? String
		dueDateCost = dictionary["due_date_cost"] as? String
		instalmentType = dictionary["instalment_type"] as? String
		isPaid = dictionary["is_paid"] as? String
		paidDate = dictionary["paid_date"] as? String
		paymentScheduleSeq = dictionary["payment_schedule_seq"] as? String
		programImage = dictionary["program_image"] as? String
		programName = dictionary["program_name"] as? String
		programSeq = dictionary["program_seq"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if divisionSeq != nil{
			dictionary["division_seq"] = divisionSeq
		}
		if dueDate != nil{
			dictionary["due_date"] = dueDate
		}
		if dueDateCost != nil{
			dictionary["due_date_cost"] = dueDateCost
		}
		if instalmentType != nil{
			dictionary["instalment_type"] = instalmentType
		}
		if isPaid != nil{
			dictionary["is_paid"] = isPaid
		}
		if paidDate != nil{
			dictionary["paid_date"] = paidDate
		}
		if paymentScheduleSeq != nil{
			dictionary["payment_schedule_seq"] = paymentScheduleSeq
		}
		if programImage != nil{
			dictionary["program_image"] = programImage
		}
		if programName != nil{
			dictionary["program_name"] = programName
		}
		if programSeq != nil{
			dictionary["program_seq"] = programSeq
		}
		return dictionary
	}

}