//
//	ChatDetail.swift
//
//	Create by Money Mahesh on 2/4/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class ChatDetail : NSObject, NSCoding{
    
	var chatTitle : String!
	var chatType : Int!
	var imageURL : String!
	var message : String!
	var senderId : Int!
	var senderName : String!
	var timeStamp : Int!
    var uname : String!
    var messageImg : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		chatTitle = dictionary["chatTitle"] as? String
        chatType = dictionary["chatType"] as? Int
		imageURL = dictionary["imageURL"] as? String
		message = dictionary["message"] as? String
		senderId = dictionary["senderId"] as? Int
		senderName = dictionary["senderName"] as? String
		timeStamp = dictionary["timeStamp"] as? Int
        uname = dictionary["uname"] as? String
        messageImg = dictionary["messageImg"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if chatTitle != nil{
			dictionary["chatTitle"] = chatTitle
		}
		if chatType != nil{
			dictionary["chatType"] = chatType
		}
		if imageURL != nil{
			dictionary["imageURL"] = imageURL
		}
		if message != nil{
			dictionary["message"] = message
		}
		if senderId != nil{
			dictionary["senderId"] = senderId
		}
		if senderName != nil{
			dictionary["senderName"] = senderName
		}
		if timeStamp != nil{
			dictionary["timeStamp"] = timeStamp
		}
        if uname != nil{
            dictionary["uname"] = uname
        }
        if messageImg != nil{
            dictionary["messageImg"] = messageImg
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         chatTitle = aDecoder.decodeObjectForKey("chatTitle") as? String
         chatType = aDecoder.decodeObjectForKey("chatType") as? Int
         imageURL = aDecoder.decodeObjectForKey("imageURL") as? String
         message = aDecoder.decodeObjectForKey("message") as? String
         senderId = aDecoder.decodeObjectForKey("senderId") as? Int
         senderName = aDecoder.decodeObjectForKey("senderName") as? String
         timeStamp = aDecoder.decodeObjectForKey("timeStamp") as? Int
        uname = aDecoder.decodeObjectForKey("uname") as? String
        messageImg = aDecoder.decodeObjectForKey("messageImg") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if chatTitle != nil{
			aCoder.encodeObject(chatTitle, forKey: "chatTitle")
		}
		if chatType != nil{
			aCoder.encodeObject(chatType, forKey: "chatType")
		}
		if imageURL != nil{
			aCoder.encodeObject(imageURL, forKey: "imageURL")
		}
		if message != nil{
			aCoder.encodeObject(message, forKey: "message")
		}
		if senderId != nil{
			aCoder.encodeObject(senderId, forKey: "senderId")
		}
		if senderName != nil{
			aCoder.encodeObject(senderName, forKey: "senderName")
		}
		if timeStamp != nil{
			aCoder.encodeObject(timeStamp, forKey: "timeStamp")
		}
        if uname != nil{
            aCoder.encodeObject(uname, forKey: "uname")
        }
        if messageImg != nil{
            aCoder.encodeObject(messageImg, forKey: "messageImg")
        }

	}

}