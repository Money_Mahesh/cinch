//
//  AccountStatus.swift
//  Cinch
//
//  Created by MONEY on 01/04/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import Foundation

class AccountStatus : NSObject, NSCoding{
    
    var isMobileVerified : String!
    var isUserVerified : String!
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: NSDictionary){
        isMobileVerified = dictionary["is_mobile_verified"] as? String
        isUserVerified = dictionary["is_user_verified"] as? String
    }
    
    /**
     * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> NSDictionary
    {
        let dictionary = NSMutableDictionary()
        if isMobileVerified != nil{
            dictionary["is_mobile_verified"] = isMobileVerified
        }
        if isUserVerified != nil{
            dictionary["is_user_verified"] = isUserVerified
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        isMobileVerified = aDecoder.decodeObjectForKey("is_mobile_verified") as? String
        isUserVerified = aDecoder.decodeObjectForKey("is_user_verified") as? String
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encodeWithCoder(aCoder: NSCoder)
    {
        if isMobileVerified != nil{
            aCoder.encodeObject(isMobileVerified, forKey: "is_mobile_verified")
        }
        if isUserVerified != nil{
            aCoder.encodeObject(isUserVerified, forKey: "is_user_verified")
        }

    }
    
}