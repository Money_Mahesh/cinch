//
//  SignInViewController.swift
//  Cinch
//
//  Created by MONEY on 14/02/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class SignInViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var textFieldForEmail: AGFloatLabelTextField!
    @IBOutlet weak var textFieldForPassword: AGFloatLabelTextField!
    
    @IBOutlet weak var containerView: UIScrollView!
    var parentScrollViewRef : UIScrollView!
    var parentController: UIViewController!
    
    var userEmail : String?
    var userPassword : String?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func refreshScreen() {
        textFieldForEmail.text = ""
        textFieldForPassword.text = ""
    }
    
    //MARK: - IBActions
    @IBAction func forgotPasswordAction(sender: AnyObject) {
        self.view.endEditing(true)
        
        let forgetPasswordViewControllerObj = mainStoryBoard.instantiateViewControllerWithIdentifier("ForgetPasswordViewController")
        
        let navCont = UINavigationController(rootViewController: forgetPasswordViewControllerObj)
        
        self.parentController?.presentViewController(navCont, animated:
            true, completion:nil)
        

    }
    
    @IBAction func signInAction(sender: AnyObject) {
        
        userEmail = textFieldForEmail.text
        userPassword = textFieldForPassword.text
        
        if userEmail!.characters.count == 0 {
            Utility.showAlert(nil, message: "Please enter the email id", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
        }
        else if DataValidationUtility.isValidEmail(userEmail!) == false {
            Utility.showAlert(nil, message: "Please enter the valid email id", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
        }
        else if userEmail!.characters.count == 0 {
            Utility.showAlert(nil, message: "Please enter the password", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
        }
        else {
            self.view.endEditing(true)
            self.hitSignInWebService(CSAccountType.Simple)
        }
    }
    
    
    @IBAction func facebookAction(sender: AnyObject) {
        self.view.endEditing(true)

        textFieldForEmail.text = ""
        textFieldForPassword.text = ""
        
        CSActivityIndicator.showToMainViewWithMessage("Fetching facebook account details.")

        FacebookClass.loginToFacebookWithSuccess(ROOT_NAVVIGATION_CONROLLER.topViewController!, successBlock: { () -> () in
            
            CSActivityIndicator.hideAllFromMainView()
            
            self.hitSignInWebService(CSAccountType.Facebook)

            },andFailure: { (error: NSError?) -> () in
                CSActivityIndicator.hideAllFromMainView()

                print("Error \(error)")
        })
    }
    
    @IBAction func signUpAction(sender: AnyObject) {
        self.view.endEditing(true)

        parentScrollViewRef.setContentOffset(CGPointMake(MAIN_SCREEN_WIDTH, 0), animated: true)
    }
    
    //MARK:- Hit Webservice
    func hitSignInWebService(accountType: CSAccountType) {
        
        CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Signing in")
        
        let signInObj = RegisterationService(httpRequestType: HTTP_REQUEST_TYPE.POST_REQUEST, requestTag: "SIGN_IN_SERVICE", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
        
        signInObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
            
            if errorMessage != nil
            {
                print(response)
            }
            
            let signInData =  CSSignIn(fromDictionary: response as! NSDictionary)
            if signInData.errorCode == 0 {
                
                if (signInData.data.count != 0) {
                    print(signInData.data[0].registrationStatus)
                    
                    let userData = signInData.data[0]
                    
                    UserProfileManager.userSequence = userData.userSeq
                    UserProfileManager.name = userData.name
                    UserProfileManager.emailId = userData.email
                    UserProfileManager.role = userData.role
                    UserProfileManager.status = userData.status
                    UserProfileManager.country = userData.country
                    UserProfileManager.phoneNo = userData.mobile
                    UserProfileManager.imageUrl = userData.profilePic
                    UserProfileManager.status = userData.status
                    UserProfileManager.isFacebook = userData.loginType == "S" ? "N" : "Y"
                    UserProfileManager.invitationSeq = userData.invitationSeq
                    
                    
                    if userData.registrationStatus == "1" {
                        if userData.isMobileVerified == "N" && userData.isUserVerified == "N" {
                            
                            let createAccountViewControllerObj = amitStoryBoard.instantiateViewControllerWithIdentifier("CreateAccountViewController") as! CreateAccountViewController
                            
                            Utility.saveStringInDefault(SIGN_UP_STATUS, value: "CreateAccountViewController")
                            
                            ROOT_NAVVIGATION_CONROLLER.topViewController?.navigationController!.pushViewController(createAccountViewControllerObj, animated: true)
                        }
                        else {
                            let addPlayerViewControllerObj = amitStoryBoard.instantiateViewControllerWithIdentifier("AddPlayerViewController") as! AddPlayerViewController
                            
                            Utility.saveStringInDefault(SIGN_UP_STATUS, value: "AddPlayerViewController")
                            
                            ROOT_NAVVIGATION_CONROLLER.topViewController?.navigationController!.pushViewController(addPlayerViewControllerObj, animated: true)
                            
                        }
                    }
                    else {
                        let sWRevealViewControllerObj = amitStoryBoard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
                        
                        Utility.saveStringInDefault(SIGN_UP_STATUS, value: "SWRevealViewController")
                        
                        ROOT_NAVVIGATION_CONROLLER.topViewController?.navigationController!.pushViewController(sWRevealViewControllerObj, animated: true)
                    }
                }
                else {
                    Utility.showAlert(ALERT_TITLE, message: "Data not found", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
                }
                
            } else {
                
                Utility.showAlert(ALERT_TITLE, message: signInData.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            }
            
            print(response as! NSDictionary)
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
        }
        signInObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)

            print("error ------------------------------\(error)")
        }
        
        var parameterDict = Dictionary <String, String>()
        
        parameterDict["cmd"] = "loginUser"
        parameterDict["email"] = accountType == .Simple ? userEmail : UserProfileManager.emailId
        parameterDict["passwd"] = accountType == .Simple ? userPassword : "*"
        parameterDict["api_id"] = accountType == .Simple ? "" : UserProfileManager.fbID
        parameterDict["login_type"] = accountType == .Simple ? "" : "F"
        
        signInObj.signInHit(parameterDict)
    }
    
    func hitForgotPassswordWebService() {
        
        let forgotPwdObj = RegisterationService(httpRequestType: HTTP_REQUEST_TYPE.POST_REQUEST, requestTag: "FORGOT_PASSWORD_SERVICE", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
        
        forgotPwdObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
            
            if errorMessage != nil
            {
                //print(response)
            }
            
            print(response as! NSDictionary)
            let responseDict = response as! NSDictionary
            
            if responseDict.objectForKey("errorCode") as! NSNumber == 1 {
                Utility.showAlert(ALERT_TITLE, message: responseDict.objectForKey("errorMsg") as! String, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            }
            else {
                
            }
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)

        }
        forgotPwdObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
            print("error ------------------------------\(error)")
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)

        }
        
        
        var parameterDict = Dictionary <String, String>()
        
        parameterDict["cmd"] = "mailLinkToResetPassword"
        parameterDict["email"] = textFieldForEmail.text
        
        for keyString in parameterDict.keys {
            print("Key \(keyString) Value \(parameterDict[keyString])")
        }
        
        forgotPwdObj.forgotPasswordHit(parameterDict)
    }

    //Facebook Methods
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
