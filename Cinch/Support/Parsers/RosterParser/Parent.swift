//
//	Parent.swift
//
//	Create by Money Mahesh on 21/3/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class Parent : NSObject, NSCoding{

    var parentData = [Father]()
	var father : [Father]!
	var mother : [Father]!
    var guardian : [Father]!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		father = [Father]()
		if let fatherArray = dictionary["father"] as? [NSDictionary]{
			for dic in fatherArray{
				let value = Father(fromDictionary: dic)
				father.append(value)
                parentData.append(value)

			}
		}
		mother = [Father]()
		if let motherArray = dictionary["mother"] as? [NSDictionary]{
			for dic in motherArray{
				let value = Father(fromDictionary: dic)
				mother.append(value)
                parentData.append(value)

			}
		}
        
        guardian = [Father]()
        if let guardianArray = dictionary["guardian"] as? [NSDictionary]{
            for dic in guardianArray{
                let value = Father(fromDictionary: dic)
                guardian.append(value)
                parentData.append(value)
                
            }
        }
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if father != nil{
			var dictionaryElements = [NSDictionary]()
			for fatherElement in father {
				dictionaryElements.append(fatherElement.toDictionary())
			}
			dictionary["father"] = dictionaryElements
		}
		if mother != nil{
			var dictionaryElements = [NSDictionary]()
			for motherElement in mother {
				dictionaryElements.append(motherElement.toDictionary())
			}
			dictionary["mother"] = dictionaryElements
		}
        if guardian != nil{
            var dictionaryElements = [NSDictionary]()
            for guardianElement in guardian {
                dictionaryElements.append(guardianElement.toDictionary())
            }
            dictionary["guardian"] = dictionaryElements
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         father = aDecoder.decodeObjectForKey("father") as? [Father]
         mother = aDecoder.decodeObjectForKey("mother") as? [Father]
        guardian = aDecoder.decodeObjectForKey("guardian") as? [Father]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if father != nil{
			aCoder.encodeObject(father, forKey: "father")
		}
		if mother != nil{
			aCoder.encodeObject(mother, forKey: "mother")
		}
        if guardian != nil{
            aCoder.encodeObject(guardian, forKey: "guardian")
        }

	}

}