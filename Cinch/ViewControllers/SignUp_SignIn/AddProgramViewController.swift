//
//  AddProgramViewController.swift
//  Cinch
//
//  Created by MONEY on 20/02/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class AddProgramViewController: UIViewController, PickerCellDelegate, CollectionGridDelegate {
    
    let unSelectedPlayerColor = UIColor(red: 217/255, green: 217/255, blue: 217/255, alpha: 1.0)
    let selectedPlayerColor = UIColor(red: 46.0/255, green: 51.0/255, blue: 47.0/255, alpha: 1.0)

    @IBOutlet weak var tableView: UITableView!
    
//    @IBOutlet var sectionHeaderView: SectionHeader!
    
    let customPickerViewObj = CustomPickerView()
    
    var tableContentArray = NSMutableArray()
//    var selectedPlayerIndex = Int(0)
    
    var clubList = NSMutableArray()
    var programList = NSMutableArray()
    var playerList = NSMutableArray()

    var addressDetail : FatherAdd?

    var mSeq: String = ""
    var fSeq: String = ""
    var gSeq: String = ""

    var selectedClub: ClubData?
    var selectedPlayer: PlayerData?
    var selectedProgram: ProgramData?
    var isSelectPlayerPicker = true
    var isComingFrom = ComingFrom.SignUp
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Utility.setBackButton(forViewController: self, withTitle: "")

        self.title = "Add a Program"

        customPickerViewObj.isParentNavigationBarPresent = (isComingFrom == .SignUp ? true : false)
        
        //Table Footer View
        let tableFooterView = UIView(frame: CGRectMake(0, 0, MAIN_SCREEN_WIDTH, 1))
        tableFooterView.backgroundColor = UIColor(red: 238.0/255.0, green: 238.0/255.0, blue: 238.0/255.0, alpha: 1.0)
        tableView.tableFooterView = tableFooterView
        
        //Register Cell
        tableView.registerNib(UINib(nibName: "SectionHeaderWithLabelAndButton", bundle: nil), forHeaderFooterViewReuseIdentifier: "SectionHeaderWithLabelAndButton")
        tableView.registerNib(UINib(nibName: "CellWithLabelAndButtonTableViewCell", bundle: nil), forCellReuseIdentifier: "SelectPlayerCell")
        tableView.registerNib(UINib(nibName: "ExpandableTableViewCell", bundle: nil), forCellReuseIdentifier: "ExpandableTableViewCell")
        tableView.registerNib(UINib(nibName: "SwitchTableViewCell", bundle: nil), forCellReuseIdentifier: "SwitchTableViewCell")
        
        if revealViewController() != nil {
            Utility.setMenuButton(forViewController: self, revealController: revealViewController())
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        } else {
            Utility.setBackButton(forViewController: self, withTitle: "")
        }
        
        if isComingFrom == ComingFrom.Dashboard || isComingFrom == ComingFrom.AddPlayerSideMenu || isComingFrom == ComingFrom.SignUp {
            self.getClubListService()
            self.createTableComponent()
        }
        else {
            self.getPlayerListService()
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.hidden = false
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.hidden = true
    }
    
    func createTableComponent() {
        
//        if playerList.count > 0 {
//            //Select Player
//            let selectPlayerCellArray = NSMutableArray()
//            
//            for index in 0..<self.playerList.count {
//                let selectPlayerCell = tableView.dequeueReusableCellWithIdentifier("SelectPlayerCell") as! CellWithLabelAndButtonTableViewCell
//                selectPlayerCell.selectionStyle = UITableViewCellSelectionStyle.None
//                selectPlayerCell.actionButton.enabled = false
//                selectPlayerCell.actionButton.hidden = true
//                
//                if index == 0 {
//                    selectPlayerCell.titleLabel.textColor = selectedPlayerColor
//                }
//                else {
//                    selectPlayerCell.titleLabel.textColor = unSelectedPlayerColor
//                }
//                
//                selectedPlayerIndex = 0
//                selectedPlayer = playerList.objectAtIndex(selectedPlayerIndex) as? PlayerData
//                
//                let playerData = playerList.objectAtIndex(index) as! PlayerData
//                selectPlayerCell.titleLabel.text = "\(playerData.fname == nil ? "" : playerData.fname) \(playerData.lname == nil ? "" : playerData.lname)"
//                selectPlayerCellArray.addObject(selectPlayerCell)
//            }
//            tableContentArray.addObject(self.createCellContentArrayDict("SELECT YOUR PLAYER", withAsseccoryImage: true, cell: selectPlayerCellArray))
//            
//        }
        
        var selectPlayerNameArray = [String]()
        
        for index in 0..<self.playerList.count {
            selectPlayerNameArray.append(((playerList.objectAtIndex(index) as! PlayerData).fname)! + " " + ((playerList.objectAtIndex(index) as! PlayerData).lname)!)
        }

        let createSelectPlayerPickerCell = PickerTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "SelectPlayerPickerCell", componentTitleArray: nil, arrayTobeLoadedInPicker: [selectPlayerNameArray], selectedRowIndexs: nil, customPickerViewObj : customPickerViewObj, placeHolderValue: (selectPlayerNameArray.count == 1 ? selectPlayerNameArray[0] : "SELECT YOUR PLAYER"), mainViewDelegate: self, withSupplementaryImage: (selectPlayerNameArray.count == 1 ? "" : "Down Arrow Icon"))
        
        tableContentArray.addObject(self.createCellContentDict("", withAsseccoryImage: true, cell: createSelectPlayerPickerCell))
        
        //Picker Cell
        var clubNameArray = [String]()
        for clubData in self.clubList {
            clubNameArray.append((clubData as! ClubData).clubName)
        }
        
        let pickerContentArray = [clubNameArray]
        
        print(pickerContentArray)
        
        let createSelectOrgPickerCell = PickerTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "SelectOrganisationPickerCell", componentTitleArray: nil, arrayTobeLoadedInPicker: pickerContentArray, selectedRowIndexs: nil, customPickerViewObj : customPickerViewObj, placeHolderValue: "SELECT ORGANIZATION", mainViewDelegate: self, withSupplementaryImage: "Down Arrow Icon")
        
        tableContentArray.addObject(self.createCellContentDict("", withAsseccoryImage: true, cell: createSelectOrgPickerCell))
        
        self.tableView.reloadData()
    }
    
    func createVolunteerCellAtLast() {
        //Create Volunteer Switch Cell
        
        let volunteerSwitchCell = tableView.dequeueReusableCellWithIdentifier("SwitchTableViewCell") as! SwitchTableViewCell
        
        tableContentArray.addObject(self.createCellContentDict("", withAsseccoryImage: false, cell: volunteerSwitchCell))
    }
    
    func createProgramDescriptionCellAtIndex(index : Int) {
        //Program Description Cell
        let programDescriptionCell = tableView.dequeueReusableCellWithIdentifier("ExpandableTableViewCell") as! ExpandableTableViewCell
        programDescriptionCell.selectionStyle = UITableViewCellSelectionStyle.None
        programDescriptionCell.accessoryImageView?.hidden = false
        
        programDescriptionCell.titleLabel.textColor = UIColor(red: 119/255, green: 119/255, blue: 119/255, alpha: 1.0)
        programDescriptionCell.titleLabel.text = "PROGRAM DESCRIPTION"
        
        tableContentArray.insertObject(self.createCellContentDict("", withAsseccoryImage: false, cell: programDescriptionCell), atIndex: index)
    }
    
    func createProgramGridCellAtIndex(index : Int) {
        //CollectionGridCell
        let collectionGridCell = CollectionGridTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "SelectProgramGridCell", collectionViewSize: CGSizeMake( MAIN_SCREEN_WIDTH, 100), cellSize: CGSizeMake(125, 75), selectedCellIndex: 0, andCellDetailArray : self.programList, delegate: self)
        
        self.tableContentArray.insertObject(self.createCellContentDict("SELECT PROGRAM", withAsseccoryImage: true, cell: collectionGridCell), atIndex: index)

    }
    
    // MARK:- TableView Data Source and Delegates
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return tableContentArray.count
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if (tableContentArray.objectAtIndex(section)["sectionDetail"] as! NSDictionary).objectForKey("leftHeader") as! String != "" {
            
            return 50
        }
        return 0
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if (tableContentArray.objectAtIndex(section)["sectionDetail"] as! NSDictionary).objectForKey("leftHeader") as! String != "" {
            
            let headerView = tableView.dequeueReusableHeaderFooterViewWithIdentifier("SectionHeaderWithLabelAndButton") as! SectionHeaderWithLabelAndButton
            
            headerView.contentView.backgroundColor = UIColor.whiteColor()
            headerView.button.userInteractionEnabled = false
            headerView.lowerSeperator.hidden = true
            headerView.label.text = (tableContentArray.objectAtIndex(section)["sectionDetail"] as! NSDictionary).objectForKey("leftHeader") as? String
            return headerView
        }
        else {
            return UIView(frame: CGRectMake(0, 0, 0, 0))
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        if (tableContentArray.objectAtIndex(indexPath.section) as! NSDictionary).objectForKey("cell")?.isKindOfClass(CollectionGridTableViewCell.self) == true {
            return 100
        }
        
        if (tableContentArray.objectAtIndex(indexPath.section) as! NSDictionary).objectForKey("cell")?.isKindOfClass(ExpandableTableViewCell) == true {
            
            let cell = ((tableContentArray.objectAtIndex(indexPath.section) as! NSDictionary).objectForKey("cell") as! ExpandableTableViewCell)
            
            if cell.isExpanded {
                return 210
            }
            else {
                return 50

            }
        }
        return 50
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 97
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if (tableContentArray.objectAtIndex(section) as! NSDictionary).objectForKey("cell")?.isKindOfClass(NSMutableArray) == true {
            return ((tableContentArray.objectAtIndex(section) as! NSDictionary).objectForKey("cell") as! NSMutableArray).count
        }
        return 1
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if (tableContentArray.objectAtIndex(indexPath.section) as! NSDictionary).objectForKey("cell")?.isKindOfClass(NSMutableArray) == true {
            return ((tableContentArray.objectAtIndex(indexPath.section) as! NSDictionary).objectForKey("cell") as! NSMutableArray).objectAtIndex(indexPath.row) as! UITableViewCell
        }
        else {
            return (tableContentArray.objectAtIndex(indexPath.section) as! NSDictionary).objectForKey("cell") as! UITableViewCell
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if (tableContentArray.objectAtIndex(indexPath.section) as! NSDictionary).objectForKey("cell")?.isKindOfClass(PickerTableViewCell) == true {
            
            if ((tableContentArray.objectAtIndex(indexPath.section) as! NSDictionary).objectForKey("cell"))!.reuseIdentifier == "SelectPlayerPickerCell" {
                
                isSelectPlayerPicker = true
                if self.playerList.count > 1 {
                    ((tableContentArray.objectAtIndex(indexPath.section) as! NSDictionary).objectForKey("cell") as! PickerTableViewCell).setPickerViewValue()
                }
            }
            else {
                
                isSelectPlayerPicker = false
                if self.clubList.count != 0 {
                    ((tableContentArray.objectAtIndex(indexPath.section) as! NSDictionary).objectForKey("cell") as! PickerTableViewCell).setPickerViewValue()
                }
                else {
                    Utility.showAlert(ALERT_TITLE, message: "Club List is not available", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
                }
            }
            
        }
//        else if (tableContentArray.objectAtIndex(indexPath.section) as! NSDictionary).objectForKey("cell")?.isKindOfClass(NSMutableArray) == true {
//            
//            (((tableContentArray.objectAtIndex(indexPath.section) as! NSDictionary).objectForKey("cell") as! NSMutableArray).objectAtIndex(selectedPlayerIndex) as! CellWithLabelAndButtonTableViewCell).titleLabel.textColor = unSelectedPlayerColor
//            
//            selectedPlayerIndex = indexPath.row
//            selectedPlayer = playerList.objectAtIndex(selectedPlayerIndex) as? PlayerData
//
//            (((tableContentArray.objectAtIndex(indexPath.section) as! NSDictionary).objectForKey("cell") as! NSMutableArray).objectAtIndex(selectedPlayerIndex) as! CellWithLabelAndButtonTableViewCell).titleLabel.textColor = selectedPlayerColor
//        }
        else if (tableContentArray.objectAtIndex(indexPath.section) as! NSDictionary).objectForKey("cell")?.isKindOfClass(ExpandableTableViewCell) == true {
            
            let cell = ((tableContentArray.objectAtIndex(indexPath.section) as! NSDictionary).objectForKey("cell") as! ExpandableTableViewCell)
            
            if cell.isExpanded {
                cell.setHeightForTextViewTo(0)

                cell.isExpanded = false
            }
            else {
                cell.setHeightForTextViewTo(160)

                cell.isExpanded = true
            }
            
            tableView.beginUpdates()
            tableView.endUpdates()
        }
    }
    
    //MARK: - IBActions
    @IBAction func nextBtnAction(sender: AnyObject) {
        
        if clubList.count > 0 && selectedClub == nil {
            Utility.showAlert(nil, message: "Please select organization", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
        }
        else if programList.count > 0 &&  selectedProgram == nil {
            Utility.showAlert(nil, message: "Please select program", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
        }else {
            self.view.endEditing(true)
            
            let formsViewControllerObj = amitStoryBoard.instantiateViewControllerWithIdentifier("FormsViewController") as! FormsViewController
            
            formsViewControllerObj.addressDetail = addressDetail
            formsViewControllerObj.mSeq = mSeq
            formsViewControllerObj.fSeq = fSeq
            formsViewControllerObj.gSeq = gSeq
            formsViewControllerObj.selectedClub = selectedClub
            formsViewControllerObj.selectedPlayer = selectedPlayer
            formsViewControllerObj.selectedProgram = selectedProgram
            formsViewControllerObj.isComingFrom = isComingFrom
            
            formsViewControllerObj.isVolunteer = self.getCellWithIdentifier("SwitchTableViewCell") == nil ? "N" : ((self.getCellWithIdentifier("SwitchTableViewCell") as! SwitchTableViewCell).switchRef.on ? "Y" : "N")
            
            self.navigationController!.pushViewController(formsViewControllerObj, animated: true)
        }
        
    }
    
    
    func backAction() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    //MARK:- Picker Cell Delegate
    func pickerDoneBtnAction(selectedIndex: [Int]) {
        
        if isSelectPlayerPicker == true {
            selectedPlayer = playerList.objectAtIndex(selectedIndex[0]) as? PlayerData
        }
        else {
            selectedClub = (self.clubList[selectedIndex[0]] as! ClubData)
            self.getProgramListService((selectedClub?.clubSeq)!)
        }
        
    }
    
    //MARK:- Collection Grid Cell Delegate
    func selectedGridCell(index : Int) {
        
        selectedProgram = (self.programList[index] as! ProgramData)
        
        Utility.saveStringInDefault(BIRTH_CERT_REQ, value: (self.programList[index] as! ProgramData).isBirthCert)
        
        (self.getCellWithIdentifier("ExpandableTableViewCell") as! ExpandableTableViewCell).textView.text = (self.programList[index] as! ProgramData).programDescription
        
        if selectedProgram?.volunteerEnabled == "E" {
            if self.getIndexOfCellWithIdentifier("SwitchTableViewCell") == -1 {
                self.createVolunteerCellAtLast()
                tableView.reloadData()
            }
        }
        else {
            if self.getIndexOfCellWithIdentifier("SwitchTableViewCell") != -1 {
                self.tableContentArray.removeLastObject()
                tableView.reloadData()
            }
        }
    }
    
    //MARK: - Web service hit methods
    
    func getProgramListService (forClubSeq : String) {
        
        CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Getting Program List. Please wait..")
        
        let clubListServiceObj = ClubListServices(httpRequestType: HTTP_REQUEST_TYPE.POST_REQUEST, requestTag: "GET_PROGRAM_LIST_SERVICE", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
        
        clubListServiceObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
            
            
            if errorMessage != nil
            {
                //print(response)
            }
            
            print(response as! NSDictionary)
            let responseDict = CSProgramListData.init(fromDictionary: response as! NSDictionary)
            
            if responseDict.errorCode == 1 {
                Utility.showAlert(ALERT_TITLE, message: responseDict.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            }
            else {
                
                print(responseDict.data)
                if self.programList.count > 0 {
                    self.programList.removeAllObjects()
                }
                self.programList.addObjectsFromArray(responseDict.data)
                
                if self.programList.count > 0 {
                    
                    if self.getIndexOfCellWithIdentifier("SelectProgramGridCell") != -1 {
                        self.tableContentArray.removeObjectAtIndex(self.getIndexOfCellWithIdentifier("SelectProgramGridCell"))
                    }
                    
                    if self.getIndexOfCellWithIdentifier("ExpandableTableViewCell") != -1 {
                        self.tableContentArray.removeObjectAtIndex(self.getIndexOfCellWithIdentifier("ExpandableTableViewCell"))
                    }
                    
                    let index = self.playerList.count > 0 ? 2 : 1
                    self.createProgramDescriptionCellAtIndex(index)
                    self.createProgramGridCellAtIndex(index)
                    
                }
                else {
                    Utility.showAlert(ALERT_TITLE, message: "No program available under this club", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)

                    if self.getIndexOfCellWithIdentifier("SelectProgramGridCell") != -1 {
                        self.tableContentArray.removeObjectAtIndex(self.getIndexOfCellWithIdentifier("SelectProgramGridCell"))
                    }
                    
                    if self.getIndexOfCellWithIdentifier("ExpandableTableViewCell") != -1 {
                        self.tableContentArray.removeObjectAtIndex(self.getIndexOfCellWithIdentifier("ExpandableTableViewCell"))
                    }

                }
                self.tableView.reloadData()

                
                //tableContentArray.addObject(self.createCellContentDict("SELECT PROGRAM", withAsseccoryImage: true, cell: collectionGridCell))
            }
            CSActivityIndicator.hideAllFromMainWindow()

        }
        clubListServiceObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
            print("error ------------------------------\(error)")
            CSActivityIndicator.hideAllFromMainWindow()
            
        }
        
        
        var parameterDict = Dictionary <String, String>()
        parameterDict["cmd"] = "checkPlayerProgram"
        parameterDict["dob"] = selectedPlayer?.dob
        parameterDict["club_seq"] = forClubSeq
        parameterDict["program_seq"] = ""
        parameterDict["player_seq"] = selectedPlayer?.playerSeq
        clubListServiceObj.programListHit(parameterDict)
    }
    
    func getClubListService () {
        
        CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Fetching club list. Please Wait..")
        
        let clubListServiceObj = ClubListServices(httpRequestType: HTTP_REQUEST_TYPE.POST_REQUEST, requestTag: "GET_CLUB_LIST_SERVICE", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
        
        clubListServiceObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)

            
            if errorMessage != nil
            {
                //print(response)
            }
            
            print(response as! NSDictionary)
            let responseDict = CSClubListData.init(fromDictionary: response as! NSDictionary)
            
            if responseDict.errorCode == 1 {
                Utility.showAlert(ALERT_TITLE, message: responseDict.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            }
            else {
                
                print(responseDict.data)
                if self.clubList.count > 0 {
                    self.clubList.removeAllObjects()
                }
                
                self.clubList.addObjectsFromArray(responseDict.data)

                var clubNameArray = [String]()
                for clubData in self.clubList {
                    clubNameArray.append((clubData as! ClubData).clubName)
                }
                
                let pickerContentArray = [clubNameArray]
                
                print(self.getIndexOfCellWithIdentifier("SelectOrganisationPickerCell"))
                ((self.tableContentArray.objectAtIndex(self.getIndexOfCellWithIdentifier("SelectOrganisationPickerCell")) as! NSDictionary).objectForKey("cell") as! PickerTableViewCell).pickerContentArray = pickerContentArray
                
            }
        }
        clubListServiceObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)

            
            print("error ------------------------------\(error)")
        }
        
        
        var parameterDict = Dictionary <String, String>()
        parameterDict["cmd"] = "checkPlayerClub"
        parameterDict["dob"] = selectedPlayer?.dob
        parameterDict["club_seq"] = ""
        
        clubListServiceObj.clubListHit(parameterDict)
    }
    
    func getPlayerListService () {
        
        CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Fetching player list. Please Wait..")

        let playerListServiceObj = CommonServiceClass(httpRequestType: HTTP_REQUEST_TYPE.POST_REQUEST, requestTag: "GET_PLAYER_List_SERVICE", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
        
        playerListServiceObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
            
            
            if errorMessage != nil
            {
                //print(response)
            }
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)

            print(response as! NSDictionary)
            let responseDict = CSPlayerDetailData(fromDictionary: response as! NSDictionary)
            
            if responseDict.errorCode == 0 {
                print(responseDict.data)
                if self.playerList.count > 0 {
                    self.playerList.removeAllObjects()
                }
                self.playerList.addObjectsFromArray(responseDict.data)
            }
            else {
                
                self.playerList.removeAllObjects()
                
                Utility.showAlert(ALERT_TITLE, message: responseDict.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            }
            
            self.getClubListService()

            self.createTableComponent()

        }
        playerListServiceObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
            print("error ------------------------------\(error)")
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)

        }
        
        var parameterDict = Dictionary <String, String>()
        parameterDict["cmd"] = "getPlayersDetailsByParents"
        parameterDict["parent_seq"] = UserProfileManager.userSequence!
        
        playerListServiceObj.commonServiceHit(parameterDict)
    }

    
    //MARK:- Cell Methods
    func createCellContentDict(leftHeader : String, withAsseccoryImage : Bool, cell : UITableViewCell) ->  Dictionary <String, AnyObject>{
        
        let sectionDetail = ["leftHeader" : leftHeader, "withAsseccoryImage" : withAsseccoryImage]
        
        return ["sectionDetail" : sectionDetail, "cell" : cell]
    }
    
    func createCellContentArrayDict(leftHeader : String, withAsseccoryImage : Bool, cell : NSMutableArray) ->  Dictionary <String, AnyObject>{
        
        let sectionDetail = ["leftHeader" : leftHeader, "withAsseccoryImage" : withAsseccoryImage]
        
        return ["sectionDetail" : sectionDetail, "cell" : cell]
    }
    
    func getIndexOfCellWithIdentifier(cellIdentifier : String) -> Int{
        
        for index in 0...tableContentArray.count-1 {
            if (tableContentArray.objectAtIndex(index) as! NSDictionary).objectForKey("cell")!.isKindOfClass(NSMutableArray) == false && ((tableContentArray.objectAtIndex(index) as! NSDictionary).objectForKey("cell") as! UITableViewCell).reuseIdentifier == cellIdentifier {
                
                return index
            }
        }
        
        return -1
    }

    func getCellWithIdentifier(cellIdentifier : String) -> UITableViewCell? {
        
        for index in 0...tableContentArray.count-1 {
            if (tableContentArray.objectAtIndex(index) as! NSDictionary).objectForKey("cell")!.isKindOfClass(NSMutableArray) == false && ((tableContentArray.objectAtIndex(index) as! NSDictionary).objectForKey("cell") as! UITableViewCell).reuseIdentifier == cellIdentifier {
                
                return ((tableContentArray.objectAtIndex(index) as! NSDictionary).objectForKey("cell") as! UITableViewCell)
            }
        }
        
        return nil
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
