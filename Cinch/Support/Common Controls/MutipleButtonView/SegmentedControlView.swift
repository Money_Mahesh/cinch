//
//  SegmentedControlView.swift
//  Cinch
//
//  Created by Amit Garg on 28/02/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class SegmentedControlView: UIView {

    @IBInspectable var noOfDigit : Int = 0
    @IBInspectable var interspace : CGFloat = 0
    
    @IBInspectable var fontSize: CGFloat = 10.0
    @IBInspectable var fontFamily: String = "Lato-Regular"
    
    @IBInspectable var lineColor : UIColor = UIColor.clearColor()
    @IBInspectable var textColor : UIColor = UIColor.blackColor()

    var isDrew : Bool = false
    
    var segmentedButtons = [UIButton]()
    var buttonTitle = [String]()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.buildSegmentedView()
    }
    
    func buildSegmentedView() {
        
        if isDrew {return}
        isDrew = true
        
        let buttonView = UIView(frame: self.bounds)
        
        for position in 0..<noOfDigit {
            
            let viewWidthAfterRemovingInterSpace = self.bounds.size.width - interspace * CGFloat(noOfDigit-1)
            let button = UIButton(frame: CGRectMake((CGFloat(position) * (viewWidthAfterRemovingInterSpace / CGFloat(noOfDigit))) + (CGFloat(position) * interspace), 0, (viewWidthAfterRemovingInterSpace / CGFloat(noOfDigit)), self.bounds.size.height-2))
            
            button.tag =  position
            button.titleLabel!.font =  UIFont(name: self.fontFamily, size: self.fontSize)!
            button.setTitle("button", forState: .Normal)
            button.titleLabel!.textColor =  textColor
            button.backgroundColor = UIColor.redColor()
            
            segmentedButtons.append(button)
//            
//            let bottomLine = UIView(frame: CGRectMake(textfield.frame.origin.x, textfield.frame.size.height + 1, textfield.frame.size.width, 1))
//            bottomLine.backgroundColor = lineColor
//            
//            buttonView.addSubview(bottomLine)
            
            buttonView.addSubview(segmentedButtons[position])

            self.addSubview(buttonView)
        }
    }


}
