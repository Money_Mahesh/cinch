//
//	PaymentPlanDetail.swift
//
//	Create by Money Mahesh on 9/4/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class PaymentPlanDetail : NSObject, NSCoding{

	var divisionName : String!
	var divisionSeq : String!
	var feeAmount : String!
	var paymentPlan : String!
	var paymentPlanName : String!
	var programSeq : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		divisionName = dictionary["division_name"] as? String
		divisionSeq = dictionary["division_seq"] as? String
		feeAmount = dictionary["fee_amount"] as? String
		paymentPlan = dictionary["payment_plan"] as? String
		paymentPlanName = dictionary["payment_plan_name"] as? String
		programSeq = dictionary["program_seq"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if divisionName != nil{
			dictionary["division_name"] = divisionName
		}
		if divisionSeq != nil{
			dictionary["division_seq"] = divisionSeq
		}
		if feeAmount != nil{
			dictionary["fee_amount"] = feeAmount
		}
		if paymentPlan != nil{
			dictionary["payment_plan"] = paymentPlan
		}
		if paymentPlanName != nil{
			dictionary["payment_plan_name"] = paymentPlanName
		}
		if programSeq != nil{
			dictionary["program_seq"] = programSeq
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         divisionName = aDecoder.decodeObjectForKey("division_name") as? String
         divisionSeq = aDecoder.decodeObjectForKey("division_seq") as? String
         feeAmount = aDecoder.decodeObjectForKey("fee_amount") as? String
         paymentPlan = aDecoder.decodeObjectForKey("payment_plan") as? String
         paymentPlanName = aDecoder.decodeObjectForKey("payment_plan_name") as? String
         programSeq = aDecoder.decodeObjectForKey("program_seq") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if divisionName != nil{
			aCoder.encodeObject(divisionName, forKey: "division_name")
		}
		if divisionSeq != nil{
			aCoder.encodeObject(divisionSeq, forKey: "division_seq")
		}
		if feeAmount != nil{
			aCoder.encodeObject(feeAmount, forKey: "fee_amount")
		}
		if paymentPlan != nil{
			aCoder.encodeObject(paymentPlan, forKey: "payment_plan")
		}
		if paymentPlanName != nil{
			aCoder.encodeObject(paymentPlanName, forKey: "payment_plan_name")
		}
		if programSeq != nil{
			aCoder.encodeObject(programSeq, forKey: "program_seq")
		}

	}

}