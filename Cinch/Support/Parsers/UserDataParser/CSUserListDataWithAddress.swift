//
//	CSUserListDataWithAddress.swift
//
//	Create by Amit Garg on 8/3/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class CSUserListDataWithAddress{

	var data : [ParentDataWithAddress]!
	var errorCode : Int!
	var errorMsg : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		data = [ParentDataWithAddress]()
		if let dataArray = dictionary["data"] as? [NSDictionary]{
			for dic in dataArray{
				let value = ParentDataWithAddress(fromDictionary: dic)
				data.append(value)
			}
		}
		errorCode = dictionary["errorCode"] as? Int
		errorMsg = dictionary["errorMsg"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if data != nil{
			var dictionaryElements = [NSDictionary]()
			for dataElement in data {
				dictionaryElements.append(dataElement.toDictionary())
			}
			dictionary["data"] = dictionaryElements
		}
		if errorCode != nil{
			dictionary["errorCode"] = errorCode
		}
		if errorMsg != nil{
			dictionary["errorMsg"] = errorMsg
		}
		return dictionary
	}

}