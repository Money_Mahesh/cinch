//
//  MessageViewController.swift
//  Cinch
//
//  Created by Amit Garg on 12/03/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class MessageViewController: JSQMessagesViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var user: FAuthData?
    var senderImageUrl: String!
    var avatars = Dictionary<String, JSQMessagesAvatarImage>()

    let incomingBubble = JSQMessagesBubbleImageFactory().incomingMessagesBubbleImageWithColor(UIColor(red: 249/255, green: 249/255, blue: 249/255, alpha: 1.0))
    
    let outgoingBubble = JSQMessagesBubbleImageFactory().outgoingMessagesBubbleImageWithColor(UIColor(red: 0/255, green: 159/255, blue: 232/255, alpha: 1.0))
    
    var messages = [JSQMessage]()
    var imagePicker = UIImagePickerController()
    
    var chatNodeID : String!
    var receiverNode : String!
    var receiverId : String!
    var profilePicUrl : String!

    // *** STEP 1: STORE FIREBASE REFERENCES
    var messagesRef: Firebase!
    var comingFromTeamMateProfile = false
    
    func setupFirebase() {
        // *** STEP 2: SETUP FIREBASE
        messagesRef = Firebase(url: "https://cinchsports.firebaseio.com/Chat-DB/Live-Chat_Data/\(chatNodeID)")
        
        var count:UInt = 0

        // *** STEP 4: RECEIVE MESSAGES FROM FIREBASE (limited to latest 25 messages)
        messagesRef.queryLimitedToLast(25).observeEventType(FEventType.ChildAdded, withBlock: { (snapshot) in
            print(snapshot.value)

            let text = snapshot.value["message"] as? String
            let sender = snapshot.value["senderId"] as? Int
            let imageString = snapshot.value["messageImg"] as? String

            count++

            if sender != nil {
                if text != nil && text != "" {
                    let message = JSQMessage(senderId: String(sender!), displayName: String(sender!), text: text)
                    
                    self.messages.append(message)
                    self.finishReceivingMessage()

                }
                
                if imageString != nil && imageString != "" {
                    
                    let imageData = NSData(contentsOfURL: NSURL(string: imageString!)!)
                    let image = UIImage(data: imageData!)
                    let photoItem = JSQPhotoMediaItem(image: image)
                    let message = JSQMessage(senderId: String(sender!), displayName: String(sender!), media: photoItem)
                    self.messages.append(message)
                    self.finishReceivingMessage()

                }
            }

            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)

        })
        
        messagesRef.queryLimitedToLast(25).observeEventType(.Value, withBlock: { snapshot in
            print(snapshot.value)
            print("initial data loaded! \(count == snapshot.childrenCount)")
            if (count == snapshot.childrenCount) {
                CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
            }
        })
        
    }
    
    func setFireBaseHistory(var messageDetail: [String: AnyObject!], withImage: Bool) {
        
        let chatHistoryRef = Firebase(url: "https://cinchsports.firebaseio.com/Chat-DB/Users_Chat_History/\(UserProfileManager.userSequence!)")

        let chatHistoryObj = chatHistoryRef.childByAppendingPath("\(receiverNode!)")
        
        if receiverNode.hasPrefix("G") {
            messageDetail["chatType"] = 1
        }
        else {
            messageDetail["chatType"] = 0
        }
        
        messageDetail["senderName"] = senderDisplayName
        messageDetail["senderId"] = Int(receiverId)
        messageDetail["chatTitle"] = senderDisplayName
        
        if messageDetail["uname"] != nil {
            messageDetail.removeValueForKey("uname")
        }
        chatHistoryObj.setValue(messageDetail)
    
    }
    
    func sendMessage(text: String!, sender: String!) {
        // *** STEP 3: ADD A MESSAGE TO FIREBASE
        let timeStamp = Int(NSDate().timeIntervalSince1970 * 1000)

        let messageDetail : [String : AnyObject!] = [
            "message":text,
            "imageURL": self.profilePicUrl != nil ? self.profilePicUrl : "",
            "uname": UserProfileManager.name!,
            "senderId":Int(sender),
            "messageImg" : "",
            "timeStamp":timeStamp
        ]
        
        messagesRef.childByAutoId().setValue(messageDetail)
        
        self.setFireBaseHistory(messageDetail, withImage: false)
    }
    
    func tempSendMessage(text: String!, sender: String!) {
        let message = JSQMessage(senderId: sender, displayName: sender, text: text)
        messages.append(message)
    }
    
    func setupAvatarImage(name: String, imageUrl: String?, incoming: Bool) {
        if let stringUrl = imageUrl {
            if let url = NSURL(string: stringUrl) {
                if let data = NSData(contentsOfURL: url) {
                    let image = UIImage(data: data)
                    let diameter = incoming ? UInt(collectionView!.collectionViewLayout.incomingAvatarViewSize.width) : UInt(collectionView!.collectionViewLayout.outgoingAvatarViewSize.width)
                    let avatarImage = JSQMessagesAvatarImageFactory.avatarImageWithImage(image, diameter: diameter)
                    avatars[name] = avatarImage
                    return
                }
            }
        }
        
        // At some point, we failed at getting the image (probably broken URL), so default to avatarColor
        setupAvatarColor(name, incoming: incoming)
    }
    
    func setupAvatarColor(name: String, incoming: Bool) {
        let diameter = incoming ? UInt(collectionView!.collectionViewLayout.incomingAvatarViewSize.width) : UInt(collectionView!.collectionViewLayout.outgoingAvatarViewSize.width)
        
        let rgbValue = name.hash
        let r = CGFloat(Float((rgbValue & 0xFF0000) >> 16)/255.0)
        let g = CGFloat(Float((rgbValue & 0xFF00) >> 8)/255.0)
        let b = CGFloat(Float(rgbValue & 0xFF)/255.0)
        let color = UIColor(red: r, green: g, blue: b, alpha: 0.5)
        
        let nameLength = name.characters.count
        let initials : String? = name.substringToIndex(senderId.startIndex.advancedBy(min(3, nameLength)))
        let userImage = JSQMessagesAvatarImageFactory.avatarImageWithUserInitials(initials, backgroundColor: color, textColor: UIColor.blackColor(), font: UIFont.systemFontOfSize(CGFloat(13)), diameter: diameter)
        
        avatars[name] = userImage
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imagePicker.delegate = self
        
        senderId = (senderId != nil) ? senderId! : UserProfileManager.userSequence!
        Utility.setBackButton(forViewController: self, withTitle: "")

        let imageView = UIImageView(frame: CGRectMake(0, 0, 40, 40))
        imageView.backgroundColor = UIColor.clearColor()
        imageView.layer.cornerRadius = 20
        imageView.clipsToBounds = true
        
        Utility.setImageWithURl(profilePicUrl, inImageView: imageView)
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: imageView)
        
        let profileImageUrl = user?.providerData["cachedUserProfile"]?["profile_image_url_https"] as? NSString
        if let urlString = profileImageUrl {
            setupAvatarImage(senderId, imageUrl: urlString as String, incoming: false)
            senderImageUrl = urlString as String
        } else {
            setupAvatarColor(senderId, incoming: false)
            senderImageUrl = ""
        }
        
        self.title = senderDisplayName
        
        CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Fetching chat...")

        setupFirebase()
        
        self.collectionView!.collectionViewLayout.springinessEnabled = true;
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
     
        if comingFromTeamMateProfile {
            self.navigationController?.navigationBarHidden = false
        }
    }
    
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        if comingFromTeamMateProfile {
            self.navigationController?.navigationBarHidden = true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - IBActions
    func backAction() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func reloadMessagesView() {
        self.collectionView?.reloadData()
    }
    
    
    //MARK:- Image Picker method
    func showActionSheet() {
        
        let actionSheet = UIAlertController(title: "Player Headshot", message: "", preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        let imageGalleryAction = UIAlertAction(title: "Image Gallery", style: UIAlertActionStyle.Default) { (action) -> Void in
            self.openImageGallary()
        }
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.Default) { (action) -> Void in
            self.openCamera()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil)
        
        actionSheet.addAction(imageGalleryAction)
        actionSheet.addAction(cameraAction)
        actionSheet.addAction(cancelAction)
        
        self.presentViewController(actionSheet, animated: true, completion: nil)
    }
    
    func openImageGallary() {
        imagePicker.sourceType = .PhotoLibrary
        self.presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    func openCamera() {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
            imagePicker.sourceType = .Camera
            self.presentViewController(imagePicker, animated: true, completion: nil)
        }
        else {
            let alertController = UIAlertController(title: "Oops!!", message: "Camera is not available", preferredStyle: UIAlertControllerStyle.Alert)
            let cancelAction = UIAlertAction(title: ALERT_OK_BUTTON_TITLE, style: UIAlertActionStyle.Cancel, handler: nil)
            
            alertController.addAction(cancelAction)
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    //MARK:- Image Picker Delegate
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        if let pickerImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            print(pickerImage)
            self.sendPhoto(pickerImage)
        }
        self.imagePicker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.imagePicker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func didPressAccessoryButton(sender: UIButton!) {
        
        self.showActionSheet(sender)
    }
    
    func showActionSheet(sender: AnyObject) {
        // 1
        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .ActionSheet)
        
        // 2
        let deleteAction = UIAlertAction(title: "Send photo", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.showActionSheet()
            
        })
//        let saveAction = UIAlertAction(title: "Send location", style: .Default, handler: {
//            (alert: UIAlertAction!) -> Void in
//        })
//        
//        
//        let videoAction = UIAlertAction(title: "Send video", style: .Default, handler: {
//            (alert: UIAlertAction!) -> Void in
//        })
        
        //
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        
        // 4
        optionMenu.addAction(deleteAction)
//        optionMenu.addAction(saveAction)
//        optionMenu.addAction(videoAction)
        optionMenu.addAction(cancelAction)
        
        // 5
        self.presentViewController(optionMenu, animated: true, completion: nil)
    }
    
    func sendPhoto(image:UIImage) {
        
        CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Sending image...")

        Utility.uploadImageToserver(image) { (image: UIImage?,headShotImageUrl) -> Void in
            
            let timeStamp = Int(NSDate().timeIntervalSince1970 * 1000)

            let messageDetail : [String : AnyObject!] = [
                "senderId":Int(self.senderId),
                "message" : "",
                "messageImg":headShotImageUrl,
                "imageURL": self.profilePicUrl,
                "uname": UserProfileManager.name!,
                "timeStamp":timeStamp
            ]
            
            self.messagesRef.childByAutoId().setValue(messageDetail)
            self.finishSendingMessage()
            self.setFireBaseHistory(messageDetail, withImage: true)
            
            self.messagesRef.observeEventType(.Value, withBlock: { snapshot in
                print("initial data loaded!")
                
                CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
            })

        }
    }
}

//MARK - Setup
extension MessageViewController {
    func addDemoMessages() {
        for i in 1...10 {
            let sender = (i%2 == 0) ? "Server" : self.senderId
            let messageContent = "Message nr. \(i)"
            
            let message = JSQMessage(senderId: sender, displayName: sender, text: messageContent)
            self.messages += [message]
        }
        self.reloadMessagesView()
    }
    
    func setup() {
        self.senderId = UIDevice.currentDevice().identifierForVendor?.UUIDString
        self.senderDisplayName = UIDevice.currentDevice().identifierForVendor?.UUIDString
    }
}

//MARK - Data Source
extension MessageViewController {
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.messages.count
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, messageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageData! {
        let data = self.messages[indexPath.row]
        return data
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, didDeleteMessageAtIndexPath indexPath: NSIndexPath!) {
        self.messages.removeAtIndex(indexPath.row)
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageBubbleImageDataSource! {
        let data = messages[indexPath.row]
        switch(data.senderId!) {
        case self.senderId:
            return self.outgoingBubble
        default:
            return self.incomingBubble
        }
    }
    
    // Collection View text color and hyperlink
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = super.collectionView(collectionView, cellForItemAtIndexPath: indexPath) as! JSQMessagesCollectionViewCell
        
        let message = messages[indexPath.row]
        
        if (!message.isMediaMessage) {
            
            switch(message.senderId!) {
            case self.senderId:
                cell.textView!.textColor = UIColor.whiteColor()
            default:
                cell.textView!.textColor = UIColor(red: 119.0/255.0, green: 119.0/255.0, blue: 119.0/255.0, alpha: 1.0)
            }
            // Under line links
            let attributes : [String:AnyObject] = [NSForegroundColorAttributeName:cell.textView!.textColor!, NSUnderlineStyleAttributeName: 1]
            cell.textView!.linkTextAttributes = attributes

        }
        else {
           print(message.media)
            print(message.isMediaMessage)
        }
        
        
        return cell
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageAvatarImageDataSource! {
        return nil
    }
}

//MARK - Toolbar
extension MessageViewController {
    override func didPressSendButton(button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: NSDate!) {
        
        JSQSystemSoundPlayer.jsq_playMessageSentSound()
        
        sendMessage(text, sender: senderId)
        
        self.finishSendingMessage()
    }
}


