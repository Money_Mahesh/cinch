//
//  SectionHeaderWithLabelAndButton.swift
//  Cinch
//
//  Created by MONEY on 21/02/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class SectionHeaderWithLabelAndButton: UITableViewHeaderFooterView {

    @IBOutlet weak var upperSeperator: UIView!
    @IBOutlet weak var lowerSeperator: UIView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var button: UIButton!
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.contentView.backgroundColor = UIColor.whiteColor()
    }
    
    @IBAction func buttonAction(sender: AnyObject) {
    }
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
