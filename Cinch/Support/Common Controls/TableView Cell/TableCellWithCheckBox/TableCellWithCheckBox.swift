//
//  TableCellWithCheckBox.swift
//  Cinch
//
//  Created by MONEY on 06/04/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class TableCellWithCheckBox: UITableViewCell {

    @IBOutlet weak var cellTitle: UILabel!
    @IBOutlet weak var option1: CSCustomButton!
    @IBOutlet weak var option2: CSCustomButton!
    
    var selectedBtn = CSCustomButton()
    
    var mainView : UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setUpView()
        
        self.selectedBtn = option1
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setUpView() {
        
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "TableCellWithCheckBox", bundle: bundle)
        mainView = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        
        mainView.frame = self.bounds
        mainView.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        self.addSubview(mainView)
    }
    
    func bindCellWithTitle(title: String, buttonTitle: [String]) {
        
        cellTitle.text = title
        
        option1.setTitleForAllState(buttonTitle[0])
        option2.setTitleForAllState(buttonTitle[1])

    }
    
    @IBAction func optionSelected(sender: CSCustomButton) {
        
        option1.CSBorderWidth = 0
        option2.CSBorderWidth = 0

        selectedBtn = sender
        sender.CSBorderWidth = 2
    }
    
}
