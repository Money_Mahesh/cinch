//
//  TestService.swift
//  Cinch
//
//  Created by Money Mahesh on 14/02/16.
//  Copyright © 2016 tbsl. All rights reserved.
//

import Foundation


class TestService: ServerConnect {
    func testHit(testParameters: Dictionary<String, String>?)->Void{
        
        ServerConnect.requestOperationManager.GET("mobileSearch.html?campCode=iphone&searchType=30&bgmn=11800&bgmx=1002219&lt=101817&ct=4378&ty=10002,10017,10001,10000&iba=Z,X,Y&cg=B&srt=recent&page=0&resultPerPage=8&isWap=true&maxOffset=0&groupstart=0&offset=0&email=(null)", parameters: nil, success: {(operation : AFHTTPRequestOperation! , responseObject: AnyObject!) in
            
            let serverResponse = self.processAfterMakingRequestWithParameters(responseObject)
            
            self.completionBlockWithSuccess?(statusCode: 0, requestType: self.requestTag, response: serverResponse, errorMessage: nil);
           
            
        },
        failure: {(operation: AFHTTPRequestOperation?, error:NSError) in
        
            self.completionBlockWithFailure?(requestType: self.requestTag, error: error)
                
            }
        )
    }
}