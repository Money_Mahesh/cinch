//
//  CollectionGridTableViewCell.swift
//  MagicBricks
//
//  Created by Money Mahesh on 10/09/15.
//  Copyright © 2015 tbsl. All rights reserved.
//

import Foundation

protocol CollectionGridDelegate : NSObjectProtocol {
    func selectedGridCell(index : Int)
}

//MARK: - Custom CollectionGrid View Class
class CollectionGridTableViewCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate
{
    private var collectionView : UICollectionView!
    private var cellSize : CGSize!
    private var selectedCellIndex: Int!
    
    private let selectedCellColor = UIColor(red: 0, green: 159.0/255.0, blue: 232.0/255.0, alpha: 1.0)
    private let unSelectedCellColor = UIColor(red:  217.0/255.0, green: 217.0/255.0, blue: 217.0/255.0, alpha: 1.0)
    
    private let unSelectedTilteCellColor = UIColor(red: 101/255.0, green: 107.0/255.0, blue: 111.0/255.0, alpha: 1.0)
    
    private let unSelectedSubTitleCellColor = UIColor(red:  74.0/255.0, green: 74.0/255.0, blue: 74.0/255.0, alpha: 1.0)

    var cellDetailArray = NSMutableArray()
    
    weak var delegate : CollectionGridDelegate?
    
    //MARK: - Default Constructor
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    convenience init(style: UITableViewCellStyle, reuseIdentifier: String?, collectionViewSize : CGSize, cellSize : CGSize, selectedCellIndex : Int, andCellDetailArray cellDetailArray: NSMutableArray, delegate : CollectionGridDelegate) {
        self.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectedCellIndex = selectedCellIndex
        self.cellSize = cellSize
        self.cellDetailArray = cellDetailArray
        self.delegate = delegate
        
        self.delegate?.selectedGridCell(selectedCellIndex)

        let collectionViewFlowLayout = UICollectionViewFlowLayout()
        collectionViewFlowLayout.scrollDirection = UICollectionViewScrollDirection.Horizontal
        collectionViewFlowLayout.minimumInteritemSpacing = 0.0
        collectionViewFlowLayout.minimumLineSpacing = 10.0
        
        self.collectionView = UICollectionView(frame: CGRectMake(0, 0, collectionViewSize.width, collectionViewSize.height), collectionViewLayout: collectionViewFlowLayout)
        self.collectionView.registerNib(UINib(nibName: "CollectionGridCell", bundle: nil), forCellWithReuseIdentifier: "CollectionGridCell")
        self.collectionView.backgroundColor = UIColor.clearColor()
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        
        self.frame.size = collectionViewSize
        self.addSubview(self.collectionView)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    //MARK: - CollectionView DataSource and Delegate
    func collectionView(collectionView : UICollectionView,layout collectionViewLayout:UICollectionViewLayout,sizeForItemAtIndexPath indexPath:NSIndexPath) -> CGSize {
        return cellSize
    }
    
    func collectionView(collectionView : UICollectionView,layout collectionViewLayout:UICollectionViewLayout,insetForSectionAtIndex section:NSInteger) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 10, 5, 10);
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.cellDetailArray.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("CollectionGridCell", forIndexPath: indexPath) as! CollectionGridCell
        
        cell.cellBackgroundView.userInteractionEnabled = false
        
        let programObj = self.cellDetailArray.objectAtIndex(indexPath.row) as! ProgramData
        cell.title.text = programObj.name
        cell.subTitle.text = "$ \(programObj.fee_amount)"
        
        if indexPath.row == selectedCellIndex {
            cell.cellBackgroundView.layer.borderColor = selectedCellColor.CGColor
            cell.title.textColor = selectedCellColor
            cell.subTitle.textColor = selectedCellColor
        }
        else {
            cell.cellBackgroundView.layer.borderColor = unSelectedCellColor.CGColor
            cell.title.textColor = unSelectedTilteCellColor
            cell.subTitle.textColor = unSelectedSubTitleCellColor
        }
        
        return cell
    }

    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        (collectionView.cellForItemAtIndexPath(NSIndexPath(forItem: selectedCellIndex, inSection: 0)) as! CollectionGridCell).cellBackgroundView.layer.borderColor = unSelectedCellColor.CGColor
        (collectionView.cellForItemAtIndexPath(NSIndexPath(forItem: selectedCellIndex, inSection: 0)) as! CollectionGridCell).title.textColor = unSelectedTilteCellColor
        (collectionView.cellForItemAtIndexPath(NSIndexPath(forItem: selectedCellIndex, inSection: 0)) as! CollectionGridCell).subTitle.textColor = unSelectedSubTitleCellColor
        
        selectedCellIndex = indexPath.row
        
        (collectionView.cellForItemAtIndexPath(NSIndexPath(forItem: selectedCellIndex, inSection: 0)) as! CollectionGridCell).cellBackgroundView.layer.borderColor = selectedCellColor.CGColor
        (collectionView.cellForItemAtIndexPath(NSIndexPath(forItem: selectedCellIndex, inSection: 0)) as! CollectionGridCell).title.textColor = selectedCellColor
        (collectionView.cellForItemAtIndexPath(NSIndexPath(forItem: selectedCellIndex, inSection: 0)) as! CollectionGridCell).subTitle.textColor = selectedCellColor
        
        self.delegate?.selectedGridCell(selectedCellIndex)
    }
}

//MARK: - Class - CollectionGridCell
class CollectionGridCell: UICollectionViewCell {
    
    @IBOutlet weak var cellBackgroundView: UIView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subTitle: UILabel!

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        
        
    }

}
