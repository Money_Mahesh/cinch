//
//  WebViewController.swift
//  Cinch
//
//  Created by Amit Garg on 12/04/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class WebViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    var urlToLoad : String!
    var navigationHeaderTitle: String!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Utility.setRightNavigationButton(forViewController: self, withImage: "", withTitle: "Close")
        
        self.navigationItem.title = navigationHeaderTitle
        UIWebView.loadRequest(webView)(NSURLRequest(URL: NSURL(string: urlToLoad!)!))

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func rightBtnAction() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

}
