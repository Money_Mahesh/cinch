//
//  CustomPickerView.swift
//  Cinch
//
//  Created by MONEY on 20/02/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

protocol PickerViewDelegate : NSObjectProtocol {
    func pickerViewSelected(rowArray: [Int])
}

class CustomPickerView: UIView, UIPickerViewDelegate, UIPickerViewDataSource {

    let pickerViewHeight : CGFloat = 255.0
    let componentTitleViewHeight : CGFloat = 35.0
    let pickerViewToolbarHeight : CGFloat = 44.0

    let pickerViewToolbarButtonFontHeight : CGFloat = 20.0
    let pickerViewToolbarButtonWidth : CGFloat = 100.0
    let pickerViewToolbarBackgroundColor : UIColor = UIColor(red: 23.0/255.0, green: 22/255, blue: 22/255, alpha: 1.0)

    let componentTitleHeight : CGFloat = 20.0
    let componentTitleColor : UIColor = UIColor(red: 23.0/255.0, green: 22/255, blue: 22/255, alpha: 1.0)
    
    var pickerContentArray = [[String]]()

    var pickerComponentTitleView : UIView?
    var containerForPickerView : UIView?
    var pickerView : UIPickerView!
    var cancelBtn, doneBtn : UIButton!
    
    var isPickerViewPresent = false
    var isParentNavigationBarPresent = false

    weak var delegate: PickerViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    //MARK:- Create Picker Controller
    func setUpCustomPickerView() {
        
        self.backgroundColor = UIColor.whiteColor()
        if containerForPickerView == nil {
            containerForPickerView = UIView(frame: CGRectMake(0, MAIN_SCREEN_HEIGHT, MAIN_SCREEN_WIDTH, pickerViewHeight + pickerViewToolbarHeight))
            
            containerForPickerView!.backgroundColor = UIColor.whiteColor()

            containerForPickerView!.addSubview(addToolBarWithHeight(pickerViewToolbarHeight))
            containerForPickerView!.addSubview(addPickerView())
            
            ROOT_NAVVIGATION_CONROLLER.topViewController?.view.addSubview(containerForPickerView!)
        }
    }
    
    //MARK:- Add PickerView
    func addPickerView() ->UIPickerView {
        
        pickerView = UIPickerView(frame: CGRectMake(0, pickerViewToolbarHeight, MAIN_SCREEN_WIDTH, pickerViewHeight))

        return pickerView
    }
    
    //MARK:- PickerView DataSource
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return pickerContentArray.count
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if (pickerContentArray[component]).count == 1 && (pickerContentArray[component][0]) == "2016"{
            return Int(INT16_MAX)
        }
        
        return (pickerContentArray[component]).count
    }
    
    //MARK:- PickerView Delegate
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if (pickerContentArray[component]).count == 1 && (pickerContentArray[component][0]) == "2016" {
            return "\(Int(pickerContentArray[component][0])! + row)"
        }
    
        return pickerContentArray[component][row]
    }
    
    //MARK:- Add Remove Component Tilte to PickerView Methods
    func addComponentTitleToPickerView(titles : [String]) {
        
        containerForPickerView!.frame = CGRectMake(0, containerForPickerView!.frame.origin.y - componentTitleViewHeight , MAIN_SCREEN_WIDTH, pickerViewHeight + componentTitleViewHeight + pickerViewToolbarHeight)
        pickerComponentTitleView = UIView(frame: CGRectMake(0, pickerViewToolbarHeight, MAIN_SCREEN_WIDTH, componentTitleViewHeight))
        pickerView.frame.origin = CGPointMake(0, (componentTitleViewHeight + pickerViewToolbarHeight))
        
        pickerComponentTitleView!.backgroundColor = UIColor.clearColor()
        
        for position in 0..<titles.count {
            
            let interspace = CGFloat(2)
            let viewWidthAfterRemovingInterSpace = MAIN_SCREEN_WIDTH - interspace * CGFloat(titles.count-1)
            
            let titleLabel = UILabel(frame: CGRectMake((CGFloat(position) * (viewWidthAfterRemovingInterSpace / CGFloat(titles.count))) + (CGFloat(position) * interspace), 0, (viewWidthAfterRemovingInterSpace / CGFloat(titles.count)), componentTitleViewHeight))
            
            titleLabel.textColor = componentTitleColor
            titleLabel.textAlignment = NSTextAlignment.Center
            titleLabel.font = UIFont(name: "Lato-Regular", size: componentTitleHeight)
            titleLabel.text = titles[position]
            
            pickerComponentTitleView!.addSubview(titleLabel)
            
        }
        
        pickerComponentTitleView!.clipsToBounds = true
        containerForPickerView?.addSubview(pickerComponentTitleView!)
    }
    
    func removeComponentTitleToPickerView() {
        
        if pickerComponentTitleView != nil {
            pickerComponentTitleView!.removeFromSuperview()
            pickerComponentTitleView = nil
            
            containerForPickerView!.frame = CGRectMake(0, containerForPickerView!.frame.origin.y + componentTitleViewHeight ,MAIN_SCREEN_WIDTH, pickerViewHeight + pickerViewToolbarHeight)
            pickerView.frame.origin = CGPointMake(0, pickerViewToolbarHeight)
            
            self.layoutIfNeeded()
        }
    }
    
    //MARK:- Add PickerViewToolBar
    func addToolBarWithHeight(toolBarHeight : CGFloat) ->UIView {
        let toolBarView = UIView(frame: CGRectMake(0, 0, MAIN_SCREEN_WIDTH, toolBarHeight))
        toolBarView.backgroundColor = pickerViewToolbarBackgroundColor
        
        cancelBtn = UIButton(frame: CGRectMake(0, 0, pickerViewToolbarButtonWidth, toolBarHeight))
        cancelBtn.setTitleForAllState("Cancel")
        cancelBtn.setTitleColorForAllState(UIColor.whiteColor())
        cancelBtn.backgroundColor = UIColor.clearColor()
        cancelBtn.titleLabel?.font = UIFont(name: "Lato-Regular", size: pickerViewToolbarButtonFontHeight)
        cancelBtn.addTarget(self, action: Selector("cancelBtnAction:"), forControlEvents: UIControlEvents.TouchUpInside)

        doneBtn = UIButton(frame: CGRectMake(MAIN_SCREEN_WIDTH - pickerViewToolbarButtonWidth, 0, pickerViewToolbarButtonWidth, toolBarHeight))
        doneBtn.setTitleForAllState("Done")
        doneBtn.setTitleColorForAllState(UIColor.whiteColor())
        doneBtn.backgroundColor = UIColor.clearColor()
        doneBtn.titleLabel?.font = UIFont(name: "Lato-Regular", size: pickerViewToolbarButtonFontHeight)
        doneBtn.addTarget(self, action: Selector("doneBtnAction:"), forControlEvents: UIControlEvents.TouchUpInside)
        
        toolBarView.addSubview(cancelBtn)
        toolBarView.addSubview(doneBtn)
        return toolBarView
    }
    
    //MARK:- PickerViewToolBar Button Action
    func cancelBtnAction(sender : UIButton) {
        print("Cancel Button Tapped")
        dissmisPickerView()
    }
    
    func doneBtnAction(sender : UIButton) {
        print("Done Button Tapped")
        
        var selectedRowArray = [Int]()
        for index in 0..<pickerContentArray.count {
            selectedRowArray.append(pickerView.selectedRowInComponent(index))
        }
        
        if delegate != nil {
            delegate?.pickerViewSelected(selectedRowArray)
        }
        
        delegate = nil
        dissmisPickerView()
    }
    
    //MARK:- Animate PickerView
    func presentPickerView() {
        
        if !isPickerViewPresent {
            isPickerViewPresent = true
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.containerForPickerView!.frame.origin = CGPointMake(0, MAIN_SCREEN_HEIGHT - self.containerForPickerView!.frame.size.height - (self.isParentNavigationBarPresent ? 64 :0))
            })
        }
    }
    
    func dissmisPickerView() {
        
        if isPickerViewPresent {
            isPickerViewPresent = false

            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.containerForPickerView!.frame.origin = CGPointMake(0, MAIN_SCREEN_HEIGHT)
            },
            completion: {(Bool) ->Void in
                self.unloadPickerView()
                
                ROOT_NAVVIGATION_CONROLLER.topViewController?.view.userInteractionEnabled = true
                
            })
        }
    }
    
    func loadPickerViewForSelected(pickerContentArray : [[String]], row: [Int]?, withTitle titleArray : [String]?) {
        
        if titleArray != nil {
            self.removeComponentTitleToPickerView()
            self.addComponentTitleToPickerView(titleArray!)
        }
        else {
            self.removeComponentTitleToPickerView()
        }
        
        self.pickerContentArray = pickerContentArray
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.reloadAllComponents()
        
        for index in 0..<pickerContentArray.count {
            
            pickerView.selectRow((row != nil ? row![index] : 0), inComponent: index, animated: false)
        }
        
        self.presentPickerView()
    }
    
    func unloadPickerView() {
        pickerView.delegate = nil
        pickerView.dataSource = nil
        
        self.removeComponentTitleToPickerView()
    }

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
