//
//	ChatPlayerData.swift
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class ChatPlayerData : NSObject, NSCoding{

	var email : String!
	var name : String!
	var profilePic : String!
	var role : String!
	var status : String!
	var userSeq : String!
    var isSelected : Bool!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		email = dictionary["email"] as? String
		name = dictionary["name"] as? String
		profilePic = dictionary["profile_pic"] as? String
		role = dictionary["role"] as? String
		status = dictionary["status"] as? String
		userSeq = dictionary["user_seq"] as? String
        isSelected = false
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if email != nil{
			dictionary["email"] = email
		}
		if name != nil{
			dictionary["name"] = name
		}
		if profilePic != nil{
			dictionary["profile_pic"] = profilePic
		}
		if role != nil{
			dictionary["role"] = role
		}
		if status != nil{
			dictionary["status"] = status
		}
		if userSeq != nil{
			dictionary["user_seq"] = userSeq
		}
        
        dictionary["isSelected"] = 0

		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         email = aDecoder.decodeObjectForKey("email") as? String
         name = aDecoder.decodeObjectForKey("name") as? String
         profilePic = aDecoder.decodeObjectForKey("profile_pic") as? String
         role = aDecoder.decodeObjectForKey("role") as? String
         status = aDecoder.decodeObjectForKey("status") as? String
         userSeq = aDecoder.decodeObjectForKey("user_seq") as? String
         isSelected = aDecoder.decodeObjectForKey("isSelected") as? Bool

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if email != nil{
			aCoder.encodeObject(email, forKey: "email")
		}
		if name != nil{
			aCoder.encodeObject(name, forKey: "name")
		}
		if profilePic != nil{
			aCoder.encodeObject(profilePic, forKey: "profile_pic")
		}
		if role != nil{
			aCoder.encodeObject(role, forKey: "role")
		}
		if status != nil{
			aCoder.encodeObject(status, forKey: "status")
		}
		if userSeq != nil{
			aCoder.encodeObject(userSeq, forKey: "user_seq")
		}

        if isSelected != nil{
            aCoder.encodeObject(userSeq, forKey: "isSelected")
        }
	}

}