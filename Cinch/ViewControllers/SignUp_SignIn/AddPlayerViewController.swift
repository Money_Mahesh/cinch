//
//  AddPlayerViewController.swift
//  Cinch
//
//  Created by Amit Garg on 18/02/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class AddPlayerViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, AddressViewDelegate {
    
    @IBOutlet weak var imageViewForProfilePic: UIImageView!

    @IBOutlet weak var nameTextField: AGFloatLabelTextField!
    @IBOutlet weak var addresstextField: AGFloatLabelTextField!
    @IBOutlet weak var birthdayTextField: AGFloatLabelTextField!
    @IBOutlet weak var playerHeadShotButton: UIButton!
    @IBOutlet weak var femaleButton: UIButton!
    @IBOutlet weak var maleButton: UIButton!
    
    var imagePicker = UIImagePickerController()
    let datePicker = UIDatePicker()
    var playerList = NSMutableArray()

    var headShotImageUrl = String()
    var addressDetail : FatherAdd?
    
    var selectedPlayer: PlayerData?
    var isComingFrom = ComingFrom.SignUp

    override func viewDidLoad() {
        super.viewDidLoad()
        
        selectedPlayer = PlayerData()
        
        self.birthdayTextField.delegate = self
        imageViewForProfilePic.layer.cornerRadius = 45
        imageViewForProfilePic.clipsToBounds = true
        
        if revealViewController() != nil {
            Utility.setMenuButton(forViewController: self, revealController: revealViewController())
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        } else {
            Utility.setBackButton(forViewController: self, withTitle: "")
        }
        
        self.maleButton.selected = true
        self.imagePicker.delegate = self
        
        self.title = "Add Player"
        
        //Date Picker 
        
        self.datePicker.datePickerMode = .Date
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.hidden = false
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.hidden = true
    }
    
    //MARK:- IBActions
    @IBAction func roleButtonTapped(sender: AnyObject) {
        self.view.endEditing(true)

        self.maleButton.selected = false
        self.femaleButton.selected = false
        (sender as! UIButton).selected = true
    }
    
    @IBAction func nextBtnAction(sender: AnyObject) {
        
        if headShotImageUrl == "" {
            Utility.showAlert(ALERT_TITLE, message: "Profile image is mandatory", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)

        }
        else if self.nameTextField.text!.characters.count == 0 {
            Utility.showAlert(ALERT_TITLE, message: "Please enter the name", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
        }
        else if self.birthdayTextField.text!.characters.count == 0 {
            Utility.showAlert(nil, message: "Please enter the birth date", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
        }
        else if self.addresstextField.text!.characters.count == 0 {
            Utility.showAlert(nil, message: "Please select address", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
        }
        else {
            self.view.endEditing(true)
            let addProgramViewControllerObj = mainStoryBoard.instantiateViewControllerWithIdentifier("AddProgramViewController") as! AddProgramViewController
            
            selectedPlayer?.fname = self.nameTextField.text!.componentsSeparatedByString(" ")[0]
            selectedPlayer?.lname = self.nameTextField.text!.componentsSeparatedByString(" ").count > 1 ? self.nameTextField.text!.componentsSeparatedByString(" ").last : ""

            addProgramViewControllerObj.selectedPlayer = selectedPlayer
            addProgramViewControllerObj.selectedPlayer?.dob = self.birthdayTextField.text
            addProgramViewControllerObj.selectedPlayer?.gender = femaleButton.selected ? "F" : "M"
            addProgramViewControllerObj.selectedPlayer?.profilePic = self.headShotImageUrl
            addProgramViewControllerObj.addressDetail = self.addressDetail
            addProgramViewControllerObj.playerList = [addProgramViewControllerObj.selectedPlayer!]
            addProgramViewControllerObj.isComingFrom = isComingFrom

            self.navigationController?.pushViewController(addProgramViewControllerObj, animated: true)
        }
    }
    
    func backAction() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    //MARK: - Player address delegate
    func addPlayerAddress(addressDetail: FatherAdd?) {
        
        self.addressDetail = addressDetail
        let name = addressDetail?.name == nil ? "" : "\((addressDetail!.name)!), "
        let address = addressDetail?.address == nil ? "" : "\((addressDetail!.address)!), "
        let city = addressDetail?.city == nil ? "" : "\((addressDetail!.city)!), "
        let zipcode = addressDetail?.zipcode == nil ? "" : "\((addressDetail!.zipcode)!), "

        selectedPlayer?.address = (addressDetail?.address == nil ? "" : "\((addressDetail!.address)!)")
        
        var completeAddress : String = name + address + city + zipcode
        
        if completeAddress != "" {
            completeAddress = String(completeAddress.characters.dropLast(2))
        }
        PlayerDetailManager.playerAddress = completeAddress
        self.addresstextField.text = completeAddress
    }

    //MARK:- IBActions
    @IBAction func addAddressButtonTapped(sender: AnyObject) {
        self.view.endEditing(true)

        let parentAddressViewControllerObj = amitStoryBoard.instantiateViewControllerWithIdentifier("ParentAddressViewController") as! ParentAddressViewController
        parentAddressViewControllerObj.selectedPlayer = selectedPlayer
        parentAddressViewControllerObj.delegate = self
        self.navigationController?.pushViewController(parentAddressViewControllerObj, animated: true)
    }
    
    @IBAction func playerHeadshotTapped(sender: AnyObject) {
        self.view.endEditing(true)

        self.showActionSheet()
    }
    
    //MARK:- Image Picker method
    func showActionSheet() {
        
        let actionSheet = UIAlertController(title: "Player Headshot", message: "", preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        let imageGalleryAction = UIAlertAction(title: "Image Gallery", style: UIAlertActionStyle.Default) { (action) -> Void in
            self.openImageGallary()
        }
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.Default) { (action) -> Void in
            self.openCamera()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil)
        
        actionSheet.addAction(imageGalleryAction)
        actionSheet.addAction(cameraAction)
        actionSheet.addAction(cancelAction)
        
        self.presentViewController(actionSheet, animated: true, completion: nil)
    }
    
    func openImageGallary() {
        imagePicker.sourceType = .PhotoLibrary
        self.presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    func openCamera() {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
            imagePicker.sourceType = .Camera
            self.presentViewController(imagePicker, animated: true, completion: nil)
        }
        else {
            let alertController = UIAlertController(title: "Oops!!", message: "Camera is not available", preferredStyle: UIAlertControllerStyle.Alert)
            let cancelAction = UIAlertAction(title: ALERT_OK_BUTTON_TITLE, style: UIAlertActionStyle.Cancel, handler: nil)
            
            alertController.addAction(cancelAction)
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    //MARK:- Image Picker Delegate
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        if let pickerImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            print(pickerImage)
            let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
            activityIndicator.frame.size = self.playerHeadShotButton.frame.size
            
            activityIndicator.startAnimating()
            self.playerHeadShotButton.addSubview(activityIndicator)
            
            Utility.uploadImageToserver(pickerImage, resetImageViewBlock: {(selectedImage: UIImage?, headShotImageUrl: String?) in
                
                activityIndicator.removeFromSuperview()
                if selectedImage != nil {
                    
                    self.headShotImageUrl = headShotImageUrl == nil ? "" : headShotImageUrl!
                    self.imageViewForProfilePic.image = selectedImage
                }
                else {
                }
            })
            
            self.playerHeadShotButton.layer.cornerRadius = 45.0
            self.playerHeadShotButton.clipsToBounds = true
        }
        
        self.imagePicker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.imagePicker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    //MARK: - Text field Delegate
    
    func textFieldDidBeginEditing(textField: UITextField) {
        
        if textField == self.birthdayTextField {
            textField.inputView = self.datePicker
        }
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        
        if textField == self.birthdayTextField {
            let dateformatter = NSDateFormatter()
            dateformatter.dateFormat = "yyyy-MM-dd"
            textField.text = dateformatter.stringFromDate(self.datePicker.date)
        }
        
    }
    
}
