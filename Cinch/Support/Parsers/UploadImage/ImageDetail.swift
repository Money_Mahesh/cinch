//
//	ImageDetail.swift
//
//	Create by Money Mahesh on 5/3/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class ImageDetail : NSObject, NSCoding{

	var imagejpg : Image_jpg!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		if let imagejpgData = dictionary["image.jpg"] as? NSDictionary{
			imagejpg = Image_jpg(fromDictionary: imagejpgData)
		}
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if imagejpg != nil{
			dictionary["image.jpg"] = imagejpg.toDictionary()
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         imagejpg = aDecoder.decodeObjectForKey("image.jpg") as? Image_jpg

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if imagejpg != nil{
			aCoder.encodeObject(imagejpg, forKey: "image.jpg")
		}

	}

}