//
//  PlayerDetailManager.swift
//  Cinch
//
//  Created by MONEY on 12/03/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import Foundation

class PlayerDetailManager: NSObject {
    
    static func logOut() {
        programSelected = nil
        planSelected = nil
        clubSelected = nil
        playerAddress = nil
        playerDetail = nil
        waiverSeq = nil
        insuranceSignatureUrl  = nil
        birthCertificateUrl = nil
        
        Utility.removeParameterFromUserDefault(WAIVER_SEQUENCE)
        Utility.removeParameterFromUserDefault(INSURANCE_SIGNATURE_URL)
        Utility.removeParameterFromUserDefault(PLAYER_DETAIL)
        Utility.removeParameterFromUserDefault(PLAYER_ADDRESS)
        Utility.removeParameterFromUserDefault(PROGRAM_SELECTED)
        Utility.removeParameterFromUserDefault(PLAN_SELECTED)
        Utility.removeParameterFromUserDefault(CLUB_SELECTED)
        Utility.removeParameterFromUserDefault(BIRTH_CERTIFICATE_URL)
    }
    
    static private var _waiverSeq: String?
    static var waiverSeq: String? {
        set{
        _waiverSeq = newValue
        
        DataValidationUtility.isNull(_waiverSeq) ? Utility.removeParameterFromUserDefault(WAIVER_SEQUENCE) : Utility.saveStringInDefault(WAIVER_SEQUENCE, value:_waiverSeq!)
        
        }
        get{
            
            if DataValidationUtility.isNull(_waiverSeq) {
                
                if DataValidationUtility.isNull(Utility.fetchStringFromDefault(WAIVER_SEQUENCE)) {
                    return ""
                }
                
                _waiverSeq = Utility.fetchStringFromDefault(WAIVER_SEQUENCE) as? String
            }
            return _waiverSeq
        }
    }
    
    static private var _insuranceSignatureUrl: String?
    static var insuranceSignatureUrl: String? {
        set{
        _insuranceSignatureUrl = newValue
        
        DataValidationUtility.isNull(_insuranceSignatureUrl) ? Utility.removeParameterFromUserDefault(INSURANCE_SIGNATURE_URL) : Utility.saveStringInDefault(INSURANCE_SIGNATURE_URL, value:_insuranceSignatureUrl!)
        
        }
        get{
            
            if DataValidationUtility.isNull(_insuranceSignatureUrl) {
                
                if DataValidationUtility.isNull(Utility.fetchStringFromDefault(INSURANCE_SIGNATURE_URL)) {
                    return ""
                }
                
                _insuranceSignatureUrl = Utility.fetchStringFromDefault(INSURANCE_SIGNATURE_URL) as? String
            }
            return _insuranceSignatureUrl
        }
    }
    
    static private var _birthCertificateUrl: String?
    static var birthCertificateUrl: String? {
        set{
        _birthCertificateUrl = newValue
        
        DataValidationUtility.isNull(_birthCertificateUrl) ? Utility.removeParameterFromUserDefault(BIRTH_CERTIFICATE_URL) : Utility.saveStringInDefault(BIRTH_CERTIFICATE_URL, value:_birthCertificateUrl!)
        
        }
        get{
            
            if DataValidationUtility.isNull(_birthCertificateUrl) {
                
                if DataValidationUtility.isNull(Utility.fetchStringFromDefault(BIRTH_CERTIFICATE_URL)) {
                    return ""
                }
                
                _birthCertificateUrl = Utility.fetchStringFromDefault(BIRTH_CERTIFICATE_URL) as? String
            }
            return _birthCertificateUrl
        }
    }
    
    static private var _playerDetail: PlayerData?
    static var playerDetail: PlayerData? {
        set{
        _playerDetail = newValue
        
        let address = _playerDetail?.address == nil ? "" : "\((_playerDetail!.address)!), "
        let city = _playerDetail?.city == nil ? "" : "\((_playerDetail!.city)!), "
        let zipcode = _playerDetail?.zipcode == nil ? "" : "\((_playerDetail!.zipcode)!), "
        
        var completeAddress : String = address + city + zipcode
        if completeAddress != "" {
            completeAddress = String(completeAddress.characters.dropLast(2))
        
            if String(completeAddress.characters.first) == "," {
                completeAddress = String(completeAddress.characters.dropFirst(2))
            }
        }
        
        PlayerDetailManager.playerAddress = completeAddress
        
        DataValidationUtility.isNull(_playerDetail) ? Utility.removeParameterFromUserDefault(PLAYER_DETAIL) : Utility.saveObjectInDefault(PLAYER_DETAIL, value:_playerDetail!)
        
        }
        get{
            
            if DataValidationUtility.isNull(_playerDetail) {
                
                if DataValidationUtility.isNull(Utility.fetchObjectFromDefault(PLAYER_DETAIL)) {
                    return nil
                }
                
                _playerDetail = Utility.fetchObjectFromDefault(PLAYER_DETAIL) as? PlayerData
            }
            
            return _playerDetail
        }
    }
    
    static private var _playerAddress: String?
    static var playerAddress: String? {
        set{
        _playerAddress = newValue
        
        DataValidationUtility.isNull(_playerAddress) ? Utility.removeParameterFromUserDefault(PLAYER_ADDRESS) : Utility.saveStringInDefault(PLAYER_ADDRESS, value:_playerAddress!)
        
        }
        get{
            
            if DataValidationUtility.isNull(_playerAddress) {
                
                if DataValidationUtility.isNull(Utility.fetchStringFromDefault(PLAYER_ADDRESS)) {
                    return ""
                }
                
                _playerAddress = Utility.fetchStringFromDefault(PLAYER_ADDRESS) as? String
            }
            return _playerAddress
        }
    }
    
    static private var _programSelected: ProgramData?
    static var programSelected: ProgramData? {
        set{
        _programSelected = newValue
        
        DataValidationUtility.isNull(_programSelected) ? Utility.removeParameterFromUserDefault(PROGRAM_SELECTED) : Utility.saveObjectInDefault(PROGRAM_SELECTED, value:_programSelected!)
        
        }
        get{
            
            if DataValidationUtility.isNull(_programSelected) {
                
                if DataValidationUtility.isNull(Utility.fetchObjectFromDefault(PROGRAM_SELECTED)) {
                    return nil
                }
                
                _programSelected = Utility.fetchObjectFromDefault(PROGRAM_SELECTED) as? ProgramData
            }
            return _programSelected
        }
    }
    
    static private var _planSelected: PaymentPlanDetail?
    static var planSelected: PaymentPlanDetail? {
        set{
        _planSelected = newValue
        
        DataValidationUtility.isNull(_planSelected) ? Utility.removeParameterFromUserDefault(PLAN_SELECTED) : Utility.saveObjectInDefault(PLAN_SELECTED, value:_planSelected!)
        
        }
        get{
            
            if DataValidationUtility.isNull(_planSelected) {
                
                if DataValidationUtility.isNull(Utility.fetchObjectFromDefault(PLAN_SELECTED)) {
                    return nil
                }
                
                _planSelected = Utility.fetchObjectFromDefault(PLAN_SELECTED) as? PaymentPlanDetail
            }
            return _planSelected
        }
    }
    
    static private var _clubSelected: ClubData?
    static var clubSelected: ClubData? {
        set{
        _clubSelected = newValue
        
        DataValidationUtility.isNull(_clubSelected) ? Utility.removeParameterFromUserDefault(CLUB_SELECTED) : Utility.saveObjectInDefault(CLUB_SELECTED, value:_clubSelected!)
        
        }
        get{
            
            if DataValidationUtility.isNull(_clubSelected) {
                
                if DataValidationUtility.isNull(Utility.fetchObjectFromDefault(CLUB_SELECTED)) {
                    return ClubData()
                }
                
                _clubSelected = Utility.fetchObjectFromDefault(CLUB_SELECTED) as? ClubData
            }
            return _clubSelected
        }
    }
    
    static private var _eventSelected: EventDetail?
    static var eventSelected: EventDetail? {
        set{
        _eventSelected = newValue
        
        DataValidationUtility.isNull(_eventSelected) ? Utility.removeParameterFromUserDefault(EVENT_SELECTED) : Utility.saveObjectInDefault(EVENT_SELECTED, value:_eventSelected!)
        
        }
        get{
            
            if DataValidationUtility.isNull(_eventSelected) {
                
                if DataValidationUtility.isNull(Utility.fetchObjectFromDefault(EVENT_SELECTED)) {
                    return EventDetail()
                }
                
                _eventSelected = Utility.fetchObjectFromDefault(EVENT_SELECTED) as? EventDetail
            }
            return _eventSelected
        }
    }

}