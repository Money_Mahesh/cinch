//
//  TextfieldTableViewCell.swift
//  Cinch
//
//  Created by Amit Garg on 20/10/15.

import UIKit

@objc protocol TextFieldCellDelegate : NSObjectProtocol {
    optional func setActiveTextField(cell : UITableViewCell)
    optional func textFieldCellEndEditing(textFieldCell: TextfieldTableViewCell, text: NSString)
    optional func textFieldCellDidBeginEditing(textFieldCell: TextfieldTableViewCell)
    optional func textFieldCellShouldReturn (textFieldCell: TextfieldTableViewCell)
    
}

class TextfieldTableViewCell: UITableViewCell, UITextFieldDelegate {

    @IBOutlet weak var textField: UITextField!
    var delegate : TextFieldCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textField.delegate = self
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    //MARK: - TextField Delegate
    
    func textFieldDidBeginEditing(textField: UITextField) {
        
        delegate?.textFieldCellDidBeginEditing!(self)
    }
    
    
    func textFieldDidEndEditing(textField: UITextField) {
        
        if !DataValidationUtility.isNull(delegate) {
            delegate?.textFieldCellEndEditing!(self, text:(textField.text)!)
        }
        
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        if textField.isFirstResponder() {
            textField.resignFirstResponder()
        }
        
        return true
    }
    
    
    
    
}
