//
//  ChatsViewController.swift
//  Cinch
//
//  Created by Amit Garg on 13/03/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class ChatsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var chatsListTable: UITableView!
    var filteredArray = [String]()
    var recentMessageArray : [ChatDetail]!

    // *** STEP 1: STORE FIREBASE REFERENCES
    var messagesRef: Firebase!
    
    //    var sender : String?
    
    func setupFirebase() {
        // *** STEP 2: SETUP FIREBASE
        messagesRef = Firebase(url: "https://cinchsports.firebaseio.com/Chat-DB/Users_Chat_History/\(UserProfileManager.userSequence!)")
        
        messagesRef.observeEventType(.Value, withBlock: { snapshot in
            
            print(snapshot.value)
            
            
            if snapshot.value.isKindOfClass(NSNull) != true {
                print(snapshot.value)
                
                self.recentMessageArray = CSRecentChat(fromDictionary: (snapshot.value as! NSDictionary)).recentChatDetailArray
                self.chatsListTable.reloadData()
            }
            
            }, withCancelBlock: { error in
                print(error.description)
                
                CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)

        })
        
        
        messagesRef.queryOrderedByKey().observeEventType(FEventType.Value, withBlock: { (snapshot) in
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)

        })
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if revealViewController() != nil {
            Utility.setMenuButton(forViewController: self, revealController: revealViewController())
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        self.chatsListTable.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.topItem?.title = "Chats"
        
        let rightButton: UIButton = UIButton()
        rightButton.setImage(UIImage(named: "Edit Icon"), forState: .Normal)
        rightButton.frame = CGRectMake(0.0, 0.0, 38.0, 44.0)
        
        //Adding Action Left Navigation Bar Button Item.
        rightButton.addTarget(self, action: "rightBtnAction", forControlEvents: .TouchUpInside)
        rightButton.setTitle("", forState: .Normal)
        rightButton.titleLabel?.font = UIFont(name: "Lato-Regular", size: 12)
        rightButton.setTitleColorForAllState(UIColor(red: 160/255, green: 160/255, blue: 160/255, alpha: 1.0))
        
        let rightItem:UIBarButtonItem = UIBarButtonItem()
        rightItem.customView = rightButton
        
        
        self.tabBarController?.navigationItem.rightBarButtonItem = rightItem
        
        
        
        CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Fetching chat...")
        
        self.setupFirebase()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- TableView Data Source and Delegates
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRectMake(0, 0, MAIN_SCREEN_WIDTH, 1))
        footerView.backgroundColor = UIColor(red: 247.0/255.0, green: 247.0/255.0, blue: 247.0/255.0, alpha: 1.0)
        
        return footerView
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if recentMessageArray == nil {
            return 0
        }
        return recentMessageArray.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let availabilityEventCell = tableView.dequeueReusableCellWithIdentifier("AvailabilityTableViewCell") as! AvailabilityEventCell
        
        availabilityEventCell.labelForName.text = recentMessageArray[indexPath.row].senderName
        
        if recentMessageArray[indexPath.row].message == nil || recentMessageArray[indexPath.row].message == ""{
            availabilityEventCell.labelForStatus.text = "Image"
        }
        else {
            availabilityEventCell.labelForStatus.text = recentMessageArray[indexPath.row].message
        }

        if let timeStamp = recentMessageArray[indexPath.row].timeStamp {
            
            let date = NSDate(timeIntervalSince1970: Double(timeStamp))
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateStyle = .ShortStyle
            availabilityEventCell.labeForTime.text = dateFormatter.stringFromDate(date)
        }
        
        Utility.setImageWithURl(recentMessageArray[indexPath.row].imageURL, inImageView: availabilityEventCell.imageViewForProfilePic)
        
        return availabilityEventCell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let messageViewControllerObj = amitStoryBoard.instantiateViewControllerWithIdentifier("MessageViewController") as! MessageViewController
        
        messageViewControllerObj.senderDisplayName = recentMessageArray[indexPath.row].senderName
        
        let senderId = recentMessageArray[indexPath.row].senderId
        
        if Int(UserProfileManager.userSequence!) < Int(senderId) {
            
            messageViewControllerObj.chatNodeID = recentMessageArray[indexPath.row].chatType == 0 ? "\(UserProfileManager.userSequence!)-\(senderId)" : "G-\(senderId)"
            
        }
        else {
            
            messageViewControllerObj.chatNodeID = recentMessageArray[indexPath.row].chatType == 0 ? "\(senderId)-\(UserProfileManager.userSequence!)" : "G-\(senderId)"
            
        }

        messageViewControllerObj.receiverNode = recentMessageArray[indexPath.row].chatType == 0 ? "\(senderId)" : "G-\(senderId)"
        messageViewControllerObj.receiverId = String(senderId!)
        messageViewControllerObj.profilePicUrl = recentMessageArray[indexPath.row].imageURL

        self.navigationController?.pushViewController(messageViewControllerObj, animated: true)
    }
    
    //MARK: - Right Message Button 
    
    func rightBtnAction() {
        
        let newChatListViewControllerObj = amitStoryBoard.instantiateViewControllerWithIdentifier("NewChatListViewController") as! NewChatListViewController
        
        self.navigationController!.pushViewController(newChatListViewControllerObj, animated: true)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
