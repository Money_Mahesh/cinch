//
//	OrderDetail.swift
//
//	Create by Money Mahesh on 16/4/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class OrderDetail : NSObject, NSCoding{

	var cinchFees : String!
	var clubFee : String!
	var clubName : String!
	var dueNow : Int!
	var feeAmount : String!
	var procesingAmt : String!
	var programName : String!
	var totalPrice : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		cinchFees = dictionary["cinch_fees"] as? String
		clubFee = dictionary["club_fee"] as? String
		clubName = dictionary["club_name"] as? String
		dueNow = dictionary["due_now"] as? Int
		feeAmount = dictionary["fee_amount"] as? String
		procesingAmt = dictionary["procesing_amt"] as? String
		programName = dictionary["program_name"] as? String
		totalPrice = dictionary["total_price"] as? Int
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if cinchFees != nil{
			dictionary["cinch_fees"] = cinchFees
		}
		if clubFee != nil{
			dictionary["club_fee"] = clubFee
		}
		if clubName != nil{
			dictionary["club_name"] = clubName
		}
		if dueNow != nil{
			dictionary["due_now"] = dueNow
		}
		if feeAmount != nil{
			dictionary["fee_amount"] = feeAmount
		}
		if procesingAmt != nil{
			dictionary["procesing_amt"] = procesingAmt
		}
		if programName != nil{
			dictionary["program_name"] = programName
		}
		if totalPrice != nil{
			dictionary["total_price"] = totalPrice
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         cinchFees = aDecoder.decodeObjectForKey("cinch_fees") as? String
         clubFee = aDecoder.decodeObjectForKey("club_fee") as? String
         clubName = aDecoder.decodeObjectForKey("club_name") as? String
         dueNow = aDecoder.decodeObjectForKey("due_now") as? Int
         feeAmount = aDecoder.decodeObjectForKey("fee_amount") as? String
         procesingAmt = aDecoder.decodeObjectForKey("procesing_amt") as? String
         programName = aDecoder.decodeObjectForKey("program_name") as? String
         totalPrice = aDecoder.decodeObjectForKey("total_price") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if cinchFees != nil{
			aCoder.encodeObject(cinchFees, forKey: "cinch_fees")
		}
		if clubFee != nil{
			aCoder.encodeObject(clubFee, forKey: "club_fee")
		}
		if clubName != nil{
			aCoder.encodeObject(clubName, forKey: "club_name")
		}
		if dueNow != nil{
			aCoder.encodeObject(dueNow, forKey: "due_now")
		}
		if feeAmount != nil{
			aCoder.encodeObject(feeAmount, forKey: "fee_amount")
		}
		if procesingAmt != nil{
			aCoder.encodeObject(procesingAmt, forKey: "procesing_amt")
		}
		if programName != nil{
			aCoder.encodeObject(programName, forKey: "program_name")
		}
		if totalPrice != nil{
			aCoder.encodeObject(totalPrice, forKey: "total_price")
		}

	}

}