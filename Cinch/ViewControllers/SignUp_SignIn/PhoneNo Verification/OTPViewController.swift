//
//  OTPViewController.swift
//  Cinch
//
//  Created by MONEY on 19/02/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class OTPViewController: UIViewController, TextFieldDelegate {
    
    @IBOutlet weak var backBtnRef: UIButton!
    var userPhoneNumber : String?
    var verificationCode: String?
    
    @IBOutlet weak var otpView: OTP_View!
    var isComingFromCreateAccount = true

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.otpView.delegate = self

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.hidden = true
    }
    
    // MARK: - IBActions
    @IBAction func backBtnAction(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func btnForSubmitAction(sender: AnyObject) {
        
        self.view.endEditing(true)
        
        if self.verificationCode?.characters.count == 0 {
            
            Utility.showAlert(ALERT_TITLE, message: "Please enter verification code", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
        } else {
            
            self.hitVerifyPhoneNumberService()
        }

    }
    
    @IBAction func btnForResendAction(sender: AnyObject) {
        self.view.endEditing(true)
        
        self.hitAddPhoneNumberService()
    }
    
    func textFieldUpdate(completeString: String) {
        self.verificationCode = completeString
    }
    
    func hitVerifyPhoneNumberService() {
        
        CSActivityIndicator.showToMainWindowWithMessage("Verifying. Please wait..")
        
        let sendVerificationObj = CommonServiceClass(httpRequestType: HTTP_REQUEST_TYPE.POST_REQUEST, requestTag: "VERIFY_PHONENUMBER_SERVICE", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
        
        sendVerificationObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
            
            
            if errorMessage != nil
            {
                //print(response)
            }
            
            print(response as! NSDictionary)
            let responseDict = OtpVerificationData(fromDictionary: response as! NSDictionary)
            
            if responseDict.errorCode == 0 {
                print(responseDict.data)
                
                if responseDict.data == true {
                    
                    if self.isComingFromCreateAccount == true {
                        let addPlayerViewControllerObj = amitStoryBoard.instantiateViewControllerWithIdentifier("AddPlayerViewController") as! AddPlayerViewController
                        
                        UserProfileManager.phoneNo = self.userPhoneNumber
                        Utility.saveStringInDefault(SIGN_UP_STATUS, value: "AddPlayerViewController")
                        
                        self.navigationController?.pushViewController(addPlayerViewControllerObj, animated: true)
                    }
                    else {
                        print(self.navigationController?.viewControllers)
                        if self.navigationController?.viewControllers.count != 0 {
                            
                            for vc in (self.navigationController?.viewControllers)! {
                               
                                if vc.isKindOfClass(SWRevealViewController) == true {
                                    if ((vc as! SWRevealViewController).frontViewController as! UINavigationController).topViewController!.isKindOfClass(AccountSettingsViewController) == true {
                                        
                                        (((vc as! SWRevealViewController).frontViewController as! UINavigationController).topViewController as!
                                            AccountSettingsViewController).phoneNo = self.userPhoneNumber
                                        
                                        let curVC = (((vc as! SWRevealViewController).frontViewController as! UINavigationController).topViewController as!
                                            AccountSettingsViewController)
                                        
                                        ((vc as! SWRevealViewController).frontViewController as! UINavigationController).setViewControllers([curVC], animated: false)
                                        ROOT_NAVVIGATION_CONROLLER.setViewControllers([vc], animated: true)
                                        break;

                                    }
                                }
                            }
                        }
//                        self.navigationController?.popToRootViewControllerAnimated(true)
                    }
                    
                } else {
                    
                    Utility.showAlert(ALERT_TITLE, message: "Please enter correct OTP", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
                    
                }                
            }
            else {
                
                Utility.showAlert(ALERT_TITLE, message: responseDict.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            }
            
            CSActivityIndicator.hideAllFromMainWindow()
            
        }
        sendVerificationObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
            CSActivityIndicator.hideAllFromMainWindow()
            print("error ------------------------------\(error)")
        }
        
        var parameterDict = Dictionary <String, String>()
        parameterDict["cmd"] = "verifyUserVerificationCode"
        parameterDict["verification_code"] = self.verificationCode
        parameterDict["mobile_no"] = self.userPhoneNumber!
        sendVerificationObj.commonServiceHit(parameterDict)
        
    }
    
    func hitAddPhoneNumberService() {
        
        CSActivityIndicator.showToMainWindowWithMessage("Adding Phone Number. Please wait..")
        
        let sendVerificationObj = CommonServiceClass(httpRequestType: HTTP_REQUEST_TYPE.POST_REQUEST, requestTag: "SEND_VERIFICATION_CODE", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
        
        sendVerificationObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
            
            
            if errorMessage != nil
            {
                //print(response)
            }
            
            print(response as! NSDictionary)
            let responseDict = PhoneNumberAddedData(fromDictionary: response as! NSDictionary)
            
            if responseDict.errorCode == 0 {
                print(responseDict.data)

                
                Utility.showAlert(ALERT_TITLE, message: "A verification code has been sent to your phone", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            }
            else {
                
                Utility.showAlert(ALERT_TITLE, message: responseDict.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            }
            
            CSActivityIndicator.hideAllFromMainWindow()
            
        }
        sendVerificationObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
            CSActivityIndicator.hideAllFromMainWindow()
            print("error ------------------------------\(error)")
        }
        
        var parameterDict = Dictionary <String, String>()
        parameterDict["cmd"] = "sendVerificationCode"
        parameterDict["mobile_no"] = userPhoneNumber!
        sendVerificationObj.commonServiceHit(parameterDict)
    }
}
