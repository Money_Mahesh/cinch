//
//  ForgetPasswordViewController.swift
//  Cinch
//
//  Created by Amit Garg on 09/04/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class ForgetPasswordViewController: UIViewController {
    
    var emailAddress : String!
    @IBOutlet weak var textFieldForEmail: AGFloatLabelTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Utility.setRightNavigationButton(forViewController: self, withImage: "", withTitle: "Close")
        
        self.title = "Forgot Password"

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func rightBtnAction() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    //MARK: - Web Service hit
    
    func sendResetPassword() {
        
        CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Please wait...")
        
        let playerListServiceObj = RegisterationService(httpRequestType: HTTP_REQUEST_TYPE.POST_REQUEST, requestTag: "RESET_PASSWORD_SERVICE", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
        
        playerListServiceObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
            
            if errorMessage != nil
            {
                //print(response)
            }
            
            print(response as! NSDictionary)
            let responseDict = CommonDataAsArrayParser(fromDictionary: response as! NSDictionary)
            
            if responseDict.errorCode == 1 {
                
                Utility.showAlert(ALERT_TITLE, message: responseDict.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            }
            else {
                
                let alert = UIAlertController(title:ALERT_TITLE, message:"Reset Password link has been sent to your email ", preferredStyle: UIAlertControllerStyle.Alert)
                
                let okAction = UIAlertAction(title: ALERT_OK_BUTTON_TITLE, style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
                    self.dismissViewControllerAnimated(true, completion: nil)
                })
                
                
                alert.addAction(okAction)
               self.presentViewController(alert, animated: true, completion: nil)
                
                
                
            }
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
            
        }
        playerListServiceObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
            print("error ------------------------------\(error)")
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
            
        }
        
        var parameterDict = Dictionary <String, String>()
        parameterDict["cmd"] = "mailLinkToResetPassword"
        parameterDict["email"] = self.emailAddress
        
        playerListServiceObj.forgotPasswordHit(parameterDict)
    }
    

    @IBAction func doneButtonAction(sender: AnyObject) {
        
        self.view.endEditing(true)
        
        self.emailAddress = self.textFieldForEmail.text!
        
        if DataValidationUtility.isEmpty(self.emailAddress) == true {
            
            Utility.showAlert(ALERT_TITLE, message: "Please enter a email", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            
        } else if DataValidationUtility.isValidEmail(self.emailAddress) == false {
            
            Utility.showAlert(ALERT_TITLE, message: "Please enter a  valid email", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            
        } else {
            
            self.sendResetPassword()
            
            
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
