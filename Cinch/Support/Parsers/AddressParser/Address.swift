//
//	Address.swift
//
//	Create by Money Mahesh on 16/3/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class Address : NSObject, NSCoding{

	var city : String!
	var countryCode : String!
	var state : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		city = dictionary["city"] as? String
		countryCode = dictionary["country_code"] as? String
		state = dictionary["state"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if city != nil{
			dictionary["city"] = city
		}
		if countryCode != nil{
			dictionary["country_code"] = countryCode
		}
		if state != nil{
			dictionary["state"] = state
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         city = aDecoder.decodeObjectForKey("city") as? String
         countryCode = aDecoder.decodeObjectForKey("country_code") as? String
         state = aDecoder.decodeObjectForKey("state") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if city != nil{
			aCoder.encodeObject(city, forKey: "city")
		}
		if countryCode != nil{
			aCoder.encodeObject(countryCode, forKey: "country_code")
		}
		if state != nil{
			aCoder.encodeObject(state, forKey: "state")
		}

	}

}