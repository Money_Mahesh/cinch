//
//	Data.swift
//
//	Create by Money Mahesh on 11/3/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class PlayerDetail : NSObject, NSCoding{

	var playerSeq : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		playerSeq = dictionary["player_seq"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if playerSeq != nil{
			dictionary["player_seq"] = playerSeq
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         playerSeq = aDecoder.decodeObjectForKey("player_seq") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if playerSeq != nil{
			aCoder.encodeObject(playerSeq, forKey: "player_seq")
		}

	}

}