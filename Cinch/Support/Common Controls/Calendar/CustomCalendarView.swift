//
//  CalendarViewController.swift
//  Cinch
//
//  Created by MONEY on 15/03/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

protocol CalendarDelegate : NSObjectProtocol {
    func selectedDate(date: [Date])
}

class CustomCalendarView: UIView, CalendarViewDelegate {
    
    @IBOutlet var placeholderView: UIView!
    var selectedDates = [Date]()
    weak var delegate : CalendarDelegate?
    
    convenience init(frame: CGRect, selectedDates: [Date]) {
        self.init(frame: frame)
        
        setUpXib()
        
        setUpCalender(selectedDates)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)!
    }
    
    func setUpXib() {
        let view = loadViewFromNib()
        view.frame = self.bounds
        view.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        self.addSubview(view)
    }
    
    func loadViewFromNib() -> UIView{
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "CustomCalendarView", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        return view
    }

    func setUpCalender(selectedDates: [Date]) {
        // todays date.
        let date = NSDate()
        
        // create an instance of calendar view with
        // base date (Calendar shows 12 months range from current base date)
        // selected date (marked dated in the calendar)
        let calendarView = CalendarView.instance(date, selectedDate: selectedDates)
        calendarView.delegate = self
        calendarView.translatesAutoresizingMaskIntoConstraints = false
        placeholderView.addSubview(calendarView)
        
        // Constraints for calendar view - Fill the parent view.
        placeholderView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[calendarView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["calendarView": calendarView]))
        placeholderView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[calendarView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["calendarView": calendarView]))
    }
    
    func didSelectDate(date: [Date]) {
        
        selectedDates = date
        
    }
    
    @IBAction func doneBtnAction(sender: AnyObject) {
        
        for date1 in selectedDates {
            print("\(date1.year)-\(date1.month)-\(date1.day)")
        }
        
        if delegate != nil {
            delegate?.selectedDate(selectedDates)
        }
    }
    
    @IBAction func tapInBackground(sender: AnyObject) {
        self.removeFromSuperview()
    }
}