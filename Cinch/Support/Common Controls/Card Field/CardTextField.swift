//
//  CardTextField.swift
//  PracticeProject
//
//  Created by Hitesh on 3/11/16.
//  Copyright © 2016 Money. All rights reserved.
//

import UIKit

class CardTextField: UITextField, UITextFieldDelegate, CardTextFieldDelegate {

    @IBInspectable var noOfFields : Int = 4
    @IBInspectable var noOfDigitPerField : Int = 4
    
    weak var cardfieldDelegate : CardTextFieldDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.delegate = self
        self.addTarget(self, action: "textFieldDidChange", forControlEvents: .EditingChanged)
    }
    
    func textFieldDidChange () {
        let attributedString = NSMutableAttributedString(string: self.text!)
        
        let characterCount = Int((self.text?.characters.count)!)
        
        for index in 1..<noOfFields  {
            
            if characterCount >= noOfDigitPerField * index {
                attributedString.addAttribute(NSKernAttributeName, value: 5, range: NSMakeRange((noOfDigitPerField * index) - 1, 1))
            }
        }
        
        attributedString.addAttribute(NSFontAttributeName, value: UIFont(name: "Lato-Regular", size: 14)!, range: NSMakeRange(0, (self.text?.characters.count)!))
        
        if characterCount > noOfFields * noOfDigitPerField {
            attributedString.deleteCharactersInRange(NSMakeRange(noOfFields * noOfDigitPerField, 1))
        }
        
        self.attributedText = attributedString
        
        if cardfieldDelegate != nil {
            cardfieldDelegate?.cardTextFieldUpdate!(self.text!)
        }
    }
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}

@objc protocol CardTextFieldDelegate : NSObjectProtocol {
    optional func cardTextFieldUpdate(completeString : String)
}
