//
//	Schedule.swift
//
//	Create by Money Mahesh on 16/3/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class Schedule : NSObject, NSCoding{

	var divisionSeq : String!
	var dueDate : String!
	var dueDateCost : String!
	var instalmentType : String!
	var isPaid : String!
	var paidDate : String!
	var paymentScheduleSeq : String!
	var programName : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		divisionSeq = dictionary["division_seq"] as? String
		dueDate = dictionary["due_date"] as? String
		dueDateCost = dictionary["due_date_cost"] as? String
		instalmentType = dictionary["instalment_type"] as? String
		isPaid = dictionary["is_paid"] as? String
		paidDate = dictionary["paid_date"] as? String
		paymentScheduleSeq = dictionary["payment_schedule_seq"] as? String
		programName = dictionary["program_name"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if divisionSeq != nil{
			dictionary["division_seq"] = divisionSeq
		}
		if dueDate != nil{
			dictionary["due_date"] = dueDate
		}
		if dueDateCost != nil{
			dictionary["due_date_cost"] = dueDateCost
		}
		if instalmentType != nil{
			dictionary["instalment_type"] = instalmentType
		}
		if isPaid != nil{
			dictionary["is_paid"] = isPaid
		}
		if paidDate != nil{
			dictionary["paid_date"] = paidDate
		}
		if paymentScheduleSeq != nil{
			dictionary["payment_schedule_seq"] = paymentScheduleSeq
		}
		if programName != nil{
			dictionary["program_name"] = programName
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         divisionSeq = aDecoder.decodeObjectForKey("division_seq") as? String
         dueDate = aDecoder.decodeObjectForKey("due_date") as? String
         dueDateCost = aDecoder.decodeObjectForKey("due_date_cost") as? String
         instalmentType = aDecoder.decodeObjectForKey("instalment_type") as? String
         isPaid = aDecoder.decodeObjectForKey("is_paid") as? String
         paidDate = aDecoder.decodeObjectForKey("paid_date") as? String
         paymentScheduleSeq = aDecoder.decodeObjectForKey("payment_schedule_seq") as? String
         programName = aDecoder.decodeObjectForKey("program_name") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if divisionSeq != nil{
			aCoder.encodeObject(divisionSeq, forKey: "division_seq")
		}
		if dueDate != nil{
			aCoder.encodeObject(dueDate, forKey: "due_date")
		}
		if dueDateCost != nil{
			aCoder.encodeObject(dueDateCost, forKey: "due_date_cost")
		}
		if instalmentType != nil{
			aCoder.encodeObject(instalmentType, forKey: "instalment_type")
		}
		if isPaid != nil{
			aCoder.encodeObject(isPaid, forKey: "is_paid")
		}
		if paidDate != nil{
			aCoder.encodeObject(paidDate, forKey: "paid_date")
		}
		if paymentScheduleSeq != nil{
			aCoder.encodeObject(paymentScheduleSeq, forKey: "payment_schedule_seq")
		}
		if programName != nil{
			aCoder.encodeObject(programName, forKey: "program_name")
		}

	}

}