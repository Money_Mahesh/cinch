//
//  SignUpViewController.swift
//  Cinch
//
//  Created by MONEY on 14/02/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {

    @IBOutlet weak var containerView: UIScrollView!
    
    @IBOutlet weak var textFieldForName: AGFloatLabelTextField!
    @IBOutlet weak var textFieldForEmail: AGFloatLabelTextField!
    @IBOutlet weak var textFieldForPassword: AGFloatLabelTextField!
    @IBOutlet weak var textFieldForConfirmPassword: AGFloatLabelTextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        refreshScreen()
    }
    
    func refreshScreen() {
        textFieldForName.text = ""
        textFieldForEmail.text = ""
        textFieldForPassword.text = ""
        textFieldForConfirmPassword.text = ""
    }
    
    //MARK: - IBActions
    @IBAction func nextBtnAction(sender: AnyObject) {
        
        if textFieldForName.text?.characters.count == 0 {
            Utility.showAlert(nil, message: "Please enter the name", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
        }
        else if textFieldForEmail.text?.characters.count == 0 {
            Utility.showAlert(nil, message: "Please enter the email id", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
        }
        else if DataValidationUtility.isValidEmail(textFieldForEmail.text!) == false {
            Utility.showAlert(nil, message: "Please enter the valid email id", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
        }
        else if textFieldForPassword.text?.characters.count == 0 {
            Utility.showAlert(nil, message: "Please enter the password", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
        }
        else if textFieldForConfirmPassword.text?.characters.count == 0 {
            Utility.showAlert(nil, message: "Please enter the confirm password", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
        }
        else if textFieldForConfirmPassword.text != textFieldForPassword.text {
            Utility.showAlert(nil, message: "Confirm password doesn't match the password", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            textFieldForConfirmPassword.text = ""
        }
        else {
            self.view.endEditing(true)
            self.hitSignUpWebService(CSAccountType.Simple)
        }
    }
    
    
    @IBAction func facebookAction(sender: AnyObject) {
        self.view.endEditing(true)
        
        textFieldForEmail.text = ""
        textFieldForPassword.text = ""
        textFieldForName.text = ""
        textFieldForConfirmPassword.text = ""
        
        CSActivityIndicator.showToMainViewWithMessage("Fetching facebook account details.")
        
        FacebookClass.loginToFacebookWithSuccess(ROOT_NAVVIGATION_CONROLLER.topViewController!, successBlock: { () -> () in
            
            CSActivityIndicator.hideAllFromMainView()
            
            self.hitSignUpWebService(CSAccountType.Facebook)
            
            },andFailure: { (error: NSError?) -> () in
                CSActivityIndicator.hideAllFromMainView()
                
                print("Error \(error)")
        })
    }
    
    //MARK:- Hit Webservice
    func hitSignUpWebService(accountType: CSAccountType) {
        
        
        CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Signing up")

        let signUpObj = RegisterationService(httpRequestType: HTTP_REQUEST_TYPE.POST_REQUEST, requestTag: "SIGN_UP_SERVICE", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
        
        signUpObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
            
            
            if errorMessage != nil
            {
                //print(response)
            }
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)

            print(response as! NSDictionary)

            let signInData =  CSAccountDetail(fromDictionary: response as! NSDictionary)
            
            if signInData.errorCode == 0 {
                
                let userData = signInData.data
                
                UserProfileManager.userSequence = userData.userSeq
                
                if accountType == .Simple {
                    UserProfileManager.name = self.textFieldForName.text
                    UserProfileManager.emailId = self.textFieldForEmail.text
                    UserProfileManager.fbID = nil
                }
                
                UserProfileManager.gender = ""
                UserProfileManager.accountType = "P"
                
                Utility.saveStringInDefault(SIGN_UP_STATUS, value: "CreateAccountViewController")
                
                let createAccountViewControllerObj = amitStoryBoard.instantiateViewControllerWithIdentifier("CreateAccountViewController") as! CreateAccountViewController
                
                ROOT_NAVVIGATION_CONROLLER.pushViewController(createAccountViewControllerObj, animated: true)
            }
            else {
                Utility.showAlert(ALERT_TITLE, message: signInData.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            }
            
        }
        signUpObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)

            print("error ------------------------------\(error)")
        }
        
        var parameterDict = Dictionary <String, String>()
        
        parameterDict["cmd"] = "signupUser"
        parameterDict["name"] = accountType == .Simple ? textFieldForName.text : UserProfileManager.name
        parameterDict["email"] = accountType == .Simple ? textFieldForEmail.text : UserProfileManager.emailId
        parameterDict["passwd"] = accountType == .Simple ? textFieldForPassword.text : "*"
        parameterDict["acc_type"] = "P"
        parameterDict["role"] = ""
        parameterDict["term_accept"] = "Y"
        parameterDict["api_id"] = accountType == .Simple ? "" : UserProfileManager.fbID
        parameterDict["login_type"] = accountType == .Simple ? "" : "F"

       
        print("\(parameterDict)")

        signUpObj.signUpHit(parameterDict)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
