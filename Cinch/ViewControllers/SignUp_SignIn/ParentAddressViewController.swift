//
//  ParentAddressViewController.swift
//  Cinch
//
//  Created by Amit Garg on 19/02/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

protocol AddressViewDelegate : NSObjectProtocol {
    func addPlayerAddress(addressDetail: FatherAdd?)
}

class ParentAddressViewController: UIViewController {

    @IBOutlet weak var sameAsMotherButton: CSCustomButton!
    @IBOutlet weak var sameAsFatherButton: CSCustomButton!
    @IBOutlet weak var sameAsGaurdianButton: CSCustomButton!
    @IBOutlet weak var constForSameAddressBtnView: NSLayoutConstraint!
    
    @IBOutlet weak var textFieldForName: AGFloatLabelTextField!
    @IBOutlet weak var textFieldForAddress1: AGFloatLabelTextField!
    @IBOutlet weak var textFieldForAddress2: AGFloatLabelTextField!
    @IBOutlet weak var textfFieldForZipCode: AGFloatLabelTextField!
    @IBOutlet weak var textFieldForCity: AGFloatLabelTextField!
    var motherAddress : FatherAdd?
    var fatherAddress : FatherAdd?
    var guardianAddress : FatherAdd?
    
    weak var delegate: AddressViewDelegate?

    var addressDetail : FatherAdd?
    var selectedPlayer: PlayerData?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.getAddressForPlayer()
        
        Utility.setBackButton(forViewController: self, withTitle: "")

        self.title = "Parent Address"
        
//        if UserProfileManager.role == "F" { //F - Father
//            
//            self.sameAsFatherButton.enabled = true
//            self.sameAsMotherButton.enabled = false
//            self.sameAsGaurdianButton.enabled = false
//
//            
//        } else if UserProfileManager.role == "M" {
//            
//            self.sameAsFatherButton.enabled = false
//            self.sameAsMotherButton.enabled = true
//            self.sameAsGaurdianButton.enabled = false
//
//        }
//        else {
//            self.sameAsFatherButton.enabled = false
//            self.sameAsMotherButton.enabled = false
//            self.sameAsGaurdianButton.enabled = true
//        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.hidden = false
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.hidden = true
    }

    @IBAction func addressCopyTapped(sender: CSCustomButton) {
       
        self.sameAsFatherButton.selected = false
        self.sameAsMotherButton.selected = false
        self.sameAsGaurdianButton.selected = false

        self.sameAsMotherButton.backgroundColor = UIColor.clearColor()
        self.sameAsFatherButton.backgroundColor = UIColor.clearColor()
        self.sameAsGaurdianButton.backgroundColor = UIColor.clearColor()

        sender.selected = true
        if sender.selected == true {
            sender.backgroundColor = UIColor(red: 0/255, green: 169/255, blue: 255/255, alpha: 1.0)
        }
        
        switch sender.titleLabel!.text! {
        case "SAME AS FATHER":
            self.updateValues(self.fatherAddress!)
        case "SAME AS MOTHER":
            self.updateValues(self.motherAddress!)
        case "SAME AS GUARDIAN":
            self.updateValues(self.guardianAddress!)
        default:
            print("Something Broken")
        }
    }
    
    
    @IBAction func doneButtonTapped(sender: CSCustomButton) {
        
        selectedPlayer?.address = self.textFieldForAddress1.text
        selectedPlayer?.zipcode = self.textfFieldForZipCode.text
        selectedPlayer?.city = self.textFieldForCity.text

        self.delegate?.addPlayerAddress(addressDetail)
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func backAction() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
//    func getParentAddressData(role : String) {
//        
//        CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Getting Parent Address. Please wait..")
//        
//        let clubListServiceObj = ClubListServices(httpRequestType: HTTP_REQUEST_TYPE.POST_REQUEST, requestTag: "GET_PARENT_ADDRESS_SERVICE", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
//        
//        clubListServiceObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
//            
//            
//            if errorMessage != nil
//            {
//                //print(response)
//            }
//            
//            print(response as! NSDictionary)
//            let responseDict = CSUserListDataWithAddress.init(fromDictionary: response as! NSDictionary)
//            
//            if responseDict.errorCode == 1 {
//                Utility.showAlert(ALERT_TITLE, message: responseDict.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
//            }
//            else {
//                print(responseDict.data)
//                
//                if (responseDict.data.count != 0) {
//                    
//                    
//                    
//    
//                }
//                else {
//                    Utility.showAlert(ALERT_TITLE, message: "Data not found", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
//                }
//            }
//            CSActivityIndicator.hideAllFromMainWindow()
//
//        }
//        clubListServiceObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
//            print("error ------------------------------\(error)")
//            CSActivityIndicator.hideAllFromMainWindow()
//
//        }
//        
//        
//        var parameterDict = Dictionary <String, String>()
//        parameterDict["cmd"] = "getUserList"
//        parameterDict["user_seq"] = UserProfileManager.userSequence
//        parameterDict["role"] = role
//        
//        clubListServiceObj.clubListHit(parameterDict)
//    }
    
    func getAddressForPlayer() {
        
        CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Getting Parent Address. Please wait..")
        
        let clubListServiceObj = CommonServiceClass(httpRequestType: HTTP_REQUEST_TYPE.POST_REQUEST, requestTag: "GET_PARENT_ADDRESS_SERVICE", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
        
        clubListServiceObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
            
            
            if errorMessage != nil
            {
                //print(response)
            }
            
            print(((response as! NSDictionary).valueForKey("data") as! NSDictionary).allKeys)
            let responseDict = AddressListData.init(fromDictionary: response as! NSDictionary)
            
            if responseDict.errorCode == 1 {
                Utility.showAlert(ALERT_TITLE, message: responseDict.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            }
            else {
                print(responseDict.data)
                
                if (responseDict.data != nil) {
                    
                    var tempArray = [FatherAdd]()
                    
                    if responseDict.data.motherAdd != nil {
                      tempArray.append(responseDict.data.motherAdd)
                       self.motherAddress = responseDict.data.motherAdd
                    }
                    
                    if responseDict.data.fatherAdd != nil {
                        tempArray.append(responseDict.data.fatherAdd)
                        self.fatherAddress = responseDict.data.fatherAdd

                    }
                    
                    if responseDict.data.guardianAdd != nil {
                        tempArray.append(responseDict.data.guardianAdd)
                        self.guardianAddress = responseDict.data.guardianAdd

                    }
                
                    self.updateView(tempArray)
                    
                }
                else {
                    Utility.showAlert(ALERT_TITLE, message: "Data not found", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
                }
            }
            CSActivityIndicator.hideAllFromMainWindow()
            
        }
        clubListServiceObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
            print("error ------------------------------\(error)")
            CSActivityIndicator.hideAllFromMainWindow()
            
        }
        
        
        var parameterDict = Dictionary <String, String>()
        parameterDict["cmd"] = "getAddressForPlayer"
        
        if UserProfileManager.role == "M" {
            parameterDict["mother_seq"] = UserProfileManager.userSequence!
            parameterDict["father_seq"] = ""
            parameterDict["guardian_seq"] = ""
            
        } else if UserProfileManager.role == "F"  {
            parameterDict["mother_seq"] = ""
            parameterDict["father_seq"] = UserProfileManager.userSequence!
            parameterDict["guardian_seq"] = ""
            
        } else if UserProfileManager.role == "G" {
            parameterDict["mother_seq"] = ""
            parameterDict["father_seq"] = ""
            parameterDict["guardian_seq"] = UserProfileManager.userSequence!
            
        }
        
        clubListServiceObj.commonServiceHit(parameterDict)
    }
    
    func updateView(address : [FatherAdd]) {
       
        if address.count == 1 {
            self.sameAsFatherButton.hidden = true
            self.sameAsMotherButton.hidden = true
            self.sameAsGaurdianButton.hidden = false
        } else if address.count == 2 {
            self.sameAsFatherButton.hidden = false
            self.sameAsMotherButton.hidden = false
            self.sameAsGaurdianButton.hidden = true
        } else if address.count == 3 {
            self.sameAsFatherButton.hidden = false
            self.sameAsMotherButton.hidden = false
            self.sameAsGaurdianButton.hidden = false
        }
        
        var index = 0
        
        for obj in address {
            
            if address.count == 1 {
               
                if obj.role == "M" {
                    
                    self.sameAsGaurdianButton.setTitleForAllState("SAME AS MOTHER")
                    
                } else if  obj.role == "F"{
                    
                    self.sameAsGaurdianButton.setTitleForAllState("SAME AS FATHER")
                    
                } else {
                    
                    self.sameAsGaurdianButton.setTitleForAllState("SAME AS GUARDIAN")
                    
                }
                
            } else if  address.count == 2 {
            
                if index == 0 {
                    
                    if obj.role == "M" {
                        
                        self.sameAsFatherButton.setTitleForAllState("SAME AS MOTHER")
                        
                    } else if  obj.role == "F"{
                        
                        self.sameAsFatherButton.setTitleForAllState("SAME AS FATHER")
                        
                    } else {
                        
                        self.sameAsFatherButton.setTitleForAllState("SAME AS GUARDIAN")
                        
                    }
                    
                } else if index == 1 {
                    
                    if obj.role == "M" {
                        
                        self.sameAsMotherButton.setTitleForAllState("SAME AS MOTHER")
                        
                    } else if  obj.role == "F"{
                        
                        self.sameAsMotherButton.setTitleForAllState("SAME AS FATHER")
                        
                    } else {
                        
                        self.sameAsMotherButton.setTitleForAllState("SAME AS GUARDIAN")
                        
                    }
                    
                }
               
            } else if  address.count == 3 {
                
                if index == 0 {
                    
                    if obj.role == "M" {
                        
                        self.sameAsFatherButton.setTitleForAllState("SAME AS MOTHER")
                        
                    } else if  obj.role == "F"{
                        
                        self.sameAsFatherButton.setTitleForAllState("SAME AS FATHER")
                        
                    } else {
                        
                        self.sameAsFatherButton.setTitleForAllState("SAME AS GUARDIAN")
                        
                    }
                    
                } else if index == 1 {
                    
                    if obj.role == "M" {
                        
                        self.sameAsGaurdianButton.setTitleForAllState("SAME AS MOTHER")
                        
                    } else if  obj.role == "F"{
                        
                        self.sameAsGaurdianButton.setTitleForAllState("SAME AS FATHER")
                        
                    } else {
                        
                        self.sameAsGaurdianButton.setTitleForAllState("SAME AS GUARDIAN")
                        
                    }
                    
                }
                else if index == 2 {
                    
                    if obj.role == "M" {
                        
                        self.sameAsMotherButton.setTitleForAllState("SAME AS MOTHER")
                        
                    } else if  obj.role == "F"{
                        
                        self.sameAsMotherButton.setTitleForAllState("SAME AS FATHER")
                        
                    } else {
                        
                        self.sameAsMotherButton.setTitleForAllState("SAME AS GUARDIAN")
                        
                    }
                    
                }
                
                
            }
           
            index++
            
        }
        
    }
    
    func updateValues(fromDictionary : FatherAdd!) {
        
        self.textFieldForName.text = fromDictionary.name
//        self.textFieldForAddress1.text = fromDictionary.address
        self.textFieldForAddress2.text = fromDictionary.address
        self.textfFieldForZipCode.text = fromDictionary.zipcode
        self.textFieldForCity.text = fromDictionary.city
        
        self.addressDetail = fromDictionary
        
    }
    
}
