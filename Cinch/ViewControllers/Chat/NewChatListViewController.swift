//
//  NewChatListViewController.swift
//  Cinch
//
//  Created by Amit Garg on 14/03/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class NewChatListViewController: UIViewController {

    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var newChatListTableView: UITableView!
    
    var nameArray = [ChatPlayerData]()
    var filteredArray = [ChatPlayerData]()
    
    // *** STEP 1: STORE FIREBASE REFERENCES
    var messagesRef: Firebase!
    var groupSeq: String!
    var groupMemberSeq = String()
    
    //    var sender : String?
    
    func setupFirebase(chatPlayerSelected: ChatPlayerData) {
        
        let messageViewControllerObj = amitStoryBoard.instantiateViewControllerWithIdentifier("MessageViewController") as! MessageViewController
        
        messageViewControllerObj.senderDisplayName = chatPlayerSelected.name
        
        let chatNodeID: String!
        if Int(UserProfileManager.userSequence!) < Int(chatPlayerSelected.userSeq!) {
            chatNodeID = ("\(UserProfileManager.userSequence!)-\(chatPlayerSelected.userSeq)")
        }
        else {
            chatNodeID = ("\(chatPlayerSelected.userSeq)-\(UserProfileManager.userSequence!)")
        }
        
        messageViewControllerObj.chatNodeID = chatNodeID
        messageViewControllerObj.receiverNode = chatPlayerSelected.userSeq!
        messageViewControllerObj.receiverId = chatPlayerSelected.userSeq!
        messageViewControllerObj.profilePicUrl = chatPlayerSelected.profilePic!

        self.navigationController?.pushViewController(messageViewControllerObj, animated: true)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "New Message"
        Utility.setBackButton(forViewController: self, withTitle: "")
        
        
        self.getPlayerListService()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- TableView Data Source and Delegates
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        if self.nameArray.count > 0 {
            return 1
        }
        return 0
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView(frame: CGRectMake(0, 0, MAIN_SCREEN_WIDTH, 22))
        headerView.backgroundColor = UIColor(red: 247.0/255.0, green: 247.0/255.0, blue: 247.0/255.0, alpha: 1.0)
        let label = UILabel(frame: CGRectMake(16, 0, MAIN_SCREEN_WIDTH - 16.0, 22))
        label.text = "Title \(section)"
        label.font = UIFont(name: "Lato-Black", size: 12.0)
        label.textColor = UIColor.blackColor()
        
        headerView.addSubview(label)
        return headerView
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRectMake(0, 0, MAIN_SCREEN_WIDTH, 1))
        footerView.backgroundColor = UIColor(red: 247.0/255.0, green: 247.0/255.0, blue: 247.0/255.0, alpha: 1.0)
        
        return footerView
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filteredArray.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let availabilityEventCell = tableView.dequeueReusableCellWithIdentifier("AvailabilityTableViewCell") as! AvailabilityEventCell
        
        let chatPlayer = self.filteredArray[indexPath.row]
        availabilityEventCell.labelForName.text = chatPlayer.name
        availabilityEventCell.labelForStatus.text = ""
        Utility.setImageWithURl(chatPlayer.profilePic, inImageView: availabilityEventCell.imageViewForProfilePic)
        return availabilityEventCell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if filteredArray[indexPath.row].userSeq == UserProfileManager.userSequence {
            Utility.showAlert(ALERT_TITLE, message: "You can not send message to yourself", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
        }
        else {
            setupFirebase(self.filteredArray[indexPath.row])
        }
    }
    
    //MARK:- Textfield Delegate
    @IBAction func textfieldDidChanged(sender: UITextField) {
        
        if sender.text == "" {
            filteredArray = nameArray
        }
        else {
            filteredArray = nameArray.filter() {
                return $0.name.lowercaseString.rangeOfString(String(sender.text!).lowercaseString) == nil ? false : true
            }
        }
        newChatListTableView.reloadData()
    }

    //MARK:- IBActions
    @IBAction func createNewBtnAction(sender: AnyObject) {
        let groupChatViewControllerObj = amitStoryBoard.instantiateViewControllerWithIdentifier("GroupChatViewController") as! GroupChatViewController
        groupChatViewControllerObj.nameArray = self.nameArray
        self.navigationController!.pushViewController(groupChatViewControllerObj, animated: true)
    }
    
    func backAction() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    //MARK: - Web Service hit
    func getPlayerListService () {
        
        CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Fetching Chat List...")
        
        let playerListServiceObj = CommonServiceClass(httpRequestType: HTTP_REQUEST_TYPE.POST_REQUEST, requestTag: "GET_CHAT_LIST_SERVICE", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
        
        playerListServiceObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
            
            if errorMessage != nil
            {
                //print(response)
            }
            
            print(response as! NSDictionary)
            let responseDict = ChatListData(fromDictionary: response as! NSDictionary)
            
            if responseDict.errorCode == 1 {
                
                Utility.showAlert(ALERT_TITLE, message: responseDict.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            }
            else {
                
                print(responseDict.data)
                
                self.nameArray.appendContentsOf(responseDict.data)
                self.filteredArray = self.nameArray
                
                if responseDict.data.count > 0 {
                    
                    self.newChatListTableView.reloadData()
                    
                } else {
                    
                    Utility.showAlert(ALERT_TITLE, message: "Oops! No player added", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
                }
                
                
            }
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
            
        }
        playerListServiceObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
            print("error ------------------------------\(error)")
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
            
        }
        
        
        var parameterDict = Dictionary <String, String>()
        parameterDict["cmd"] = "getUserListForChat"
        parameterDict["team_seq"] = PlayerDetailManager.eventSelected!.teamSeq
        
        playerListServiceObj.commonServiceHit(parameterDict)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
