//
//  HistoryViewController.swift
//  Cinch
//
//  Created by Amit Garg on 17/03/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class HistoryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    var completedPayments : [PaymentData]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 75
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.getTransactionListService()
        self.navigationController?.navigationBarHidden = false
        self.tabBarController?.navigationItem.title = "Transaction History"

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        
        if self.completedPayments?.count > 0 {
            return (self.completedPayments?.count)!
        }
        
        return 0
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let transactionHistoryCell = tableView.dequeueReusableCellWithIdentifier("transactionHistoryTableCell", forIndexPath: indexPath) as! TransactionHistoryTableCell
        transactionHistoryCell.selectionStyle = .None
        
        let completedObj = self.completedPayments![indexPath.row]
        transactionHistoryCell.programName.text = completedObj.programName
        transactionHistoryCell.dueCost.text = "$\(completedObj.dueDateCost) Paid on \(completedObj.dueDate)"
        Utility.setImageWithURl(completedObj.programImage, inImageView: transactionHistoryCell.programImageView)
        
        transactionHistoryCell.statusLabel.text = "PAID"
        
        return transactionHistoryCell
    }
    
    // MARK: - Table view delegate
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        
    }
    
    //MARK: - Web Service hit
    
    func getTransactionListService () {
        
        CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Fetching Balance Data...")
        
        let playerListServiceObj = CommonServiceClass(httpRequestType: HTTP_REQUEST_TYPE.POST_REQUEST, requestTag: "GET_BALANCE_SERVICE", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
        
        playerListServiceObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
            
            if errorMessage != nil
            {
                //print(response)
            }
            
            print(response as! NSDictionary)
            let responseDict = PaymentListData(fromDictionary: response as! NSDictionary)
            
            if responseDict.errorCode == 1 {
                
                Utility.showAlert(ALERT_TITLE, message: responseDict.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            }
            else {
                
                
                self.completedPayments = []

                let tempArray = responseDict.data
                print(tempArray)

                for paymentObj in tempArray {
                    print(paymentObj.isPaid)

                    if paymentObj.isPaid == "Y" {
                        self.completedPayments?.append(paymentObj)
                    }
                }
                
                if self.completedPayments?.count > 0 {
                    self.tableView.reloadData()
                }
                
            }
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
            
        }
        playerListServiceObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
            print("error ------------------------------\(error)")
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
            
        }
        
        var parameterDict = Dictionary <String, String>()
        parameterDict["cmd"] = "getPlayerPaymentScheduleByDivision"
        parameterDict["player_seq"] = PlayerDetailManager.playerDetail?.playerSeq!
        
        playerListServiceObj.commonServiceHit(parameterDict)
    }
}

class TransactionHistoryTableCell : UITableViewCell {
    
    @IBOutlet weak var programImageView: UIImageView!
    @IBOutlet weak var programName: UILabel!
    @IBOutlet weak var dueCost: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    
    
}
