//
//	Data.swift
//
//	Create by Amit Garg on 5/3/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class ProgramData : NSObject, NSCoding {

    var programDescription : String!
	var divisionName : String!
	var divisionSeq : String!
	var isBirthCert : String!
	var name : String!
	var programSeq : String!
    var fee_amount : String!
    var volunteerEnabled : String!

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
    override init() {
        programDescription = ""
        divisionName = ""
        divisionSeq = ""
        isBirthCert = ""
        name = ""
        programSeq = ""
        fee_amount = ""
        volunteerEnabled = ""
    }
    
	init(fromDictionary dictionary: NSDictionary){
        programDescription = dictionary["description"] as? String
		divisionName = dictionary["division_name"] as? String
		divisionSeq = dictionary["division_seq"] as? String
		isBirthCert = dictionary["is_birth_cert"] as? String
		name = dictionary["name"] as? String
		programSeq = dictionary["program_seq"] as? String
        fee_amount = dictionary["fee_amount"] as? String
        volunteerEnabled = dictionary["volunteerEnabled"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
        if programDescription != nil{
            dictionary["description"] = programDescription
        }
		if divisionName != nil{
			dictionary["division_name"] = divisionName
		}
		if divisionSeq != nil{
			dictionary["division_seq"] = divisionSeq
		}
		if isBirthCert != nil{
			dictionary["is_birth_cert"] = isBirthCert
		}
		if name != nil{
			dictionary["name"] = name
		}
		if programSeq != nil{
			dictionary["program_seq"] = programSeq
		}
        if fee_amount != nil{
            dictionary["fee_amount"] = fee_amount
        }
        if volunteerEnabled != nil{
            dictionary["volunteerEnabled"] = volunteerEnabled
        }

		return dictionary
	}

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        programDescription = aDecoder.decodeObjectForKey("description") as? String
        divisionName = aDecoder.decodeObjectForKey("divisionName") as? String
        divisionSeq = aDecoder.decodeObjectForKey("divisionSeq") as? String
        isBirthCert = aDecoder.decodeObjectForKey("isBirthCert") as? String
        name = aDecoder.decodeObjectForKey("name") as? String
        programSeq = aDecoder.decodeObjectForKey("programSeq") as? String
        fee_amount = aDecoder.decodeObjectForKey("fee_amount") as? String
        volunteerEnabled = aDecoder.decodeObjectForKey("volunteerEnabled") as? String
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encodeWithCoder(aCoder: NSCoder)
    {
        if programDescription != nil{
            aCoder.encodeObject(programDescription, forKey: "description")
        }
        if divisionName != nil{
            aCoder.encodeObject(divisionName, forKey: "divisionName")
        }
        if divisionSeq != nil{
            aCoder.encodeObject(divisionSeq, forKey: "divisionSeq")
        }
        if isBirthCert != nil{
            aCoder.encodeObject(isBirthCert, forKey: "isBirthCert")
        }
        if name != nil{
            aCoder.encodeObject(name, forKey: "name")
        }
        if programSeq != nil{
            aCoder.encodeObject(programSeq, forKey: "programSeq")
        }
        if fee_amount != nil{
            aCoder.encodeObject(fee_amount, forKey: "fee_amount")
        }
        if volunteerEnabled != nil{
            aCoder.encodeObject(programSeq, forKey: "volunteerEnabled")
        }
    }
}