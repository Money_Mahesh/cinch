//
//  UpcomingEventViewController.swift
//  Cinch
//
//  Created by MONEY on 27/02/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class UpcomingEventViewController: UIViewController {

    @IBOutlet weak var eventTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
//        self.navigationItem.setHidesBackButton(true, animated:true);
        if revealViewController() != nil {
            Utility.setMenuButton(forViewController: self, revealController: revealViewController())
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            
        }
        
        //Utility.setRightNavigationButton(forViewController: self, withImage: "+", withTitle: "")
        
        
        //Table Footer View
        let tableFooterView = UIView(frame: CGRectMake(0, 0, MAIN_SCREEN_WIDTH, 1))
        tableFooterView.backgroundColor = UIColor(red: 238.0/255.0, green: 238.0/255.0, blue: 238.0/255.0, alpha: 1.0)
        eventTableView.tableFooterView = tableFooterView
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.topItem?.title = "Upcoming Events"
//        Utility.setRightNavigationButton(forViewController: self, withImage: "Edit Icon", withTitle:"")

        let rightButton: UIButton = UIButton()
        rightButton.setImage(UIImage(named: "Edit Icon"), forState: .Normal)
        rightButton.frame = CGRectMake(0.0, 0.0, 38.0, 44.0)
        
        //Adding Action Left Navigation Bar Button Item.
        rightButton.addTarget(self, action: "rightBtnAction", forControlEvents: .TouchUpInside)
        rightButton.setTitle("", forState: .Normal)
        rightButton.titleLabel?.font = UIFont(name: "Lato-Regular", size: 12)
        rightButton.setTitleColorForAllState(UIColor(red: 160/255, green: 160/255, blue: 160/255, alpha: 1.0))
        
        let rightItem:UIBarButtonItem = UIBarButtonItem()
        rightItem.customView = rightButton
        
        
        self.tabBarController?.navigationItem.rightBarButtonItem = rightItem
        //self.navigationController?.navigationBar.hidden = false
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    // MARK:- TableView Data Source and Delegates
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 57
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let upComingEventCell = tableView.dequeueReusableCellWithIdentifier("UpComingEventTableViewCell") as! UpComingEventTableViewCell
        
        return upComingEventCell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let eventViewControllerObj = mainStoryBoard.instantiateViewControllerWithIdentifier("EventViewController")
        self.navigationController?.pushViewController(eventViewControllerObj, animated: true)
    }

    //MARK: - IBActions
    func backAction() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func rightBtnAction() {
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

class UpComingEventTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imageViewForBigMapPin: UIImageView!
    @IBOutlet weak var labelForEventName: UILabel!
    @IBOutlet weak var labelForEventTime: UILabel!
    @IBOutlet weak var switchForAvailabilty: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        switchForAvailabilty.tintColor = UIColor(red: 0.0/255.0, green: 159/255, blue: 232/255, alpha: 1.0)
        switchForAvailabilty.onTintColor = UIColor(red: 0.0/255.0, green: 159/255, blue: 232/255, alpha: 1.0)
    }
    
    @IBAction func switchValueChanged(sender: AnyObject) {
        print("switchValueChanged")
        
    }
}
