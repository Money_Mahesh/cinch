import UIKit

class ServiceRequestManager: AFHTTPRequestOperationManager {
    
    
    override func GET(URLString: String, parameters: AnyObject?, success: ((AFHTTPRequestOperation, AnyObject) -> Void)?, failure: ((AFHTTPRequestOperation?, NSError) -> Void)?) -> AFHTTPRequestOperation? {
        
        let status = AFNetworkReachabilityManager.sharedManager().networkReachabilityStatus
        let networkType : String
        
        if status == AFNetworkReachabilityStatus.ReachableViaWWAN || status == AFNetworkReachabilityStatus.ReachableViaWiFi {
            networkType = "3g"
        }else{
            networkType = "2g"
        }
        
        if parameters != nil && (parameters is Dictionary<String, String>) {
            var requestParameters = parameters  as! Dictionary<String, String>
            requestParameters["networkType"] = networkType
            
            return super.GET(URLString, parameters: requestParameters, success: success, failure: failure)
        }
        
        
        var completionBlockWithFailure: ((AFHTTPRequestOperation?, NSError) -> Void)?
        completionBlockWithFailure  = ({(operation: AFHTTPRequestOperation?, error : NSError?) -> Void in
            InternetConnectivityManager.checkForServerError(error!)
            failure!(operation, error!)
        })
        
        return super.GET(URLString, parameters: parameters, success: success, failure: completionBlockWithFailure)
        
        
    }
    
    override func POST(URLString: String, parameters: AnyObject?, constructingBodyWithBlock block: ((AFMultipartFormData) -> Void)?, success: ((AFHTTPRequestOperation, AnyObject) -> Void)?, failure: ((AFHTTPRequestOperation?, NSError) -> Void)?) -> AFHTTPRequestOperation? {
        
        let status = AFNetworkReachabilityManager.sharedManager().networkReachabilityStatus
        let networkType : String
        
        if status == AFNetworkReachabilityStatus.ReachableViaWWAN || status == AFNetworkReachabilityStatus.ReachableViaWiFi {
            networkType = "3g"
        }else{
            networkType = "2g"
        }
        
        if parameters != nil && (parameters is Dictionary<String, String>) {
            var requestParameters = parameters  as! Dictionary<String, String>
            requestParameters["networkType"] = networkType
            
            return super.POST(URLString, parameters: requestParameters, constructingBodyWithBlock: block, success: success, failure: failure)
        }
        
        var completionBlockWithFailure: ((AFHTTPRequestOperation?, NSError) -> Void)?
        completionBlockWithFailure  = ({(operation: AFHTTPRequestOperation?, error : NSError?) -> Void in
            InternetConnectivityManager.checkForServerError(error!)
            failure!(operation, error!)
        })
        
        return super.POST(URLString, parameters: parameters, constructingBodyWithBlock: block, success: success, failure: completionBlockWithFailure)
    }
}
