//
//	Event_ScheduleList.swift
//
//	Create by Money Mahesh on 16/3/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class Event_ScheduleList : NSObject, NSCoding{

	var events : [Event]!
	var schedule : [Schedule]!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		events = [Event]()
		if let eventsArray = dictionary["events"] as? [NSDictionary]{
			for dic in eventsArray{
				let value = Event(fromDictionary: dic)
				events.append(value)
			}
		}
		schedule = [Schedule]()
		if let scheduleArray = dictionary["schedule"] as? [NSDictionary]{
			for dic in scheduleArray{
				let value = Schedule(fromDictionary: dic)
				schedule.append(value)
			}
		}
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if events != nil{
			var dictionaryElements = [NSDictionary]()
			for eventsElement in events {
				dictionaryElements.append(eventsElement.toDictionary())
			}
			dictionary["events"] = dictionaryElements
		}
		if schedule != nil{
			var dictionaryElements = [NSDictionary]()
			for scheduleElement in schedule {
				dictionaryElements.append(scheduleElement.toDictionary())
			}
			dictionary["schedule"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         events = aDecoder.decodeObjectForKey("events") as? [Event]
         schedule = aDecoder.decodeObjectForKey("schedule") as? [Schedule]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if events != nil{
			aCoder.encodeObject(events, forKey: "events")
		}
		if schedule != nil{
			aCoder.encodeObject(schedule, forKey: "schedule")
		}

	}

}