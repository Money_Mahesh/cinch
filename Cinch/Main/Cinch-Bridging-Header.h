//
//  Cinch-Bridging-Header.h
//  Cinch
//
//  Created by MONEY on 13/02/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

#ifndef Cinch_Bridging_Header_h
#define Cinch_Bridging_Header_h

@import AFNetworking;
#import <AFNetworking/UIImageView+AFNetworking.h>

#import "JSQMessages.h"
#import "JSQMessageData.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

#import "SWRevealViewController.h"

@import Firebase;
#import <Firebase/Firebase.h>

//Remove this before releasing
//@import AFNetworkActivityLogger;
//#import <AFNetworkActivityLogger/AFNetworkActivityLogger.h>

#endif /* Cinch_Bridging_Header_h */
