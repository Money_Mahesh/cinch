//
//	SignInDetail.swift
//
//	Create by Money Mahesh on 19/3/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class SignInDetail : NSObject, NSCoding{

	var address : String!
	var city : String!
	var country : String!
	var email : String!
	var invitationSeq : String!
	var isMobileVerified : String!
	var isUserVerified : String!
	var loginType : String!
	var mobile : String!
	var name : String!
	var playerCount : Int!
	var profilePic : String!
	var registrationStatus : String!
	var role : String!
	var signaturePic : String!
	var state : String!
	var status : String!
	var userSeq : String!
	var zipcode : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		address = dictionary["address"] as? String
		city = dictionary["city"] as? String
		country = dictionary["country"] as? String
		email = dictionary["email"] as? String
		invitationSeq = dictionary["invitation_seq"] as? String
		isMobileVerified = dictionary["is_mobile_verified"] as? String
		isUserVerified = dictionary["is_user_verified"] as? String
		loginType = dictionary["login_type"] as? String
		mobile = dictionary["mobile"] as? String
		name = dictionary["name"] as? String
		playerCount = dictionary["player_count"] as? Int
		profilePic = dictionary["profile_pic"] as? String
		registrationStatus = dictionary["registration_status"] as? String
		role = dictionary["role"] as? String
		signaturePic = dictionary["signature_pic"] as? String
		state = dictionary["state"] as? String
		status = dictionary["status"] as? String
		userSeq = dictionary["user_seq"] as? String
		zipcode = dictionary["zipcode"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if address != nil{
			dictionary["address"] = address
		}
		if city != nil{
			dictionary["city"] = city
		}
		if country != nil{
			dictionary["country"] = country
		}
		if email != nil{
			dictionary["email"] = email
		}
		if invitationSeq != nil{
			dictionary["invitation_seq"] = invitationSeq
		}
		if isMobileVerified != nil{
			dictionary["is_mobile_verified"] = isMobileVerified
		}
		if isUserVerified != nil{
			dictionary["is_user_verified"] = isUserVerified
		}
		if loginType != nil{
			dictionary["login_type"] = loginType
		}
		if mobile != nil{
			dictionary["mobile"] = mobile
		}
		if name != nil{
			dictionary["name"] = name
		}
		if playerCount != nil{
			dictionary["player_count"] = playerCount
		}
		if profilePic != nil{
			dictionary["profile_pic"] = profilePic
		}
		if registrationStatus != nil{
			dictionary["registration_status"] = registrationStatus
		}
		if role != nil{
			dictionary["role"] = role
		}
		if signaturePic != nil{
			dictionary["signature_pic"] = signaturePic
		}
		if state != nil{
			dictionary["state"] = state
		}
		if status != nil{
			dictionary["status"] = status
		}
		if userSeq != nil{
			dictionary["user_seq"] = userSeq
		}
		if zipcode != nil{
			dictionary["zipcode"] = zipcode
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         address = aDecoder.decodeObjectForKey("address") as? String
         city = aDecoder.decodeObjectForKey("city") as? String
         country = aDecoder.decodeObjectForKey("country") as? String
         email = aDecoder.decodeObjectForKey("email") as? String
         invitationSeq = aDecoder.decodeObjectForKey("invitation_seq") as? String
         isMobileVerified = aDecoder.decodeObjectForKey("is_mobile_verified") as? String
         isUserVerified = aDecoder.decodeObjectForKey("is_user_verified") as? String
         loginType = aDecoder.decodeObjectForKey("login_type") as? String
         mobile = aDecoder.decodeObjectForKey("mobile") as? String
         name = aDecoder.decodeObjectForKey("name") as? String
         playerCount = aDecoder.decodeObjectForKey("player_count") as? Int
         profilePic = aDecoder.decodeObjectForKey("profile_pic") as? String
         registrationStatus = aDecoder.decodeObjectForKey("registration_status") as? String
         role = aDecoder.decodeObjectForKey("role") as? String
         signaturePic = aDecoder.decodeObjectForKey("signature_pic") as? String
         state = aDecoder.decodeObjectForKey("state") as? String
         status = aDecoder.decodeObjectForKey("status") as? String
         userSeq = aDecoder.decodeObjectForKey("user_seq") as? String
         zipcode = aDecoder.decodeObjectForKey("zipcode") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if address != nil{
			aCoder.encodeObject(address, forKey: "address")
		}
		if city != nil{
			aCoder.encodeObject(city, forKey: "city")
		}
		if country != nil{
			aCoder.encodeObject(country, forKey: "country")
		}
		if email != nil{
			aCoder.encodeObject(email, forKey: "email")
		}
		if invitationSeq != nil{
			aCoder.encodeObject(invitationSeq, forKey: "invitation_seq")
		}
		if isMobileVerified != nil{
			aCoder.encodeObject(isMobileVerified, forKey: "is_mobile_verified")
		}
		if isUserVerified != nil{
			aCoder.encodeObject(isUserVerified, forKey: "is_user_verified")
		}
		if loginType != nil{
			aCoder.encodeObject(loginType, forKey: "login_type")
		}
		if mobile != nil{
			aCoder.encodeObject(mobile, forKey: "mobile")
		}
		if name != nil{
			aCoder.encodeObject(name, forKey: "name")
		}
		if playerCount != nil{
			aCoder.encodeObject(playerCount, forKey: "player_count")
		}
		if profilePic != nil{
			aCoder.encodeObject(profilePic, forKey: "profile_pic")
		}
		if registrationStatus != nil{
			aCoder.encodeObject(registrationStatus, forKey: "registration_status")
		}
		if role != nil{
			aCoder.encodeObject(role, forKey: "role")
		}
		if signaturePic != nil{
			aCoder.encodeObject(signaturePic, forKey: "signature_pic")
		}
		if state != nil{
			aCoder.encodeObject(state, forKey: "state")
		}
		if status != nil{
			aCoder.encodeObject(status, forKey: "status")
		}
		if userSeq != nil{
			aCoder.encodeObject(userSeq, forKey: "user_seq")
		}
		if zipcode != nil{
			aCoder.encodeObject(zipcode, forKey: "zipcode")
		}

	}

}