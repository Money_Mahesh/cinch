//
//	ClubData.swift
//
//	Create by Amit Garg on 5/3/2016
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class ClubData  : NSObject, NSCoding {

	var clubName : String!
	var clubSeq : String!
	var email : String!
	var mobile : String!
	var status : String!
	var userName : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
    
    override init() {
        clubName = ""
        clubSeq = ""
        email = ""
        mobile = ""
        status = ""
        userName = ""
    }
    
	init(fromDictionary dictionary: NSDictionary){
		clubName = dictionary["club_name"] as? String
		clubSeq = dictionary["club_seq"] as? String
		email = dictionary["email"] as? String
		mobile = dictionary["mobile"] as? String
		status = dictionary["status"] as? String
		userName = dictionary["user_name"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if clubName != nil{
			dictionary["club_name"] = clubName
		}
		if clubSeq != nil{
			dictionary["club_seq"] = clubSeq
		}
		if email != nil{
			dictionary["email"] = email
		}
		if mobile != nil{
			dictionary["mobile"] = mobile
		}
		if status != nil{
			dictionary["status"] = status
		}
		if userName != nil{
			dictionary["user_name"] = userName
		}
		return dictionary
	}
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        clubName = aDecoder.decodeObjectForKey("clubName") as? String
        clubSeq = aDecoder.decodeObjectForKey("club_seq") as? String
        email = aDecoder.decodeObjectForKey("email") as? String
        mobile = aDecoder.decodeObjectForKey("mobile") as? String
        status = aDecoder.decodeObjectForKey("status") as? String
        userName = aDecoder.decodeObjectForKey("userName") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encodeWithCoder(aCoder: NSCoder)
    {
        if clubName != nil{
            aCoder.encodeObject(clubName, forKey: "clubName")
        }
        if clubSeq != nil{
            aCoder.encodeObject(clubSeq, forKey: "club_seq")
        }
        if email != nil{
            aCoder.encodeObject(email, forKey: "email")
        }
        if mobile != nil{
            aCoder.encodeObject(mobile, forKey: "mobile")
        }
        if status != nil{
            aCoder.encodeObject(status, forKey: "status")
        }
        if userName != nil{
            aCoder.encodeObject(userName, forKey: "userName")
        }
        
    }


}