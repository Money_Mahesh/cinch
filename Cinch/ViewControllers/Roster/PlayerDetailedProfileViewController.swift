//
//  PlayerDetailedProfileViewController.swift
//  Cinch
//
//  Created by MONEY on 13/03/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class PlayerDetailedProfileViewController: UIViewController {

    @IBOutlet weak var imageViewForPlayerProfilePic: UIImageView!
    @IBOutlet weak var labelForName: UILabel!
    @IBOutlet weak var labelForClub: UILabel!
    @IBOutlet weak var labelClubDetail: UILabel!
    @IBOutlet weak var labelForID: UILabel!
    
    @IBOutlet weak var parentTableView: UITableView!

    var playerDetail : PlayerData!
    var playerParentDetail : RosterPlayerDetail?

    override func viewDidLoad() {
        super.viewDidLoad()
        Utility.setBackButton(forViewController: self, withTitle: "")

        self.hitParentProfileWebService()
        
        parentTableView.estimatedRowHeight = 180
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    // MARK:- TableView Data Source and Delegates
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if playerParentDetail == nil {
            return 0
        }
        else {
            
            var noOfRows = 0
            
            if playerParentDetail?.parents.father.count != 0 {
                noOfRows++
            }
            if playerParentDetail?.parents.mother.count != 0 {
                noOfRows++
            }
            if playerParentDetail?.parents.guardian.count != 0 {
                noOfRows++
            }
            
            return noOfRows
        
        }
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let parentProfileCell = tableView.dequeueReusableCellWithIdentifier("ParentProfileCell") as! ParentProfileCell
        
        if indexPath.row == 0 {
            if playerParentDetail?.parents.mother.count != 0 {
                parentProfileCell.bindCellWithData((playerParentDetail?.parents.mother[0])!)
            }
            else {
                if playerParentDetail?.parents.father.count != 0 {
                    parentProfileCell.bindCellWithData((playerParentDetail?.parents.father[0])!)
                }
                else {
                    parentProfileCell.bindCellWithData((playerParentDetail?.parents.guardian[0])!)
                }
            }
        }
        else if indexPath.row == 1 {
            if playerParentDetail?.parents.father.count != 0 {
                parentProfileCell.bindCellWithData((playerParentDetail?.parents.father[0])!)
            }
            else {
                parentProfileCell.bindCellWithData((playerParentDetail?.parents.guardian[0])!)
            }
        }
        else {
            parentProfileCell.bindCellWithData((playerParentDetail?.parents.guardian[0])!)
        }
        
        return parentProfileCell
    }

    //MARK: - IBActions
    func backAction() {
        self.navigationController?.popViewControllerAnimated(true)
    }

    
    //MARK:- Hit Webservice
    func hitParentProfileWebService() {
        
        CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Fetching Detail...")
        
        let parentProfileObj = CommonServiceClass(httpRequestType: HTTP_REQUEST_TYPE.POST_REQUEST, requestTag: "PARENT_PROFILE", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
        
        parentProfileObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
            
            if errorMessage != nil
            {
                print(response)
            }
            print(response as! NSDictionary)
            
            let playerParentDataObj =  CSPlayerParent(fromDictionary: response as! NSDictionary)
            
            if playerParentDataObj.errorCode == 0 {
                
                if playerParentDataObj.data.count != 0 {
                    self.playerParentDetail = playerParentDataObj.data[0]
                    
                    // set player Detail
                    Utility.setImageWithURl((self.playerParentDetail?.profilePic)!, inImageView: self.imageViewForPlayerProfilePic)
                    self.labelForName.text = (self.playerParentDetail?.fname)! + (self.playerParentDetail?.lname == nil ? "" : (self.playerParentDetail?.lname)!)
                    self.labelForClub.text = PlayerDetailManager.clubSelected?.clubName
                    self.labelClubDetail.text = PlayerDetailManager.clubSelected?.clubName
                    self.labelForID.text = "ID# " + (PlayerDetailManager.playerDetail?.mobile)!
                    
                    self.parentTableView.reloadData()
                }
                else {
                    Utility.showAlert(ALERT_TITLE, message: "Data not found", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
                }
                
            } else {
                
                Utility.showAlert(ALERT_TITLE, message: playerParentDataObj.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            }
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
        }
        parentProfileObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
            
            print("error ------------------------------\(error)")
        }
        
        
        var parameterDict = Dictionary <String, String>()
        
        /*
        cmd:getPlayerList, player_seq:5,gender:'',name:'',divideByYear:'',year:'', getParentsDetails:'Y'
        */
        parameterDict["cmd"] = "getPlayerList"
        parameterDict["player_seq"] = playerDetail.playerSeq
        parameterDict["gender"] = playerDetail.gender
        parameterDict["divideByYear"] = ""//playerDetail.gender
        parameterDict["year"] = ""
        parameterDict["getParentsDetails"] = "Y"
        
        parentProfileObj.commonServiceHit(parameterDict)
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

class ParentProfileCell : UITableViewCell {
    
    @IBOutlet weak var imageViewForProfilePic: UIImageView!
    @IBOutlet weak var labelForName: UILabel!
    @IBOutlet weak var labelForAddress: UILabel!
    @IBOutlet weak var labelForGender: UILabel!
    @IBOutlet weak var labelForPhoneNo: UILabel!

    var parentDetail: Father!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imageViewForProfilePic.layer.cornerRadius = imageViewForProfilePic.frame.width / 2
        imageViewForProfilePic.clipsToBounds = true
        
        self.selectionStyle = .None
    }
    
    func bindCellWithData(parentDetail: Father) {
        
        self.parentDetail = parentDetail
        
        Utility.setImageWithURl(parentDetail.profilePic, inImageView: imageViewForProfilePic)
        labelForName.text = parentDetail.name!
        labelForPhoneNo.text = parentDetail.mobile
        
        switch parentDetail.gender {
        case "M" :
            labelForGender.text = "Mother"
            
        case "F" :
            labelForGender.text = "Father"
            
        case "G" :
            labelForGender.text = "Gaurdian"
            
        default :
            print("Something is broken")
        }

        
        let address = parentDetail.address == nil ? "" : "\((parentDetail.address)!), "
        let city = parentDetail.city == nil ? "" : "\((parentDetail.city)!), "
        let zipcode = parentDetail.zipcode == nil ? "" : "\((parentDetail.zipcode)!), "
        
        var completeAddress : String = address + city + zipcode
        if completeAddress != "" {
            completeAddress = String(completeAddress.characters.dropLast(2))
        }
        labelForAddress.text = completeAddress
        
    }
    
    @IBAction func messageBtnAction(sender: AnyObject) {
        
        if UserProfileManager.userSequence == parentDetail.seq {
            
            switch parentDetail.role {
            case "M" :
                Utility.showAlert(ALERT_TITLE, message: "You are a mother of this player", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)

            case "F" :
                Utility.showAlert(ALERT_TITLE, message: "You are a father of this player", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)

            case "G" :
                Utility.showAlert(ALERT_TITLE, message: "You are a Gaurdian of this player", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)

            default :
                print("Something is broken")
            }
        }
        else {
            let messageViewControllerObj = amitStoryBoard.instantiateViewControllerWithIdentifier("MessageViewController") as! MessageViewController
            
            messageViewControllerObj.senderDisplayName = parentDetail.name!
            messageViewControllerObj.chatNodeID = "\(UserProfileManager.userSequence!)-\(parentDetail.seq!)"
            messageViewControllerObj.receiverNode =  String(parentDetail.seq!)
            messageViewControllerObj.receiverId = String(parentDetail.seq!)
            messageViewControllerObj.profilePicUrl = parentDetail.profilePic!
            messageViewControllerObj.comingFromTeamMateProfile = true
            
            if parentDetail.seq == UserProfileManager.userSequence {
                Utility.showAlert(ALERT_TITLE, message: "You can not send message to yourself", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            }
            else {
                TOP_VIEWCONTROLLER?.navigationController!.pushViewController(messageViewControllerObj, animated: true)
            }
        }
    }

    @IBAction func callBtnAction(sender: AnyObject) {
        
        if parentDetail.seq == UserProfileManager.userSequence {
            Utility.showAlert(ALERT_TITLE, message: "You can not call to yourself", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
        }
        else {
            if parentDetail.mobile != nil && parentDetail.mobile != "" {
                let phoneNoStr = "tel://\((parentDetail.mobile)!)"
                let url = NSURL(string: phoneNoStr)
                
                if UIApplication.sharedApplication().canOpenURL(url!) {
                    UIApplication.sharedApplication().openURL(url!)
                }
                else {
                    Utility.showAlert(ALERT_TITLE, message: "Call facility is not available!", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
                }
            }
            else {
                Utility.showAlert(ALERT_TITLE, message: "Phone no. is not available!", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            }
        }
    }
}
