//
//	Main.swift
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class Main : NSObject, NSCoding{

	var grndLevel : Float!
	var humidity : Int!
	var pressure : Float!
	var seaLevel : Float!
	var temp : Float!
	var tempMax : Float!
	var tempMin : Float!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		grndLevel = dictionary["grnd_level"] as? Float
		humidity = dictionary["humidity"] as? Int
		pressure = dictionary["pressure"] as? Float
		seaLevel = dictionary["sea_level"] as? Float
		temp = dictionary["temp"] as? Float
		tempMax = dictionary["temp_max"] as? Float
		tempMin = dictionary["temp_min"] as? Float
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if grndLevel != nil{
			dictionary["grnd_level"] = grndLevel
		}
		if humidity != nil{
			dictionary["humidity"] = humidity
		}
		if pressure != nil{
			dictionary["pressure"] = pressure
		}
		if seaLevel != nil{
			dictionary["sea_level"] = seaLevel
		}
		if temp != nil{
			dictionary["temp"] = temp
		}
		if tempMax != nil{
			dictionary["temp_max"] = tempMax
		}
		if tempMin != nil{
			dictionary["temp_min"] = tempMin
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         grndLevel = aDecoder.decodeObjectForKey("grnd_level") as? Float
         humidity = aDecoder.decodeObjectForKey("humidity") as? Int
         pressure = aDecoder.decodeObjectForKey("pressure") as? Float
         seaLevel = aDecoder.decodeObjectForKey("sea_level") as? Float
         temp = aDecoder.decodeObjectForKey("temp") as? Float
         tempMax = aDecoder.decodeObjectForKey("temp_max") as? Float
         tempMin = aDecoder.decodeObjectForKey("temp_min") as? Float

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if grndLevel != nil{
			aCoder.encodeObject(grndLevel, forKey: "grnd_level")
		}
		if humidity != nil{
			aCoder.encodeObject(humidity, forKey: "humidity")
		}
		if pressure != nil{
			aCoder.encodeObject(pressure, forKey: "pressure")
		}
		if seaLevel != nil{
			aCoder.encodeObject(seaLevel, forKey: "sea_level")
		}
		if temp != nil{
			aCoder.encodeObject(temp, forKey: "temp")
		}
		if tempMax != nil{
			aCoder.encodeObject(tempMax, forKey: "temp_max")
		}
		if tempMin != nil{
			aCoder.encodeObject(tempMin, forKey: "temp_min")
		}

	}

}