//
//	Data.swift
//
//	Create by Amit Garg on 6/3/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class PlayerData : NSObject, NSCoding{

	var address : String!
	var city : String!
	var country : String!
	var dob : String!
	var dobCert : String!
	var fatherSeq : String!
	var fname : String!
	var gender : String!
	var guardianSeq : String!
	var insuranceCert : String!
	var licenceCert : String!
	var lname : String!
	var mobile : String!
	var motherSeq : String!
	var playerSeq : String!
	var profilePic : String!
	var state : String!
	var status : String!
	var verified : String!
	var zipcode : String!

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
    
    override init() {
        address = ""
        city = ""
        country = ""
        dob = ""
        dobCert = ""
        fatherSeq = ""
        fname = ""
        gender = ""
        guardianSeq = ""
        insuranceCert = ""
        licenceCert = ""
        lname = ""
        mobile = ""
        motherSeq = ""
        playerSeq = ""
        profilePic = ""
        state = ""
        status = ""
        verified = ""
        zipcode = ""
        
    }
    
	init(fromDictionary dictionary: NSDictionary){
		address = dictionary["address"] as? String
		city = dictionary["city"] as? String
		country = dictionary["country"] as? String
		dob = dictionary["dob"] as? String
		dobCert = dictionary["dob_cert"] as? String
		fatherSeq = dictionary["father_seq"] as? String
		fname = dictionary["fname"] as? String
		gender = dictionary["gender"] as? String
		guardianSeq = dictionary["guardian_seq"] as? String
		insuranceCert = dictionary["insurance_cert"] as? String
		licenceCert = dictionary["licence_cert"] as? String
		lname = dictionary["lname"] as? String
		mobile = dictionary["mobile"] as? String
		motherSeq = dictionary["mother_seq"] as? String
		playerSeq = dictionary["player_seq"] as? String
		profilePic = dictionary["profile_pic"] as? String
		state = dictionary["state"] as? String
		status = dictionary["status"] as? String
		verified = dictionary["verified"] as? String
		zipcode = dictionary["zipcode"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if address != nil{
			dictionary["address"] = address
		}
		if city != nil{
			dictionary["city"] = city
		}
		if country != nil{
			dictionary["country"] = country
		}
		if dob != nil{
			dictionary["dob"] = dob
		}
		if dobCert != nil{
			dictionary["dob_cert"] = dobCert
		}
		if fatherSeq != nil{
			dictionary["father_seq"] = fatherSeq
		}
		if fname != nil{
			dictionary["fname"] = fname
		}
		if gender != nil{
			dictionary["gender"] = gender
		}
		if guardianSeq != nil{
			dictionary["guardian_seq"] = guardianSeq
		}
		if insuranceCert != nil{
			dictionary["insurance_cert"] = insuranceCert
		}
		if licenceCert != nil{
			dictionary["licence_cert"] = licenceCert
		}
		if lname != nil{
			dictionary["lname"] = lname
		}
		if mobile != nil{
			dictionary["mobile"] = mobile
		}
		if motherSeq != nil{
			dictionary["mother_seq"] = motherSeq
		}
		if playerSeq != nil{
			dictionary["player_seq"] = playerSeq
		}
		if profilePic != nil{
			dictionary["profile_pic"] = profilePic
		}
		if state != nil{
			dictionary["state"] = state
		}
		if status != nil{
			dictionary["status"] = status
		}
		if verified != nil{
			dictionary["verified"] = verified
		}
		if zipcode != nil{
			dictionary["zipcode"] = zipcode
		}
		return dictionary
	}
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
    
        address = aDecoder.decodeObjectForKey("address") as? String
        city = aDecoder.decodeObjectForKey("city") as? String
        country = aDecoder.decodeObjectForKey("country") as? String
        dob = aDecoder.decodeObjectForKey("dob") as? String
        dobCert = aDecoder.decodeObjectForKey("dobCert") as? String
        fatherSeq = aDecoder.decodeObjectForKey("fatherSeq") as? String
        fname = aDecoder.decodeObjectForKey("fname") as? String
        gender = aDecoder.decodeObjectForKey("gender") as? String
        guardianSeq = aDecoder.decodeObjectForKey("guardianSeq") as? String
        insuranceCert = aDecoder.decodeObjectForKey("insuranceCert") as? String
        licenceCert = aDecoder.decodeObjectForKey("licenceCert") as? String
        lname = aDecoder.decodeObjectForKey("lname") as? String
        mobile = aDecoder.decodeObjectForKey("mobile") as? String
        motherSeq = aDecoder.decodeObjectForKey("motherSeq") as? String
        playerSeq = aDecoder.decodeObjectForKey("player_seq") as? String
        profilePic = aDecoder.decodeObjectForKey("profilePic") as? String
        state = aDecoder.decodeObjectForKey("state") as? String
        status = aDecoder.decodeObjectForKey("status") as? String
        verified = aDecoder.decodeObjectForKey("verified") as? String
        zipcode = aDecoder.decodeObjectForKey("zipcode") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encodeWithCoder(aCoder: NSCoder)
    {
        if address != nil{
            aCoder.encodeObject(address, forKey: "address")
        }
        if city != nil{
            aCoder.encodeObject(city, forKey: "city")
        }
        if country != nil{
            aCoder.encodeObject(country, forKey: "country")
        }
        if dob != nil{
            aCoder.encodeObject(dob, forKey: "dob")
        }
        if dobCert != nil{
            aCoder.encodeObject(dobCert, forKey: "dobCert")
        }
        if fatherSeq != nil{
            aCoder.encodeObject(fatherSeq, forKey: "fatherSeq")
        }
        if fname != nil{
            aCoder.encodeObject(fname, forKey: "fname")
        }
        if gender != nil{
            aCoder.encodeObject(gender, forKey: "gender")
        }
        if guardianSeq != nil{
            aCoder.encodeObject(guardianSeq, forKey: "guardianSeq")
        }
        if insuranceCert != nil{
            aCoder.encodeObject(insuranceCert, forKey: "insuranceCert")
        }
        if licenceCert != nil{
            aCoder.encodeObject(licenceCert, forKey: "licenceCert")
        }
        if lname != nil{
            aCoder.encodeObject(lname, forKey: "lname")
        }
        if mobile != nil{
            aCoder.encodeObject(mobile, forKey: "mobile")
        }
        if motherSeq != nil{
            aCoder.encodeObject(motherSeq, forKey: "motherSeq")
        }
        if playerSeq != nil{
            aCoder.encodeObject(playerSeq, forKey: "player_seq")
        }
        if profilePic != nil{
            aCoder.encodeObject(profilePic, forKey: "profilePic")
        }
        if state != nil{
            aCoder.encodeObject(state, forKey: "state")
        }
        if status != nil{
            aCoder.encodeObject(status, forKey: "status")
        }
        if verified != nil{
            aCoder.encodeObject(verified, forKey: "verified")
        }
        if zipcode != nil{
            aCoder.encodeObject(zipcode, forKey: "zipcode")
        }
    }


}