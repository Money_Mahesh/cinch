//
//	Father.swift
//
//	Create by Money Mahesh on 21/3/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class Father : NSObject, NSCoding{

	var address : String!
	var city : String!
	var country : String!
	var createTime : String!
	var dob : String!
	var email : String!
	var gender : String!
	var mobile : String!
	var name : String!
	var profilePic : String!
	var registerationIp : String!
	var role : String!
	var seq : String!
	var signaturePic : String!
	var state : String!
	var status : String!
	var updateTime : String!
	var zipcode : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		address = dictionary["address"] as? String
		city = dictionary["city"] as? String
		country = dictionary["country"] as? String
		createTime = dictionary["create_time"] as? String
		dob = dictionary["dob"] as? String
		email = dictionary["email"] as? String
		gender = dictionary["gender"] as? String
		mobile = dictionary["mobile"] as? String
		name = dictionary["name"] as? String
		profilePic = dictionary["profile_pic"] as? String
		registerationIp = dictionary["registeration_ip"] as? String
		role = dictionary["role"] as? String
		seq = dictionary["seq"] as? String
		signaturePic = dictionary["signature_pic"] as? String
		state = dictionary["state"] as? String
		status = dictionary["status"] as? String
		updateTime = dictionary["update_time"] as? String
		zipcode = dictionary["zipcode"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if address != nil{
			dictionary["address"] = address
		}
		if city != nil{
			dictionary["city"] = city
		}
		if country != nil{
			dictionary["country"] = country
		}
		if createTime != nil{
			dictionary["create_time"] = createTime
		}
		if dob != nil{
			dictionary["dob"] = dob
		}
		if email != nil{
			dictionary["email"] = email
		}
		if gender != nil{
			dictionary["gender"] = gender
		}
		if mobile != nil{
			dictionary["mobile"] = mobile
		}
		if name != nil{
			dictionary["name"] = name
		}
		if profilePic != nil{
			dictionary["profile_pic"] = profilePic
		}
		if registerationIp != nil{
			dictionary["registeration_ip"] = registerationIp
		}
		if role != nil{
			dictionary["role"] = role
		}
		if seq != nil{
			dictionary["seq"] = seq
		}
		if signaturePic != nil{
			dictionary["signature_pic"] = signaturePic
		}
		if state != nil{
			dictionary["state"] = state
		}
		if status != nil{
			dictionary["status"] = status
		}
		if updateTime != nil{
			dictionary["update_time"] = updateTime
		}
		if zipcode != nil{
			dictionary["zipcode"] = zipcode
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         address = aDecoder.decodeObjectForKey("address") as? String
         city = aDecoder.decodeObjectForKey("city") as? String
         country = aDecoder.decodeObjectForKey("country") as? String
         createTime = aDecoder.decodeObjectForKey("create_time") as? String
         dob = aDecoder.decodeObjectForKey("dob") as? String
         email = aDecoder.decodeObjectForKey("email") as? String
         gender = aDecoder.decodeObjectForKey("gender") as? String
         mobile = aDecoder.decodeObjectForKey("mobile") as? String
         name = aDecoder.decodeObjectForKey("name") as? String
         profilePic = aDecoder.decodeObjectForKey("profile_pic") as? String
         registerationIp = aDecoder.decodeObjectForKey("registeration_ip") as? String
         role = aDecoder.decodeObjectForKey("role") as? String
         seq = aDecoder.decodeObjectForKey("seq") as? String
         signaturePic = aDecoder.decodeObjectForKey("signature_pic") as? String
         state = aDecoder.decodeObjectForKey("state") as? String
         status = aDecoder.decodeObjectForKey("status") as? String
         updateTime = aDecoder.decodeObjectForKey("update_time") as? String
         zipcode = aDecoder.decodeObjectForKey("zipcode") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if address != nil{
			aCoder.encodeObject(address, forKey: "address")
		}
		if city != nil{
			aCoder.encodeObject(city, forKey: "city")
		}
		if country != nil{
			aCoder.encodeObject(country, forKey: "country")
		}
		if createTime != nil{
			aCoder.encodeObject(createTime, forKey: "create_time")
		}
		if dob != nil{
			aCoder.encodeObject(dob, forKey: "dob")
		}
		if email != nil{
			aCoder.encodeObject(email, forKey: "email")
		}
		if gender != nil{
			aCoder.encodeObject(gender, forKey: "gender")
		}
		if mobile != nil{
			aCoder.encodeObject(mobile, forKey: "mobile")
		}
		if name != nil{
			aCoder.encodeObject(name, forKey: "name")
		}
		if profilePic != nil{
			aCoder.encodeObject(profilePic, forKey: "profile_pic")
		}
		if registerationIp != nil{
			aCoder.encodeObject(registerationIp, forKey: "registeration_ip")
		}
		if role != nil{
			aCoder.encodeObject(role, forKey: "role")
		}
		if seq != nil{
			aCoder.encodeObject(seq, forKey: "seq")
		}
		if signaturePic != nil{
			aCoder.encodeObject(signaturePic, forKey: "signature_pic")
		}
		if state != nil{
			aCoder.encodeObject(state, forKey: "state")
		}
		if status != nil{
			aCoder.encodeObject(status, forKey: "status")
		}
		if updateTime != nil{
			aCoder.encodeObject(updateTime, forKey: "update_time")
		}
		if zipcode != nil{
			aCoder.encodeObject(zipcode, forKey: "zipcode")
		}

	}

}