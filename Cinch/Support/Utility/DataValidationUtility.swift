//
//  DataValidationUtility.swift
//  Cinch
//
//  Created by Money Mahesh on 14/02/16.
//  Copyright © 2016 tbsl. All rights reserved.
//

import UIKit

class DataValidationUtility: NSObject {
    
    class func isValidEmail(emailId:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(emailId)
    }
    
    class func isValidText(inputText:String) -> Bool {
        
        let letters = NSCharacterSet(charactersInString: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ")
        
        let range = inputText.rangeOfCharacterFromSet(letters)
        
        // range will be nil if no letters is found
        if range != nil {
            print("letters found")
        }
        else {
            print("letters not found")
        }

        
//        let characterSet:NSCharacterSet = NSCharacterSet(charactersInString: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ")
//        if ((inputText.rangeOfCharacterFromSet(characterSet.invertedSet)) != nil){
//            print("Contains only alphabets")
//        } else {
//             print("Contains others also")
//            
//        }
        
        let textRegEx = "[^A-Za-z]"
        let stringTest = NSPredicate(format:"SELF MATCHES %@", textRegEx)
        return stringTest.evaluateWithObject(inputText)
    }
    
    class func isValidContactPhoneNumber(phoneNumber:String) -> Bool {
        let phoneRegEx = "0123456789"
        let phoneTest = NSPredicate(format:"SELF MATCHES %@", phoneRegEx)
        return phoneTest.evaluateWithObject(phoneNumber)
    }
    
    class func isValidPhoneNumber(phoneNumber:String) -> Bool {
        var isValid = false
        let phoneRegEx = "^[0-9]{1,15}$"
        let phoneTest = NSPredicate(format:"SELF MATCHES %@", phoneRegEx)
        if (phoneTest.evaluateWithObject(phoneNumber) && phoneNumber.characters.count == 10) {
            isValid = true
        }
        return isValid
    }
    
    class func isEmpty(testString:NSString) -> Bool {
        if testString.length == 0 {
            return true
        }
        return false
    }
    
    class func isNull(object:AnyObject?) -> Bool {
        if object == nil {
            return true
        }
        return false
    }
    
    class func containsCharacter(string:String) -> Bool {
        let characterSetRegEx = "^[a-zA-Z]+( )?([a-zA-Z]+( )?)*[a-zA-Z]$"

        let stringTest = NSPredicate(format:"SELF MATCHES %@", characterSetRegEx)
        return stringTest.evaluateWithObject(string)
    }
}
