//
//  MyCoachesTableViewCell.swift
//  Cinch
//
//  Created by Amit Garg on 29/02/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class MyCoachesTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var followButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
