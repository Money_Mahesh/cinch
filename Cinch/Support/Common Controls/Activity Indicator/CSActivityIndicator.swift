//
//  CSActivityIndicator.swift
//  Cinch
//
//  Created by Amit Garg on 23/02/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class CSActivityIndicator: UIView {
    
    @IBOutlet weak var indicatorImageView: UIImageView!
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var activityView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.loadViewFromXibWithFrame(frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func loadViewFromXibWithFrame(frame : CGRect) {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "CSActivityIndicator", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        
        self.activityView.layer.shadowColor = UIColor.blackColor().CGColor;
        self.activityView.layer.shadowOffset = CGSizeMake(10, 10);
        self.activityView.layer.shadowOpacity = 0.5;
        self.activityView.layer.shadowRadius = 1.0;
        
        view.frame = frame
        view.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        
        self.addSubview(view)
    }
    
    class func showToMainWindowWithMessage(message: String?) {
        let mainView = KEY_WINDOW
        let activityIndicator = CSActivityIndicator(frame: mainView!.frame)
        
        activityIndicator.message.text = message
        
        //add images to the array
        var logoImages: [UIImage] = []
        //use for loop
        for position in 1...24
        {
            let strImageName : String = "rs_\(position).png"
            logoImages.append(UIImage(named: strImageName)!)
        }
        
        activityIndicator.indicatorImageView.animationImages = logoImages;
        activityIndicator.indicatorImageView.animationDuration = 2.0
        activityIndicator.indicatorImageView.startAnimating()
        
        
        mainView!.addSubview(activityIndicator)
    }
    
    class func hideAllFromMainWindow() {
        let mainView = KEY_WINDOW

        let mainViewSubViews = (mainView!.subviews as NSArray).reverseObjectEnumerator().allObjects
        
        for subView in mainViewSubViews {
            if subView.isKindOfClass(self) {
                subView.removeFromSuperview()
            }
        }
    }
    
    class func showToMainViewWithMessage(message: String?) {
        let mainView = ROOT_NAVVIGATION_CONROLLER.topViewController?.view
        let activityIndicator = CSActivityIndicator(frame: mainView!.frame)
        
        activityIndicator.message.text = message
        
        //add images to the array
        var logoImages: [UIImage] = []
        //use for loop
        for position in 1...24
        {
            let strImageName : String = "rs_\(position).png"
            logoImages.append(UIImage(named: strImageName)!)
        }
        
        activityIndicator.indicatorImageView.animationImages = logoImages;
        activityIndicator.indicatorImageView.animationDuration = 2.0
        activityIndicator.indicatorImageView.startAnimating()
        
        
        mainView!.addSubview(activityIndicator)
    }
    
    class func hideAllFromMainView() {
        let mainView = ROOT_NAVVIGATION_CONROLLER.topViewController?.view
        
        let mainViewSubViews = (mainView!.subviews as NSArray).reverseObjectEnumerator().allObjects
        
        for subView in mainViewSubViews {
            if subView.isKindOfClass(self) {
                subView.removeFromSuperview()
            }
        }
    }

    class func showToView(mainView : UIView, frame : CGRect, withMessage message : String?) {
        let activityIndicator = CSActivityIndicator(frame: frame)
        
        activityIndicator.message.text = message
        
        //add images to the array
        var logoImages: [UIImage] = []
        //use for loop
        for position in 1...24
        {
            let strImageName : String = "rs_\(position).png"
            logoImages.append(UIImage(named: strImageName)!)
        }
        
        activityIndicator.indicatorImageView.animationImages = logoImages;
        activityIndicator.indicatorImageView.animationDuration = 2.0
        activityIndicator.indicatorImageView.startAnimating()
        
        mainView.addSubview(activityIndicator)
    }
    
    class func hideFromView(mainView : UIView) {
        let mainViewSubViews = (mainView.subviews as NSArray).reverseObjectEnumerator().allObjects
        
        for subView in mainViewSubViews {
            if subView.isKindOfClass(self) {
                subView.removeFromSuperview()
                break
            }
        }
    }
    
    class func hideAllFromView(mainView : UIView) {
        let mainViewSubViews = (mainView.subviews as NSArray).reverseObjectEnumerator().allObjects
        
        for subView in mainViewSubViews {
            if subView.isKindOfClass(self) {
                subView.removeFromSuperview()
            }
        }
    }
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
    // Drawing code
    }
    */

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
