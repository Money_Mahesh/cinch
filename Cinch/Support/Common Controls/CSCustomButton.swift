//
//  CSCustomButton.swift
//  Cinch
//
//  Created by Amit Garg on 18/02/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

@IBDesignable class CSCustomButton: UIButton {
    
    var title = UILabel()
    
    //MARK: - Properties
    
    @IBInspectable var CSCornerRadius:CGFloat = 0.0 {
        didSet {
                self.layer.cornerRadius = CSCornerRadius
        }
    }
    
    @IBInspectable var CSBorderColor: UIColor = UIColor.clearColor() {
        didSet {
            self.layer.borderColor = CSBorderColor.CGColor
        }
    }
    
    @IBInspectable var CSBorderWidth:CGFloat = 0.0 {
        didSet {
            self.layer.borderWidth = CSBorderWidth
        }
    }
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
