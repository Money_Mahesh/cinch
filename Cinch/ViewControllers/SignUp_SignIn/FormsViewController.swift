//
//  FormsViewController.swift
//  Cinch
//
//  Created by Amit Garg on 21/02/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class FormsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    let UnSelectedColor = UIColor(red: 208/255, green: 2/255, blue: 27/255, alpha: 1.0)
    let SelectedColor = UIColor(red: 126.0/255, green: 211.0/255, blue: 33.0/255, alpha: 1.0)
    
    @IBOutlet weak var tableView: UITableView!
    var imagePicker = UIImagePickerController()
    

    var addressDetail : FatherAdd?
    
    var mSeq: String = ""
    var fSeq: String = ""
    var gSeq: String = ""
    
    var selectedClub: ClubData?
    var selectedPlayer: PlayerData?
    var selectedProgram: ProgramData?
    var isVolunteer : String!

    var isComingFrom = ComingFrom.SignUp
    
    var birthCertificateUrl = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Utility.removeParameterFromUserDefault(BIRTH_CERTIFICATE_STATUS)
        Utility.removeParameterFromUserDefault(AGREEMENT_STATUS)

        Utility.setBackButton(forViewController: self, withTitle: "")
        self.title = "Forms"

        imagePicker.delegate = self
        
        self.tableView.registerNib(UINib(nibName: "CellWithLabelAndButtonTableViewCell", bundle: nil), forCellReuseIdentifier: "buttonLabelCell")
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.hidden = false
        
        self.tableView.reloadData()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.hidden = true
    }
    //MARK: - TableViewDataSource Methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if Utility.fetchStringFromDefault(BIRTH_CERT_REQ) != nil && Utility.fetchStringFromDefault(BIRTH_CERT_REQ) as! String == "Y" {
            return 2
        }
        return 1
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 0
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 50.0
    }

    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("buttonLabelCell", forIndexPath: indexPath) as! CellWithLabelAndButtonTableViewCell
        cell.actionButton.enabled = false
        //cell.actionButton.setTitleColorForAllState(UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0))
        cell.titleLabel.textColor = UIColor(red: 119/255, green: 119/255, blue: 119/255, alpha: 1.0)
        cell.titleLabel.font = UIFont(name: "Lato-Regular", size: 11.0)
        cell.selectionStyle = .None
        
        switch(indexPath.row) {
            
        case 0:
            cell.titleLabel.text = "CYSA INSURANCE FORM"
            Utility.fetchBoolFromDefault(AGREEMENT_STATUS)
            if (Utility.fetchBoolFromDefault(AGREEMENT_STATUS) == nil || Utility.fetchBoolFromDefault(AGREEMENT_STATUS)! == false) {
                cell.actionButton.setTitle("AGREE & SIGN", forState: .Normal)
                cell.actionButton.setTitleColorForAllState(UnSelectedColor)
            }
            else {
                cell.actionButton.setTitle("COMPLETED", forState: .Normal)
                cell.actionButton.setTitleColorForAllState(SelectedColor)
            }
            
        case 1:
            cell.titleLabel.text = "BIRTH CERTIFICATE"
            
            if (Utility.fetchBoolFromDefault(BIRTH_CERTIFICATE_STATUS) == nil || Utility.fetchBoolFromDefault(BIRTH_CERTIFICATE_STATUS)! == false) {
                
                cell.actionButton.setTitle("COMPLETE", forState: .Normal)
                cell.actionButton.setTitleColorForAllState(UnSelectedColor)
            }
            else {
                cell.actionButton.setTitle("COMPLETED", forState: .Normal)
                cell.actionButton.setTitleColorForAllState(SelectedColor)
            }
            
        default:
            print("Something is broken")
            
        }
        
        return cell
        
    }
    
    //MARK: - TableViewDelegate Methods
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        if indexPath.row == 0 {
            let insuranceFormViewControllerObj = mainStoryBoard.instantiateViewControllerWithIdentifier("InsuranceFormViewController") as! InsuranceFormViewController
            insuranceFormViewControllerObj.playerName = (selectedPlayer?.fname)! + " " + (selectedPlayer?.lname == nil ? "" : (selectedPlayer?.lname)!)
            
            let insuranceFormNavigationController = UINavigationController(rootViewController: insuranceFormViewControllerObj)
            
            
            ROOT_NAVVIGATION_CONROLLER.presentViewController(insuranceFormNavigationController, animated: true, completion: nil)
        }
        if indexPath.row == 1 {
            self.showActionSheet()
        }
    }
    
    //MARK:- Image Picker method
    func showActionSheet() {
        
        let actionSheet = UIAlertController(title: "BIRTH CERTIFICATE", message: "", preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        let imageGalleryAction = UIAlertAction(title: "Image Gallery", style: UIAlertActionStyle.Default) { (action) -> Void in
            self.openImageGallary()
        }
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.Default) { (action) -> Void in
            self.openCamera()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil)
        
        actionSheet.addAction(imageGalleryAction)
        actionSheet.addAction(cameraAction)
        actionSheet.addAction(cancelAction)
        
        self.presentViewController(actionSheet, animated: true, completion: nil)
    }
    
    func openImageGallary() {
        imagePicker.sourceType = .PhotoLibrary
        self.presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    func openCamera() {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
            imagePicker.sourceType = .Camera
            self.presentViewController(imagePicker, animated: true, completion: nil)
        }
        else {
            let alertController = UIAlertController(title: "Oops!!", message: "Camera is not available", preferredStyle: UIAlertControllerStyle.Alert)
            let cancelAction = UIAlertAction(title: ALERT_OK_BUTTON_TITLE, style: UIAlertActionStyle.Cancel, handler: nil)
            
            alertController.addAction(cancelAction)
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    //MARK:- Image Picker Delegate
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        if let pickerImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            print(pickerImage)
            
            CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Uploading Certificate...")

            Utility.uploadImageToserver(pickerImage, resetImageViewBlock: {(selectedImage: UIImage?, headShotImageUrl: String?) in
                
                CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
                if selectedImage != nil {
                    
                    self.birthCertificateUrl = headShotImageUrl!
                    PlayerDetailManager.birthCertificateUrl = headShotImageUrl!
                    Utility.saveBoolInDefault(BIRTH_CERTIFICATE_STATUS, value: true)
                }
                else {
                    Utility.saveBoolInDefault(BIRTH_CERTIFICATE_STATUS, value: false)
                }
                
                self.tableView.reloadData()
            })
        }
        
        self.imagePicker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.imagePicker.dismissViewControllerAnimated(true, completion: nil)
    }

    //MARK:- IBAction
    func backAction() {
        self.navigationController?.popViewControllerAnimated(true)
    }

    @IBAction func doneBtnAction(sender: AnyObject) {
        
        if (Utility.fetchBoolFromDefault(AGREEMENT_STATUS) == nil || Utility.fetchBoolFromDefault(AGREEMENT_STATUS)! == false) {
            
            Utility.showAlert(nil, message: "Signature on Agreement is required", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)

        }
        else if (Utility.fetchStringFromDefault(BIRTH_CERT_REQ) as! String) == "Y" && (Utility.fetchBoolFromDefault(BIRTH_CERTIFICATE_STATUS) == nil || Utility.fetchBoolFromDefault(BIRTH_CERTIFICATE_STATUS)! == false) {
            
            Utility.showAlert(nil, message: "Certificate is required", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)

        }
        else {
            
            if isComingFrom == ComingFrom.AddProgramSideMenu || isComingFrom == ComingFrom.Dashboard {
                self.hitAddProgramWebService()
            }
            else {
                self.hitCreatePlayerWebService()
            }
        }
    }
    
    //MARK: - Web service hit methods
    func hitCreatePlayerWebService() {
        
        CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Creating player...")
        
        let addProgramObj =  ClubListServices(httpRequestType: HTTP_REQUEST_TYPE.POST_REQUEST, requestTag: "Create_PLAYER_SERVICE", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
        
        addProgramObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
            
            if errorMessage != nil
            {
                print(response)
            }
            print(response as! NSDictionary)
            
            let playerDataObj =  CSPlayer(fromDictionary: response as! NSDictionary)
            
            
            if playerDataObj.errorCode == 0 {
                
                if playerDataObj.data != nil {
                    self.selectedPlayer?.playerSeq = playerDataObj.data.playerSeq
                }
                
                PlayerDetailManager.playerDetail = self.selectedPlayer
                PlayerDetailManager.programSelected = self.selectedProgram
                PlayerDetailManager.clubSelected = self.selectedClub
                
                if self.isComingFrom == ComingFrom.SignUp {
                    Utility.saveStringInDefault(SIGN_UP_STATUS, value: "ReviewViewController")
                }
                
                let reviewViewControllerObj = amitStoryBoard.instantiateViewControllerWithIdentifier("ReviewViewController") as! ReviewViewController
                reviewViewControllerObj.isComingFrom = self.isComingFrom
                
                self.navigationController!.pushViewController(reviewViewControllerObj, animated: true)
                
            } else {
                
                Utility.showAlert(ALERT_TITLE, message: playerDataObj.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            }
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
        }
        addProgramObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
            
            print("error ------------------------------\(error)")
        }
        
        let parameterDict = NSMutableDictionary()//Dictionary <String, AnyObject>()
        
        parameterDict["cmd"] = "createPlayer"
        parameterDict["p_seq"] = selectedPlayer?.playerSeq == nil ? "" : selectedPlayer?.playerSeq
        parameterDict["fname"] = selectedPlayer?.fname
        parameterDict["lname"] = selectedPlayer?.lname
        parameterDict["dob"] = selectedPlayer?.dob
        parameterDict["gender"] = selectedPlayer?.gender
        parameterDict["club_seq"] = selectedClub?.clubSeq
        parameterDict["program_seq"] = selectedProgram?.programSeq
        parameterDict["profile_pic"] = selectedPlayer?.profilePic
        parameterDict["dob_cert"] = self.birthCertificateUrl
        parameterDict["addr"] = addressDetail?.address == nil ? "" : addressDetail!.address
        parameterDict["city"] = addressDetail?.city == nil ? "" : addressDetail!.city
        parameterDict["state"] = addressDetail?.state == nil ? "" : addressDetail!.state
        parameterDict["zip"] = addressDetail?.zipcode == nil ? "" : addressDetail!.zipcode
        parameterDict["country"] = addressDetail?.country == nil ? "" : addressDetail!.country
        parameterDict["old_program_seq"] = ""
        parameterDict["divi_seq"] = selectedProgram?.divisionSeq
        parameterDict["invitation_seq"] = UserProfileManager.invitationSeq == nil ? "" : UserProfileManager.invitationSeq
        parameterDict["waiver_seq"] = [PlayerDetailManager.waiverSeq!]
        parameterDict["signature_url"] = [PlayerDetailManager.insuranceSignatureUrl!]
        parameterDict["is_volunteer"] = isVolunteer

        parameterDict["m_seq"] = mSeq
        parameterDict["g_seq"] = gSeq
        parameterDict["f_seq"] = fSeq
        
        if isComingFrom == ComingFrom.SignUp {
            if addressDetail?.gender != nil && addressDetail?.role == "M" {
                parameterDict["m_seq"] = addressDetail?.seq == nil ? "" : addressDetail!.seq
            }
            else if addressDetail?.gender != nil && addressDetail?.role == "G" {
                parameterDict["g_seq"] = addressDetail?.seq == nil ? "" : addressDetail!.seq
            }
            else {
                parameterDict["f_seq"] = addressDetail?.seq == nil ? "" : addressDetail!.seq
            }
        }
        
        
        print(parameterDict as NSDictionary)
        
        addProgramObj.addProgramHit(parameterDict)
    }

    
    func hitAddProgramWebService() {
        
        CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Adding program...")
        
        let addProgramObj =  ClubListServices(httpRequestType: HTTP_REQUEST_TYPE.POST_REQUEST, requestTag: "ADD_PROGRAM_SERVICE", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
        
        addProgramObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
            
            if errorMessage != nil
            {
                print(response)
            }
            print(response as! NSDictionary)
            
            let playerDataObj =  OtpVerificationData(fromDictionary: response as! NSDictionary)
            
            
            if playerDataObj.errorCode == 0 {
                
                if playerDataObj.data == true {
                    
                    PlayerDetailManager.playerDetail = self.selectedPlayer
                    PlayerDetailManager.programSelected = self.selectedProgram
                    PlayerDetailManager.clubSelected = self.selectedClub
                    
                    if self.isComingFrom == ComingFrom.SignUp {
                        Utility.saveStringInDefault(SIGN_UP_STATUS, value: "ReviewViewController")
                    }
                    
                    let reviewViewControllerObj = amitStoryBoard.instantiateViewControllerWithIdentifier("ReviewViewController") as! ReviewViewController
                    reviewViewControllerObj.isComingFrom = self.isComingFrom
                    
                    self.navigationController!.pushViewController(reviewViewControllerObj, animated: true)
                }
                else {
                    Utility.showAlert(ALERT_TITLE, message: playerDataObj.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
                }
            } else {
                
                Utility.showAlert(ALERT_TITLE, message: playerDataObj.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            }
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
        }
        addProgramObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
            
            print("error ------------------------------\(error)")
        }
        
        
        let parameterDict = NSMutableDictionary()//Dictionary <String, AnyObject>()
        
        parameterDict["cmd"] = "addPlayerToProgram"
        parameterDict["player_seq"] = selectedPlayer?.playerSeq == nil ? "" : selectedPlayer?.playerSeq
        parameterDict["divi_seq"] = selectedProgram?.divisionSeq
        parameterDict["program_seq"] = selectedProgram?.programSeq
        parameterDict["waiver_seq"] = [PlayerDetailManager.waiverSeq!]
        parameterDict["signature_url"] = [PlayerDetailManager.insuranceSignatureUrl!]
        parameterDict["is_volunteer"] = isVolunteer
        parameterDict["birth_cert"] = PlayerDetailManager.birthCertificateUrl

    
        print(parameterDict as NSDictionary)
        
        addProgramObj.addProgramHit(parameterDict)
    }
}
