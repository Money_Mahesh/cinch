//
//  CreateAccountViewController.swift
//  Cinch
//
//  Created by Amit Garg on 16/02/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class CreateAccountViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var guardianButton: UIButton!
    @IBOutlet weak var momButton: UIButton!
    @IBOutlet weak var dadButton: UIButton!
    @IBOutlet weak var parentHeadShotButton: UIButton!
    var imagePicker = UIImagePickerController()

    @IBOutlet weak var imageViewForProfilePic: UIImageView!
    @IBOutlet weak var textFieldForAddress: AGFloatLabelTextField!
    @IBOutlet weak var textFieldForZip: AGFloatLabelTextField!
    @IBOutlet weak var textFieldForApt: AGFloatLabelTextField!
    @IBOutlet weak var textFieldForCity: AGFloatLabelTextField!
    @IBOutlet weak var textFieldForState: AGFloatLabelTextField!
    @IBOutlet weak var textFieldToInviteMomEmail: AGFloatLabelTextField!
    @IBOutlet weak var textFieldToInviteDadEmail: AGFloatLabelTextField!

    @IBOutlet weak var inviteFatherTextFieldTopConstraint: NSLayoutConstraint!
    
    var headShotImageUrl = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.setHidesBackButton(true, animated:true);
        imageViewForProfilePic.layer.cornerRadius = 45
        imageViewForProfilePic.clipsToBounds = true
        self.title = "Create Account"
        
        self.dadButton.selected = true
        inviteFatherTextFieldTopConstraint.constant = 16
        self.textFieldToInviteDadEmail.hidden = true
        self.textFieldToInviteMomEmail.hidden = false

        self.imagePicker.delegate = self
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.hidden = false
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.hidden = true
    }
    //MARK: - IBActions
    
    @IBOutlet weak var termsAndConditionBtnTapped: UIButton!
    
    @IBAction func createAccountTapped(sender: AnyObject) {
        

        if textFieldForAddress.text?.characters.count == 0 {
            Utility.showAlert(nil, message: "Please enter Address", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
        }
        else if textFieldForZip.text?.characters.count == 0 {
            Utility.showAlert(nil, message: "Please enter Zip", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
        }
        else if textFieldForState.text?.characters.count == 0 {
            Utility.showAlert(nil, message: "Please enter State", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
        }
        else if textFieldForCity.text?.characters.count == 0 {
            Utility.showAlert(nil, message: "Please enter City", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
        }
        else if textFieldToInviteMomEmail.text?.characters.count != 0 && DataValidationUtility.isValidEmail(textFieldToInviteMomEmail.text!) == false {
            Utility.showAlert(nil, message: "Please enter the valid email id for Mom", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
        }
        else if textFieldToInviteDadEmail.text?.characters.count != 0 && DataValidationUtility.isValidEmail(textFieldToInviteDadEmail.text!) == false {
            Utility.showAlert(nil, message: "Please enter the valid email id for Dad", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
        }
        else {
            self.view.endEditing(true)

            self.hitCreateParentAccountWebService()
        }
    }
    
    
    @IBAction func roleButtonTapped(sender: UIButton) {
        
        self.view.endEditing(true)
        
        self.dadButton.selected = false
        self.momButton.selected = false
        self.guardianButton.selected = false
        sender.selected = true
        self.textFieldToInviteDadEmail.text = ""
        self.textFieldToInviteMomEmail.text = ""
        
        switch sender.titleLabel!.text! {
        case "DAD":
            inviteFatherTextFieldTopConstraint.constant = 16

            self.textFieldToInviteDadEmail.hidden = true
            self.textFieldToInviteMomEmail.hidden = false
            
        case "MOM":
            inviteFatherTextFieldTopConstraint.constant = 16

            self.textFieldToInviteDadEmail.hidden = false
            self.textFieldToInviteMomEmail.hidden = true
            
        case "GUARDIAN":
            inviteFatherTextFieldTopConstraint.constant = 59

            self.textFieldToInviteDadEmail.hidden = false
            self.textFieldToInviteMomEmail.hidden = false
            
        default:
            print("Something Broken")
        }
        
        
    }
    
    @IBAction func parentHeadShotTapped(sender: AnyObject) {
        self.view.endEditing(true)

        self.showActionSheet()
    }
    
    @IBAction func termsAndConditionTaped(sender: AnyObject) {
        
        let vc = amitStoryBoard.instantiateViewControllerWithIdentifier("WebViewController") as! WebViewController
        
        vc.urlToLoad = "https://www.cinchsports.com/user/user_terms_condition/"
        vc.navigationHeaderTitle = "Terms & Conditions"
        
        
        let navCont = UINavigationController(rootViewController: vc)
        
        self.presentViewController(navCont, animated: true, completion:nil)
    }
    
    //MARK:- Image Picker method
    func showActionSheet() {
        
        let actionSheet = UIAlertController(title: "Parent Headshot", message: "", preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        let imageGalleryAction = UIAlertAction(title: "Image Gallery", style: UIAlertActionStyle.Default) { (action) -> Void in
            self.openImageGallary()
        }
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.Default) { (action) -> Void in
            self.openCamera()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil)
        
        actionSheet.addAction(imageGalleryAction)
        actionSheet.addAction(cameraAction)
        actionSheet.addAction(cancelAction)
        
        self.presentViewController(actionSheet, animated: true, completion: nil)
    }
    
    func openImageGallary() {
        imagePicker.sourceType = .PhotoLibrary
        self.presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    func openCamera() {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
            imagePicker.sourceType = .Camera
            self.presentViewController(imagePicker, animated: true, completion: nil)
        }
        else {
            let alertController = UIAlertController(title: "Oops!!", message: "Camera is not available", preferredStyle: UIAlertControllerStyle.Alert)
            let cancelAction = UIAlertAction(title: ALERT_OK_BUTTON_TITLE, style: UIAlertActionStyle.Cancel, handler: nil)
            
            alertController.addAction(cancelAction)
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    //MARK:- Image Picker Delegate
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        if let pickerImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            print(pickerImage)
            
            let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
            activityIndicator.frame.size = self.parentHeadShotButton.frame.size
            
            activityIndicator.startAnimating()
            self.parentHeadShotButton.addSubview(activityIndicator)

            Utility.uploadImageToserver(pickerImage, resetImageViewBlock: {(selectedImage: UIImage?, headShotImageUrl: String?) in
                
                activityIndicator.removeFromSuperview()
                if selectedImage != nil {
                    
                    self.headShotImageUrl = headShotImageUrl == nil ? "" : headShotImageUrl!
                    UserProfileManager.imageUrl = headShotImageUrl
                    self.imageViewForProfilePic.image = selectedImage
                }
                else {
                }
            })
            
            self.parentHeadShotButton.layer.cornerRadius = 45.0
            self.parentHeadShotButton.clipsToBounds = true
        }
        
        self.imagePicker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.imagePicker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    //MARK:- Textfield Delegate 
    func textFieldDidEndEditing(textField: UITextField) {
        
        if textField == textFieldForZip {
           hitGetAddressWebService(textField.text!)
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        if textField == textFieldForZip {
            hitGetAddressWebService(textField.text!)
        }
        return true
    }
    
    //MARK:- Hit Webservice
    func hitCreateParentAccountWebService() {
        
        CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Registering...")
        
        let createParentAccountObj = RegisterationService(httpRequestType: HTTP_REQUEST_TYPE.POST_REQUEST, requestTag: "CREATE_PARENT_ACCOUNT", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
        
        createParentAccountObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
            
            if errorMessage != nil
            {
                print(response)
            }
            print(response as! NSDictionary)

            let createParentAccountDataObj =  CSCreateAccount(fromDictionary: response as! NSDictionary)
            
            
            if createParentAccountDataObj.errorCode == 0 {
                
                if self.momButton.selected {
                    UserProfileManager.role = "M"
                }
                else if self.dadButton.selected {
                    UserProfileManager.role = "F"
                }
                else {
                    UserProfileManager.role = "G"
                }
                
                UserProfileManager.invitationSeq = createParentAccountDataObj.data.invitation_seq
                
                let phoneNoVerificationViewControllerObj = mainStoryBoard.instantiateViewControllerWithIdentifier("PhoneNoVerificationViewController") as! PhoneNoVerificationViewController
                
                Utility.saveStringInDefault(SIGN_UP_STATUS, value: "PhoneNoVerificationViewController")

                ROOT_NAVVIGATION_CONROLLER.topViewController?.navigationController!.pushViewController(phoneNoVerificationViewControllerObj, animated: true)
                
            } else {
                
                Utility.showAlert(ALERT_TITLE, message: createParentAccountDataObj.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            }
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
        }
        createParentAccountObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
            
            print("error ------------------------------\(error)")
        }
        
        
        var parameterDict = Dictionary <String, String>()
        
        parameterDict["cmd"] = "updateUserDetails"
        parameterDict["user_seq"] = UserProfileManager.userSequence
        parameterDict["name"] = UserProfileManager.name
        parameterDict["email"] = UserProfileManager.emailId
        parameterDict["profile_pic"] = headShotImageUrl
        parameterDict["mobile"] = UserProfileManager.phoneNo
        parameterDict["addr"] = textFieldForAddress.text
        parameterDict["zip"] = textFieldForZip.text
        parameterDict["state"] = textFieldForState.text
        parameterDict["city"] = textFieldForCity.text
        parameterDict["country"] = UserProfileManager.country
        parameterDict["m_invitation_email"] = textFieldToInviteMomEmail.text
        parameterDict["f_invitation_email"] = textFieldToInviteDadEmail.text
        parameterDict["registration_status"] = "1"

        if momButton.selected {
            parameterDict["role"] = "M" //Mother
            parameterDict["gender"] = "F"//Female

        }
        else if dadButton.selected {
            parameterDict["role"] = "F"//Father
            parameterDict["gender"] = "M"//Male
        }
        else {
            parameterDict["role"] = "G"
        }
        
        createParentAccountObj.createParentAccountHit(parameterDict)
    }

    func hitGetAddressWebService(zipCode: String) {
        
        CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Retrieving address...")
        
        let retrieveAddressObj = CommonServiceClass(httpRequestType: HTTP_REQUEST_TYPE.POST_REQUEST, requestTag: "RETRIEVE_ADDRESS", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
        
        retrieveAddressObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
            
            if errorMessage != nil
            {
                print(response)
            }
            print(response as! NSDictionary)
            
            let addressObj =  CSAddress(fromDictionary: response as! NSDictionary)

            if addressObj.errorCode == 0 {
                
                if addressObj.data.count > 0 {
                    
                    let addressDetail = addressObj.data[0]
                    self.textFieldForCity.text = addressDetail.city
                    //self.textFieldForAddress.text = addressDetail.countryCode
                    self.textFieldForState.text = addressDetail.state
                }
                else {
                    Utility.showAlert(ALERT_TITLE, message: "Data not found", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
                }
                
            } else {
                
                Utility.showAlert(ALERT_TITLE, message: addressObj.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            }
            
        }
        retrieveAddressObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
            
            print("error ------------------------------\(error)")
        }
        
        
        var parameterDict = Dictionary <String, String>()

        parameterDict["cmd"] = "getZipAddress"
        parameterDict["zip"] = zipCode
        
        print(parameterDict)
        
        retrieveAddressObj.commonServiceHit(parameterDict)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
