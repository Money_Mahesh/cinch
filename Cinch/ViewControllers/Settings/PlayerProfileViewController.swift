//
//  PlayerProfileViewController.swift
//  Cinch
//
//  Created by Amit Garg on 20/03/16.
//  Copyright © 2016 MONEY. All rights reserved.
//

import UIKit

class PlayerProfileViewController: UIViewController, UIAlertViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, TextFieldCellDelegate {

    @IBOutlet weak var accountTableView: UITableView!
    var player : Player?
    var playerDetail : PlayerDetailData?
    var imagePicker = UIImagePickerController()
    var isEditModeOn = false
    var fatherEmail = ""
    var motherEmail = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.accountTableView.registerNib(UINib(nibName: "ProfilePicTableViewCell", bundle: nil), forCellReuseIdentifier: "ProfilePicTableViewCell")
        self.accountTableView.registerNib(UINib(nibName: "SectionHeaderWithLabelAndButton", bundle: nil), forHeaderFooterViewReuseIdentifier: "SectionHeaderWithLabelAndButton")
        self.accountTableView.registerNib(UINib(nibName: "OneButtonTableViewCell", bundle: nil), forCellReuseIdentifier: "OneButtonTableViewCell")
        self.accountTableView.registerNib(UINib(nibName: "TextfieldTableViewCell", bundle: nil), forCellReuseIdentifier: "textFieldCell")
        
         Utility.setRightNavigationButton(forViewController: self, withImage: "", withTitle: "EDIT")
        
        if !DataValidationUtility.isNull(player){
            self.getPlayerProfileData()
        }
        self.imagePicker.delegate = self
        
        self.title = "Player Profile Settings"
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        
        if !DataValidationUtility.isNull(self.playerDetail) {
            
            if self.isEditModeOn {
                return 4
            } else {
                return 3
            }
        }
        return 0
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        
        if section == 0 {
            return 1
        } else if section == 1 {
            return 1 + (self.playerDetail?.parents.parentData.count)!
        } else if section == 2 || section == 3  {
            if self.playerDetail?.dobCert != nil {
                return 3
            } else  {
                return 2
            }
        } else {
            return 0
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            return 150
        } else if indexPath.section == 3 {
            return 67.0
        } else {
            return 50
        }
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 2  {
            return 50.0
        }
        return 0.0
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        if section == 1 || section == 2  {
            
            return 20
            
        }
        return 0.0
    }
    
    //https://www.cinchsports.com/user/services_v2/createPDF?cmd=generatePDF&p_seq=1
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            if indexPath.row == 0 {
                
                let profilePicTableViewCell = tableView.dequeueReusableCellWithIdentifier("ProfilePicTableViewCell", forIndexPath: indexPath) as! ProfilePicTableViewCell
                profilePicTableViewCell.selectionStyle = .None
                Utility.setImageWithURl((self.playerDetail?.profilePic)!, inImageView: profilePicTableViewCell.profileImageView)
                return profilePicTableViewCell
            }
            
            break
            
        case 1:
            
            if indexPath.row == 0 {
                
                let textfieldCell = tableView.dequeueReusableCellWithIdentifier("textFieldCell", forIndexPath: indexPath) as! TextfieldTableViewCell
                
                textfieldCell.textLabel?.font = UIFont(name: "Lato-Regular", size: 16.0)
                textfieldCell.accessoryType = .None
                textfieldCell.textField?.text = (self.playerDetail?.fname)! + " " + (self.playerDetail?.lname)!
                textfieldCell.textField?.textColor = UIColor(red: 23/255, green: 22/255, blue: 23/255, alpha: 1.0)
                textfieldCell.selectionStyle = .None

                
                 if self.isEditModeOn {
                    textfieldCell.textField.userInteractionEnabled = true
                 } else {
                    textfieldCell.textField.userInteractionEnabled = false
                }
                
            
                return textfieldCell
                
            } else  {
                
                let textfieldCell = tableView.dequeueReusableCellWithIdentifier("textFieldCell", forIndexPath: indexPath) as! TextfieldTableViewCell
                textfieldCell.textField?.font = UIFont(name: "Lato-Regular", size: 16.0)
                textfieldCell.accessoryType = .None
                textfieldCell.selectionStyle = .None
                textfieldCell.textField?.textColor = UIColor(red: 160/255, green: 160/255, blue: 160/255, alpha: 1.0)
                textfieldCell.delegate = self
                
                if self.isEditModeOn {
                    
                    if self.playerDetail?.parents.parentData[indexPath.row - 1].role != UserProfileManager.role {
                        
                        textfieldCell.textField.userInteractionEnabled = true
                        textfieldCell.textField?.text = self.playerDetail?.parents.parentData[indexPath.row - 1].email
                        
                    }
                }
                else {
                        
                        textfieldCell.textField.userInteractionEnabled = false
                        textfieldCell.textField?.text = self.playerDetail?.parents.parentData[indexPath.row - 1].name
                }
                return textfieldCell
            }
            
            
            
        case 2:
            
            let parentCell = tableView.dequeueReusableCellWithIdentifier("tableCell", forIndexPath: indexPath) as! PlayerSettingsCell
            parentCell.textLabel?.font = UIFont(name: "Lato-Regular", size: 16.0)
            parentCell.accessoryType = .None
            parentCell.textlabel?.textColor = UIColor(red: 160/255, green: 160/255, blue: 160/255, alpha: 1.0)
            if indexPath.row == 0 {
                parentCell.textlabel?.text = "Waiver's form"
            } else {
                if self.playerDetail?.dobCert != nil {
                    if indexPath.row == 1 {
                        parentCell.textlabel?.text = "Birth Certificate"
                    } else {
                        parentCell.textlabel?.text = "History"
                    }
                } else {
                   parentCell.textlabel?.text = "History"
                }
            }
            return parentCell
            

        case 3:
            let oneButtonTableViewCell = tableView.dequeueReusableCellWithIdentifier("OneButtonTableViewCell", forIndexPath: indexPath) as! OneButtonTableViewCell
            oneButtonTableViewCell.cellButton.titleLabel?.font = UIFont(name: "Lato-Bold", size: 16.0)
            oneButtonTableViewCell.selectionStyle = .None
            
            if indexPath.row == 0 {
                oneButtonTableViewCell.cellButton.setTitle("Save Changes", forState: .Normal)
                oneButtonTableViewCell.cellButton.CSBorderColor = UIColor(red: 186/255, green: 186/255, blue: 186/255, alpha: 1.0)
                oneButtonTableViewCell.cellButton.addTarget(self, action: "saveChangesTapped:", forControlEvents: .TouchUpInside)
                oneButtonTableViewCell.cellButton.setTitleColor(UIColor(red: 23/255, green: 22/255, blue: 22/255, alpha: 1.0), forState: .Normal)
                
            } else if indexPath.row == 1 {
                oneButtonTableViewCell.cellButton.setTitle("Delete account", forState: .Normal)
                oneButtonTableViewCell.cellButton.CSBorderColor = UIColor.clearColor()
                oneButtonTableViewCell.cellButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                oneButtonTableViewCell.cellButton.addTarget(self, action: "deleteAccountTapped:", forControlEvents: .TouchUpInside)
                oneButtonTableViewCell.cellButton.backgroundColor = UIColor(red: 255/255, green: 62/255, blue: 62/255, alpha: 1.0)
                
            }
            return oneButtonTableViewCell
            
        default:
            print("Something broke")
            break
            
        }
        
        return UITableViewCell()
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        switch section {
        case 2:
            let headerView = tableView.dequeueReusableHeaderFooterViewWithIdentifier("SectionHeaderWithLabelAndButton") as! SectionHeaderWithLabelAndButton
            headerView.button.hidden = true
            headerView.label.hidden = false
            headerView.label.text = "PERSONAL INFORMATION"
            return headerView
            
        default:
            return nil
            
        }
        
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        switch section {
        case 1,2:
            let headerView = tableView.dequeueReusableHeaderFooterViewWithIdentifier("SectionHeaderWithLabelAndButton") as! SectionHeaderWithLabelAndButton
            headerView.button.hidden = true
            headerView.label.hidden = true
            headerView.upperSeperator.hidden = true
            headerView.lowerSeperator.hidden = true
            return headerView
            
        default:
            return nil
            
        }
        
    }
    
    // MARK: - Table view delegate
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if self.isEditModeOn {
            if indexPath.section == 0 && indexPath.row == 0 {
                self.view.endEditing(true)
                self.showActionSheet()
            }
        }
        
        if indexPath.section == 2 {
            
            let vc = amitStoryBoard.instantiateViewControllerWithIdentifier("WebViewController") as! WebViewController
            
            if indexPath.row == 0 {
                
                vc.urlToLoad = "https://www.cinchsports.com/user/services_v2/createPDF?cmd=generatePDF&p_seq=\(playerDetail!.playerSeq)"
                vc.navigationHeaderTitle = "Waiver Doucment"
                
            } else if self.playerDetail?.dobCert != nil && indexPath.row == 1 {
                
                vc.urlToLoad = playerDetail?.dobCert!
                vc.navigationHeaderTitle = "Birth Certificate"
            }
            
            let navCont = UINavigationController(rootViewController: vc)
            
            self.presentViewController(navCont, animated: true, completion:nil)
        }
        
        
        
    }
    
    //MARK:- Hit Webservice
    
    func getPlayerProfileData () {
        
        CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Fetching Data...")
        
        let accountSettingServiceObj = CommonServiceClass(httpRequestType: HTTP_REQUEST_TYPE.POST_REQUEST, requestTag: "PLAYER_SETTINGS_SERVICE", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
        
        accountSettingServiceObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
            
            if errorMessage != nil
            {
                //print(response)
            }
            
            print(response as! NSDictionary)
            let responseDict = PlayerSettingData(fromDictionary: response as! NSDictionary)
            
            if responseDict.errorCode == 1 {
                
                Utility.showAlert(ALERT_TITLE, message: responseDict.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            }
            else {
                
                print(responseDict.data)
                if (responseDict.data.count != 0) {
                    
                    self.playerDetail = responseDict.data[0]
                    self.accountTableView.reloadData()
                }
                else {
                    Utility.showAlert(ALERT_TITLE, message: "Data not found", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
                }
            }
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
            
        }
        accountSettingServiceObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
            print("error ------------------------------\(error)")
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
            
        }
        
        var parameterDict = Dictionary <String, String>()
        parameterDict["cmd"] = "getPlayerList"
        parameterDict["player_seq"] = player?.playerSeq
        parameterDict["gender"] = ""
        parameterDict["name"] = ""
        parameterDict["divideByYear"] = ""
        parameterDict["year"] = ""
        parameterDict["getParentsDetails"] = "Y"
        
        accountSettingServiceObj.commonServiceHit(parameterDict)
    }
    
    
    func updatePlayerProfileData () {
        
        CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Updating Data...")
        
        let accountSettingServiceObj = CommonServiceClass(httpRequestType: HTTP_REQUEST_TYPE.POST_REQUEST, requestTag: "UPDATE_PLAYER_SETTINGS_SERVICE", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
        
        accountSettingServiceObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
            
            if errorMessage != nil
            {
                //print(response)
            }
            
            print(response as! NSDictionary)
            let responseDict = OtpVerificationData(fromDictionary: response as! NSDictionary)
            
            if responseDict.errorCode == 1 {
                
                Utility.showAlert(ALERT_TITLE, message: responseDict.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            }
            else {
                
                self.isEditModeOn = false
                
                self.accountTableView.reloadData()
                
                Utility.showAlert(ALERT_TITLE, message: "Player Profile Updated", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
                
            }
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
            
        }
        accountSettingServiceObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
            print("error ------------------------------\(error)")
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
            
        }
        
        //f_email: father@gmail.com, f_seq:2, m_email: mother@gmail.com, m_seq:1, player_image: Amazon URL
        
        var parameterDict = Dictionary <String, String>()
        parameterDict["cmd"] = "updatePlayerProfileSetting"
        parameterDict["player_seq"] = player!.playerSeq
        parameterDict["player_name"] = player!.fname + " " + player!.lname
        
        if UserProfileManager.role == "M" {
            
            parameterDict["f_seq"] = player!.fatherSeq
            parameterDict["f_email"] = self.fatherEmail
            parameterDict["m_seq"] = ""
            parameterDict["m_email"] = ""
        
            
        } else if UserProfileManager.role == "F" {
            
            parameterDict["f_seq"] = ""
            parameterDict["f_email"] = ""
            parameterDict["m_seq"] = player!.motherSeq
            parameterDict["m_email"] = self.motherEmail
            
        } else {
            
            parameterDict["f_seq"] = player!.fatherSeq
            parameterDict["f_email"] = self.fatherEmail
            parameterDict["m_seq"] = player!.motherSeq
            parameterDict["m_email"] = self.motherEmail
            
        }
        
        parameterDict["player_image"] = player!.profilePic
        accountSettingServiceObj.commonServiceHit(parameterDict)
    }
    
    func deletePlayerProfile (password: String) {
        
        CSActivityIndicator.showToView(AppDelegateInstance.window!, frame: CGRectMake(0, 0, AppDelegateInstance.window!.frame.width, AppDelegateInstance.window!.frame.height), withMessage: "Deleting Player...")
        
        let accountSettingServiceObj = CommonServiceClass(httpRequestType: HTTP_REQUEST_TYPE.POST_REQUEST, requestTag: "DELETE_PLAYER_PROFILE_SERVICE", requestPriority:REQUEST_PRIORITY.MODERATE_PRIORITY)
        
        accountSettingServiceObj.completionBlockWithSuccess = {(statusCode: Int, requestType: String!, response: AnyObject?, errorMessage: String?) in
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
            
            if errorMessage != nil
            {
                //print(response)
            }
            
            print(response as! NSDictionary)
            let responseDict = OtpVerificationData(fromDictionary: response as! NSDictionary)
            
            if responseDict.errorCode == 1 {
                
                Utility.showAlert(ALERT_TITLE, message: responseDict.errorMsg, cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
            }
            else {
                
                print(responseDict.data)
                
                if responseDict.data == true {
                    self.navigationController?.popViewControllerAnimated(true)
                }
            }
            
        }
        accountSettingServiceObj.completionBlockWithFailure = {(requestType: String!, error: NSError!) in
            print("error ------------------------------\(error)")
            
            CSActivityIndicator.hideAllFromView(AppDelegateInstance.window!)
            
        }
        
        var parameterDict = Dictionary <String, String>()
        parameterDict["cmd"] = "deletePlayerAccount"
        parameterDict["player_seq"] = player!.playerSeq
        parameterDict["password"] = password
        accountSettingServiceObj.commonServiceHit(parameterDict)
    }
    
    //MARK: - Button Action Methods
    
    func rightBtnAction() {
        
        self.isEditModeOn = !self.isEditModeOn
        
        if self.isEditModeOn == true {
            self.navigationItem.rightBarButtonItem = nil
        }
        
        self.accountTableView.reloadData()
    }
    
    func deleteAccountTapped(sender : CSCustomButton) {
        
        let alert = UIAlertView()
        alert.title = "Enter Password"
        alert.addButtonWithTitle("Done")
        alert.alertViewStyle = UIAlertViewStyle.SecureTextInput
        alert.delegate = self
        alert.addButtonWithTitle("Cancel")
        alert.show()
    }
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if buttonIndex == 0 {
            print((alertView.textFieldAtIndex(0)?.text!)!)
            self.deletePlayerProfile((alertView.textFieldAtIndex(0)?.text!)!)
        }
    }
    
    func saveChangesTapped(sender : CSCustomButton) {
        
        self.view.endEditing(true)
        
        self.updatePlayerProfileData()
                
    }
    
    //MARK:- Image Picker method
    func showActionSheet() {
        
        let actionSheet = UIAlertController(title: "Profile Pic", message: "", preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        let imageGalleryAction = UIAlertAction(title: "Image Gallery", style: UIAlertActionStyle.Default) { (action) -> Void in
            self.openImageGallary()
        }
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.Default) { (action) -> Void in
            self.openCamera()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil)
        
        actionSheet.addAction(imageGalleryAction)
        actionSheet.addAction(cameraAction)
        actionSheet.addAction(cancelAction)
        
        self.presentViewController(actionSheet, animated: true, completion: nil)
    }
    
    func openImageGallary() {
        imagePicker.sourceType = .PhotoLibrary
        self.presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    func openCamera() {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
            imagePicker.sourceType = .Camera
            self.presentViewController(imagePicker, animated: true, completion: nil)
        }
        else {
            let alertController = UIAlertController(title: "Oops!!", message: "Camera is not available", preferredStyle: UIAlertControllerStyle.Alert)
            let cancelAction = UIAlertAction(title: ALERT_OK_BUTTON_TITLE, style: UIAlertActionStyle.Cancel, handler: nil)
            
            alertController.addAction(cancelAction)
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    //MARK:- Image Picker Delegate
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        if let pickerImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            print(pickerImage)
            
            let profilePicCell = self.accountTableView.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0)) as! ProfilePicTableViewCell
            
            
            
            let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
            activityIndicator.frame.size = profilePicCell.profileImageView.frame.size
            
            activityIndicator.startAnimating()
            profilePicCell.profileImageView.addSubview(activityIndicator)
            
            Utility.uploadImageToserver(pickerImage, resetImageViewBlock: {(selectedImage: UIImage?, headShotImageUrl: String?) in
                activityIndicator.removeFromSuperview()
                if selectedImage != nil {
                    profilePicCell.profileImageView.image = selectedImage
                    self.player!.profilePic = headShotImageUrl == nil ? "" : headShotImageUrl!
                }
                else {
                }
            })
            
        }
        
        self.imagePicker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.imagePicker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    //MARK: - Textfield Cell delegate
    
    
    func textFieldCellEndEditing(textFieldCell: TextfieldTableViewCell, text: NSString) {
        
        let indexPath = self.accountTableView.indexPathForCell(textFieldCell)
        
        if indexPath?.section == 1 && indexPath?.row == 0 {
            
            let fullName = textFieldCell.textField.text!
            
            if fullName.characters.count == 0 {
                
                Utility.showAlert(ALERT_TITLE, message: "Player name cannot be blank.", cancelButtonTitle: ALERT_OK_BUTTON_TITLE)
                
            } else {
                 let nameArray = fullName.componentsSeparatedByString(" ")
                
                if nameArray.count == 2 {
                    player!.fname = nameArray[0]
                    player!.lname = nameArray[1]
                    
                } else if nameArray.count == 1 {
                    player!.fname = nameArray[0]
                }
            }
            
        } else if indexPath?.section == 1 && indexPath?.row > 0 {
            
            if self.playerDetail?.parents.parentData[indexPath!.row - 1].role != UserProfileManager.role {
                
                if self.playerDetail?.parents.parentData.count == 3 {
                    
                    
                    
                    
                }
                
            }
            
            if self.playerDetail?.parents.parentData[indexPath!.row - 1].role == "G" {
                
                
            
            }
            
        }
        
        if UserProfileManager.role == "G" {
            
            let motherCell = self.accountTableView.cellForRowAtIndexPath(NSIndexPath(forRow: 1, inSection: 1)) as! TextfieldTableViewCell
            
            self.motherEmail = motherCell.textField.text!
            
            let fatherCell = self.accountTableView.cellForRowAtIndexPath(NSIndexPath(forRow: 2, inSection: 1)) as! TextfieldTableViewCell
            
            self.fatherEmail = fatherCell.textField.text!
            
        } else {
            
           
            
            if indexPath?.section == 1 && indexPath?.row == 1 && UserProfileManager.role == "F" {
                
                self.motherEmail = textFieldCell.textField.text!
                
            } else if indexPath?.section == 1 && indexPath?.row == 2 && UserProfileManager.role == "M" {
                
                self.fatherEmail = textFieldCell.textField.text!
            }
            
        }
        
    }
    
    func textFieldCellDidBeginEditing(textFieldCell: TextfieldTableViewCell) {
        
        
    }
    
    func textFieldCellShouldReturn(textFieldCell: TextfieldTableViewCell) {
        
    }

}

class PlayerSettingsCell : UITableViewCell {
    
    @IBOutlet weak var textlabel: UILabel!
    
    
}
